/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import nl.han.hantune.config.VersionInfo;
import org.junit.Test;

public class VersionInfoTest {

    @Test
    public void testVersionFileOk() {
        VersionInfo testSubject = VersionInfo.getInstanceTest("test/nl/han/hantune/config/resources/TestVersionData.properties");
        
        assertEquals("1.23", testSubject.getVersionNumber());
        assertEquals(456, testSubject.getBuildNumber());
        assertEquals("1.23 HANtune 456", testSubject.getFullVersionText());
        assertEquals("1.23-456 HANtune 456", testSubject.getFullBuildVersionText());
        assertEquals(1.23, testSubject.getVersionFloat(), 0.00001);
    }


    @Test
    public void testVersionFileOk2() {
        VersionInfo testSubject = VersionInfo.getInstanceTest("test/nl/han/hantune/config/resources/TestVersionData2.properties");
        
        assertEquals("2.2", testSubject.getVersionNumber());
        assertEquals(1232, testSubject.getBuildNumber());
        assertEquals("2.2 \"Fail when quotes are removed\"", testSubject.getFullVersionText());
        assertEquals("2.2-1232 \"Fail when quotes are removed\"", testSubject.getFullBuildVersionText());
        assertEquals(2.2, testSubject.getVersionFloat(), 0.00001);
    }


    @Test
    public void testVersionFileNok() {
        VersionInfo testSubject = VersionInfo.getInstanceTest("test/nl/han/hantune/config/resources/TestVersionData3.properties");
        
        assertEquals("0.0", testSubject.getVersionNumber());
        assertEquals(1232, testSubject.getBuildNumber());
        assertEquals("0.0 Error 'version.properties'", testSubject.getFullVersionText());
        assertEquals("0.0-1232 Error 'version.properties'", testSubject.getFullBuildVersionText());
        assertEquals(0.0, testSubject.getVersionFloat(), 0.00001);
    }


    @Test
    public void testVersionFileNotFound() {
        // try to open nonexistent file
        VersionInfo testSubject = VersionInfo.getInstanceTest("test/HANtune/resources/NotFound.properties");

        // should yield all default version numbers/values
        assertEquals("0.0", testSubject.getVersionNumber());
        assertEquals(0, testSubject.getBuildNumber());
        assertEquals("0.0-0 Error 'version.properties'", testSubject.getFullBuildVersionText());
    }

}
