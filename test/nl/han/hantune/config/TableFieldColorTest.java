/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.config;


import java.awt.Color;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import HANtune.editors.TableFieldColor;
import org.junit.Test;

import static HANtune.editors.TableFieldColor.MAX_COLOR_VALUES;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_BOOLEAN_USE_MID_VALUE;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_COLOR_MAX;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_COLOR_MID;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_COLOR_MIN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

//public class TableFieldColorTest extends TableFieldColor {
public class TableFieldColorTest {
    //
    private void initTest(String propFile){
        // invoke private methods for UnitTesting using reflection
        try {
            Method applPropInit = ApplicationProperties.class.getDeclaredMethod("initApplicationProperties", String.class);
            applPropInit.setAccessible(true);
            applPropInit.invoke(null, propFile);

            Method tableFieldInitColors = TableFieldColor.class.getDeclaredMethod("initColors");
            tableFieldInitColors.setAccessible(true);
            tableFieldInitColors.invoke(null);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getColorTest() {
        initTest("test/nl/han/HANtune/config/resources/TestTableFieldColor.properties");

//        for (int i = 0; i < MAX_COLOR_VALUES; i++) {
//            System.out.println(TableFieldColor.getColor(i));
//        }

        assertFalse(ApplicationProperties.getBoolean(TABLE_FIELD_BOOLEAN_USE_MID_VALUE));
        assertEquals(new Color(39, 158, 11), ApplicationProperties.getColor(TABLE_FIELD_COLOR_MIN));
        assertEquals(new Color(0, 2, 231), ApplicationProperties.getColor(TABLE_FIELD_COLOR_MAX));

        assertEquals(new Color(39, 158, 11), TableFieldColor.getColor(-10));
        assertEquals(new Color(39, 158, 11), TableFieldColor.getColor(0));
        assertEquals(new Color(20, 84, 116), TableFieldColor.getColor(19));
        assertEquals(new Color(20, 80, 121), TableFieldColor.getColor(20)); // twice the same value for Red
        assertEquals(new Color(11, 45, 171), TableFieldColor.getColor(29));
        assertEquals(new Color(0, 2, 231), TableFieldColor.getColor(99));
    }

    @Test
    public void getColorUseMidValueTest() {
        initTest("test/nl/han/HANtune/config/resources/TestTableFieldColorUseMidValue.properties");

        for (int i = 0; i < MAX_COLOR_VALUES; i++) {
            System.out.println(TableFieldColor.getColor(i));
        }

        assertTrue(ApplicationProperties.getBoolean(TABLE_FIELD_BOOLEAN_USE_MID_VALUE));
        assertEquals(new Color(39, 158, 11), ApplicationProperties.getColor(TABLE_FIELD_COLOR_MIN));
        assertEquals(new Color(0, 2, 231), ApplicationProperties.getColor(TABLE_FIELD_COLOR_MAX));
        assertEquals(new Color(255, 255, 255), ApplicationProperties.getColor(TABLE_FIELD_COLOR_MID));

        assertEquals(new Color(39, 158, 11), TableFieldColor.getColor(-10));
        assertEquals(new Color(39, 158, 11), TableFieldColor.getColor(0));
        assertEquals(new Color(255, 255, 255), TableFieldColor.getColor(20));
        assertEquals(new Color(140, 141, 244), TableFieldColor.getColor(29));
        assertEquals(new Color(0, 2, 231), TableFieldColor.getColor(99));
    }

    @Test
    public void checkDefaultColorValues() {
        // Force default properties using non-existent filename
        initTest("test/nl/han/HANtune/config/resources/FileNotFound");

        assertEquals(new Color(100, 150, 255), ApplicationProperties.getColor(TABLE_FIELD_COLOR_MIN));
        assertEquals(new Color(255, 255, 50), ApplicationProperties.getColor(TABLE_FIELD_COLOR_MAX));

        assertEquals(new Color(100, 150, 255), TableFieldColor.getColor(-10));
        assertEquals(new Color(100, 150, 255), TableFieldColor.getColor(0));
        assertEquals(new Color(255, 255, 255), TableFieldColor.getColor(MAX_COLOR_VALUES / 2)); // middle index
        assertEquals(new Color(255, 255, 163), TableFieldColor.getColor(29));
        assertEquals(new Color(255, 255, 50), TableFieldColor.getColor(99));
    }

}
