/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.net.ftp;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.ftp.FTPClient;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import org.apache.commons.net.ftp.FTPFile;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author Bailey Bosma & Michiel Poels
 */
public class HANtuneApacheFTPTest {

    private static final String TESTFOLDER_PATH = System.getProperty("user.dir") + "/testfiles";
    private static final String REMOTE_UPLOAD_PATH = "../client_file.txt";
    private static final String LOCAL_UPLOAD_PATH = "./test/nl/han/hantune/net/ftp/client_file.txt";
    
    private static final String REMOTE_DOWNLOAD_PATH = "../server_file.txt";
    private static final String LOCAL_DOWNLOAD_PATH = "./test/nl/han/hantune/net/ftp/server_file.txt";

    private static final int ONE_TIME = 1;
    private static final int fakeArrayLength= 5;
    
    private final String ip = "localhost";
    private final int port = 21;
    private final String username = "user";
    private final String password = "password";

    private static final String FAKE_PATH= "fakePath";
    private static final String TXT_EXTENSION= ".txt";
    private static final String LOGFILE_NAME= "LogFile";

    @Before
    public void setUp() throws IOException {

        File outputFolder = new File(TESTFOLDER_PATH);
        outputFolder.mkdirs();
        File outputFile = new File(LOCAL_UPLOAD_PATH);
        outputFile.createNewFile();
    }

    /**
     * Test of createConnection method, of class HANtuneApacheFTP. 
     * This test checks if the call to the method and the calls in the methode are invoked.
     */
    @Test
    public void testCreateConnection() {
        try {
            HANtuneApacheFTP instance = new HANtuneApacheFTP();
            FTPClient mockedClient = mock(FTPClient.class);
            instance.setFTPClient(mockedClient);
            when(mockedClient.login(username, password)).thenReturn(true);
            assertTrue(instance.createConnection(ip, port, username, password));
        } catch (IOException ex) {
            Logger.getLogger(HANtuneApacheFTPTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of showFileList method, of class HANtuneApacheFTP. This test checks
     * if the calls to the method and the calls in the method are al invoked.
     * @throws java.io.IOException
     */
    @Test
    public void testShowFileList() throws IOException {
        FTPFile[] fakeFileArray = new FTPFile[fakeArrayLength];
        for (int i = 0; i < fakeFileArray.length; i++) {
            fakeFileArray[i] = new FTPFile();
            fakeFileArray[i].setName(LOGFILE_NAME + i + TXT_EXTENSION);
        }
        FTPFile[] result;
        HANtuneApacheFTP instance = new HANtuneApacheFTP();
        FTPClient mockedClient = mock(FTPClient.class);
        instance.setFTPClient(mockedClient);
        when(mockedClient.listFiles(FAKE_PATH)).thenReturn(fakeFileArray);
        FTPFile[] exp = fakeFileArray;
        result = instance.showFileList(FAKE_PATH);
        assertArrayEquals(exp, result);
    }

    /**
     * Test of downloadFile method, of class HANtuneApacheFTP (adapter class).
     * To test the download functionality of the Apache adapter class. An
     * FTPClient will be mocked and will be set as the FTPClient of the adapter
     * class. To be able to see the parameters passed to the methods of the
     * mocked client, two Argument Captors will be created and added to the
     * verify statement. A normal FileStream, like a BufferedOutputStream, does
     * not have the ability to return the file or path it is writing to.
     * Therefore, the Argument Captor will have the type of a
     * CustomBufferedOutputStream, a class which is able to store the path of
     * the file.
     */
    @Test
    public void testDownloadFile() {
            HANtuneApacheFTP ftp = new HANtuneApacheFTP();
            boolean connected = ftp.createConnection(ip, port, username, password);
            assertEquals(true, connected);
            boolean result = ftp.downloadFile(REMOTE_DOWNLOAD_PATH, LOCAL_DOWNLOAD_PATH);
            assertEquals(true, result);
            File file = new File(LOCAL_DOWNLOAD_PATH);
            file.delete();
    }

    /**
     * Test of uploadFile method, of class HANtuneApacheFTP (adapter class). To
     * test the upload functionality of the Apache adapter class. An FTPClient
     * will be mocked and will be set as the FTPClient of the adapter class. To
     * be able to see the parameters passed to the methods of the mocked client,
     * two Argument Captors will be created and added to the verify statement. A
     * normal FileStream, like a BufferedOutputStream, does not have the ability
     * to return the file or path it is writing to. Therefore, the Argument
     * Captor will have the type of a CustomBufferedInputStream, a class which
     * is able to store the path of the file.
     * @throws java.io.IOException
     */
    @Test
    public void testUploadFile() throws IOException {
            HANtuneApacheFTP ftp = new HANtuneApacheFTP();
            boolean connected = ftp.createConnection(ip, port, username, password);
            assertEquals(true, connected);
            boolean result = ftp.uploadFile(REMOTE_UPLOAD_PATH, LOCAL_UPLOAD_PATH);
            assertEquals(true, result);
            File file = new File("C:\\Users\\Michiel\\Desktop\\ftp server\\client_file.txt");
            file.delete();
    }

    /**
     * Test of closeConnection method, of class HANtuneApacheFTP.
     */
    @Test
    public void testCloseConnection() {
        try {
            HANtuneApacheFTP instance = new HANtuneApacheFTP();
            FTPClient mockedClient = mock(FTPClient.class);
            instance.setFTPClient(mockedClient);
            instance.closeConnection();
            verify(mockedClient, times(ONE_TIME)).disconnect();
        } catch (IOException ex) {
            Logger.getLogger(HANtuneApacheFTPTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
