/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.scripting.jython;

import java.io.StringReader;
import javax.swing.JTextPane;
import nl.han.hantune.scripting.Console;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

/**
 *
 * @author Michiel Klifman
 */
public class InteractiveJythonInterpreterTest {
    
    private static final String P1 = ">>> ";
    private static final String P2 = "... ";
    private static final String CR = "\r";
    private static final String LF = "\n";
    
    @Test
    public void testExpression() throws InterruptedException {        
        JTextPane textPane = (JTextPane)Console.getGUI().getViewport().getView();
        textPane.setText("");
        StringReader input = new StringReader("1+1");
        Thread thread = new Thread(new InteractiveJythonInterpreter(input));
        thread.start();
        thread.join();
        String expectedResult = "2" + CR + CR + LF + P1;
        String result = textPane.getText();
        assertThat(result, is(expectedResult));
    }
    
    @Test
    public void testStatement() throws InterruptedException {        
        JTextPane textPane = (JTextPane)Console.getGUI().getViewport().getView();
        textPane.setText("");
        StringReader input = new StringReader("while(1):");
        Thread thread = new Thread(new InteractiveJythonInterpreter(input));
        thread.start();
        thread.join();
        String expectedResult = P2;
        String result = textPane.getText();
        assertThat(result, is(expectedResult));
    }
    
}
