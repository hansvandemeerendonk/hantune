/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.scripting;

import datahandling.Reference;
import java.util.ArrayList;
import java.util.List;
import nl.han.hantune.scripting.jython.JythonScript;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Michiel
 */
public class ScriptingManagerTest {
    
    public ScriptingManagerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        ScriptingManager instance = ScriptingManager.getInstance();
        instance.getScripts().clear();
        instance.getScriptedSignals().clear();
    }

    /**
     * Test of addScript method, of class ScriptingManager.
     */
    @Test
    public void testAddScript() {
        System.out.println("addScript");
        Script script = new JythonScript("test", "test.py");
        ScriptingManager instance = ScriptingManager.getInstance();
        instance.addScript(script);
        Script expectedResult = script;
        Script result = instance.getScripts().get(0);
        assertEquals(expectedResult, result);
    }

    /**
     * Test of removeScript method, of class ScriptingManager.
     */
    @Test
    public void testRemoveScript() {
        System.out.println("removeScript");
        Script script = new JythonScript("test", "test.py");
        ScriptingManager instance = ScriptingManager.getInstance();
        
        instance.addScript(script);
        int expectedResult = 1;
        int result = instance.getScripts().size();
        assertEquals(expectedResult, result);
        
        instance.removeScript(script);
        expectedResult = 0;
        result = instance.getScripts().size();
        assertEquals(expectedResult, result);
    }

    /**
     * Test of getScriptByName method, of class ScriptingManager.
     */
    @Test
    public void testGetScriptByName() {
        System.out.println("getScriptByName");
        Script script = new JythonScript("test", "test.py");
        ScriptingManager instance = ScriptingManager.getInstance();
        instance.addScript(script);
        Script expectedResult = script;
        Script result = instance.getScriptByName("test");
        assertEquals(expectedResult, result);
    }

    /**
     * Test of removeAllScripts method, of class ScriptingManager.
     */
    @Test
    public void testRemoveAllScripts() {
        System.out.println("removeAllScripts");
        ScriptingManager instance = ScriptingManager.getInstance();
        
        instance.addScript(new JythonScript("test1", "test1.py"));
        instance.addScript(new JythonScript("test2", "test2.py"));
        int expectedResult = 2;
        int result = instance.getScripts().size();
        assertEquals(result, expectedResult);
        
        instance.removeAllScripts();
        expectedResult = 0;
        result = instance.getScripts().size();
        assertEquals(expectedResult, result);
    }

    /**
     * Test of addScriptedSignal method, of class ScriptingManager.
     */
    @Test
    public void testAddScriptedSignal() {
        System.out.println("addScriptedSignal");
        ScriptedSignal signal = new ScriptedSignal("test");
        ScriptingManager instance = ScriptingManager.getInstance();
        instance.addScriptedSignal(signal);
        ScriptedSignal expectedResult = signal;
        ScriptedSignal result = instance.getScriptedSignals().get(0);
        assertEquals(expectedResult, result);
    }

    /**
     * Test of updateListeners method, of class ScriptingManager.
     */
    @Test
    public void testUpdateListeners() {
        System.out.println("updateListeners");
        ScriptingManager instance = ScriptingManager.getInstance();
        final boolean[] resultArray = new boolean[]{false};
        ScriptingManagerListener listener = () -> {
            resultArray[0] = true;
        };
        instance.addListener(listener);
        instance.updateListeners();
        assertTrue(resultArray[0]);
    }

    /**
     * Test of getReferences method, of class ScriptingManager.
     */
    @Test
    public void testGetReferences() {
        System.out.println("getReferences");
        ScriptingManager instance = ScriptingManager.getInstance();
        Script script = new JythonScript("test", "test.py");
        ScriptedSignal signal = new ScriptedSignal("testSignal");
        
        List<Reference> expectedResult = new ArrayList<>();
        expectedResult.add(script);
        expectedResult.add(signal);
                
        instance.addScript(script);
        instance.addScriptedSignal(signal);
        
        List<? extends Reference> result = instance.getReferences();
        assertEquals(expectedResult, result);
    }
    
}
