/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XCP;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Ignore;
import org.junit.Test;

import datahandling.CurrentConfig;
import peak.can.basic.TPCANTimestamp;

public class XCPonCANBasicTest {

    @Test
    public void testCalculateTimeStampMicros() {

        // dummy call, to execute XCPonCANBasic initialization. Takes relatively long
        XCPonCANBasic.calculateTimeStampMicros();


        CurrentConfig config = CurrentConfig.getInstance();

        int expectedTimeStampMicros = 1234 * 1000 + 567; // 1234567 micros = 1,234567 sec

        // test-time: 430 sec since startup. safe value no overflow
        long testStartupTimeMicros = 430 * 1000 * 1000L;
        config.setTimeOffsetMicros( - (System.nanoTime() / 1000 - testStartupTimeMicros - expectedTimeStampMicros));

        long testTimeStamp = XCPonCANBasic.calculateTimeStampMicros();
        assertEquals((double)expectedTimeStampMicros, (double)testTimeStamp - testStartupTimeMicros, 10);

        // test-time: 43000 sec since startup
        testStartupTimeMicros = 43000 * 1000 * 1000L;
        config.setTimeOffsetMicros( - (System.nanoTime() / 1000 - testStartupTimeMicros - expectedTimeStampMicros));

        testTimeStamp = XCPonCANBasic.calculateTimeStampMicros();
        assertEquals((double)expectedTimeStampMicros, (double)testTimeStamp - testStartupTimeMicros, 10);

        // test-time: 4300000 sec since startup
        testStartupTimeMicros = 4300000 * 1000 * 1000L;
        config.setTimeOffsetMicros( - (System.nanoTime() / 1000 - testStartupTimeMicros - expectedTimeStampMicros));

        testTimeStamp = XCPonCANBasic.calculateTimeStampMicros();
        assertEquals((double)expectedTimeStampMicros, (double)testTimeStamp - testStartupTimeMicros, 10);

        // test-time: 430000000 sec since startup (about 5000 days)
        testStartupTimeMicros = 430000000 * 1000 * 1000L;
        config.setTimeOffsetMicros( - (System.nanoTime() / 1000 - testStartupTimeMicros - expectedTimeStampMicros));

        testTimeStamp = XCPonCANBasic.calculateTimeStampMicros();
        assertEquals((double)expectedTimeStampMicros, (double)testTimeStamp - testStartupTimeMicros, 10);
    }


    @Test
    public void testGetUnsignedMillis() {
        TPCANTimestamp timeStamp = new TPCANTimestamp();

        Integer intVal = Integer.parseUnsignedInt("3000000000");

        timeStamp.setMillis(intVal);

        assertEquals(3000000000L, Integer.toUnsignedLong(timeStamp.getMillis()));
    }


    @Test
    // Wrong calculation. results in correct value: 1234567L in this test,
    // but incorrect value when UP-time is over 4300 sec (see next test)
    public void testCalculateTimeStampMicrosNoOverflow() {
        CurrentConfig config = CurrentConfig.getInstance();


        int expectedTimeStampMicros = 1234 * 1000 + 567; // 1234567 micros = 1,234567 sec

        // test-time: 430 sec since startup. Safe value, no overflow
        long testStartupTimeMicrosSafe = 430 * 1000 * 1000L;
        config.setTimeOffsetMicros(-testStartupTimeMicrosSafe);
        TPCANTimestamp tpcanTimeMock = mock(TPCANTimestamp.class);
        when(tpcanTimeMock.getMillis())
        .thenReturn((int)(testStartupTimeMicrosSafe / 1000 + (expectedTimeStampMicros / 1000)));
        when(tpcanTimeMock.getMicros()).thenReturn((short)(expectedTimeStampMicros % 1000));
        when(tpcanTimeMock.getMillis_overflow()).thenReturn((short)0);

//        long testTimeStamp = XCPonCANBasic.calculateTimeStamp(tpcanTimeMock);
//        assertEquals(1234567L, testTimeStamp);
    }


    @Test
    // Wrong calculation. results in wrong value: -4293732729L (overflow), should be 1234567L
    public void testCalculateTimeStampMicrosWithOverflow() {
        CurrentConfig config = CurrentConfig.getInstance();
        int expectedTimeStampMicros = 1234 * 1000 + 567; // 1234567 micros = 1,234567 sec


        // test-time: 4300 sec since startup. Unsafe value, internal overflow
        long testStartupTimeMicrosOverflow = 4300 * 1000 * 1000L;

        config.setTimeOffsetMicros(-testStartupTimeMicrosOverflow);
        TPCANTimestamp tpcanTimeMock = mock(TPCANTimestamp.class);
        when(tpcanTimeMock.getMillis())
        .thenReturn((int)(testStartupTimeMicrosOverflow / 1000 + (expectedTimeStampMicros / 1000)));
        when(tpcanTimeMock.getMicros()).thenReturn((short)(expectedTimeStampMicros % 1000));
        when(tpcanTimeMock.getMillis_overflow()).thenReturn((short)0);


//        long testTimeStamp = XCPonCANBasic.calculateTimeStamp(tpcanTimeMock);
//        assertEquals(-4293732729L, testTimeStamp);  //WRONG value
//        assertEquals(1234567L, testTimeStamp);  //correct value
    }
}
