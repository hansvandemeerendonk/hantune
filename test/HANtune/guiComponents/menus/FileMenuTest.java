/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.guiComponents.menus;

import java.util.prefs.Preferences;
import javax.swing.JMenu;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import HANtune.HANtune;
import HANtune.HANtuneManager;
import datahandling.CurrentConfig;
import java.util.ArrayList;
import java.util.List;
import org.junit.Ignore;

/**
 *
 * @author Roel
 */
public class FileMenuTest {
    
    public FileMenuTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of rebuildRecentProjectsMenu method, of class FileMenu.
     */
    @Test
    public void testRebuildRecentProjectsMenu() {

        Preferences userPrefsMock = mock(Preferences.class);
        HANtuneManager hantuneManagerMock = mock(HANtuneManager.class);
        List<String> pathList = new ArrayList<>();
        
        pathList.add("C:\\Users\\Roel\\Desktop\\HANtunetests\\test3.hml");
        pathList.add("C:\\Users\\Roel\\Desktop\\HANtunetests\\test4.hml");
        pathList.add("C:\\Users\\Roel\\Desktop\\HANtunetests\\test5.hml");
        pathList.add("C:\\Users\\Roel\\Desktop\\HANtunetests\\test6.hml");
        pathList.add("C:\\Users\\Roel\\Desktop\\HANtunetests\\test7.hml");
        pathList.add("C:\\Users\\Roel\\Desktop\\HANtunetests\\test8.hml");
        pathList.add("C:\\Users\\Roel\\Desktop\\HANtunetests\\test9.hml");
        pathList.add("C:\\Users\\Roel\\Desktop\\HANtunetests\\test10.hml");
        pathList.add("C:\\Users\\Roel\\Desktop\\HANtunetests\\test11.hml");
        pathList.add("C:\\Users\\Roel\\Desktop\\HANtunetests\\test12.hml");
        pathList.add("C:\\Users\\Roel\\Desktop\\HANtunetests\\test13.hml");
        pathList.add("C:\\Users\\Roel\\Desktop\\HANtunetests\\test14.hml");
        pathList.add("C:\\Users\\Roel\\Desktop\\HANtunetests\\test15.hml");
        pathList.add("C:\\Users\\Roel\\Desktop\\HANtunetests\\test16.hml");       
        pathList.add("C:\\Users\\Roel\\Desktop\\HANtunetests\\test17.hml");
        
        when(hantuneManagerMock.getLastProjectPathList(userPrefsMock)).thenReturn(pathList);
        CurrentConfig.getInstance().setHANtuneManager(hantuneManagerMock);
                
        FileMenu testFileMenu = new FileMenu(HANtune.getInstance());
        
        JMenu testRecentProjectsMenu = testFileMenu.rebuildRecentProjectsMenu(userPrefsMock);
        assertEquals(testRecentProjectsMenu.getItem(0).getText(), "C:\\Users\\Roel\\Desktop\\HANtunetests\\test17.hml");
        assertEquals(testRecentProjectsMenu.getItem(14).getText(),"C:\\Users\\Roel\\Desktop\\HANtunetests\\test3.hml");
        
        when(hantuneManagerMock.getLastProjectPathList(userPrefsMock)).thenReturn(new ArrayList<>());       // check with empty list for a virgin HANtune
        testRecentProjectsMenu = testFileMenu.rebuildRecentProjectsMenu(userPrefsMock);
        assertEquals(testRecentProjectsMenu.getItem(0).getText(), "empty...");
        assertFalse(testRecentProjectsMenu.getItem(0).isEnabled());
    }

    @Ignore("To be defined")
    @Test
    /**
     * Test of updateItemsServiceModeVisibility method, of class FileMenu.
    */
    public void testUpdateItemsServiceModeVisibility() {
        // *** Test to be defined ***
    }
    
}
