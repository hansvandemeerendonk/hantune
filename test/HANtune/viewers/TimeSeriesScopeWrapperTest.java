/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.viewers;

import org.jfree.data.time.FixedMillisecond;
import org.jfree.data.time.TimeSeries;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TimeSeriesScopeWrapperTest {

    private TimeSeriesScopeWrapper testTimeSeriesScopeWrapper;
    private ScopeViewer mockedScope = mock(ScopeViewer.class);


    @Test
    public void testValueUpdate() {

        TimeSeries timeSeries = new TimeSeries("testName");
        when(mockedScope.isPlaying()).thenReturn(true);

        testTimeSeriesScopeWrapper = new TimeSeriesScopeWrapper(timeSeries, mockedScope);

        testTimeSeriesScopeWrapper.valueUpdate(100.0, 10000);

        assertFalse(timeSeries.getNotify());

        testTimeSeriesScopeWrapper.valueUpdate(200.0, 20000);

        assertEquals(2, timeSeries.getItemCount());
        FixedMillisecond period = new FixedMillisecond(10000 / 1000);
        assertEquals(100.0d, (double)timeSeries.getDataItem(period).getValue(), 0.0001d);

        period = new FixedMillisecond(20000 / 1000);
        assertEquals(200.0d, (double)timeSeries.getDataItem(period).getValue(), 0.0001d);

    }

    @Test
    public void stateUpdate() {
    }
}
