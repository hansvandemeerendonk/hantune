/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.util.List;
import java.util.prefs.Preferences;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author Roel
 */
public class HANtuneManagerTest {

    HANtuneManager hantuneManager = new HANtuneManager(HANtune.getInstance());
    String testProjectList = "C:\\Users\\Roel\\Desktop\\HANtunetests\\test1.hml;" // Please note: oldest ones in front, most recent ones at the back!!!
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test2.hml;"
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test3.hml;"
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test4.hml;"
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test5.hml;"
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test6.hml;"
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test7.hml;"
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test8.hml;"
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test9.hml;"
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test10.hml;"
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test10.hml;" // ...add some duplicate entries to the list
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test10.hml;"
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test10.hml;"
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test11.hml;"
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test12.hml;"
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test13.hml;" // ...too many projects for the list
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test14.hml;"
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test15.hml;"
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test16.hml;"
            + "C:\\Users\\Roel\\Desktop\\HANtunetests\\test17.hml;";

    @Test
    public void getLastProjectPathList() {
        Preferences userPrefsMock = mock(Preferences.class);
        when(userPrefsMock.get("lastProjectPathList", "")).thenReturn(testProjectList);
        List<String> projectList = hantuneManager.getLastProjectPathList(userPrefsMock);
        assertEquals(projectList.get(0), "C:\\Users\\Roel\\Desktop\\HANtunetests\\test3.hml");
        assertEquals(projectList.get(1), "C:\\Users\\Roel\\Desktop\\HANtunetests\\test4.hml");
        assertEquals(projectList.get(11), "C:\\Users\\Roel\\Desktop\\HANtunetests\\test14.hml");
        assertEquals(projectList.get(13), "C:\\Users\\Roel\\Desktop\\HANtunetests\\test16.hml");
        assertEquals(projectList.get(14), "C:\\Users\\Roel\\Desktop\\HANtunetests\\test17.hml");
    }

}
