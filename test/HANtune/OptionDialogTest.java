/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class OptionDialogTest extends OptionDialog {
    private static final double PRECISION = 0.000000001;
    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @Test
    public void testOptionValueVerifierFactor2() {

        double testFactor = 2.0d;

        OptionDialog testOuter = new OptionDialog();
        OptionDialog.OptionValueVerifier testVerifier = testOuter.new OptionValueVerifier();
        double testSubj[] = new double[10];
        testSubj[0] = testVerifier.convertToNearestDiscreteValue(testFactor, 15.3d);
        testSubj[1] = testVerifier.convertToNearestDiscreteValue(testFactor, 13.50001d);
        testSubj[2] = testVerifier.convertToNearestDiscreteValue(testFactor, 0.01);
        testSubj[3] = testVerifier.convertToNearestDiscreteValue(testFactor, -0.01d);
        testSubj[4] = testVerifier.convertToNearestDiscreteValue(testFactor, 32767.9d);
        testSubj[5] = testVerifier.convertToNearestDiscreteValue(testFactor, 32766.9d);
        testSubj[6] = testVerifier.convertToNearestDiscreteValue(testFactor, 32800);
        testSubj[7] = testVerifier.convertToNearestDiscreteValue(testFactor, -32767.9);
        testSubj[8] = testVerifier.convertToNearestDiscreteValue(testFactor, -32766.9);
        testSubj[9] = testVerifier.convertToNearestDiscreteValue(testFactor, -101.9d);

        assertEquals(16.0, testSubj[0], PRECISION);
        assertEquals(14.0, testSubj[1], PRECISION);
        assertEquals(0.0, testSubj[2], PRECISION);
        assertEquals(0.0, testSubj[3], PRECISION);
        assertEquals(32768.0, testSubj[4], PRECISION);
        assertEquals(32766.0, testSubj[5], PRECISION);
        assertEquals(32800.0, testSubj[6], PRECISION);
        assertEquals(-32768.0, testSubj[7], PRECISION);
        assertEquals(-32766.0, testSubj[8], PRECISION);
        assertEquals(-102.0, testSubj[9], PRECISION);
    }


    @Test
    public void testOptionValueVerifierFactor0123() {

        double testFactor = 0.123d;

        OptionDialog testOuter = new OptionDialog();
        OptionDialog.OptionValueVerifier testVerifier = testOuter.new OptionValueVerifier();
        double testSubj[] = new double[10];
        testSubj[0] = testVerifier.convertToNearestDiscreteValue(testFactor, 15.259d);
        testSubj[1] = testVerifier.convertToNearestDiscreteValue(testFactor, 13.521d);
        testSubj[2] = testVerifier.convertToNearestDiscreteValue(testFactor, 0.01);
        testSubj[3] = testVerifier.convertToNearestDiscreteValue(testFactor, -0.01d);
        testSubj[4] = testVerifier.convertToNearestDiscreteValue(testFactor, 32767.9d);
        testSubj[5] = testVerifier.convertToNearestDiscreteValue(testFactor, 32766.9d);
        testSubj[6] = testVerifier.convertToNearestDiscreteValue(testFactor, 32800);
        testSubj[7] = testVerifier.convertToNearestDiscreteValue(testFactor, -32767.9);
        testSubj[8] = testVerifier.convertToNearestDiscreteValue(testFactor, -32766.9);
        testSubj[9] = testVerifier.convertToNearestDiscreteValue(testFactor, -101.9d);

        assertEquals(15.252, testSubj[0], PRECISION);
        assertEquals(13.53, testSubj[1], PRECISION);
        assertEquals(0.0, testSubj[2], PRECISION);
        assertEquals(0.0, testSubj[3], PRECISION);
        assertEquals(32767.938, testSubj[4], PRECISION);
        assertEquals(32766.954, testSubj[5], PRECISION);
        assertEquals(32800.041, testSubj[6], PRECISION);
        assertEquals(-32767.938, testSubj[7], PRECISION);
        assertEquals(-32766.954, testSubj[8], PRECISION);
        assertEquals(-101.844, testSubj[9], PRECISION);
    }

}
