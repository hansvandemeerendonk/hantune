/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package util;

import static org.junit.Assert.assertTrue;

import java.security.Key;
import java.util.Arrays;

import javax.crypto.Cipher;

import org.junit.Test;

public class ServiceToolEncryptionTest {


    @Test
    public void testServiceEncryption() {
        String passPhrase = "1121234512345673";
        byte[] plainBytesOrg = "HELLO JCE55555555555555555555555555555555555555555|".getBytes();
        try {
            EncryptDecrypt encr = new EncryptDecrypt();
            byte[] encrBytes = encr.encryptData(plainBytesOrg, passPhrase);

            // Create a NEW instance of EncryptDecrypt!!
            EncryptDecrypt decr = new EncryptDecrypt();
            byte[] plainBytesDecrypt = decr.decryptData(encrBytes, passPhrase);

            System.out.println("DECRYPTED DATA : " + new String(plainBytesDecrypt));
            assertTrue(Arrays.equals(plainBytesDecrypt, plainBytesOrg));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Test
    public void testServiceEncryptionRandomIV() {
        byte[] plainBytesOrg = "HELLO JCE33333333333333333333333333333333333333333|".getBytes();
        try {
            // This version 'works'. But only because only 1 instance of cipher is created.
            // It won't work when a new session of cipher is created (activate commented line below),
            // because of a different randomly generated IV
            byte[] cipherBytes = null;
            byte[] plainBytesDecrypt2 = null;
            Cipher cipher = null;
            Key key = null;
            {
                key = EncryptDecrypt.generateKey("1234567654321234");
                // Create Cipher instance and initialize it to encryption mode
                cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                cipher.init(Cipher.ENCRYPT_MODE, key);
                cipherBytes = cipher.doFinal(plainBytesOrg);

                key = EncryptDecrypt.generateKey("1234567654321234");

                // When line below is activated, a new instance of cipher will be created with a new
                // random IV. The first 16 bytes of the plainBytes will be rubbish and the test will fail.
                // cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

                // Reinitialize the Cipher to decryption mode
                cipher.init(Cipher.DECRYPT_MODE, key, cipher.getParameters());
                plainBytesDecrypt2 = cipher.doFinal(cipherBytes);
                System.out.println("DECRYPTED DATA : " + new String(plainBytesDecrypt2));
                assertTrue(Arrays.equals(plainBytesDecrypt2, plainBytesOrg));

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
