/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Mike
 */
public class DeepCopyTest {
    DeepCopy deepCopier;
    
    @Before
    public void setUp() {
        deepCopier = new DeepCopy();
    }

    @Test
    public void testDeepCopyOfString_IsNotTheSame() {
        Object stringToCopy = "HelloWorld";
        Object newData = deepCopier.copy(stringToCopy);
        assertNotSame(stringToCopy, newData);
    }
    
    @Test
    public void testDeepCopyOfNotSerializableClass_ShouldReturnNull() {
        Object newData = deepCopier.copy(this);
        assertSame(null, newData);
    }
    
    @Test
    public void testDeepCopy_WithNull() {
        Object newData = deepCopier.copy(null);
        assertEquals(null, newData);
    }
}