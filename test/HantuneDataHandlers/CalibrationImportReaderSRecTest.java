/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HantuneDataHandlers;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import ASAP2.ASAP2Characteristic;
import ASAP2.ASAP2Data;
import ASAP2.ASAP2Datatype;
import ASAP2.Asap2Parameter;
import datahandling.CalibrationExportWriterSRec;
import datahandling.CalibrationHandler;
import datahandling.CalibrationImportReader;
import datahandling.CalibrationImportReader.ImportReaderResult;
import datahandling.CalibrationImportReaderSRec;
import datahandling.CurrentConfig;
import datahandling.LineDataObject;
import datahandling.SRecObject;
import haNtuneHML.Calibration;
import haNtuneHML.Calibration.ASAP2;
import haNtuneHML.HANtuneDocument;

public class CalibrationImportReaderSRecTest {

    public CalibrationHandler calibrationHandler;
    private CurrentConfig config;
    private File file;
    private byte[] testData = null;
    private String testEpromId = "TestIdEprom";
    private Long testAddressEpk = 0x22223333L;

    @Before
    public void setUp() {
        calibrationHandler = CalibrationHandler.getInstance();
        config = CurrentConfig.getInstance();

        try {
            file = File.createTempFile("HANTestReaderSRec", null);

            CalibrationExportWriterSRec exportWriter = new CalibrationExportWriterSRec();

            ArrayList<LineDataObject> srecObjs = new ArrayList<>();

            // place TestEprom Id in iHexObjs at testAddress
            SRecObject srObjEpromId = new SRecObject(testAddressEpk, testEpromId.getBytes());
            srecObjs.add(srObjEpromId);

            // place test data in iHexObjs
            testData = new byte[240];
            for (int iCnt = 0; iCnt < testData.length; iCnt++) {
                testData[iCnt] = (byte) iCnt;
            }
            SRecObject srObj = new SRecObject(0xffffff00, testData);
            srecObjs.add(srObj);
            exportWriter.writeToFile("TestWriteToFile", file, srecObjs);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testHandleImportCalibrationSRec() {
        HANtuneDocument hantuneDocument = HANtuneDocument.Factory.newInstance();
        hantuneDocument.addNewHANtune();
        hantuneDocument.getHANtune().addNewCalibration();
        Calibration calib = hantuneDocument.getHANtune().getCalibrationArray(0);
        config.setASAP2Data(new ASAP2Data());

        byte[] data = new byte[123];
        Arrays.fill(data, (byte) 99);

        addCalibrationItemToTest(calib, config, "SRecTest1", ASAP2Datatype.SBYTE, data, 1.0,
                ASAP2Characteristic.Type.CURVE, 1, 0xffffff02);

        // test importReader
        try {
            CalibrationImportReader importReader
                    = new CalibrationImportReaderSRec(testEpromId, testAddressEpk, 0L, true);

            ImportReaderResult importResult = importReader.readCalibration(file);
            assertEquals(importResult, ImportReaderResult.READER_RESULT_OK);

            importReader.transferDataIntoCalibration(calib, config.getASAP2Data().getASAP2Characteristics());

            // in srec file: address 0xffffff00, data: 0, 1, 2, 3, 4... (see setup() addClalibra...())
            // expect calibration: address 0xffffff02 !!!
            // therefore expect values: 2, 3, 4, 5, ....
            byte[] expectData = new byte[123];
            for (int idx = 0; idx < expectData.length; idx++) {
                expectData[idx] = (byte) (idx + 2);
            }

            assertArrayEquals(expectData, calib.getASAP2Array(0).getByteArrayValue());

        } catch (IOException | IndexOutOfBoundsException ex) {
            ex.printStackTrace();
            fail("Test failed, exception occurred");

        }
    }

    private void addCalibrationItemToTest(Calibration calib, CurrentConfig config, String naam,
            ASAP2Datatype datType, byte values[], double factor, ASAP2Characteristic.Type characterType,
            int numRows, long address) {

        ASAP2Characteristic asapPar = new Asap2Parameter();
        asapPar.setName(naam);
        asapPar.setDatatype(datType);
        asapPar.setType(characterType);
        asapPar.setColumnCount(values.length / numRows);
        asapPar.setRowCount(numRows);
        asapPar.setFactor(factor);
        asapPar.setAddress((int) address);

        config.getASAP2Data().getASAP2Characteristics().put(naam, asapPar);

        ASAP2 asap2 = calib.addNewASAP2();
        asap2.setName(naam);

        asap2.setByteArrayValue(values);
    }

}
