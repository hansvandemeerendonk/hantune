/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package can;

import javax.swing.SwingUtilities;

import ErrorLogger.AppendToLogfile;
import HANtune.HANtune;
import XCP.XCPonCANBasic;
import datahandling.CurrentConfig;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import peak.can.basic.IRcvEventProcessor;
import peak.can.basic.PCANBasic;
import peak.can.basic.RcvEventDispatcher;
import peak.can.basic.TPCANHandle;
import peak.can.basic.TPCANMessageType;
import peak.can.basic.TPCANMode;
import peak.can.basic.TPCANMsg;
import peak.can.basic.TPCANStatus;
import peak.can.basic.TPCANTimestampAdapter;
import peak.can.basic.TPCANType;

/**
 * Handles a CAN connection without use of the XCP protocol. Currently not
 * implemented or tested.
 *
 * @author Michiel Klifman, Roel van den Boom
 */
public class CanConnectionHandler extends Thread implements IRcvEventProcessor {

    private PCANBasic pcan;
    private final TPCANHandle channel = TPCANHandle.PCAN_USBBUS1;
    private byte CAN_message_type = TPCANMessageType.PCAN_MESSAGE_EXTENDED.getValue();
    private String error_msg = "";
    private TPCANHandle CAN_hardware = TPCANHandle.PCAN_USBBUS1;
    private TPCANTimestampAdapter rcvTime = new TPCANTimestampAdapter();
    private CurrentConfig currentConfig = CurrentConfig.getInstance();
    private TPCANStatus status = null;
    private String connectionError = "";
    protected boolean connected = false;
    protected boolean XCPconnected = false;
    protected boolean terminateConnection = false;
    long timer = System.currentTimeMillis();
    protected HANtune hanTune = HANtune.getInstance();
    protected int msgCount = 0;
    protected long meanTime = 0;
    protected long timeStamp = 0;
    protected int busLoad = 0;
    private TPCANStatus CANstatus = TPCANStatus.PCAN_ERROR_OK;
    private boolean isInitialized = false;
    private final List<TPCANMsg> buffer = Collections.synchronizedList(new LinkedList<>());

    @Override
    public void run() {
        if (currentConfig.getXcp() != null) {
            XCPconnected = currentConfig.getXcp().isConnected();
        }
        connected = true;
        try {
            do {
                Thread.sleep(50);
            } while ((CANstatus == TPCANStatus.PCAN_ERROR_OK || CANstatus == TPCANStatus.PCAN_ERROR_QRCVEMPTY) && terminateConnection == false);
        } catch (Exception e) {
            AppendToLogfile.appendMessage("Error running CanConnectionHander run() method");
            AppendToLogfile.appendError(Thread.currentThread(), e);
            unInitializeCanDriver();
        }
        connected = false;
        System.out.println("CAN connection terminated, status: " + CANstatus);
    }

    private boolean initializeCanDriver() {
        pcan = new PCANBasic();

        if (!initializeAPI()) {
            connectionError = "Could not initialize PCAN API";
            return false;
        }

        status = pcan.Initialize(channel, currentConfig.getXcpsettings().getCANBaudrate(), TPCANType.PCAN_TYPE_NONE, 0, (short) 0);
        pcan.FilterMessages(channel, 0x00000000, 0x1FFFFFFF, TPCANMode.PCAN_MODE_EXTENDED);
        if (status != TPCANStatus.PCAN_ERROR_OK) {
            pcan.Uninitialize(channel);
            connectionError = "Could not initialize PCAN driver. Please close all other applications using the PCAN hardware and try again.";
            return false;
        }

        RcvEventDispatcher.setListener(this);
        status = pcan.SetRcvEvent(channel);
        if (status != TPCANStatus.PCAN_ERROR_OK) {
            pcan.Uninitialize(channel);
            connectionError = "Could not initialize CAN interrupt handler.";
            return false;
        }
        return true;
    }

    private boolean initializeAPI() {
        try {
            isInitialized = pcan.initializeAPI();
        } catch (UnsatisfiedLinkError e) {
            ErrorLogger.AppendToLogfile.appendError(Thread.currentThread(), e);
            this.setError(e.getMessage());
            isInitialized = false;
        }
        return isInitialized;
    }

    private void unInitializeCanDriver() {
        if (pcan != null) {
            if (status == TPCANStatus.PCAN_ERROR_OK) {
                status = pcan.ResetRcvEvent(CAN_hardware);
            }
            pcan.Uninitialize(CAN_hardware);
            pcan = null;
        }
    }

    public void stopSession() {
        terminateConnection = true;

        try {
            this.join(1000);
            if (isInitialized) {
                unInitializeCanDriver();
            }
        } catch (InterruptedException ex) {
            AppendToLogfile.appendError(Thread.currentThread(), ex);
        }

    }

    public boolean startSession() {
        boolean isStarted = false;
        if (!initializeCanDriver()) {
            return isStarted;
        }
        if (this.connected == true) {
            stopSession();
            return isStarted;
        }
        setName("CanConnectionHandler");
        setPriority(Thread.MAX_PRIORITY);
        terminateConnection = false;
        this.start();
        isStarted = true;
        return isStarted;
    }

    public void startSession(PCANBasic pcan) {
        if (this.connected == true) {
            stopSession();
        }
        this.pcan = pcan;
        pcan.FilterMessages(channel, 0x00000000, 0x1FFFFFFF, TPCANMode.PCAN_MODE_EXTENDED);
        setName("CanConnectionHandler");
        setPriority(Thread.MAX_PRIORITY);
        terminateConnection = false;
        this.start();
    }

    @Override
    public void processRcvEvent(TPCANHandle channel) {
        TPCANMsg message = new TPCANMsg();
        while (pcan.Read(CAN_hardware, message, rcvTime) == TPCANStatus.PCAN_ERROR_OK) {
            long timestamp = XCPonCANBasic.calculateTimeStampMicros(rcvTime);
            SwingUtilities.invokeLater(() -> CanDataProcessor.processCanMessage(message, timestamp));
            write(message);
        }
    }

    public TPCANMsg read() {
        TPCANMsg message = null;
        if (!buffer.isEmpty()) {
            message = buffer.get(0);
            buffer.remove(message);
        }

        return message;
    }

    private void write(TPCANMsg message) {
        if (buffer.size() < 5) {
            buffer.add(message);
        } else {
            buffer.remove(0);
            buffer.add(message);
        }
    }

    public boolean isConnected() {
        return connected;
    }

    public String getErrorMessage() {
        return connectionError;
    }

    public boolean sendCanMessage(int id, byte[] data, boolean extId) {
        // check connection
        if (!connected) {
            return false;
        }
        if (extId) {
            CAN_message_type = TPCANMessageType.PCAN_MESSAGE_EXTENDED.getValue();
        } else {
            CAN_message_type = TPCANMessageType.PCAN_MESSAGE_STANDARD.getValue();
        }
        // make TPCANMsg object of given data
        TPCANMsg msg_send = new TPCANMsg(id, CAN_message_type, (byte) data.length, data);

        // send message and return status
        status = pcan.Write(CAN_hardware, msg_send);
        if (status != TPCANStatus.PCAN_ERROR_OK) {
            StringBuffer buff = new StringBuffer("");
            pcan.GetErrorText(status, (short) 0x09, buff);
            this.setError("PCAN-USB: " + buff);
            return false;
        }
        return true;
    }

    public boolean sendCanMessage(int id, int... data) {
        byte[] bytes = new byte[data.length];
        for (int i = 0; i < data.length; i++) {
            bytes[i] = (byte) data[i];
        }
        return sendCanMessage(id, bytes, false);
    }

    /**
     * Sets error flag active and stores the error message
     *
     * @param error_msg Contains the error message
     * @see XCP.XCP#getErrorMsg()
     */
    public void setError(String error_msg) {
        this.error_msg = error_msg;
    }

    /**
     * Resets error flag and empties the error message
     */
    public void resetError() {
        error_msg = "";
    }

    public String getErrorMsg() {
        return error_msg;
    }
    
    //TODO: Refator this
    //This class should not be reponsible for updating the gui
    public synchronized void notifyMsgReceived(long timeStamp) {
        hanTune.stepRotator();
        if ((msgCount % 20 == 0) && msgCount != 0) {                                    
            meanTime = timeStamp - this.timeStamp;
            this.timeStamp = timeStamp;
            msgCount = 0;
            return;
        }
        msgCount++;
    }

}
