/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package util;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.BitSet;

/**
 *
 * @author Mike
 */
public class BitKeys implements KeyListener {
    private static BitSet keyBits = new BitSet(256);

    @Override
    public void keyPressed(final KeyEvent event) {
        int keyCode = event.getKeyCode();
        keyBits.set(keyCode);
    }

    @Override
    public void keyReleased(final KeyEvent event) {
        int keyCode = event.getKeyCode();
        keyBits.clear(keyCode);
    }

    @Override
    public void keyTyped(final KeyEvent event) {
        // no implementation.
    }
    
    /**
     * This method clears the bitset of the key that was pressed.
     * @param keyCode :The keycode of the key that needs to be cleared.
     */
    public static void clearKeyPressed(final int keyCode) {
        keyBits.clear(keyCode);
    }
    
    /**
     * This method sets the bitset of the key that was pressed.
     * @param keyCode :The keycode of the key that needs to be set.
     */
    public static void setKeyPressed(final int keyCode) {
        keyBits.set(keyCode);
    }

    /**
     * This method returns whether the given key was pressed.
     * @param keyCode :The keycode of the key that needs to be checked.
     * @return 
     */
    public static boolean isKeyPressed(final int keyCode) {
        return keyBits.get(keyCode);
    }
}
