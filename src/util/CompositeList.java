/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package util;

import java.util.AbstractList;
import java.util.List;

/**
 * Allows two Lists to be treated as one, without actually joining them.
 * @author Kevin K, Michiel Klifman
 * @param <E> - the type of the list
 */
public class CompositeList<E> extends AbstractList<E> {

    private final List<? extends E> list1;
    private final List<? extends E> list2;
    private int offset;

    public CompositeList(List<? extends E> list1, List<? extends E> list2) {
        this.list1 = list1;
        this.list2 = list2;
    }

    @Override
    public E get(int index) {
        if (index == 0) {
                offset = 0;
            }
            index -= offset;
        if (index < list1.size()) {
            return list1.get(index);
        }
        return list2.get(index - list1.size());
    }

    @Override
    public int size() {
        return list1.size() + list2.size();
    }
    
    public void move(E element, List<E> from, List<E> to) {
        if (from.contains(element) && !to.contains(element)) {
            if (from == list1 && to == list2) {
                offset++;
            }
            to.add(element);
            from.remove(element);
        }
    }
    
    @Override
    public boolean contains(Object object) {
        return list1.contains(object) || list2.contains(object);
    }
}
