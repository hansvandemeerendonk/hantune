/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.io.File;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.prefs.Preferences;
import java.util.stream.Collectors;

import ASAP2.ASAP2Data;
import ASAP2.ASAP2Object;
import ASAP2.Asap2Listener;
import HANtune.CustomFileChooser.FileType;
import HANtune.HANtuneManager;
import HANtune.UserPreferences;
import XCP.XCP;
import XCP.XCPSettings;
import can.CanConnectionHandler;
import haNtuneHML.HANtuneDocument;
import haNtuneHML.Layout;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import nl.han.hantune.scripting.ScriptingManager;
import util.Util;

/**
 * @author linden
 */
public class CurrentConfig {

    public static final String LAST_FLASH_SREC_BASE_FILE = "lastFlashSrecBaseFile";
    public static final String LAST_FLASH_IHEX_BASE_FILE = "lastFlashIhexBaseFile";
    // The class CurrentConfig is a singleton, allowing only one instance of CurrentConfig
    private static CurrentConfig instance;

    // variables
    private HANtuneDocument hantuneDocument = null;
    private File projectFile = null;
    private boolean isServiceToolMode = false; // true: HANtune behaves like a service tool
    private ASAP2Data asap2Data = null;
    private final List<DescriptionFile> descriptionFiles = new ArrayList<>();
    private final List<Asap2Listener> asap2Listeners = new ArrayList<>();
    private final List<XcpConnectionListener> xcpListeners = new ArrayList<>();

    private HANtuneManager hantuneManager = null;
    private XCP xcp = null;
    private CanConnectionHandler can = null;
    private XCPSettings xcpsettings = new XCPSettings();
    private EnumMap<FileType, File> previousPaths = new EnumMap<>(FileType.class);
    private Preferences userPrefs = Preferences.userNodeForPackage(UserPreferences.class);
    private String logFilePath = userPrefs.get("lastLogPath", System.getProperty("user.dir") + "\\log");
    private String bodasPath = userPrefs.get("bodasPath", "");
    private File previousPath;
    private String previousFlashSRecBaseFile = "";
    private String previousFlashIHexBaseFile = "";

    private Protocol protocol = Protocol.XCP_ON_CAN;
    private CanApi canApi = CanApi.CAN_API;

    private boolean isXcpDaqListSizeLimited = false;
    private int xcpDaqListSizeLimit = Integer.MAX_VALUE;
    private boolean canApi2Available = false;
    private static final int TCP_HEADER_SIZE = 54 * 8;                 // no. of bits overhead in a TCP frame
    private static final int CAN_HEADER_SIZE = 9 * 8;                  // no. of bits overhead in a CAN message
    private static final int UART_HEADER_SIZE = 0 * 10;                // no. of bits overhead in a UART message
    private int busHeader = 0;
    private int busBandwidth = 0;

    private long timeOffsetMicros = 0L;
    private boolean timeOffsetEmpty = true;

    public enum CanApi {
        CAN_API
    }

    private CurrentConfig() {
        previousPath = (new File(System.getProperty("user.dir")));
        addDescriptionFile(ScriptingManager.getInstance());
    }

    /**
     * Get the instance of CurrentConfig
     */
    public static CurrentConfig getInstance() {
        if (instance == null) {
            instance = new CurrentConfig();
        }
        return instance;
    }

    public static void addSoftwareLibrary(String path) throws Exception {
        File file = new File(path);
        Method method = URLClassLoader.class.getDeclaredMethod("addURL", new Class<?>[]{URL.class});
        method.setAccessible(true);
        method.invoke(ClassLoader.getSystemClassLoader(), new Object[]{file.toURI().toURL()});
    }

    public String getLogFile_Path() {
        return this.logFilePath;
    }

    public void setLogFile_Path(String aLogFilePath) {
        this.logFilePath = aLogFilePath;
    }

    public String getBodasPath() {
        return this.bodasPath;
    }

    public void setBodasPath(String path) {
        this.bodasPath = path;
    }

    public String getFlashSRecBaseFileName() {
        return this.previousFlashSRecBaseFile;
    }

    public void setFlashSRecBaseFileName(String fileName) {
        this.previousFlashSRecBaseFile = fileName;
    }

    public String getFlashIHexBaseFileName() {
        return previousFlashIHexBaseFile;
    }

    public void setFlashIHexBaseFileName(String previousFlashIHexBaseFile) {
        this.previousFlashIHexBaseFile = previousFlashIHexBaseFile;
    }

    public boolean isServiceToolMode() {
        return isServiceToolMode;
    }

    public void setServiceToolMode(boolean isServiceTool) {
        this.isServiceToolMode = isServiceTool;
    }

    /**
     * Gets the current directory for the given type
     *
     * @param type the type to get the current directory of.
     * @return the current directory, null if not present.
     */
    public File getPreviousPath(FileType type) {
        File path = previousPaths.get(type);
        if (path == null) {
            path = previousPath;
        }
        return path;
    }

    public void setPreviousPath(FileType type, File previousPath) {
        previousPaths.put(type, previousPath);
        this.previousPath = previousPath;
    }

    public Protocol getProtocol() {
        return protocol;
    }

    public void setProtocol(Protocol protocol) {
        if (!this.protocol.equals(protocol)) {
            clearXcpDaqListLimited();
        }
        this.protocol = protocol;
        switch (protocol) {
            case CAN:
            case XCP_ON_CAN:
                busHeader = CAN_HEADER_SIZE;
                break;
            case XCP_ON_ETHERNET:
                busHeader = TCP_HEADER_SIZE;
                break;
            case XCP_ON_UART:
                busHeader = UART_HEADER_SIZE;
                break;
        }
    }

    public void setCanApi(CanApi canApi) {
        this.canApi = canApi;
    }

    public CanApi getCanApi() {
        return canApi;
    }

    public int getBusHeader() {
        return busHeader;
    }

    public int getBusBandwidth() {
        switch (protocol) {
            case CAN:
            case XCP_ON_CAN:
                busBandwidth = Util.baud2bits(xcpsettings.getCANBaudrate());
                break;
            case XCP_ON_ETHERNET:
                busBandwidth = xcpsettings.getEthernetBandwidth() * 1024 * 1024;
                break;
            case XCP_ON_UART:
                busBandwidth = xcpsettings.getUARTBandwidth();
                break;
        }
        return busBandwidth;
    }

    /**
     * Get lastPath
     */
    public HANtuneManager getHANtuneManager() {
        return hantuneManager;
    }

    /**
     * Set hantuneManager
     */
    public void setHANtuneManager(HANtuneManager hantuneManager) {
        this.hantuneManager = hantuneManager;
    }

    public XCP getXcp() {
        return xcp;
    }

    public void setXcp(XCP xcp) {
        this.xcp = xcp;
        updateXcpListeners();
    }

    public boolean isConnected() {
        return xcp != null && xcp.isConnected();
    }

    public void setCan(CanConnectionHandler can) {
        this.can = can;
    }

    public CanConnectionHandler getCan() {
        if (can == null) {
            can = new CanConnectionHandler();
        }
        return can;
    }

    public XCPSettings getXcpsettings() {
        return xcpsettings;
    }

    /**
     * Set asap2Data
     */
    public void setASAP2Data(ASAP2Data asap2Data) {
        if (asap2Data != null) {
            addDescriptionFile(asap2Data);
        } else {
            descriptionFiles.remove(this.asap2Data);
        }
        this.asap2Data = asap2Data;

        clearXcpDaqListLimited();

        updateAsap2Listeners();
    }

    public boolean isXcpDaqListSizeLimited() {
        return isXcpDaqListSizeLimited;
    }

    public void setXcpDaqListSizeLimited(boolean xcpDaqListSizeLimited) {
        isXcpDaqListSizeLimited = xcpDaqListSizeLimited;
    }

    public int getXcpDaqListSizeLimit() {
        return xcpDaqListSizeLimit;
    }

    public void setXcpDaqListSizeLimit(int xcpDaqListSizeLimit) {
        this.xcpDaqListSizeLimit = xcpDaqListSizeLimit;
    }

    // if daqlist size has been limited, it should have been limited for a connection only.
    // When a new connection has been made (protocol change) or another ASAP2 has been loaded: clear DaqListLimited
    private void clearXcpDaqListLimited() {
        isXcpDaqListSizeLimited = false;
        xcpDaqListSizeLimit = Integer.MAX_VALUE;
    }

    /**
     * Get asap2Data
     */
    public ASAP2Data getASAP2Data() {
        return asap2Data;
    }

    public boolean hasASAP2Data() {
        return (asap2Data != null);
    }

    /**
     * Sets the filename and document reference of the project file.
     */
    public void setProject(File projectFile, HANtuneDocument hantuneDocument) {
        this.projectFile = projectFile;
        this.hantuneDocument = hantuneDocument;
    }

    /**
     * Sets the file reference of the project file.
     *
     * @param projectFile
     */
    public void setProject(File projectFile) {
        this.projectFile = projectFile;
    }

    /**
     * Get projectFile
     *
     * @return projectFile
     */
    public File getProjectFile() {
        return projectFile;
    }

    public void setHANtuneDocument(HANtuneDocument hantuneDocument) {
        this.hantuneDocument = hantuneDocument;
    }

    public HANtuneDocument getHANtuneDocument() {
        return hantuneDocument;
    }

    public void setCanApi2Available(boolean canApi2Available) {
        this.canApi2Available = canApi2Available;
    }

    public void setTimeOffsetMicros(long timeOffsetMicros) {
        this.timeOffsetMicros = timeOffsetMicros;
        this.timeOffsetEmpty = false;
    }

    public long getTimeOffsetMicros() {
        return timeOffsetMicros;
    }

    public void setTimeOffsetMicrosEmpty() {
        this.timeOffsetMicros = 0L;
        this.timeOffsetEmpty = true;
    }

    public boolean isTimeOffsetEmpty() {
        return this.timeOffsetEmpty;
    }

    public boolean isCanApi2Available() {
        return canApi2Available;
    }

    public List<DescriptionFile> getDescriptionFiles() {
        return descriptionFiles;
    }

    public DescriptionFile[] getDescriptionFileArray() {
        return descriptionFiles.toArray(new DescriptionFile[descriptionFiles.size()]);
    }

    public void addDescriptionFile(DescriptionFile file) {
        ScriptingManager.getInstance().addReferencesToInterpreters(file.getReferences());
        descriptionFiles.add(file);
    }

    public void removeDescriptionFileByName(String name) {
        Iterator<DescriptionFile> iterator = descriptionFiles.iterator();
        while (iterator.hasNext()) {
            DescriptionFile file = iterator.next();
            if (file.getName().equals(name)) {
                iterator.remove();
                break;
            }
        }
    }

    public DescriptionFile getDescriptionFileByName(String name) {
        if (name != null) {
            for (DescriptionFile file : descriptionFiles) {
                if (name.equals(file.getName())) {
                    return file;
                }
            }
            //If no file has been found try the current ASAP2 for backwards
            //compatability purposes. Pre 1.2 references in a project were not
            //saved with a source (file name)
            if (name.equals(ASAP2Object.DEFAULT_SOURCE)) {
                return asap2Data;
            }
        }
        return null;
    }

    public boolean hasDescriptionFile(String name) {
        return getDescriptionFileByName(name) != null;
    }

    public DescriptionFile getDescriptionFile(File file) {
        if (file != null) {
            String name = file.getName();
            String path = file.getPath();
            for (DescriptionFile descriptionFile : descriptionFiles) {
                if (name.equals(descriptionFile.getName()) && path.equals(descriptionFile.getPath())) {
                    return descriptionFile;
                }
            }
        }
        return null;
    }

    public boolean hasDescriptionFile(File file) {
        return getDescriptionFile(file) != null;
    }

    public <T extends Reference> boolean isReferenceAvailable(Reference reference, Class<T> type) {
        DescriptionFile file = getDescriptionFileByName(reference.getSource());
        if (file != null) {
            return file.hasReferenceOfType(reference, type);
        }
        return false;
    }

    public <T extends Reference> T getReferenceByName(String name, Class<T> type) {
        for (DescriptionFile file : descriptionFiles) {
            for (T signal : file.getReferencesOfType(type)) {
                if (signal.getName().equals(name)) {
                    return signal;
                }
            }
        }
        return null;
    }

    public <T extends Reference> T getReferenceOfType(Reference reference, Class<T> type) {
        DescriptionFile file = getDescriptionFileByName(reference.getSource());
        if (file != null) {
            return file.getReferenceOfType(reference, type);
        }
        return null;
    }

    public <T extends Reference> List<T> getAllReferencesOfType(Class<T> type) {
        List<T> references = new ArrayList<>();
        for (DescriptionFile file : descriptionFiles) {
            references.addAll(file.getReferencesOfType(type));
        }
        return references;
    }

    public List<? extends Reference> getAllReferences() {
        return descriptionFiles.stream()
            .flatMap(f -> f.getReferences().stream())
            .collect(Collectors.toList());
    }

    public void addAsap2Listener(Asap2Listener listener) {
        if (!asap2Listeners.contains(listener)) {
            asap2Listeners.add(listener);
        }
    }

    public void updateAsap2Listeners() {
        for (Asap2Listener listener : asap2Listeners) {
            listener.asap2Update(asap2Data);
        }
    }

    public void addXcpListener(XcpConnectionListener listener) {
        if (!xcpListeners.contains(listener)) {
            xcpListeners.add(listener);
        }
    }

    public void updateXcpListeners() {
        for (XcpConnectionListener listener : xcpListeners) {
            listener.xcpConnectionUpdate(xcp != null);
        }
    }

    public boolean canUseDoubles() {
        return getProtocol() != Protocol.XCP_ON_CAN;
    }

    public enum Protocol {
        CAN("CAN"), XCP_ON_CAN("XCP on CAN"), XCP_ON_ETHERNET("XCP on Ethernet"), XCP_ON_UART("XCP on USB/UART");

        private final String value;

        private Protocol(String val) {
            this.value = val;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    /**
     * Creates a new layout based on the given name. If "name" is either null or
     * empty, the layout will be named "Unnamed". If a layout with the required
     * name already exists an index will be added to the name, to keep it unique
     *
     * @param name the name for the new layout
     */
    public void createNewLayout(String name) {
        if (name == null || name.isEmpty()) {
            name = "Unnamed";
        }

        int index = 1;
        String validName = name;
        while (layoutExists(validName)) {
            // keep appending, until it is unique
            validName = name + " (" + index + ")";
            index++;
        }
        this.getHANtuneManager().newLayout(validName);
    }

    private boolean layoutExists(String name) {
        List<Layout> layouts = this.getHANtuneDocument().getHANtune().getLayoutList();
        return layouts.stream().anyMatch(e -> e.getTitle().equals(name));
    }
}
