/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.nio.ByteBuffer;

public class SRecObject implements LineDataObject {

    static final int SREC_RADIX = 16;
    static final int EXTRA_SIZE_S3_ADDRESS_CHECKSUM = 5; // checksum: 1; S3 address: 4 extra pairs

    private String name;
    private long startAddress; // use long to store 'unsigned int'
    private byte[] data;

    public SRecObject(long address, byte[] data) {
        this.startAddress = address;
        this.data = data;
    }

    public SRecObject(String name, long address, byte[] data) {
        this.name = name;
        this.startAddress = address;
        this.data = data;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public long getStartAddress() {
        return startAddress;
    }

    @Override
    public void addStartAddressFlashOffset(long offset) {
        startAddress += offset;
    }

    // last memory address holding data
    @Override
    public long getEndAddress() {
        return startAddress + data.length - 1;
    }

    @Override
    public byte[] getData() {
        return data;
    }

    @Override
    public int getDataSize() {
        return data.length;
    }

    public byte calculateS3Checksum(int strtIdx, int iSize) {
        int checkSum = iSize + EXTRA_SIZE_S3_ADDRESS_CHECKSUM;

        byte[] adrBytes = new byte[4];
        ByteBuffer.wrap(adrBytes).putInt((int) (startAddress + strtIdx));
        for (int iCnt = 0; iCnt < 4; iCnt++) {
            checkSum += adrBytes[iCnt];
        }

        for (int iCnt = 0; iCnt < iSize; iCnt++) {
            checkSum += data[iCnt + strtIdx];
        }

        return (byte) ~((byte) checkSum); // output ones complement
    }

    static public byte calculateHeaderChecksum(byte[] hdrBytes) {
        int checkSum = 0;

        for (int iCnt = 0; iCnt < hdrBytes.length; iCnt++) {
            checkSum += hdrBytes[iCnt];
        }

        return (byte) ~((byte) checkSum); // output ones complement
    }

    static byte calculateS3CheckSum(String line) {
        int numPairs = (line.length() / 2) - 2; // 2: 1 pair 'S3', 1 pair checksum (at end of line)
        int offset = 2;
        int checkSum = 0;
        for (int iCnt = 0; iCnt < numPairs; iCnt++) {
            checkSum += Short.parseShort(line.substring(offset, offset + 2), SREC_RADIX);
            offset += 2;
        }

        return (byte) ~((byte) checkSum); // output ones complement
    }

}
