/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

/**
 * An interface that describes the common attributes between a signal (input) 
 * and parameter (output) as used by HANtune.
 * 
 * @author Michiel Klifman
 */
public interface ValueDescription extends Reference {
    
    /**
     * @return the minimum value this signal or parameter can have.
     */
    public double getMinimum();
    
    
    /**
     * @return the maximum value this signal or parameter can have.
     */
    public double getMaximum();
    
    /**
     * @return the unit for this signal or parameter (i.e. "Volt" or "Km/h").
     */
    public String getUnit();
    
    /**
     * If the signal does not use a factor, this should return 1.
     * @return the factor
     */
    public double getFactor();
    
    /**
     * @return the number of digits behind the decimal point of the value of 
     * this signal or parameter.
     */
    public int getDecimalCount();
    
    /**
     * @return whether or not the value for this signal or parameter has any 
     * digits behind the decimal point.
     */
    public boolean hasDecimals();
    
}
