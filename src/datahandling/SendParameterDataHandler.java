/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import ASAP2.ASAP2Characteristic;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.Timer;

/**
 *
 * @author Michel
 */
public class SendParameterDataHandler {

    /**
     * Boolean to check if data can be send to the ecu
     */
    private boolean canSendParameter = true;
    /**
     * Object to get classes not available in this class.
     */
    private final CurrentConfig currentConfig;
    /**
     * Queue of data that still needs to be send to the ecu.
     */
    private ArrayList<Object[]> sendDataQueue = new ArrayList<>();
    /**
     * timer that sends data to ecu from the sendDataQueue
     */
    private Timer dataQueueTimer;

    /**
     * @return the dataQueueTimer
     */
    public Timer getDataQueueTimer() {
        return dataQueueTimer;
    }

    /**
     * @return the sendDataQueue
     */
    public ArrayList<Object[]> getSendDataQueue() {
        return sendDataQueue;
    }

    /**
     * @param sendDataQueue the sendDataQueue to set
     */
    public void setSendDataQueue(final ArrayList<Object[]> sendDataQueue) {
        this.sendDataQueue = sendDataQueue;
    }

    /**
     * Method to create a timer that sends data form the dataqueue to the ecu.
     *
     * @param miliseconds interval of sending data.
     */
    private void createDataQueueTimer(final int miliseconds) {
        dataQueueTimer = new Timer(miliseconds, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canSendParameter = true;
                if (!getSendDataQueue().isEmpty()) {
                    Object[] queueData = getSendDataQueue().get(0);
                    setParameter(queueData[0].toString(), queueData[1], (int[]) queueData[2], false);
                }
            }
        });
    }

    /**
     * method to set the Timeout between sending parameters to the ecu.
     *
     * @param miliseconds number of miliseconds to wait before sending new
     * parameter.
     */
    public SendParameterDataHandler(final int miliseconds) {
        createDataQueueTimer(miliseconds);
        currentConfig = CurrentConfig.getInstance();
    }

    /**
     * Removes the object with the asap2ref in the sendDataQueue. The object
     * Will only be removed if the value is still the same. If the value is not
     * the same it needs to be send to the ecu and should not be deleted.
     *
     * @param asap2ref reference to a parameter or signal on the ecu.
     * @param value value to set
     * @param position position in table. null if not in table.
     */
    private void removeAsap2refFromSendDataQueue(final String asap2ref, final Object value, final int[] position) {
        for (int i = 0; i < getSendDataQueue().size(); i++) {
            Object[] objArr = getSendDataQueue().get(i);
            if (objArrayHasAsap2RefAndPosition(objArr, asap2ref, position) && objectArrayHasValue(objArr, value)) {
                getSendDataQueue().remove(i);
            }
        }
    }

    /**
     * Method to send data to the ecu. sends a adress to xcp
     *
     * @param asap2ref asap2reference to set the data to
     * @param value value to send
     * @param position position in a table. null if not in a table.
     * @param timestamp when data is send
     * @return true if data has been send.
     */
    private boolean sendValueToEcu(final String asap2ref, final Object value, final int[] position) {
        if (currentConfig.getXcp() != null && currentConfig.getXcp().isConnected()) {
            int address = getParameterMemoryAddress(asap2ref, position);
            char[] data = currentConfig.getASAP2Data().getASAP2Characteristics().get(asap2ref).convertValue(value);
            if (currentConfig.getXcp().setParameter(address, (char) 0, data)) {
                removeAsap2refFromSendDataQueue(asap2ref, value, position);
                return true;
            }
        }
        return false;
    }

    /**
     * Method to handle the dataqueue. Keeps track of the values that need to be
     * set at the controller. If asap2ref is already present. the value is
     * overridden.
     *
     * @param asap2ref asap2reference to set the data to
     * @param value value to send
     * @param position position in a table. null if not in a table.
     */
    private void handleSendDataQueue(final String asap2ref, final Object value, final int[] position) {
        boolean isInQueue = false;
        Object[] asap2RefArray = {asap2ref, value, position};
        for (int i = 0; i < getSendDataQueue().size(); i++) {
            Object[] objArr = getSendDataQueue().get(i);
            if (objArrayHasAsap2RefAndPosition(objArr, asap2ref, position)) {
                getSendDataQueue().set(i, asap2RefArray);
                isInQueue = true;
            }
        }
        if (!isInQueue) {
            getSendDataQueue().add(asap2RefArray);
        }
    }

    /**
     * Method to check if the provided objArr contains the asap2ref and
     * position.
     *
     * @param objArr object array to compare.
     * @param asap2ref reference to a ecu parameter
     * @param position position in a table. null if not in a table.
     * @return true if match has been found.
     */
    private boolean objArrayHasAsap2RefAndPosition(final Object[] objArr, final String asap2ref, final int[] position) {
        return objArr[0].equals(asap2ref) && Arrays.equals((int[])objArr[2], position);
    }

    /**
     * method to check if object array contains value.
     *
     * @param objArr Object[] array.
     * @param value value to compare.
     * @return true if array contains value.
     */
    private boolean objectArrayHasValue(final Object[] objArr, final Object value) {
        for (Object obj : objArr) {
            if (obj != null && obj.equals(value)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method to get the memory address of the parameter on the ecu.
     *
     * @param asap2ref reference to parameter on the ecu.
     * @param position position in table. null if value is not in table.  [x][y] --> [x indicates which row][y indicates which column]
     *        position[0] --> [indicates which column] 
     *        position[1] --> [indicates which row]
     * @return int memory address.
     */
    private int getParameterMemoryAddress(String asap2ref, int[] position) {
        int byteCount =
            currentConfig.getASAP2Data().getASAP2Characteristics().get(asap2ref).getDatatypeSize();
        int address = currentConfig.getASAP2Data().getASAP2Characteristics().get(asap2ref).getAddress();

        if (position != null) {
            if (position.length == 1) {
                address = address + position[ASAP2Characteristic.COLUMN_INDEX] * byteCount;
            } else if (position.length == 2) {
                //address = addressOfFirstCell + (RowCount x ColumnIndex + RowIndex) where RowCount is the total amount of rows and ColumnIndex and RowIndex indicate the cell to be edited.
                address = address + (position[ASAP2Characteristic.COLUMN_INDEX] * currentConfig.getASAP2Data().getASAP2Characteristics().get(asap2ref).getRowCount()
                    + position[ASAP2Characteristic.ROW_INDEX]) * byteCount;
            }
        }
        return address;
    }

    /**
     * Sends the value of a parameter to the XCP slave device and sets the value
     * in hantune's asap2data. notifies observers that data has been changed.
     * Data will be set at a interval of 10hz. Calling this method faster will
     * put the data in a sendDataQueue to be send later.
     *
     * @param asap2ref currentConfig.getASAP2Data() parameter to be modified at
     * the slave device.
     * @param value Value to send to the slave device.
     * @param position an array holding the position (column, row) of a value in
     * a table.
     * @param skipDataQueue
     * @return whether or not the parameter has been send.
     */
    public void setParameter(final String asap2ref, final Object value, final int[] position, final boolean skipDataQueue) {
        boolean parameterSent = false;
        if (canSendParameter || skipDataQueue) {
            if (!skipDataQueue) {
                canSendParameter = false;
            }
            ASAP2Characteristic characteristic = currentConfig.getASAP2Data().getCharacteristicByName(asap2ref);
            saveParameter(characteristic, (double)value, position);
            parameterSent = sendValueToEcu(asap2ref, value, position);
            setParameterInSync(characteristic, parameterSent, position);
            if (parameterSent) {
                if (!skipDataQueue) {
                    getDataQueueTimer().start();
                }
            } else {
                canSendParameter = true;
            }
        } else if (!canSendParameter) {
            handleSendDataQueue(asap2ref, value, position);
        }
    }

    public void setParameter(String asap2ref, double value, int row, int column) {
        setParameter(asap2ref, value, new int[]{column, row}, false);
    }

    public void setParameter(String asap2ref, double value, int column) {
        setParameter(asap2ref, value, new int[]{column}, false);
    }
    
    public void setParameter(final String asap2ref, final double value) {
        setParameter(asap2ref, value, null, false);
    }

    private void saveParameter(ASAP2Characteristic characteristic, double value, int[] position) {
        ASAP2Characteristic.Type type = characteristic.getType();
        switch (type) {
            case VALUE:
                characteristic.setValue(value);
                break;
            case MAP:
                characteristic.setValueAt(value, position[1], position[0]);
                break;
            case CURVE:
                characteristic.setValueAt(value, 0, position[0]);
                break;
            case AXIS_PTS:
                characteristic.setValueAt(value, position[0]);
                break;
        }
    }

    private void setParameterInSync(ASAP2Characteristic characteristic, boolean inSync, int[] position) {
        ASAP2Characteristic.Type type = characteristic.getType();
        switch (type) {
            case VALUE:
                characteristic.setInSync(inSync);
                break;
            case MAP:
                characteristic.setInSyncAt(inSync, position[1], position[0]);
                break;
            case CURVE:
                characteristic.setInSyncAt(inSync, 0, position[0]);
                break;
            case AXIS_PTS:
                characteristic.setInSyncAt(inSync, position[0]);
                break;
        }
    }
}
