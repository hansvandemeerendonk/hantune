/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import haNtuneHML.Calibration;

import static datahandling.IHexObject.IHEX_RADIX;
import static datahandling.IHexObject.RecordType;

public class CalibrationExportWriterIHex implements CalibrationExportWriter {

    static final int MAX_DATABYTES_PER_IHEXLINE = 0x10; // try to conform to commonly used line length

    @Override
    public void writeCalibration(Calibration calibration, File file) throws IOException {
        // No export of calibration ONLY possible. Always needs to be merged with target application
    }

    @Override
    // headerString is not used for IHEX file
    public void writeToFile(String headerString, File file, List<LineDataObject> ihObjs) throws IOException {
        if (!ihObjs.isEmpty()) {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));

            // writeHeader(file.getName(), headerString, bw);
            writeDataItems(ihObjs, bw);
            writeFooter(bw);

            bw.close();
        }
    }

    public String getWarningLines(){
        // not used, empty
        return "";
    }


    private void writeDataItems(List<LineDataObject> ihObjs, BufferedWriter w) throws IOException {
        int lastBaseAddr = 0;
        for (LineDataObject ldObj : ihObjs) {
            IHexObject ihObj = (IHexObject) ldObj;

            if (ihObj.getType() == RecordType.IHEX_EXTENDED_LINEAR_ADDRESS) {
                w.write(createExtendedLinearAddressLine(ihObj));
                lastBaseAddr = ihObj.getBaseAddress();

            } else if (ihObj.getType() == RecordType.IHEX_DATA) {
                if (ihObj.getDataSize() > 0) {
                    if (lastBaseAddr != ihObj.getBaseAddress()) {
                        w.write(createExtendedLinearAddressLine(ihObj));
                    }

                    w.write(createIHexDataLine(ihObj));
                    lastBaseAddr = ihObj.getBaseAddress();
                }
            }
        }

    }

    private String createExtendedLinearAddressLine(IHexObject ihObj) {
        String str = String.format(":02000004%04X", ihObj.getBaseAddress());

        return str + calcIHexChecksum(str) + LF;
    }

    private String createIHexDataLine(IHexObject obj) {
        String lines = new String();
        byte[] data = obj.getData();

        // conform to max line length. Create multiple IHex data lines if necessary
        for (int strtIdx = 0; strtIdx < data.length; strtIdx += MAX_DATABYTES_PER_IHEXLINE) {
            int size = data.length - strtIdx;

            if (size > MAX_DATABYTES_PER_IHEXLINE) {
                size = MAX_DATABYTES_PER_IHEXLINE;
            }
            StringBuilder line = new StringBuilder((size * 2) + 11); // for speed, use a StringBuilder

            // build the output line,
            line.append(String.format(":%02X%04X00", size, obj.getAddressOffset() + strtIdx));

            for (int iCnt = 0; iCnt < size; iCnt++) {
                line.append(String.format("%02X", data[iCnt + strtIdx]));
            }

            lines += line.toString() + calcIHexChecksum(line.toString()) + LF;
        }

        return lines;
    }

    private String calcIHexChecksum(String inStr) {
        int numPairs = (inStr.length() - 1) / 2; // Skip ':' at start.
        int offset = 1;
        int checkSum = 0;
        for (int iCnt = 0; iCnt < numPairs; iCnt++) {
            checkSum += Short.parseShort(inStr.substring(offset, offset + 2), IHEX_RADIX);
            offset += 2;
        }

        // return ones complement checksum as a string
        return String.format("%02X", (byte) (0x100 - ((byte) checkSum)));
    }

    private void writeFooter(BufferedWriter w) throws IOException {
        String outStr = ":00000001FF" + LF;
        w.write(outStr);

    }

}
