/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.datahandling.LogReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ErrorLogger.AppendToLogfile;

public class CsvLogInfo {

    public class DaqListInfo {
        private String idx;
        private String name;
        private String freq;

        DaqListInfo(String daqIdx, String name, String freq) {
            idx = daqIdx;
            this.name = name;
            this.freq = freq;
        }

        public String getIdx() {
            return idx;
        }

        public String getName() {
            return name;
        }

        public String getFreq() {
            return freq;
        }
    }

    public class ProtocolSignalName {
        private String protocol;
        private String name;

        ProtocolSignalName(String protocolName) {
            protocol = protocolName;
            name = "";
        }

        public String getProtocol() {
            return protocol;
        }

        public void setProtocol(String prot) {
            this.protocol = prot;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    private String projectName = null;
    private String ecuId = null;
    private List<DaqListInfo> daqListInfos = null;
    private List<String> dbcListInfos = null;
    private List<String> dataItemNames = null;
    private List<ProtocolSignalName> protocolSignalNames = null;
    private boolean isProtocolSignalNamesFilled = false;
    private char decimalSeparator = ',';

    private String startDate = null;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getEcuId() {
        return ecuId;
    }

    public void setEcuId(String ecuId) {
        this.ecuId = ecuId;
    }

    public List<DaqListInfo> getDaqListInfos() {
        return daqListInfos;
    }

    public void addDaqListInfo(String idxStr, String name, String freq) {
        if (daqListInfos == null) {
            daqListInfos = new ArrayList<>();
        }
        this.daqListInfos.add(new DaqListInfo(idxStr, name, freq));
    }

    public List<String> getDbcInfos() {
        return dbcListInfos;
    }

    public void addDbcInfo(String fileName) {
        if (dbcListInfos == null) {
            dbcListInfos = new ArrayList<>();
        }
        this.dbcListInfos.add(fileName);
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public List<ProtocolSignalName> getProtocolSignalNames() {
        return protocolSignalNames;
    }

    public void addProtocolNames(String[] protNames) {
        protocolSignalNames = new ArrayList<>();
        Arrays.stream(protNames).forEach(name -> protocolSignalNames.add(new ProtocolSignalName(name)));
    }


    public void addSignalNames(String[] sigNames) throws IOException {
        if (sigNames.length != protocolSignalNames.size()) {
            String errString = String.format("Number of protocolnames and signalnames in header don't match: %d vs %d",
                protocolSignalNames.size(), sigNames.length);
            throw new IOException(errString);
        }

        for (int idx = 0; idx < sigNames.length; idx++) {
            protocolSignalNames.get(idx).setName(sigNames[idx]);
        }
        isProtocolSignalNamesFilled = true;
    }


    public boolean isHeaderComplete() {
        return isProtocolSignalNamesFilled;
    }

    public char getDecimalSeparator() {
        return decimalSeparator;
    }

    public void setDecimalSeparator(char decimalSeparator) {
        this.decimalSeparator = decimalSeparator;
    }
}
