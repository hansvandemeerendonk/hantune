/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import ErrorLogger.AppendToLogfile;

public class VersionInfo {

    private static final String VERSIONFILE_NAME = "config/version.properties";

    enum VersionInfoProperties {
        BUILDNUMBER("HANtune.version.BuildNumber", "0"),
        VERSION_TEXT("HANtune.version.VersionText", "Error '" + VERSIONFILE_NAME + "''"),
        VERSION_NUMBER("HANtune.version.VersionNumber", "0.0");

        enum BOOLEAN {TRUE, FALSE}

        private String key;
        private String value;

        VersionInfoProperties(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }

        private static Properties getAllProperties() {
            Properties defaultProps = new Properties();

            Arrays.asList(VersionInfoProperties.values()).forEach(val -> defaultProps.put(val.getKey(), val.getValue()));
            return defaultProps;
        }
    }

    private static VersionInfo instance = null;
    private static Properties versionProps = null;


    /**
     * Singleton, private constructor
     */
    private VersionInfo(String name) {
        fillVersionData(name);
    }

    public static VersionInfo getInstance() {
        if (instance == null) {
            instance = new VersionInfo(VERSIONFILE_NAME);
        }
        return instance;
    }

    /**
     * For unit testing only! Use this initPropertiesTest() instead of
     * getInstance()
     *
     * @param fileName
     * @return instance initialized from test file
     */
    protected static VersionInfo getInstanceTest(String fileName) {
        instance = new VersionInfo(fileName);
        return instance;
    }

    private static void fillVersionData(String name) {
        try {
            versionProps = new Properties(VersionInfoProperties.getAllProperties());
            versionProps.load(new FileInputStream(name));
        } catch (IOException e) {
            AppendToLogfile.appendMessage("Error reading version file: " + name + " could not be found",
                "Version information not available in HANtune.");
        }
    }


    public String getVersionName() {
        return versionProps.getProperty(VersionInfoProperties.VERSION_TEXT.getKey());
    }

    public String getVersionNumber() {
        return versionProps.getProperty(VersionInfoProperties.VERSION_NUMBER.getKey());
    }

    public float getVersionFloat() {
        try {
            return Float.parseFloat(getVersionNumber());
        } catch (NumberFormatException e) {
            AppendToLogfile.appendMessage("Version format error: " + getVersionNumber() + " could not be converted",
                "Version " + VersionInfoProperties.VERSION_NUMBER.getValue() + " assumed");
            AppendToLogfile.appendError(Thread.currentThread(), e);
            return Float.parseFloat(VersionInfoProperties.VERSION_NUMBER.getValue()); // When this gives exception: Crash!
        }
    }

    public int getBuildNumber() {
        return Integer.decode(versionProps.getProperty(VersionInfoProperties.BUILDNUMBER.getKey()));
    }

    public String getFullVersionText() {
        return "HANtune " + getVersionNumber() + "." + getBuildNumber();
    }

    public String getFullBuildVersionText() {
        return "v" + getVersionNumber() + "." + getBuildNumber() + " - " + getVersionName();
    }

}
