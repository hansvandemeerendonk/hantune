/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.config;

import java.awt.BorderLayout;
import java.util.Arrays;
import java.util.Properties;

/**
 * Enum with all application properties and their default value. Default value can be overridden by properties file.
 */
public enum ConfigProperties {

    PARAM_OUTOFSYNC_COLOR("HANtune.color.ParamOutOfSync", "255, 153, 51"),  // orange
    TABLE_FIELD_COLOR_MIN("HANtune.color.TableFieldMin",  "50, 50, 255"), // blue
    TABLE_FIELD_COLOR_MID("HANtune.color.TableFieldMid",  "50, 255, 50"), // white
    TABLE_FIELD_COLOR_MAX("HANtune.color.TableFieldMax",  "255, 50, 50"),  // yellow
    TABLE_FIELD_BOOLEAN_USE_MID_VALUE("HANtune.boolean.TableFieldUseMidValue", "false"),
    TABLE_FIELD_BOOLEAN_USE_HUE("HANtune.boolean.TableFieldUseHue", "true"),

    BARVIEWER_LABEL_POSITION("HANtune.topBottom.BarViewerLabelPos", TopBottomLabelPos.BOTTOM.name()),
    BOOLEANVIEWER_LABEL_POSITION("HANtune.topBottom.BooleanViewerLabelPos", TopBottomLabelPos.BOTTOM.name()),
    DIGITALVIEWER_LABEL_POSITION("HANtune.topBottom.DigitalViewerLabelPos", TopBottomLabelPos.BOTTOM.name()),
    GAUGEVIEWER_LABEL_POSITION("HANtune.topBottom.GaugeViewerLabelPos", TopBottomLabelPos.BOTTOM.name()),
    LEDVIEWER_LABEL_POSITION("HANtune.topBottom.LedViewerLabelPos", TopBottomLabelPos.BOTTOM.name()),
    TABLEEDITOR_LABEL_POSITION("HANtune.topBottom.TableEditorLabelPos", TopBottomLabelPos.BOTTOM.name());

    // Enum type to set label text at the bottom or at the top of a Viewer/Table
    public enum TopBottomLabelPos {
        TOP(BorderLayout.NORTH),
        BOTTOM(BorderLayout.SOUTH);

        TopBottomLabelPos(String labelString) {
            this.labelPositionString = labelString;
        }

        private String labelPositionString;

        public String getLabelPositionString() {
            return labelPositionString;
        }
    }

    private String key;
    private String value;

    ConfigProperties(String key, String value) {
        this.key = key;
        this.value = value;
    }

    String getKey() {
        return key;
    }

    String getValue() {
        return value;
    }


    protected static Properties getAllProperties() {
        Properties props = new Properties();
        Arrays.asList(ConfigProperties.values()).forEach(val -> props.put(val.getKey(), val.getValue()));

        return props;
    }
}
