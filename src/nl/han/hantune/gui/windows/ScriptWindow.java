/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.windows;

import HANtune.ReferenceWindow;
import datahandling.ReferenceStateListener;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import nl.han.hantune.scripting.Script;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class ScriptWindow extends ReferenceWindow<Script> implements ReferenceStateListener {
    
    private Script script;
    private JButton scriptButton;
    
    public ScriptWindow() {
        super(Script.class, false);
    }

    /**
     * Rebuilds the window
     */
    @Override
    public void rebuild() {
        super.rebuild();
        pack();
        validate();
    }

    /**
     * Fill the window with its content
     */
    @Override
    protected void addReferenceContent() {
        if (!references.isEmpty()) {
            script = references.get(0);
            script.addStateListener(this);
            saveAndShowTitleLabel(getBooleanWindowSetting("showTitleLabel", false));
            scriptButton = new JButton((script.isRunning() ? "Stop " : "Start ") + script.getName());
            scriptButton.setFocusable(false);
            scriptButton.setToolTipText(script.getPath());
            
            if (script.isActive()) {
                scriptButton.addActionListener(e -> {
                    if (!script.isRunning()) {
                        script.start();
                    } else {
                        script.stop();
                    }
                });
            }

            scriptButton.setPreferredSize(new Dimension(130, 25));
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.weightx = 1;
            gridBagConstraints.weighty = 1;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.insets = new Insets(2, 2, 2, 2);
            getContentPane().add(scriptButton, gridBagConstraints);            
        }
    }

    /**
     * The ScriptWindow currently has no settings
     */
    @Override
    public void loadSettings() {
    }

    /**
     * Called when this window's script changes state (i.e. is started or
     * stopped)
     */
    @Override
    public void stateUpdate() {
            scriptButton.setEnabled(script.isActive());
        if (script.isRunning()) {
            scriptButton.setText("Stop " + script.getName());
        } else {
            scriptButton.setText("Start " + script.getName());
        }
        getContentPane().repaint();
    }

}
