/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.windows.im;

import HANtune.viewers.HANtuneViewer;
import components.ImageFilter;
import nl.han.hantune.gui.dialogs.ManageManipulationsDialog;

import datahandling.Signal;
import datahandling.SignalListener;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import util.MessagePane;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import nl.han.hantune.gui.components.TransparentJPanel;
import nl.han.hantune.gui.windows.im.ImageManipulator.IMSignalListener;

/**
 * A viewer that allows for an image to be manipulated (i.e. moved, rotated,
 * scaled and transparentized) based on signal values.
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class ImageManipulator extends HANtuneViewer<IMSignalListener> {

    private static final String IMAGE = "Image";
    private static final String MANIPULATION = "Manipulation";
    private static final String SIZE = "Size";
    private static final String TYPE = "Type";
    private static final String MODE = "Mode";
    private static final String BOUND = "Bound";
    private static final String VALUE = "Value";
    private static final String PROPERTY = "Property";

    private final ImageManipulationPanel manipulationPanel = new ImageManipulationPanel(this);
    private String path;
    private final Map<Manipulation, Signal> manipulations = new LinkedHashMap<>();
    
    private long lastRepaint = 0;

    @Override
    protected void buildDefaultReferenceWindow() {
        super.buildDefaultReferenceWindow();
        loadSettings();
    }

    @Override
    public void rebuild() {
        super.rebuild();
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        BufferedImage image = manipulationPanel.getImage();
        if (image == null) {
            setPreferredSize(new Dimension(250, 250));
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = 2;
            JButton browseButton = new JButton("Add Image");
            browseButton.addActionListener(e -> openImageDialog());
            getContentPane().add(browseButton, gridBagConstraints);
        } else {
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.weightx = 1;
            gridBagConstraints.weighty = 1;
            gridBagConstraints.gridheight = 2;
            if (this.getReferences().isEmpty()) {
                JPanel transparentPanel = new TransparentJPanel();
                transparentPanel.setBackground(new Color(255, 255, 255, 200));
                getContentPane().add(transparentPanel, gridBagConstraints);
            } else {
                addPopupMenuItems();
            }
            showBorder(NO_BORDER);
            getContentPane().add(manipulationPanel, gridBagConstraints);
        }
        if (this.getReferences().size() <= 1)pack();
    }

    /**
     * Adds items to the pop-up menu, such as choosing an image, or manage
     * manipulations
     */
    private void addPopupMenuItems() {
        JMenuItem manageManipulationsMenuItem = new JMenuItem("Manage Manipulations");
        manageManipulationsMenuItem.addActionListener(e -> new ManageManipulationsDialog(this).setVisible(true));
        addMenuItemToPopupMenu(manageManipulationsMenuItem);
    }

    @Override
    protected String getIntroMessage() {
        return "";
    }

    @Override
    protected IMSignalListener getListenerForSignal(Signal signal) {
        return new IMSignalListener(signal);
    }

    @Override
    public void loadSettings() {
        manipulations.clear();
        if (windowSettings.containsKey(IMAGE)) {
            path = windowSettings.get(IMAGE);
            loadImage(new File(path));
        }

        int manipulationCount = getWindowSetting(MANIPULATION + SIZE, 0);
        for (int i = 0; i < manipulationCount; i++) {
            for (Signal signal : getReferences()) {
                if (hasSetting(signal.getName(), MANIPULATION + i + TYPE)) {
                    String type = getStringSetting(signal, MANIPULATION + i + TYPE);
                    Manipulation manipulation = ManipulationFactory.createManipulation(type);
                    String mode = getStringSetting(signal, MANIPULATION + i + MODE);
                    manipulation.setMode(mode);
                    int boundCount = getIntSetting(signal, MANIPULATION + i + SIZE);
                    Map<Double, double[]> boundsAndProperties = manipulation.getBoundsAndProperties();
                    boundsAndProperties.clear();
                    for (int j = 0; j < boundCount; j++) {
                        double bound = getDoubleSetting(signal, MANIPULATION + i + BOUND + j + VALUE);
                        double[] properties = new double[manipulation.getPropertyNames().length];
                        for (int k = 0; k < properties.length; k++) {
                            properties[k] = getDoubleSetting(signal, MANIPULATION + i + BOUND + j + PROPERTY + k);
                        }
                        boundsAndProperties.put(bound, properties);
                    }
                    manipulations.put(manipulation, signal);
                }
            }
        }
    }

    public void saveSettings() {
        clearSettings();
        int i = 0;
        addWindowSetting(MANIPULATION + SIZE, String.valueOf(manipulations.size()));
        for (Map.Entry<Manipulation, Signal> manipulationEntry : manipulations.entrySet()) {
            Manipulation manipulation = manipulationEntry.getKey();
            String signal = manipulationEntry.getValue().getName();
            addSetting(signal, MANIPULATION + i + TYPE, manipulation.getName());
            addSetting(signal, MANIPULATION + i + MODE, manipulation.getMode());
            addSetting(signal, MANIPULATION + i + SIZE, String.valueOf(manipulation.getBoundsAndProperties().size()));
            int j = 0;
            for (Map.Entry<Double, double[]> entry : manipulation.getBoundsAndProperties().entrySet()) {
                addSetting(signal, MANIPULATION + i + BOUND + j + VALUE, String.valueOf(entry.getKey()));
                int k = 0;
                for (String setting : manipulation.getPropertyNames()) {
                    addSetting(signal, MANIPULATION + i + BOUND + j + PROPERTY + k, String.valueOf(entry.getValue()[k]));
                    k++;
                }
                j++;
            }
            i++;
        }
    }

    private void clearSettings() {
        removeWindowSetting(MANIPULATION + SIZE);
        Map<String, Map<String, String>> signalSettings = getSettings();
        for (Signal signal : getReferences()) {
            if (signalSettings.containsKey(signal.getName())) {
                signalSettings.remove(signal.getName());
            }
        }
    }

    public void openImageDialog() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.addChoosableFileFilter(new ImageFilter());
        int result = fileChooser.showOpenDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            loadImage(file);
            addWindowSetting(IMAGE, file.getPath());
            rebuild();
        }
    }

    /**
     * Loads a selected image
     *
     * @param path the path of the image
     */
    private void loadImage(File imageFile) {
        try {
            BufferedImage image = ImageIO.read(imageFile);
            manipulationPanel.setImage(image);
            setPreferredSize(new Dimension(image.getWidth() + 6, image.getHeight() + 6));
        } catch (IOException ex) {
            MessagePane.showError("Error opening image " + imageFile.getPath());
        }
    }

    public String getImagePath() {
        return path;
    }

    /**
     * @return the manipulationMap
     */
    public Map<Manipulation, Signal> getManipulations() {
        return manipulations;
    }

    @Override
    public int getGrid() {
        return 1;
    }

    public class IMSignalListener implements SignalListener {

        private final Signal signal;

        /**
         * Initiates a new ManipulationElement
         *
         * @param signal the signal from HANtune that will be given
         */
        public IMSignalListener(Signal signal) {
            this.signal = signal;
        }
        
        @Override
        public void valueUpdate(double value, long timestamp) {
            for (Map.Entry<Manipulation, Signal> e : manipulations.entrySet()) {
                if (e.getKey().isActive() && e.getValue() == signal) {
                    e.getKey().calculateManipulation(value);
                }
            }
            if (timestamp - lastRepaint > 31250) {
                manipulationPanel.repaint();
                lastRepaint = timestamp;
            }
        }

        @Override
        public void stateUpdate() {
            //Not yet necessary for this viewer.
        }
    }
}
