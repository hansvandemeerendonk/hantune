/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.dialogs;

import HANtune.HanTuneDialog;
import datahandling.Signal;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import nl.han.hantune.gui.windows.im.ImageManipulator;
import nl.han.hantune.gui.windows.im.Manipulation;
import nl.han.hantune.gui.windows.im.ManipulationFactory;
import nl.han.hantune.gui.windows.im.MoveManipulation;

/**
 * Class representing a dialog for ranking, pausing and removing manipulations.
 * An image can be added and from here, the manipulation-editing-dialog can be
 * opened.
 *
 * @author Floris Reincke, Michiel Klifman
 */
@SuppressWarnings("serial")
public final class ManageManipulationsDialog extends HanTuneDialog {

    private Box body;
    private final ImageManipulator window;
    private final List<ManipulationRow> rows = new ArrayList<>();

    /**
     * Constructs a ManageManipulationsDialog.
     *
     * @param window The window this dialog should save its manipulations
     */
    public ManageManipulationsDialog(ImageManipulator window) {
        super(true);
        this.window = window;
        setResizable(false);
        createHeader();
        createBody();
        createFooter();
        setTitle("Manage Manipulations");
        setLocationRelativeTo(null);

        JPanel panel = (JPanel) getContentPane();
        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        
        pack();
    }

    /**
     * Create a header for this dialog. Should be called once.
     *
     * @param infoAreaText The text to be displayed in the info area
     */
    private void createHeader() {
        Box header = Box.createVerticalBox();
        
        JTextArea infoArea = new JTextArea("Set manipulations for the chosen image. Note that the order of the manipulations is important; the one on the top will be executed first");
        infoArea.setLineWrap(true);
        infoArea.setWrapStyleWord(true);
        infoArea.setEditable(false);
        infoArea.setRows(2);
        infoArea.setPreferredSize(new Dimension(500, 40));
        infoArea.setBackground(this.getBackground());
        infoArea.setFont(new Font("Tahoma", 0, 11));
        header.add(infoArea);

        Box imageBox = Box.createHorizontalBox();
        
        JLabel imageLabel = new JLabel("Image: ");
        imageBox.add(imageLabel);
        
        JTextField imageTextField = new JTextField(window.getImagePath());
        imageTextField.setPreferredSize(new Dimension(400, 21));
        imageTextField.setMaximumSize(imageTextField.getPreferredSize());
        imageBox.add(imageTextField);
        
        JButton imageBrowseButton = new JButton("Browse");
        imageBrowseButton.addActionListener(e -> imageBrowseButtonPerformed(imageTextField));
        imageBox.add(imageBrowseButton);
        
        imageBox.add(Box.createHorizontalGlue());
        header.add(imageBox);
        
        header.add(Box.createVerticalStrut(15));

        Box addManipulationBox = Box.createHorizontalBox();
        addManipulationBox.add(Box.createHorizontalGlue());
        JButton addManipulationButton = new JButton("Add Manipulation");
        addManipulationButton.addActionListener(e -> createRow(null, null));
        addManipulationBox.add(addManipulationButton);
        header.add(addManipulationBox);

        Box labelPanel = Box.createHorizontalBox();
        labelPanel.add(Box.createHorizontalStrut(107));
        
        JLabel manipulationLabel = new JLabel("Manipulation");
        labelPanel.add(manipulationLabel);
        
        labelPanel.add(Box.createHorizontalStrut(80));
        
        JLabel signalLabel = new JLabel("Signal");
        labelPanel.add(signalLabel);
        labelPanel.add(Box.createHorizontalGlue());
        header.add(labelPanel);
        
        add(header, BorderLayout.NORTH);
    }

    private void createBody() {
        body = Box.createVerticalBox();
        add(body, BorderLayout.CENTER);
        Map<Manipulation, Signal> manipulationMap = window.getManipulations();
        if (manipulationMap.isEmpty()) {
            window.getReferences().forEach(s -> createRow(null, s));
        } else {
            manipulationMap.entrySet().forEach(e -> createRow(e.getKey(), e.getValue()));
        }
        updateRows();
    }

    private void createFooter() {
        Box footer = Box.createVerticalBox();
        footer.add(Box.createVerticalStrut(25));

        Box buttonBox = Box.createHorizontalBox();
        buttonBox.add(Box.createHorizontalGlue());

        JButton okButton = new JButton("OK");
        okButton.addActionListener(e -> saveAndDispose());
        buttonBox.add(okButton);

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> dispose());
        buttonBox.add(cancelButton);
        footer.add(buttonBox);

        add(footer, BorderLayout.SOUTH);
    }

    private ManipulationRow createRow(Manipulation manipulation, Signal signal) {
        ManipulationRow row = new ManipulationRow(manipulation);
        row.signalComboBox.setSelectedItem(signal != null ? signal : window.getReferences().get(0));
        row.manipulationComboBox.setSelectedItem(manipulation != null ? manipulation.getName() : MoveManipulation.NAME);
        row.activateCheckBox.setSelected(manipulation != null ? manipulation.isActive() : true);
        rows.add(row);
        updateRows();
        return row;
    }

    private void updateRows() {
        body.removeAll();
        for (ManipulationRow row : rows) {
            body.add(row);
            row.arrowUp.setEnabled(rows.indexOf(row) != 0);
            row.arrowDown.setEnabled(rows.indexOf(row) < rows.size() - 1);
        }
        pack();
    }

    private void removeRow(ManipulationRow row) {
        if (rows.size() > 1) {
            rows.remove(row);
            updateRows();
        }
    }

    private void moveRowUp(ManipulationRow row) {
        int index = rows.indexOf(row);
        if (index > 0) {
            Collections.swap(rows, index, index - 1);
        }
        updateRows();
    }

    private void moveRowDown(ManipulationRow row) {
        int index = rows.indexOf(row);
        if (index < rows.size() - 1) {
            Collections.swap(rows, index, index + 1);
        }
        updateRows();
    }

    private void saveAndDispose() {
        Map<Manipulation, Signal> manipulationMap = window.getManipulations();
        manipulationMap.clear();
        for (ManipulationRow row : rows) {
            Signal signal = row.getSelectedSignal();
            String manipulationType = row.getSelectedManipulation();
            Manipulation manipulation = row.manipulation != null ? row.manipulation : ManipulationFactory.createManipulation(manipulationType);
            manipulation.setActive(row.activateCheckBox.isSelected());
            manipulationMap.put(manipulation, signal);
        }
        window.saveSettings();
        dispose();
    }

    private void imageBrowseButtonPerformed(JTextField imageTextField) {
        window.openImageDialog();
        imageTextField.setText(window.getImagePath());
        pack();
    }

    /*
     * Class that manages the ManipulationElements linked to the ManageManipulationsDialog
     */
    public final class ManipulationRow extends Box {

        private final Box manipulationRowBox;
        private final JCheckBox activateCheckBox;
        private final JComboBox<Signal> signalComboBox;
        private final JComboBox<String> manipulationComboBox;
        private JButton arrowUp;
        private JButton arrowDown;
        public Manipulation manipulation;

        public ManipulationRow(Manipulation manipulation) {
            super(BoxLayout.Y_AXIS);
            this.manipulation = manipulation;
            
            manipulationRowBox = Box.createHorizontalBox();
            
            activateCheckBox = new JCheckBox();
            activateCheckBox.setSelected(true);
            manipulationRowBox.add(activateCheckBox);

            arrowUp = new JButton("⯅");
            arrowUp.addActionListener(e -> moveRowUp(this));
            manipulationRowBox.add(arrowUp);
            
            arrowDown = new JButton("⯆");
            arrowDown.addActionListener(e -> moveRowDown(this));
            manipulationRowBox.add(arrowDown);

            manipulationRowBox.add(Box.createHorizontalStrut(1));

            manipulationComboBox = new JComboBox<>(ManipulationFactory.getManipulationNames());
            manipulationComboBox.setMaximumSize(new Dimension(250, 21));
            manipulationRowBox.add(manipulationComboBox);

            manipulationRowBox.add(Box.createHorizontalStrut(2));

            List<Signal> signals = window.getReferences();
            Signal[] signalArray = signals.toArray(new Signal[signals.size()]);
            signalComboBox = new JComboBox<>();
            signalComboBox.setModel(new DefaultComboBoxModel<>(signalArray));
            signalComboBox.setMaximumSize(new Dimension(250, 21));
            manipulationRowBox.add(signalComboBox);

            manipulationRowBox.add(Box.createHorizontalStrut(1));

            JButton editManipulationButton = new JButton("Edit");
            editManipulationButton.addActionListener(e -> editManipulation());
            manipulationRowBox.add(editManipulationButton);

            JButton removeManipulationButton = new JButton("Remove");
            removeManipulationButton.addActionListener(e -> removeRow(this));
            manipulationRowBox.add(removeManipulationButton);

            add(manipulationRowBox);
        }

        private Signal getSelectedSignal() {
            return (Signal) signalComboBox.getSelectedItem();
        }

        private String getSelectedManipulation() {
            return (String) manipulationComboBox.getSelectedItem();
        }

        private void editManipulation() {
            Manipulation manipulationToEdit;
            String selectedManipulation = (String) manipulationComboBox.getSelectedItem();
            if (manipulation == null || !manipulation.getName().equals(selectedManipulation)) {
                manipulationToEdit = ManipulationFactory.createManipulation(selectedManipulation);
            } else {
                manipulationToEdit = manipulation;
            }
            EditManipulationDialog dialog = new EditManipulationDialog(manipulationToEdit, getSelectedSignal(), ManageManipulationsDialog.this);

            if (dialog.editManipulation()) {
                manipulation = manipulationToEdit;
            }
        }
    }
}
