# Copyright (c) 2020 [HAN University of Applied Sciences]

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
# OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from nl.han.hantune.scripting.jython import JythonEntryPoint
from nl.han.hantune.scripting import ScriptingManager
from nl.han.hantune.scripting import ScriptedSignal
from nl.han.hantune.scripting import Console
from datahandling import Signal
from datahandling import Parameter
from nl.han.hantune.net.ftp import HANtuneApacheFTP
from nl.han.hantune.net.ssh import SSHManager
from datahandling import SignalListener
from HANtune import Datalog

ht = None
ftp = None
ssh = None

def connect():
	ht.quickConnect(False)

def connectAndCalibrate():
	ht.quickConnect(True)

def disconnect():
	ht.connectProtocol(False)

def getSignal(name):
	currentConfig = ht.currentConfig
	signal= currentConfig.getReferenceByName(name, Signal)
	return signal

def getParameter(name):
	currentConfig = ht.currentConfig
	parameter = currentConfig.getReferenceByName(name, Parameter)
	return parameter

def getCan():
	return ht.currentConfig.getCan()

def sendCan(id, data):
	can = ht.currentConfig.getCan()
	can.sendCanMessage(id, data)

def receiveCan(id = None):
	from collections import namedtuple
	can = ht.currentConfig.getCan()
	peakMessage = can.read()
	if (peakMessage != None and (id == None or id == peakMessage.ID)):
		data = processCanData(peakMessage.data, peakMessage.length)
		Message = namedtuple("Message", "id data dlc")
		return Message(peakMessage.ID, data, peakMessage.length)
	else:
		return None

def processCanData(rawData, dlc):
	data= []
	i = 0
	while i < dlc:
    		data.append(rawData[i] & 0xFF)
    		i += 1			
	return data

def isConnected():
	can = ht.currentConfig.getCan()
	return can.isConnected()

def printHex(id, data):
	hexData = []
	i = 0
	while i < len(data):
    		hexData.append(hex(data[i] & 0xFF))
    		i += 1			
	print "id: " + hex(id) + " data: " + str(hexData)

def printDec(id, data):
	decData = []
	i = 0
	while i < len(data):
    		decData.append(data[i] & 0xFF)
    		i += 1			
	print "id: " + str(id) + " data: " + str(decData)

def connectToFTP(ip, port, username, password):
	global ftp
	ftp = HANtuneApacheFTP();
	return ftp.createConnection(ip, port, username, password)

def downloadFile(remotePath, localPath):
	return ftp.downloadFile(remotePath, localPath)

def uploadFile(localPath, remotePath):
	return ftp.uploadFile(remotePath, localPath)

def createSignal(name):
	signal = ScriptedSignal(name)
	manager = ScriptingManager.getInstance()
	manager.addScriptedSignal(signal)
	return signal;

def removeSignal(signal):
	manager = ScriptingManager.getInstance()
	manager.removeScriptedSignal(signal)

def removeAllSignals():
	manager = ScriptingManager.getInstance()
	manager.removeAllScriptedSignals()

def createTrigger(signal, triggerFunction):
	signalListener = ScriptSignalListener()
	signalListener.setFunction(triggerFunction)
	signal.addListener(signalListener)

def logSignal(name, value):
	Datalog.getInstance().addValue(name, value)

def updateLayout():
	manager = ht.currentConfig.getHANtuneManager()
	manager.updateAllWindows()

def clear():
	Console.clearConsole()

class JythonEntryPoint(JythonEntryPoint):

	def setHANtune(self, HANtune):
		global ht
		ht = HANtune

class ScriptSignalListener(SignalListener):

	def valueUpdate(self, value, timestamp):
		self.function()
		
	def stateUpdate(self):
		print 'signal state changed'

	def setFunction(self, function):
		self.function = function