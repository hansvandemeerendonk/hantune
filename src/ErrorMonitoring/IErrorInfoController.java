/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ErrorMonitoring;

/**
 * Error info controller interface. Contains methods for reading and clearing
 * errors (diagnostic trouble codes).
 *
 * @author Frank Voorburg
 */
public interface IErrorInfoController {
    /**
     * Method for submitting a request to clear the errors on a target.
     * 
     * @param active True if the active (RAM) errors should be cleared, false 
     *               otherwise.
     * @param stored True if the stored (EEPROM) errors should be cleared, false
     *               otherwise.
     */
    public void clearErrorInfo(boolean active, boolean stored);

    /**
     * Method for submitting a request to read the active and stored errors from
     * a target.
     * 
     * @param callback Callback that should be called when new error info was
     *                 read from the target.
     */
    public void readErrorInfo(IErrorInfoProcessor callback);
}
