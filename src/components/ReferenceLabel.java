/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components;

import HANtune.ReferenceWindow;
import datahandling.CurrentConfig;
import datahandling.Reference;
import java.awt.event.MouseEvent;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;

/**
 *
 * @author Michiel Klifman
 * @param <T>
 */
@SuppressWarnings("serial")
public class ReferenceLabel<T extends Reference> extends JLabel {
    
    private final T reference;
    private ReferenceWindow<T> window;
    
    /**
     * Constructs a JLabel that displays the name of the provided reference. The
     * label text will change color depending on the state of the reference.
     * @param reference - the reference to be displayed.
     */
    public ReferenceLabel(T reference) {
        this.reference = reference;
        this.setText(reference.getName());
        this.setToolTipText("Source: " + reference.getSource());
        reference.addStateListener(() -> stateUpdate());
        stateUpdate();
    }
    
    /**
     * Constructs a JLabel that displays the name of the provided reference. The
     * label text will change color depending on the state of the reference. The
     * provided ReferenceWindow grants the label control over the reference in 
     * the window through a context menu.
     * @param reference
     * @param window 
     */
    public ReferenceLabel(T reference, ReferenceWindow<T> window) {
        this(reference);
        this.window = window;
        addMouseListener();
    }
    
    private void addMouseListener() {
        this.addMouseListener(new MouseInputAdapter() {
            @Override
            public void mousePressed(MouseEvent event) {
                if (SwingUtilities.isRightMouseButton(event)) {
                    if (!CurrentConfig.getInstance().isServiceToolMode()) {
                        showMenu(event);
                    }
                }
            }
            
            @Override
            public void mouseEntered(MouseEvent event) {
                ReferenceLabel.this.setText("<HTML><U>" + reference.getName() + "</U></HTML>");
            }

            @Override
            public void mouseExited(MouseEvent event) {
                ReferenceLabel.this.setText(reference.getName());
            }
        });
    }
    
    private void showMenu(MouseEvent event) {
        JPopupMenu menu = new JPopupMenu();
        String type = reference.getClass().getGenericInterfaces()[0].getTypeName();
        String simpleType = type.substring(type.lastIndexOf('.') + 1);
        int index = window.getReferences().indexOf(reference);
        
        JMenuItem removeSignalMenuItem = new JMenuItem("Remove " + simpleType);
        removeSignalMenuItem.addActionListener(e -> window.removeReference(reference));
        menu.add(removeSignalMenuItem);
        
        JMenuItem moveReferenceUpMenuItem = new JMenuItem("Move " + simpleType + " up");
        moveReferenceUpMenuItem.addActionListener(e -> window.moveReferenceUp(reference));
        moveReferenceUpMenuItem.setEnabled(index > 0);
        menu.add(moveReferenceUpMenuItem);

        JMenuItem moveReferenceDownMenuItem = new JMenuItem("Move " + simpleType + " down");
        moveReferenceDownMenuItem.addActionListener(e -> window.moveReferenceDown(reference));
        moveReferenceDownMenuItem.setEnabled(index < window.getReferences().size() - 1);
        menu.add(moveReferenceDownMenuItem);
        
        menu.show(event.getComponent(), event.getX(), event.getY());
    }

    private void stateUpdate() {
        if (reference.isActive()) {
            this.setForeground(ReferenceWindow.ACTIVE_TEXT_COLOR);
        } else {
            this.setForeground(ReferenceWindow.INACTIVE_TEXT_COLOR);
        }
        JInternalFrame frame = (JInternalFrame) SwingUtilities.getAncestorOfClass(JInternalFrame.class, this);
        if (frame != null) {
            frame.repaint();
        }
    } 
}
