/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel.asap2;

import ASAP2.ASAP2Data;
import ASAP2.Asap2Listener;
import HANtune.HANtune;
import components.sidepanel.SidePanel;
import components.sidepanel.SubPanel;
import components.sidepanel.asap2.Asap2Tree.TreeType;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpringLayout;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class Asap2Panel extends SidePanel implements Asap2Listener {
    
    private Asap2Tree tree;    
    private DaqListTable daqListTable;
    private DaqListInfoTable daqListInfoTable;
    private JSpinner spinner;
    private JComboBox<TreeType> comboBox;
    private SpinnerNumberModel functionsSpinnerModel;
    private SpinnerNumberModel groupsSpinnerModel;
    private ASAP2Data asap2;

    @Override
    public void asap2Update(ASAP2Data asap2) {
        this.asap2 = asap2;
        updateSpinnerModels(asap2);
        updateSpinner();
        updateComboBox();
        tree.buildTree(asap2);
    }
    
    
    
    public Asap2Panel (String title) {
        super(title);
        SubPanel asap2TreePanel = createTreePanel();
        SubPanel daqListInfoPanel = createDaqListInfoPanel();
        SubPanel daqListPanel = createDaqListPanel();        
        
        layout.putConstraint(SpringLayout.NORTH, asap2TreePanel, 0, SpringLayout.NORTH, contentPanel);
        layout.putConstraint(SpringLayout.WEST, asap2TreePanel, 0, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.EAST, asap2TreePanel, 0, SpringLayout.EAST, contentPanel);
        layout.putConstraint(SpringLayout.SOUTH, asap2TreePanel, 0, SpringLayout.NORTH, daqListInfoPanel);
        
        layout.putConstraint(SpringLayout.WEST, daqListInfoPanel, 0, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.EAST, daqListInfoPanel, 0, SpringLayout.EAST, contentPanel);
        layout.putConstraint(SpringLayout.SOUTH, daqListInfoPanel, 0, SpringLayout.NORTH, daqListPanel);
        
        layout.putConstraint(SpringLayout.WEST, daqListPanel, 0, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.EAST, daqListPanel, 0, SpringLayout.EAST, contentPanel);
        layout.putConstraint(SpringLayout.SOUTH, daqListPanel, 0, SpringLayout.SOUTH, contentPanel);
        updateSpinner();
        updateComboBox();
    }
    
    private SubPanel createTreePanel() {
        
        comboBox = new JComboBox<>(TreeType.values());
        comboBox.setMaximumSize(new Dimension(Integer.MAX_VALUE, comboBox.getPreferredSize().height) );
        comboBox.addActionListener((ActionEvent e) -> handleComboBoxAction(e));
        
        spinner = new JSpinner();
        spinner.setMaximumSize(spinner.getPreferredSize());
        spinner.addChangeListener((ChangeEvent e) -> handleSpinnerAction(e));
        
        JLabel spinnerLabel = new JLabel("Depth");
        Border innerBorder = BorderFactory.createEmptyBorder(2, 2, 2, 2);
        Border outerBorder = BorderFactory.createMatteBorder(1, 1, 1, 0, BORDER_COLOR);
        Border compoundBorder = BorderFactory.createCompoundBorder(outerBorder, innerBorder);
        spinnerLabel.setBorder(compoundBorder);
        
        Box wrapper = Box.createHorizontalBox();
        wrapper.add(spinnerLabel);
        wrapper.add(spinner);
        wrapper.add(comboBox);
        
        tree = new Asap2Tree();
        JScrollPane asap2TreeScrollPane = new JScrollPane();
        asap2TreeScrollPane.setMinimumSize(new Dimension(0,0));
        asap2TreeScrollPane.setViewportView(tree);
        Border border = BorderFactory.createMatteBorder(0, 1, 1, 1, BORDER_COLOR);
        asap2TreeScrollPane.setBorder(border);
        
        SubPanel asap2TreePanel = new SubPanel();
        asap2TreePanel.add(wrapper);
        asap2TreePanel.setBorder(new EmptyBorder(0, 0, 3, 0));
        asap2TreePanel.add(asap2TreeScrollPane);
        asap2TreePanel.addSearchField(tree, "Search ASAP2 File");
        asap2TreePanel.setMinimumSize(new Dimension(240, 46));
        this.add(asap2TreePanel);
        return asap2TreePanel;
    }
    
    private SubPanel createDaqListInfoPanel() {
        
        daqListInfoTable = new DaqListInfoTable();
        JScrollPane daqListInfoScrollPane = new JScrollPane();
        daqListInfoScrollPane.setMinimumSize(new Dimension(0,0));
        daqListInfoScrollPane.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));
        daqListInfoScrollPane.setViewportView(daqListInfoTable);
        HANtune.getInstance().addDaqListListener(daqListInfoTable);
        
        SubPanel daqListInfoPanel = new SubPanel("DAQ List Info");
        daqListInfoPanel.add(daqListInfoScrollPane);        
        daqListInfoPanel.setPreferredSize(new Dimension(240, 92));
        daqListInfoPanel.setBorder(new EmptyBorder(0, 0, 3, 0));
        daqListInfoPanel.setMinimumSize(new Dimension(240, 18));
        this.add(daqListInfoPanel);
        return daqListInfoPanel;
    }
    
    private SubPanel createDaqListPanel() {
        SubPanel daqListPanel = new SubPanel("DAQ Lists");
        daqListTable = new DaqListTable();
        JScrollPane daqListScrollPane = new JScrollPane();
        daqListScrollPane.setMinimumSize(new Dimension(0,1));
        daqListScrollPane.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));
        daqListScrollPane.setViewportView(daqListTable);
        daqListPanel.add(daqListScrollPane);
        
        daqListPanel.setPreferredSize(new Dimension(240, 275));
        daqListPanel.setMinimumSize(new Dimension(240, 15));
        this.add(daqListPanel);
        
        HANtune.getInstance().addDaqListListener(daqListTable);
        daqListPanel.addSearchField(daqListTable, "Search DAQ Lists");
        return daqListPanel;
    }
    
    private void handleComboBoxAction(ActionEvent event) {
        TreeType type = (TreeType) comboBox.getSelectedItem();
        updateSpinner();
        tree.setType(type);
        tree.buildTree(asap2);        
    }
    
    private void handleSpinnerAction(ChangeEvent e) {
        
        tree.setDepth((int) spinner.getValue());
        tree.buildTree(asap2);
    }
    
    private void updateSpinnerModels(ASAP2Data asap2) {
        if (asap2 != null) {
            int functionDepth = asap2.getFunctionRoot().getFunctionDepth();
            functionsSpinnerModel = new SpinnerNumberModel(functionDepth, 1, functionDepth, 1);
            int groupDepth = asap2.getGroupRoot().getGroupDepth();
            groupsSpinnerModel = new SpinnerNumberModel(groupDepth, 1, groupDepth, 1);
        }
    }
    
    private void updateSpinner() {
        TreeType type = (TreeType) comboBox.getSelectedItem();
        if (type == TreeType.ALL || asap2 == null) {
            spinner.setModel(new SpinnerNumberModel(0,0,0,0));
            spinner.setEnabled(false);
        } else if (type == TreeType.GROUPS) {
            spinner.setModel(groupsSpinnerModel);
            spinner.setEnabled(true);
        } else if (type == TreeType.FUNCTIONS) {
            spinner.setModel(functionsSpinnerModel);
            spinner.setEnabled(true);
        }
        tree.setDepth((int) spinner.getValue());
    }
    

    private void updateComboBox() {
        if (asap2 != null) {
            comboBox.setEnabled(true);
            TreeType.FUNCTIONS.setValue("Functions");
            TreeType.GROUPS.setValue("Groups");

            if (asap2.getFunctionRoot().getSubFunctions().isEmpty()) {
                comboBox.removeItem(TreeType.FUNCTIONS);
                TreeType.GROUPS.setValue("Show Subsystems");
            } else if (((DefaultComboBoxModel<TreeType>)comboBox.getModel())
                .getIndexOf(TreeType.FUNCTIONS) == -1) {
                comboBox.addItem(TreeType.FUNCTIONS);
            }

            if (asap2.getGroupRoot().getSubGroups().isEmpty()) {
                comboBox.removeItem(TreeType.GROUPS);
                TreeType.FUNCTIONS.setValue("Show Subsystems");
            } else if (((DefaultComboBoxModel<TreeType>)comboBox.getModel())
                .getIndexOf(TreeType.GROUPS) == -1) {
                comboBox.addItem(TreeType.GROUPS);
            }
        } else {
            comboBox.setEnabled(false);
            comboBox.setSelectedIndex(0);
        }
    }
}
