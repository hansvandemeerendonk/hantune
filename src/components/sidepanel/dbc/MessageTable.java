/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel.dbc;

import components.sidepanel.AbstractTable;
import can.CanMessage;
import can.CanSignal;
import can.DbcObject;
import components.sidepanel.AbstractTreeListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author Michiel
 */
@SuppressWarnings("serial")
public class MessageTable extends AbstractTable implements AbstractTreeListener {

    private DefaultTableModel tableModel;    

    public MessageTable() {
        
        createTable();
        
    }

    private void createTable() {
        tableModel = new DefaultTableModel();
        tableModel.setColumnIdentifiers(new Object[]{"Name", "ID", "ID Type", "DLC", "Sending Node", "Comment"});
       
        this.setModel(tableModel);
        this.setRowSorter(new TableRowSorter<>(tableModel));
        
        //headerManager.addHiddenColumn(columnModel.getColumn(1));
        headerManager.addHiddenColumn(columnModel.getColumn(2));
        headerManager.addHiddenColumn(columnModel.getColumn(3));
        headerManager.addHiddenColumn(columnModel.getColumn(4));
        headerManager.addTableHeaderMenu();
        headerManager.addHexOptionToMenu(columnModel.getColumn(1));
    }

    private void fillTable(List<CanMessage> messages) {
        for (CanMessage message : messages) {
            tableModel.addRow(new Object[]{message.getName(), message.getId(), message.getIdType(), message.getDLC(), message.getSendingNode(), message.getComment()});
        }
    }

    @Override
    public void treeUpdate(DefaultMutableTreeNode node) {
        tableModel.setRowCount(0);
        if (node != null) {
            if (node.getUserObject() instanceof DbcObject) {
                fillTable(getMessagesFromNode(node));
                scrollToAndSelectRow(0);
            } else if (node.getUserObject() instanceof CanMessage) {
                CanMessage message = (CanMessage) node.getUserObject();
                List<CanMessage> messages = getMessagesFromNode((DefaultMutableTreeNode) node.getParent());
                fillTable(messages);
                scrollToAndSelectRow(messages.indexOf(message));
            } else if (node.getUserObject() instanceof CanSignal) {
                CanSignal signal = (CanSignal) node.getUserObject();
                List<CanMessage> messages = getMessagesFromNode((DefaultMutableTreeNode) node.getParent().getParent());
                fillTable(messages);
                scrollToAndSelectRow(messages.indexOf(signal.getParent()));
            }
        }
    }
    
    private List<CanMessage> getMessagesFromNode(DefaultMutableTreeNode node) {
        List<CanMessage> messages = new ArrayList<>();
        for (int i = 0; i < node.getChildCount(); i++) {
            DefaultMutableTreeNode messageNode = (DefaultMutableTreeNode) node.getChildAt(i);
            messages.add((CanMessage) messageNode.getUserObject());
        }
        return messages;
    }
    
    /**
     * Editing for this table is disabled. Remove this function
     * or have it return true, to make it editable.
     * @param row the row of the cell
     * @param column the column of the cell
     * @return 
     */
    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}