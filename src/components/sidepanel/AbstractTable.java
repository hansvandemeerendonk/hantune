/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel;

import java.awt.Component;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.Timer;
import javax.swing.ToolTipManager;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import components.TableHeaderManager;

/**
 *
 * @author Michiel
 */
@SuppressWarnings("serial")
public abstract class AbstractTable extends JTable implements Searchable {

    public TableHeaderManager headerManager;
    private TableRowSorter<TableModel> sorter;
    private final List<Integer> ignoredColumns = new ArrayList<>();
    private Timer tooltipTimer;
    private MouseEvent mouseEvent;
    boolean isExpanded = true;

    public AbstractTable() {
        headerManager = new TableHeaderManager(this.getTableHeader());
        setDynamicToolTip();
    }    

    public void setComponentListener(Component component) {
        component.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                toggleColumns(component);
            }
        });
    }

    public void toggleColumns(Component component) {
        if (component.getWidth() > 246 && !isExpanded) {
            expandTable();
        } else if (component.getWidth() <= 246 && isExpanded) {
            collapseTable();
        }
    }

    private void expandTable() {
        this.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        headerManager.showColumns();
        headerManager.setMenuEnabled(false);
        isExpanded = true;
    }

    private void collapseTable() {
        this.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        headerManager.hideColumns();
        headerManager.setMenuEnabled(true);
        isExpanded = false;
    }

    public void scrollToAndSelectRow(int index) {
        index = this.convertRowIndexToView(index);
        this.setRowSelectionInterval(index, index);
        this.scrollRectToVisible(new Rectangle(this.getCellRect(index, 0, false)));
    }

    private void setDynamicToolTip() {
        tooltipTimer = new Timer(1000, (ActionEvent e) -> showTooltip());
        tooltipTimer.setRepeats(false);
        this.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent event) {
                mouseEvent = event;
                startTimer();
            }
        });
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent event) {
                tooltipTimer.stop();
                ToolTipManager.sharedInstance().setEnabled(true);
            }
        });
    }
    
    private void startTimer() {
        if (tooltipTimer.isRunning()) {
            tooltipTimer.restart();
            ToolTipManager.sharedInstance().setEnabled(false);
        } else {
            tooltipTimer.start();
        }        
    }

    private void showTooltip() {
        int rowIndex = this.rowAtPoint(mouseEvent.getPoint());
        int colIndex = this.columnAtPoint(mouseEvent.getPoint());
        if (this.getValueAt(rowIndex, colIndex) != null) {
            ToolTipManager.sharedInstance().setEnabled(true);
            this.setToolTipText(this.getValueAt(rowIndex, colIndex).toString());            
            ToolTipManager.sharedInstance().mouseMoved(mouseEvent);
        }
    }
    
    public void addSorter(TableRowSorter<TableModel> sorter) {
        this.sorter = sorter;
        this.setAutoCreateRowSorter(true);
        super.setRowSorter(sorter);
    }
    
    @Override
    public void find(String textToFind) {
        RowFilter<TableModel, Object> rf;
        try {
            rf = RowFilter.regexFilter("(?i)" + textToFind, getFilterableIndices());
        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        }
        sorter.setRowFilter(rf);
    }
    
    public int[] getFilterableIndices() {
        List<Integer> indices = new ArrayList<>();
        for (int i = 0; i < this.getColumnCount(); i++) {
            if (!ignoredColumns.contains(i)) {
                indices.add(i);
            }
        }
        return indices.stream().mapToInt(i -> i).toArray();
    }
    
    public void ignoreColumnWhileFiltering(int column) {
        if (!ignoredColumns.contains(column)) {
            ignoredColumns.add(column);
        }
    }
}
