/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components;

import java.awt.Cursor;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.event.MouseInputAdapter;

/**
 *
 * @author Michiel
 */
@SuppressWarnings("serial")
public class CustomButton extends JLabel {

    private JLabel button;
    private ImageIcon defaultImage;
    private ImageIcon hoverImage;

    public CustomButton(String path) {
        defaultImage = createButtonImage(path);
        this.setIcon(defaultImage);
    }

    public JLabel createButton(String path) {
        button = new JLabel();
        defaultImage = createButtonImage(path);
        button.setIcon(defaultImage);
        return button;
    }

    private ImageIcon createButtonImage(String path) {
        URL url = getClass().getResource(path);
        if (url != null) {
            return new ImageIcon(url);
        } else {
            return null;
        }
    }

    public void setMouseOverImage(String path) {
        hoverImage = createButtonImage(path);
        button = this;
        button.addMouseListener(new MouseInputAdapter() {
            @Override
            public void mouseEntered(MouseEvent event) {
                button.setIcon(hoverImage);
            }

            @Override
            public void mouseExited(MouseEvent event) {
                button.setIcon(defaultImage);
            }
        });
    }

    public void setButtonActionPerformed(ActionListener listener) {
        this.addMouseListener(new MouseInputAdapter() {
            @Override
            public void mousePressed(MouseEvent event) {
                listener.actionPerformed(null);
            }
            
            @Override
            public void mouseEntered(MouseEvent event) {
                CustomButton.this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        });
    }

    public void setImage(String path) {
        defaultImage = createButtonImage(path);
        this.setIcon(defaultImage);
    }
}
