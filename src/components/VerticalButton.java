/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class VerticalButton extends JButton {
    
    private static final long serialVersionUID = 7526471155622776189L;

    private String caption = "";
    private boolean clockwise = false;

    public VerticalButton (String caption, boolean clockwise) {
        this.caption = caption;
        this.clockwise = clockwise;
        this.setFocusable(false);
        setActive(false);
    }

    public void setActive(boolean active){
        // configure font
        if(active){
            setFont(new java.awt.Font("Tahoma", 1, 11));
        } else {
            setFont(new java.awt.Font("Tahoma", 0, 11));
        }

        Font f = getFont();
        FontMetrics fm = getFontMetrics (f);
        int captionHeight = fm.getHeight ();
        int captionWidth = fm.stringWidth (caption);

        // create buffer
        BufferedImage bi = new BufferedImage (captionHeight + 4,
                134, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = (Graphics2D) bi.getGraphics ();

        // configure drawing area
        g.setColor (new Color (0, 0, 0, 0)); // transparent
        g.fillRect (0, 0, bi.getWidth (), bi.getHeight ());
        g.setColor (getForeground());
        g.setFont (f);
        g.setRenderingHint (RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        // rotate and draw text
        if (clockwise) {
            g.rotate (Math.PI / 2);
        } else {
            g.rotate (- Math.PI / 2);
            g.translate (-bi.getHeight (), bi.getWidth ());
        }
        g.drawString (caption, (134-captionWidth)/2, -6);

        // finalize
        Icon icon = new ImageIcon (bi);
        setIcon (icon);
        setMargin (new Insets (15, 2, 15, 2));
        setActionCommand (caption);
    }

}