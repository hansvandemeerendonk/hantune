/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.BorderFactory;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.plaf.basic.BasicProgressBarUI;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import ASAP2.ASAP2Measurement;
import DAQList.DaqListListener;
import ErrorLogger.AppendToLogfile;
import ErrorLogger.ErrorLogRestrictor;
import ErrorLogger.defaultExceptionHandler;
import ErrorMonitoring.ErrorMonitor;
import ErrorMonitoring.ErrorObject;
import ErrorMonitoring.IErrorObserver;
import HANtune.CustomFileChooser.FileType;
import HANtune.HANtuneWindowFactory.HANtuneWindowType;
import HANtune.actions.CreateCalibration;
import HANtune.actions.ExportCalibration;
import HANtune.actions.ExportFlashIHexAction;
import HANtune.actions.ExportFlashSRecAction;
import HANtune.actions.ImportCalibration;
import HANtune.actions.DaqlistAction;
import HANtune.actions.LogReaderAction;
import HANtune.actions.ProjectTreeRenderer;
import HANtune.guiComponents.menus.HANtuneMainMenuBar;
import XCP.XCP;
import XCP.XCPDaqInfo;
import XCP.XCPSettings;
import XCP.XCPonCANBasic;
import XCP.XCPonEthernet;
import XCP.XCPonUART;
import can.CanConnectionHandler;
import components.VerticalButton;
import components.sidepanel.SidePanel;
import components.sidepanel.SidePanelManager;
import components.sidepanel.TreeTransferHandler;
import components.sidepanel.asap2.Asap2Panel;
import components.sidepanel.dbc.DbcPanel;
import datahandling.CalibrationHandler;
import datahandling.CurrentConfig;
import haNtuneHML.Calibration;
import haNtuneHML.Layout;
import nl.han.hantune.config.ApplicationProperties;
import nl.han.hantune.config.VersionInfo;
import nl.han.hantune.gui.sidepanel.ConsolePanel;
import nl.han.hantune.gui.sidepanel.ScriptedElementsPanel;
import nl.han.hantune.scripting.Console;
import nl.han.hantune.scripting.Script;
import nl.han.hantune.scripting.ScriptingManager;
import nl.han.hantune.scripting.jython.JythonScript;
import util.MessagePane;
import util.ResettableTimer;
import util.Util;

import static HANtune.actions.DaqlistAction.LOAD_DAQ_LIST;
import static HANtune.actions.DaqlistAction.UNLOAD_DAQ_LIST;
import static datahandling.CurrentConfig.LAST_FLASH_IHEX_BASE_FILE;
import static datahandling.CurrentConfig.LAST_FLASH_SREC_BASE_FILE;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;

/**
 *
 * @author Aart-Jan and Roel van den Boom
 */
public class HANtune extends JFrame implements IErrorObserver {

    ScheduledExecutorService scheduler = new ScheduledThreadPoolExecutor(1);
    
    public static String applicationName = VersionInfo.getInstance().getFullVersionText();
    // error monitor supported controllers
    public static final ArrayList<String> errorMonitorSupport
            = new ArrayList<>(Arrays.asList("XCP_RC30", "RC28-14", "RC12-10", "STM32"));
    private static final long serialVersionUID = 1L;
    public static final int BUSLOAD_WARNING_PERCENTAGE = 70; // warning when more than 70% busload (orange)
    public static final int BUSLOAD_CRITICAL_PERCENTAGE = 90; // critical warning when more than 90% busload
    // (red)
    public static final int CONNECTION_TIMEOUT = 2000; // 2 second timeout on missing packets
    public CurrentConfig currentConfig = CurrentConfig.getInstance();
    private HANtuneProject hanTuneProject = HANtuneProject.getInstance();

    private ErrorMonitor errorMonitor;
    private CalibrationHandler calibrationHandler;
    private Preferences userPrefs = Preferences.userNodeForPackage(UserPreferences.class);
    public static final int DAQ_MAX_DEFAULT = 3; // default amount of allowed
    // DAQ lists
    private int activeDaqlistIdCnt = 0;
    private int activeAsap2Id = -1;
    private int activeLayoutId = -1;
    private int activeCalibrationId = -1;

    private HANtuneMainMenuBar mainMenuBar = null;

    // windows & managers
    private CommunicationSettings communicationSettings = null;

    private ResettableTimer daqListReceiveTimoutTimer;
    private Timer connectionTimoutTimer;

    // Sidepanels
    private SidePanel projectPanel;
    private Asap2Panel asap2Panel;
    private DbcPanel dbcPanel;
    private ScriptedElementsPanel scriptedElementsPanel;
    private VerticalButton scriptedElementsButton;
    private ConsolePanel consolePanel;

    // Vertical buttons sidepanels
    private javax.swing.JButton projectButton;
    private javax.swing.JButton asap2Button;
    private javax.swing.JButton dbcButton;

    // Map containing all sidepanels plus their corresponding vertical buttons
    private Map<VerticalButton, SidePanel> buttonSidePanelMap = new HashMap<>();

    private ProjectTree projectTree;
    private JScrollPane projectPanelScrollPane;
    // DAQ lists
    public List<DAQList.DAQList> daqLists = new ArrayList<>();

    // rotator status symbol
    private static int rotatorId = 0;
    private static final String[] ROTATOR_CHARS = {"-", "\\", "|", "/"};
    private static final int ROTATOR_CHARS_LEN = ROTATOR_CHARS.length;
    private long lastUpdateTimestamp = 0;
    private long updatePeriodAvg = 0;
    // status colors
    public static Color STATUSCOLOR_ON = new Color(0, 180, 0);
    public static Color STATUSCOLOR_ALERT = new Color(220, 0, 0);
    public static Color STATUSCOLOR_OFF = new Color(150, 150, 150);
    public static Color STATUSCOLOR_WARNING = new Color(255, 153, 51);
    public static ArrayList<String> missingElements = new ArrayList<>();
    public static Calendar licenceStartDate = null;
    public static Calendar licenceExpiryDate = null;
    private final static HANtune instance = new HANtune();
    private static String[] arguments;
    SidePanelManager sidePanelManager = SidePanelManager.getInstance();
    private final List<DaqListListener> daqListListeners = new ArrayList<>();
    private boolean snapLayoutToSidePanel = true;

    public ConnectionDialog connectionDialog;

    private HANtune() {
        Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
        // use System LookAndFeel
        String nativeLF = UIManager.getSystemLookAndFeelClassName();
        try {
            UIManager.setLookAndFeel(nativeLF);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
                | UnsupportedLookAndFeelException ex) {
            AppendToLogfile.appendError(Thread.currentThread(), ex);
            Logger.getLogger(HANtune.class.getName()).log(Level.SEVERE, null, ex);
        }

        /* ONLY UNCOMMENT WHEN DEBUGGING EULA!
         // Temporary debug method to reset the Eula
         if(JOptionPane.showConfirmDialog(this, "Do you want to reset the EULA?", "EULA reset?", 0, 1) == 0) {
         userPrefs.putBoolean("eulaAccept", false);
         }
         */
        // Check if user has accepted EULA.
        if (!userPrefs.getBoolean("eulaAccept", false)) {
            EulaDialog eula = new EulaDialog(this, true, true);
            eula.setVisible(true);
        }
        if (!userPrefs.getBoolean("eulaAccept", false)) {
            System.exit(0);
        }
        // instantiate error monitor
        errorMonitor = new ErrorMonitor();
        calibrationHandler = CalibrationHandler.getInstance();
    }

    public static HANtune getInstance() {
        return instance;
    }

    public JTabbedPane getTabbedPane() {
        return tabbedPane;
    }

    public JPopupMenu getTabPopupMenu() {
        return tabPopupMenu;
    }

    public JLabel getLabelDataLog() {
        return labelDataLog;
    }

    public CommunicationSettings getCommunicationSettings() {
        return communicationSettings;
    }

    public void setCommunicationSettings(CommunicationSettings communicationSettings) {
        this.communicationSettings = communicationSettings;
    }

    public HANtuneMainMenuBar getMainMenuBar() {
        return mainMenuBar;
    }

    public HANtuneProject getHanTuneProject() {
        return hanTuneProject;
    }

    /**
     * Shows question dialog
     *
     * @param title Question to be shown
     * @param value Default value to be shown
     */
    public static String showQuestion(String title, String msg, String value) {
        return (String) JOptionPane.showInputDialog(HANtune.getInstance(), msg, title,
                JOptionPane.QUESTION_MESSAGE, null, null, value);
    }

    /**
     * Shows confirmation dialog
     *
     * @param msg Confirmation message to be shown
     */
    public static int showConfirm(String msg) {
        return JOptionPane.showConfirmDialog(HANtune.getInstance(), msg, applicationName,
                JOptionPane.YES_NO_OPTION);
    }

    /**
     * Get current_layout
     *
     * @return current_layout
     */
    public int getActiveLayoutId() {
        return activeLayoutId;
    }

    public void setActiveLayoutId(int activeLayoutId) {
        this.activeLayoutId = activeLayoutId;
        mainMenuBar.enableLayoutItems(activeLayoutId != -1);

        updateProjectTree();
    }

    /**
     * @return the number of active DAQ lists
     */
    public int getActiveDaqlistIdCnt() {
        return activeDaqlistIdCnt;
    }

    /**
     * Resets activeDaqlistIdCnt
     */
    public void resetActiveDaqlistIdCnt() {
        int relId = 0;

        // loop through DAQ list
        for (int i = 0; i < daqLists.size(); i++) {
            if (daqLists.get(i).isActive()) {
                // set relative identifier
                daqLists.get(i).setRelativeId(relId);

                // increment counter
                relId++;
            } else {
                // reset relative identifier
                daqLists.get(i).setRelativeId(-1);
            }
        }

        // store value
        activeDaqlistIdCnt = relId;
    }

    /**
     * Returns the array index for the specified relative identifier
     */
    public int getDaqListId(int relativeId) {
        for (int i = 0; i < daqLists.size(); i++) {
            if (daqLists.get(i).getRelativeId() == relativeId) {
                return i;
            }
        }

        return -1;
    }

    public void addDaqListListener(DaqListListener listener) {
        if (!daqListListeners.contains(listener)) {
            daqListListeners.add(listener);
        }
    }

    public void removeDaqListListener(DaqListListener listener) {
        daqListListeners.remove(listener);
    }

    public void updateDaqListListeners() {
        daqListListeners.forEach(listener -> listener.daqListUpdate());
    }

    /**
     * Get current_asap2
     *
     * @return current_asap2
     */
    public int getActiveAsap2Id() {
        return activeAsap2Id;
    }

    public void setActiveAsap2Id(int activeAsap2Id) {

        if (this.activeAsap2Id != activeAsap2Id) {
            if (activeAsap2Id != -1) {

                // reconnect if necessary
                if (isConnected()) {
                    connectProtocol(false);
                    connectProtocol(true);
                }

                enableExport(true);

            } else {
                enableExport(false);
            }
        }

        this.activeAsap2Id = activeAsap2Id;
        if (activeAsap2Id == -1) {
            mASAP2Load.setText("Load File");
        } else {
            mASAP2Load.setText("Unload File");
        }
        updateProjectTree();
    }

    /**
     * Resets the pointers of the currently in used asap2 file, DAQlist, layout
     * and calibration.
     */
    public void clearActiveIds() {
        activeAsap2Id = -1;
        activeLayoutId = -1;
        activeCalibrationId = -1;
        currentConfig.getHANtuneManager().storeAndCloseDaqLists();
        updateProjectTree();
    }

    /**
     * Initializes the HANtune functionality
     */
    public void initHANtune() {

        initComponents();
        initCustomComponents();
        addFullScreenKeyHandler();

        UIManager.put("ToolTip.background", Color.WHITE);
        ToolTipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);

        // set program window icon
        Image img = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/HANlogo.png"));
        setIconImage(img);

        // TODO sort-out
        // init statusbar
        initStatusbar();

        // get last directory from userPrefs
        currentConfig.setPreviousPath(FileType.ASAP2_FILE,
                (new File(userPrefs.get("lastAsap2Path", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(FileType.DBC_FILE,
                (new File(userPrefs.get("lastDBCPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(FileType.LOG_FILE,
                (new File(userPrefs.get("lastLogPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(FileType.MAP_FILE,
                (new File(userPrefs.get("lastMapPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(FileType.MATLAB_FILE,
                (new File(userPrefs.get("lastMatlabPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(FileType.DCM_FILE,
                (new File(userPrefs.get("lastDcmPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(FileType.SREC_FILE,
                (new File(userPrefs.get("lastSRecPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(FileType.IHEX_FILE,
                (new File(userPrefs.get("lastHexPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(FileType.PNG_FILE,
                (new File(userPrefs.get("lastPngPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(FileType.PROJECT_FILE,
                (new File(userPrefs.get("lastProjectPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(FileType.SERVICE_PROJECT_FILE,
                (new File(userPrefs.get("lastServiceProjectPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(FileType.PYTHON_FILE,
                (new File(userPrefs.get("lastPythonPath", System.getProperty("user.dir")))));

        currentConfig.setFlashSRecBaseFileName(userPrefs.get(LAST_FLASH_SREC_BASE_FILE, ""));
        currentConfig.setFlashIHexBaseFileName(userPrefs.get(LAST_FLASH_IHEX_BASE_FILE, ""));

        // init project panel
        initProjectTreePopupMenus();

        // if (!CurrentConfig.getInstance().isServiceTool())
        {
            showProjectPanel(true);
            boolean pinLayout = userPrefs.getBoolean("pinLayout", true);
            setSnapLayoutToSidePanel(pinLayout);
        }

        if (userPrefs.getBoolean("minimizeConsole", true)) {
            consolePanel.toggleSize();
        }

        // set preferred sizes and resize to maximized state
        setMinimumSize(new Dimension(640, 480));
        setExtendedState(this.getExtendedState() | HANtune.MAXIMIZED_BOTH);

        // configure closing of window
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        // reset export function
        enableExport(false);

        // load projectFile
        hanTuneProject.loadProject(this, arguments, userPrefs);
        new JythonScript("Startup.py", "./scripts/Startup.py").start();
        updateFrequencyLabels();
        updateMaxFrequencyLabel();

        // put list of user altered properties in logfile
        String[] applicProperties = ApplicationProperties.getAllPropertyStringArray();
        if (applicProperties.length > 0) {
            //AppendToLogfile.appendMessage(applicProperties);
        }
    }

    @SuppressWarnings("serial")
    private void initCustomComponents() {

        projectPanelScrollPane = new JScrollPane();
        projectTree = new ProjectTree();
        projectTree.setDynamicToolTip();
        projectTree.setTransferHandler(new TreeTransferHandler(projectTree));
        projectPanelScrollPane.setViewportView(projectTree);
        projectPanelScrollPane.setBorder(BorderFactory.createLineBorder(SidePanel.BORDER_COLOR));

        javax.swing.JMenuItem menuTabAdd = new javax.swing.JMenuItem();
        menuTabAdd.setText("Add Tab");
        menuTabAdd.addActionListener((java.awt.event.ActionEvent evt) -> {
            menuTabAddActionPerformed(evt);
        });
        tabPopupMenu.add(menuTabAdd, 1);

        projectPanel = new SidePanel("Project Data");
        projectPanel.add(projectPanelScrollPane);
        layeredPane.add(projectPanel, JLayeredPane.PALETTE_LAYER);
        buttonSidePanelMap.put((VerticalButton) projectButton, projectPanel);

        asap2Panel = new Asap2Panel("ASAP2 File");
        currentConfig.addAsap2Listener(asap2Panel);
        layeredPane.add(asap2Panel, JLayeredPane.PALETTE_LAYER);
        buttonSidePanelMap.put((VerticalButton) asap2Button, asap2Panel);

        dbcPanel = new DbcPanel("DBC Files");
        layeredPane.add(dbcPanel, JLayeredPane.PALETTE_LAYER);
        buttonSidePanelMap.put((VerticalButton) dbcButton, dbcPanel);

        scriptedElementsButton = new VerticalButton("Scripted elements", false);
        scriptedElementsButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        scriptedElementsButton.addActionListener(e -> scriptedElementsButtonActionPerformed());
        layeredPane.add(scriptedElementsButton);
        scriptedElementsButton.setBounds(2, 480, 24, 140);

        scriptedElementsPanel = new ScriptedElementsPanel("Scripted Elements");
        layeredPane.add(scriptedElementsPanel, JLayeredPane.PALETTE_LAYER);
        buttonSidePanelMap.put(scriptedElementsButton, scriptedElementsPanel);
        ScriptingManager.getInstance().addListener(scriptedElementsPanel);

        sidePanelManager.setSidePanelBounds(24, 22, 250, layeredPane.getHeight() - 25);

        tabbedPane.setFocusable(false);
        tabbedPane.setVisible(false);

        consolePanel = new ConsolePanel();
        consolePanel.initialize();
        consolePanel.setBounds(24, 763, 1200, 200);
        layeredPane.add(consolePanel, JLayeredPane.PALETTE_LAYER);

        labelDAQD1.setName("0");
        labelDAQD2.setName("1");
        labelDAQD3.setName("2");

        labelDAQcurrD1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                handleFrequencyLabelEvent(event);
            }
        });
        labelDAQcurrD2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                handleFrequencyLabelEvent(event);
            }
        });
        labelDAQcurrD3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                handleFrequencyLabelEvent(event);
            }
        });
        labelDAQD1.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent event) {
                handleFrequencyLabelEvent(event);
            }
        });
        labelDAQD2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                handleFrequencyLabelEvent(event);
            }
        });
        labelDAQD3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                handleFrequencyLabelEvent(event);
            }
        });
    }

    /**
     * Initializes ProjectTreePopupMenus
     */
    private void initProjectTreePopupMenus() {
        // register selection listener
        projectTree.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                if (evt.getButton() == 1 && evt.getClickCount() == 2) {
                    // select closest
                    projectTree
                            .setSelectionRow(projectTree.getClosestRowForLocation(evt.getX(), evt.getY()));

                    // retrieve info
                    DefaultMutableTreeNode node
                            = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
                    if (node == null) {
                        return;
                    }

                    ProjectInfo.Type type = ((ProjectInfo) node).getType();
                    switch (type) {
                        case Asap2File:
                            mASAP2LoadActionPerformed(new ActionEvent(node, 0, "dblclick"));
                            break;

                        case Layout:
                            mLayoutLoadActionPerformed(new ActionEvent(node, 0, "dblclick"));
                            break;

                        case Daqlist:
                            mDaqlistLoadActionPerformed(new ActionEvent(node, 0, "dblclick"));
                            break;

                        case Calibration:
                            if (!((ProjectInfo) node).getName()
                                    .equals(ProjectTree.WORKING_PARAMETER_SET)) {
                                mCalibrationLoadActionPerformed(new ActionEvent(node, 0, "dblclick"));
                            }

                            break;

                        case DbcFile:
                            loadDbcActionPerformed(new ActionEvent(node, 0, "dblclick"));
                            break;

                        case Script:
                            Script script = (Script) node.getUserObject();
                            Util.openFile(script.getPath());
                            break;

                        default:
                            break;
                    }
                }
            }

            @Override
            public void mousePressed(MouseEvent evt) {
                TreePath path = projectTree.getClosestPathForLocation(evt.getX(), evt.getY());
                if (path != null) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
                    if (node != null) {
                        ProjectInfo.Type type = ((ProjectInfo) node).getType();
                        projectTree.setDragEnabled(type == ProjectInfo.Type.Script);
                    }
                }

                if (evt.isPopupTrigger()) {
                    showPopup(evt);
                }
            }

            @Override
            public void mouseReleased(MouseEvent evt) {
                if (evt.isPopupTrigger()) {
                    showPopup(evt);
                }
            }

            private void showPopup(MouseEvent evt) {
                // select closest
                projectTree.setSelectionRow(projectTree.getClosestRowForLocation(evt.getX(), evt.getY()));

                // Retrieve info
                DefaultMutableTreeNode node
                        = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
                if (node == null) {
                    return;
                }
                String name = ((ProjectInfo) node).getName();
                ProjectInfo.Type type = ((ProjectInfo) node).getType();

                // double-click
                if (evt.getButton() == 1 && evt.getClickCount() == 2) {
                }

                // right-click
                if (evt.getButton() == MouseEvent.BUTTON3) {
                    if (type == ProjectInfo.Type.Folder) {
                        if (name.equals("ASAP2 files")) {
                            asap2sPopupMenu.show(projectTree, evt.getX(), evt.getY());
                        }
                        if (name.equals("DBC files")) {
                            dbcsPopupMenu.show(projectTree, evt.getX(), evt.getY());
                        }
                        if (name.equals("DAQ lists")) {
                            daqlistsPopupMenu.show(projectTree, evt.getX(), evt.getY());
                        }
                        if (name.equals("Layouts")) {
                            layoutsPopupMenu.show(projectTree, evt.getX(), evt.getY());
                        }
                        if (name.equals("Calibrations")) {
                            calibrationsPopupMenu.show(projectTree, evt.getX(), evt.getY());
                        }
                        if (name.equals("Scripts")) {
                            getScriptsPopupMenu().show(projectTree, evt.getX(), evt.getY());
                        }
                    }
                    if (type == ProjectInfo.Type.Asap2File) {
                        mASAP2Load.setText(activeAsap2Id == ((ProjectInfo) node).getId() ? "Unload File" : "Load File");
                        asap2PopupMenu.show(projectTree, evt.getX(), evt.getY());
                    }
                    if (type == ProjectInfo.Type.DbcFile) {
                        String dbcId = ((ProjectInfo) node).getFile().getName();
                        String menuText
                                = currentConfig.hasDescriptionFile(dbcId) ? "Unload File" : "Load File";
                        mDbcLoad.setText(menuText);
                        dbcPopupMenu.show(projectTree, evt.getX(), evt.getY());
                    }
                    if (type == ProjectInfo.Type.Daqlist) {
                        mDaqlistLoad.setText(
                                daqLists.get(((ProjectInfo) node).getId()).isActive() ? UNLOAD_DAQ_LIST : LOAD_DAQ_LIST);
                        daqlistPopupMenu.show(projectTree, evt.getX(), evt.getY());
                    }
                    if (type == ProjectInfo.Type.Layout) {
                        mLayoutLoad.setText(((ProjectInfo) node).isActive() ? "Unload Layout" : "Load Layout");
                        layoutPopupMenu.show(projectTree, evt.getX(), evt.getY());
                    }
                    if (type == ProjectInfo.Type.Calibration) {
                        if (name.equals(ProjectTree.WORKING_PARAMETER_SET)) {
                            workingParameterPopupMenu.show(projectTree, evt.getX(), evt.getY());
                        } else {
                            calibrationPopupMenu.show(projectTree, evt.getX(), evt.getY());
                        }
                    }
                    if (type == ProjectInfo.Type.Script) {
                        getScriptPopupMenu().show(projectTree, evt.getX(), evt.getY());
                    }
                }
            }
        });
    }

    public JPopupMenu getScriptsPopupMenu() {
        JPopupMenu scriptsMenu = new JPopupMenu();
        JMenuItem addScriptMenuItem = new JMenuItem("Add Script");
        addScriptMenuItem.addActionListener(e -> addScript());
        scriptsMenu.add(addScriptMenuItem);
        return scriptsMenu;
    }

    public JPopupMenu getScriptPopupMenu() {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        Script script = (Script) node.getUserObject();
        JPopupMenu scriptMenu = new JPopupMenu();
        JMenuItem runOrStopMenuItem = new JMenuItem(script.isRunning() ? "Stop" : "Start");
        runOrStopMenuItem.addActionListener(e -> toggleScript(script));
        scriptMenu.add(runOrStopMenuItem);
        JCheckBoxMenuItem runOnStartupMenuItem = new JCheckBoxMenuItem("Run on startup");
        runOnStartupMenuItem.addActionListener(e -> setProjectStartupScript(script));
        runOnStartupMenuItem.setSelected(script.isRunOnStartup());
        scriptMenu.add(runOnStartupMenuItem);
        JMenuItem editFileMenuItem = new JMenuItem("Edit");
        editFileMenuItem.addActionListener(e -> Util.openFile(script.getPath()));
        scriptMenu.add(editFileMenuItem);
        JMenuItem openFileLocationMenuItem = new JMenuItem("Open File Location");
        openFileLocationMenuItem.addActionListener(e -> Util.openFileLocation(script.getPath()));
        scriptMenu.add(openFileLocationMenuItem);
        JMenuItem removeScriptMenuItem = new JMenuItem("Remove");
        removeScriptMenuItem.addActionListener(e -> removeScript(script));
        scriptMenu.add(removeScriptMenuItem);
        return scriptMenu;
    }

    public void addScript() {
        CustomFileChooser fileChooser = new CustomFileChooser("Add Script", FileType.PYTHON_FILE);
        File scriptFile = fileChooser.chooseOpenDialog(this, "Add Script");
        if (scriptFile != null) {
            currentConfig.getHANtuneManager().addScriptToProject(scriptFile);
        }
    }

    private void toggleScript(Script script) {
        if (script.isRunning()) {
            script.stop();
        } else {
            script.start();
        }
    }

    public void setProjectStartupScript(Script startupScript) {
        List<Script> scripts = ScriptingManager.getInstance().getScripts();
        scripts.forEach(s -> {
            if (s == startupScript) {
                s.setRunOnStartup(!s.isRunOnStartup());
            } else {
                s.setRunOnStartup(false);
            }
        });
        scripts.remove(startupScript);
        scripts.add(0, startupScript);
        updateProjectTree();
    }

    public void removeScript(Script script) {
        if (showConfirm("Are you sure you want to remove \"" + script.getName()
                + "\" from this project?") == 0) {
            currentConfig.getHANtuneManager().removeScriptFromProject(script);
        }
    }

    /**
     * Updates the project content in the project treeview
     */
    public void updateProjectTree() {

        // create tree
        if (currentConfig.getHANtuneDocument() == null) {
            projectTree.setModel(null);
        } else {
            projectTree.setModel(ProjectTree.getTreeModel(currentConfig.getProjectFile(), currentConfig.getHANtuneDocument(), daqLists, activeLayoutId, activeCalibrationId));
        }
        projectTree.setCellRenderer(new ProjectTreeRenderer());
        projectTree.setToggleClickCount(0);

        // expand all items
        for (int i = 0; i < projectTree.getRowCount(); i++) {
            projectTree.expandRow(i);
        }

        ToolTipManager.sharedInstance().registerComponent(projectTree);
    }

    public void updateProjectTreeUI() {
        projectTree.updateUI();
    }

    /**
     * show/hide all service tool related Gui components
     */
    public void updateServiceToolGuiComponents() {
        setAllSidePanelsVisible(!CurrentConfig.getInstance().isServiceToolMode());

        mainMenuBar.updateItemsServiceModeVisibility();
    }

    /**
     * Update title bar of main window
     */
    public void updateTitleBar() {
        String projName = null;
        if (currentConfig.getProjectFile() != null) {
            projName = currentConfig.getProjectFile().getName();
        }
        if (projName != null) {
            setTitle(applicationName + " - " + projName);
        } else {
            setTitle(applicationName);
        }
    }

    /**
     * Shows projectPanel2
     *
     * @param visible
     */
    public void showProjectPanel(boolean visible) {
        showPanel(projectPanel, visible);
    }

    /**
     * Shows or hides all side panels including their vertical buttons
     *
     * @param visible true: show
     */
    public void setAllSidePanelsVisible(boolean visible) {
        sidePanelManager.setSidePanelVisible(visible);

        for (Map.Entry<VerticalButton, SidePanel> entry : buttonSidePanelMap.entrySet()) {
            entry.getKey().setVisible(visible);
        }

        adjustLayoutSize(visible);
    }

    /**
     * Sets the visibility of the provided side panel and the appearance of the
     * corresponding button. All other side panels will be hidden. *
     *
     * @param sidePanel - the side panel to show or hide
     * @param show - whether to hide or show the side panel
     */
    public void showPanel(SidePanel sidePanel, boolean show) {
        for (Map.Entry<VerticalButton, SidePanel> entry : buttonSidePanelMap.entrySet()) {
            boolean active = entry.getValue() == sidePanel && show;
            entry.getKey().setActive(active);
            entry.getValue().setVisible(active);
        }
        adjustLayoutSize(show);
    }

    public boolean isSnapLayoutToSidePanelSet() {
        return snapLayoutToSidePanel;
    }

    public void setSnapLayoutToSidePanel(boolean snapLayoutToSidePanel) {
        if (!CurrentConfig.getInstance().isServiceToolMode()) {
            this.snapLayoutToSidePanel = snapLayoutToSidePanel;
            userPrefs.put("pinLayout", String.valueOf(snapLayoutToSidePanel));
            setSidePanelSize();
            adjustLayoutSize(sidePanelManager.isSidePanelVisible());
        }
    }

    private void setSidePanelSize() {
        if (snapLayoutToSidePanel) {
            sidePanelManager.setSidePanelBounds(20, -1, sidePanelManager.getSidePanelWidth(),
                    (layeredPane.getHeight() - 23) + 24);
        } else {
            sidePanelManager.setSidePanelBounds(20, 19, sidePanelManager.getSidePanelWidth(),
                    layeredPane.getHeight() - 19);
        }
    }

    private boolean isFullScreen = false;
    private HANtuneTab fullScreenTab;
    
    public void addFullScreenKeyHandler() {
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher((KeyEvent e) -> {
            if (e.getKeyCode() == KeyEvent.VK_F11) {
                if (e.getID() == KeyEvent.KEY_RELEASED) {
                    setFullScreen(e.isShiftDown());
                }
            }
            return false;
        });
    }

    public void setFullScreen(boolean transparent) {
        this.dispose();
        if (!isFullScreen) {
            fullScreenTab = (HANtuneTab) ((JScrollPane) tabbedPane.getSelectedComponent()).getViewport().getComponent(0);
            layeredPane.add(fullScreenTab);
            mainMenuBar.minimize();
            tabbedPane.setVisible(false);
            statusPanel.setVisible(false);
            consolePanel.setVisible(false);
            setAllSidePanelsVisible(false);
            this.setUndecorated(true);
            if (transparent) {
                fullScreenTab.setOpaque(false);
                this.setBackground(new Color(1.0f,1.0f,1.0f,0.0f));
                this.setAlwaysOnTop(true);
            }
        } else {
            ((JScrollPane) tabbedPane.getComponentAt(tabbedPane.getSelectedIndex())).getViewport().add(fullScreenTab);
            mainMenuBar.restore();
            tabbedPane.setVisible(true);
            statusPanel.setVisible(true);
            consolePanel.setVisible(true);
            setAllSidePanelsVisible(true);
            fullScreenTab.setOpaque(true);
            this.setBackground(new Color(1.0f,1.0f,1.0f,1.0f));
            this.setUndecorated(false);
            this.setAlwaysOnTop(false);
        }
        isFullScreen = !isFullScreen;
        this.setVisible(true);
        this.requestFocus();
    }
    
    public boolean isFullScreen() {
        return isFullScreen;
    }
    
    public HANtuneTab getFullScreenTab() {
        return fullScreenTab;
    }

    public void adjustLayoutSize(boolean sidePanelVisible) {
        if (isFullScreen) {
            fullScreenTab.setBounds(0, 0, HANtune.this.getWidth(), HANtune.this.getHeight());
        } else {
            int layoutWidth = layeredPane.getWidth() - 21;
            int layoutHeight = layeredPane.getHeight() - consolePanel.getHeight() - 3;
            if (sidePanelVisible && snapLayoutToSidePanel) {
                tabbedPane.setBounds(20 + sidePanelManager.getSidePanelWidth(), 1,
                        layoutWidth - sidePanelManager.getSidePanelWidth(), layoutHeight);
                consolePanel.setBounds(20 + sidePanelManager.getSidePanelWidth(), layoutHeight,
                        layoutWidth - sidePanelManager.getSidePanelWidth() - 2, consolePanel.getHeight());
            } else {
                tabbedPane.setBounds(23, 1, layoutWidth, layoutHeight);
                consolePanel.setBounds(23, layoutHeight, layoutWidth - 2, consolePanel.getHeight());
            }
            consolePanel.validate();
        }
    }

    /**
     * Initializes the statusbar
     */
    public void initStatusbar() {
        initLabelError();
        initLabelDatalog();
        connectProtocol(false);
    }

    public void initLabelError() {
        labelError.setBackground(STATUSCOLOR_OFF);
        labelError.setToolTipText("Click to open an error viewer");
        labelError.setText("Errors: OFF");
        // labelError.setForeground(Color.WHITE);
    }

    private void initLabelDatalog() {
        boolean enableLoggerOnStartUp = userPrefs.getBoolean("rememberLoggerState", false);
        boolean enableLogger = enableLoggerOnStartUp && userPrefs.getBoolean("loggerEnabled", false);
        Datalog.getInstance().setEnabled(enableLogger);
        mainMenuBar.updateItemsDataLogging();
        updateDatalogGuiComponents();
    }

    /**
     * Getter for the error monitor object.
     *
     * @return Error monitor object.
     */
    public ErrorMonitor getErrorMonitor() {
        return errorMonitor;
    }

    /**
     * Manages the XCP connection
     *
     * @param conn true = new connection, false = close current connection
     */
    public void connectProtocol(boolean conn) {
        // connect or disconnect
        if (conn) {
            // preconfigure error monitoring support if the configuration
            // contains an error viewer window
            if (currentConfig.getHANtuneManager().isErrorViewerPresent()) {
                if (currentConfig.getXcpsettings() != null) {
                    currentConfig.getXcpsettings().setErrorMonitoringEnabled(true);
                }
            }
            // Handle Read log dialog before connecting
            if (LogReaderAction.getInstance().isActive()) {
                LogReaderAction.getInstance().dispose();
            }
            connect();
        } else {
            disconnect();
        }

        updateConnectionGUI();
    }

    private void updateConnectionGUI() {
        // at this point a connection might have been established through the
        // connection dialog but it is not yet know through wich protocol
        // because the protocol can be changed on the connection dialog

        // ---------------------------------------------------------------------
        // connection established with the XCP protocol?
        // ---------------------------------------------------------------------
        if (currentConfig.getXcp() != null && currentConfig.getXcp().isConnected()) {
            // init error monitor observers to clear errors from error viewers
            errorMonitor.initObservers();
            // reset the error monitoring label on the status bar
            initLabelError();
            XCPSettings xcpSettings = currentConfig.getXcpsettings();
            // yes, connected
            mainMenuBar.communicConnectSetText("Disconnect from device");
            if (xcpSettings.slaveid.equals("")) {
                labelConnected.setText("Connected: -");
            } else {
                labelConnected.setText("Connected: " + xcpSettings.slaveid);
            }
            labelConnected.setToolTipText("Click to disconnect");
            labelConnected.setBackground(STATUSCOLOR_ON);

            updateBusloadIndicator();
            int maxFrequency = Math.round(xcpSettings.getSlaveFrequency() * 100) / 100;
            labelDAQmax.setText("Max: " + maxFrequency + "Hz");
            labelDAQmax.setToolTipText("Maximum frequency for DAQ lists: " + maxFrequency + "Hz");

            if (xcpSettings.isErrorMonitoringEnabled()) {
                if (xcpSettings.isErrorMonitoringSupported()) {
                    errorMonitor.setController(currentConfig.getXcp());
                    currentConfig.getHANtuneManager().registerErrorObservers();
                    errorMonitor.enable();
                } else {
                    MessagePane.showInfo("Controller does not support error monitoring.");
                }
            }
            // reset the lag indicator
            resetLagIndicator();
        } // Generic CAN connection active?
        else if (currentConfig.getCan() != null && currentConfig.getCan().isConnected()) {
            // reset the error monitoring label on the status bar
            initLabelError();
            mainMenuBar.communicConnectSetText("Disconnect CAN");
            labelConnected.setText("Connected to CAN-bus");
            labelConnected.setToolTipText("Click to disconnect");
            labelConnected.setBackground(STATUSCOLOR_ON);
            updateProjectTree();
            currentConfig.getHANtuneManager().updateAllWindows();
        } else {
            cancelConnectionTimoutTimer();
            cancelDaqListReceiveTimoutTimer();
            labelRotator.setBackground(null);
            errorMonitor.setController(null);
            // no, not connected
            labelConnected.setText("Connected: OFF");
            labelConnected.setToolTipText("Click to connect");
            labelConnected.setBackground(STATUSCOLOR_OFF);
            updateDAQlistGUI();
            clearLagIndicator();
            labelError.setBackground(STATUSCOLOR_OFF);
            labelError.setForeground(Color.WHITE);
            mainMenuBar.communicConnectSetText("Connect");
            busloadBar.setString("DAQ list: OFF");
        }
        updateBusloadIndicator();
        updateMaxFrequencyLabel();
    }

    public void quickConnect(boolean calibrate) {
        System.out.println("quick connect");
        switch (currentConfig.getProtocol()) {
            case XCP_ON_CAN:
                quickstartXCP(new XCPonCANBasic(), calibrate);
                break;
            case XCP_ON_ETHERNET:
                quickstartXCP(new XCPonEthernet(), calibrate);
                break;
            case XCP_ON_UART:
                quickstartXCP(new XCPonUART(), calibrate);
                break;
            case CAN:
                CanConnectionHandler canConnectionHandler = currentConfig.getCan();
                canConnectionHandler.startSession();
                Console.println("Can session started");
                break;
        }
        updateConnectionGUI();
    }

    private void quickstartXCP(XCP xcp, boolean calibrate) {
        for (int i = 0; i < daqLists.size(); i++) {
            // currently active?
            if (daqLists.get(i).isActive()) {
                // set reload flag
                daqLists.get(i).setReload(true);
            }
        }

        for (int i = 0; i < daqLists.size(); i++) {
            if (daqLists.get(i).isActive()) {
                currentConfig.getHANtuneManager().unloadDaqlist(i, false, false);
            }
        }
        currentConfig.setXcp(xcp);
        xcp.start();
        xcp.startSession();
        xcp.requestInfo();
        currentConfig.getXcpsettings().slaveid = String.valueOf(xcp.getInfo().slave_identifier);
        for (DAQList.DAQList daqList : daqLists) {
            if (daqList.getPrescaler() == 0) {
                daqList.setPrescaler((char) Math.ceil(currentConfig.getXcpsettings().getSlaveFrequency() / currentConfig.getXcpsettings().sample_freq_init));
            }
        }

        if (calibrate) {
            CalibrationHandler.getInstance().handleParameterCalibration();
        }
        currentConfig.getHANtuneManager().requestParameters();
        XCPDaqInfo daqinfo = xcp.getDaqInfo();

        HANtuneManager manager = currentConfig.getHANtuneManager();
        for (int i = 0; i < daqLists.size(); i++) {
            if (daqLists.get(i).mustReload()) {
                manager.loadDaqlist(i, false);
            }
        }

        if (getActiveDaqlistIdCnt() == 0) {
            manager.loadDaqlist(0, false);
        }

        manager.setDaqlistSize(daqinfo.max_event_channel);
        manager.startDaqlists();
        manager.checkDaqListTimeOut();
        enablePrescalers();
    }

    /**
     * Establishes a new XCP connection
     */
    public void connect() {

        connectionDialog = new ConnectionDialog();
        connectionDialog.setVisible(true);
    }

    /**
     * Determines if the program currently has a connection through one of the
     * supported communication protocols.
     *
     * @return True if connected, false otherwise.
     */
    public boolean isConnected() {
        if (currentConfig.getXcp() != null) {
            return currentConfig.getXcp().isConnected();
        }
        if (currentConfig.getCan() != null) {
            return currentConfig.getCan().isConnected();
        }
        return false;
    }

    /**
     * Callback method of the IErrorObserver interface. Gets called when new
     * error info is available that should be shown on the error label in the
     * status bar.
     *
     * @param active List with active (RAM) errors.
     * @param stored List with stored (EEPROM) errors.
     */
    @Override
    public void update(ArrayList<ErrorObject> active, ArrayList<ErrorObject> stored) {
        // update label on the status bar.
        int iActive = active.size();
        int iStored = stored.size();

        labelError.setText("Errors: A" + iActive + " / S" + iStored);
        if (iActive > 0) {
            labelError.setBackground(HANtune.STATUSCOLOR_ALERT);
        } else if (iActive == 0 && iStored > 0) {
            labelError.setBackground(HANtune.STATUSCOLOR_WARNING);
        } else {
            labelError.setBackground(HANtune.STATUSCOLOR_ON);
        }
    }

    /**
     * Closes the current XCP connection
     */
    public void disconnect() {
        // first disable the error monitor so it can no longer submit
        // requests to the communication protocol.
        errorMonitor.disable();

        try {
            // object initialized?
            if (currentConfig.getXcp() != null) {
                currentConfig.getXcp().stopSession();
                Thread.sleep(0, 500000);
            }

            if (currentConfig.getCan() != null) {
                currentConfig.getCan().stopSession();
            }

            // clear object
            currentConfig.setXcp(null);
            currentConfig.setCan(null);

            // stop datalog, if necessary
            Datalog datalog = Datalog.getInstance();
            if (datalog != null && datalog.isLogging()) {
                datalog.closeDatalog();
            }

            // reset the daqlist size
            currentConfig.getHANtuneManager().setDaqlistSize(DAQ_MAX_DEFAULT);
        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
        }
    }

    /**
     * Toggles between datalog modes
     */
    public void toggleDatalog(boolean newValue) {
        Datalog datalog = Datalog.getInstance();
        datalog.setEnabled(newValue);
        userPrefs.put("loggerEnabled", String.valueOf(datalog.isEnabled()));

        // toggle datalogging on/off
        if (datalog.isEnabled()) {
            // close previous datalog, if necessary
            datalog.closeDatalog();

            // start new datalog, if necessary
            if (currentConfig.getXcp() != null && currentConfig.getXcp().isConnected()
                    && currentConfig.getASAP2Data().getASAP2Measurements().size() > 0) {
                if (!datalog.openDatalog()) {
                    MessagePane.showError("Could not start logfile:\r\n" + datalog.getError());
                    datalog.closeDatalog();
                }
            }
        } else {
            datalog.closeDatalog();
        }
    }

    public void updateDatalogGuiComponents() {
        Datalog log = Datalog.getInstance();
        if (log.isActive()) {
            labelDataLog.setText("Logfile: " + log.getFileName());
            labelDataLog.setToolTipText("Logfile: " + log.getFileName());
            labelDataLog.setBackground(HANtune.STATUSCOLOR_ON);
        } else {
            labelDataLog.setText(log.isEnabled() ? "Logfile: STANDBY" : "Logfile: OFF");
            labelDataLog.setToolTipText("Click to enable datalogging");
            labelDataLog.setBackground(HANtune.STATUSCOLOR_OFF);
        }
    }

    /**
     * Performs the required DAQ list GUI updates
     */
    public void updateDAQlistGUI() {
        enablePrescalers();
        setDaqListReceiveTimoutTimer();
        checkMeasurementActivity();
    }

    /**
     * Enables the specified prescalers, disables the rest
     */
    public void enablePrescalers() {

        // D1
        if (getDaqListId(0) != -1) {
            labelDAQD1.setText(daqLists.get(getDaqListId(0)).getScreenId());
            labelDAQD1.setName(String.valueOf(getDaqListId(0)));
            labelDAQD1.setBackground(isConnected() ? STATUSCOLOR_ON : STATUSCOLOR_OFF);
            labelDAQD1.setToolTipText("DAQ list: " + daqLists.get(getDaqListId(0)).getName());
            labelDAQcurrD1.setEnabled(true);
            labelDAQcurrD1.setName(String.valueOf(getDaqListId(0)));
        } else {
            labelDAQD1.setText("");
            labelDAQD1.setName("");
            labelDAQD1.setBackground(STATUSCOLOR_OFF);
            labelDAQD2.setToolTipText("DAQ list: 1");
            labelDAQcurrD1.setText("");
            labelDAQcurrD1.setEnabled(false);
            labelDAQcurrD1.setName(null);
        }

        // D2
        if (getDaqListId(1) != -1) {
            labelDAQD2.setText(daqLists.get(getDaqListId(1)).getScreenId());
            labelDAQD2.setName(String.valueOf(getDaqListId(1)));
            labelDAQD2.setBackground(isConnected() ? STATUSCOLOR_ON : STATUSCOLOR_OFF);
            labelDAQD2.setToolTipText("DAQ list: " + daqLists.get(getDaqListId(1)).getName());
            labelDAQcurrD2.setEnabled(true);
            labelDAQcurrD2.setName(String.valueOf(getDaqListId(1)));
        } else {
            labelDAQD2.setText("");
            labelDAQD2.setName("");
            labelDAQD2.setBackground(STATUSCOLOR_OFF);
            labelDAQD2.setToolTipText("DAQ list: 2");
            labelDAQcurrD2.setText("");
            labelDAQcurrD2.setEnabled(false);
            labelDAQcurrD2.setName(null);
        }

        // D3
        if (getDaqListId(2) != -1) {
            labelDAQD3.setText(daqLists.get(getDaqListId(2)).getScreenId());
            labelDAQD3.setBackground(isConnected() ? STATUSCOLOR_ON : STATUSCOLOR_OFF);
            labelDAQD3.setName(String.valueOf(getDaqListId(2)));
            labelDAQD3.setToolTipText("DAQ list: " + daqLists.get(getDaqListId(2)).getName());
            labelDAQcurrD3.setEnabled(true);
            labelDAQcurrD3.setName(String.valueOf(getDaqListId(2)));
        } else {
            labelDAQD3.setText("");
            labelDAQD3.setName("");
            labelDAQD3.setBackground(STATUSCOLOR_OFF);
            labelDAQD3.setToolTipText("DAQ list: 3");
            labelDAQcurrD3.setText("");
            labelDAQcurrD3.setEnabled(false);
            labelDAQcurrD3.setName(null);
        }
    }

    /**
     * Updates the text of the busload bar
     *
     * @param busload the current busload
     */
    public void updateBusloadText(int busload) {
        if (isConnected()) {
            String busloadPercentage = busload + "%";
            String bandwidth = Util.getReadableDataRate(currentConfig.getBusBandwidth());
            String tooltip = "Busload: " + busload + "% of " + bandwidth;
            if (currentConfig.getProtocol() == CurrentConfig.Protocol.XCP_ON_UART
                    && currentConfig.getXcpsettings().isUsbVirtualComPort()) {
                busloadPercentage = "~ " + busloadPercentage;
                tooltip = "<html>" + tooltip
                        + ". Note this is only an estimate of the busload. When using a USB Virtual COM Port, the data rate"
                        + "<br/>\nis not limited by the baud rate. Therefore a default data rate of 1 Mbit/s is used to calculate the busload instead.</html>";
            }
            busloadBar.setString(busloadPercentage);
            busloadBar.setToolTipText(tooltip);
        } else {
            busloadBar.setString("0%");
            busloadBar.setToolTipText("Busload: not connected");
        }
    }

    public void checkMeasurementActivity() {
        if (currentConfig.getASAP2Data() != null) {
            for (ASAP2Measurement measurement : currentConfig.getASAP2Data().getMeasurements()) {
                boolean active = false;
                for (DAQList.DAQList daqList : daqLists) {
                    if (daqList.isActive() && daqList.contains(measurement)) {
                        active = true;
                        break;
                    }
                }
                measurement.setActive(active);
            }
        }
    }

    /**
     * enables or disables given DAQ lists
     *
     * @param mode (0=stop all; 1=start list; 2=stop list)
     * @param daqlists the DAQ lists which are stopped or started
     * @return whether this command was executed successfully
     */
    public boolean toggleDaqlists(char mode, List<Character> daqlists) {
        // start or stop DAQ lists
        if (currentConfig.getXcp() != null) {
            if (currentConfig.getXcp().isConnected() && currentConfig.getXcp().getDaqLists().length > 0) {
                char[] lists = new char[daqlists.size()];
                for (int i = 0; i < daqlists.size(); i++) {
                    lists[i] = daqlists.get(i);
                }
                if (!currentConfig.getXcp().toggleDaq(mode, lists)) {
                    MessagePane.showError(
                            "Could not toggle the DAQ list(s)!\n" + currentConfig.getXcp().getErrorMsg());
                    return false;
                }
            }
        }

        // update frequency labels in statusbar
        updateFrequencyLabels();

        updateBusloadIndicator();

        return true;
    }

    /**
     * Modifies prescaler used for DAQ list
     *
     * @param id
     * @param ps
     * @param update_slave
     * @return
     */
    public boolean changePrescaler(int id, char ps, boolean update_slave) {
        Datalog datalog = Datalog.getInstance();

        // calculate and store prescaler and derived sample frequency
        daqLists.get(id).setPrescaler(ps);
        resetLagIndicator();
        updateBusloadIndicator();
        // configure prescaler at slave
        if (update_slave && daqLists.get(id).isActive() && currentConfig.getXcp() != null
                && currentConfig.getXcp().isConnected() && currentConfig.getXcp().getDaqLists().length > 0) {
            // relative id
            int relativeId = daqLists.get(id).getRelativeId();

            // request DAQ info (for timestamp information)
            XCPDaqInfo daqinfo = currentConfig.getXcp().getDaqInfo();
            if (daqinfo == null) {
                MessagePane.showError("Could not request DAQ processor information.\n"
                        + currentConfig.getXcp().getErrorMsg());
                return false;
            }

            // set prescaler
            if (!currentConfig.getXcp().setPrescaler((char) relativeId,
                    (char) daqLists.get(id).getPrescaler())) {
                MessagePane.showError("Could not configure the specified prescaler at the slave device!\n"
                        + currentConfig.getXcp().getErrorMsg());
                return false;
            } else {
                // restart datalog
                if (datalog != null && datalog.isActive()) {
                    datalog.restart();
                }
            }
        }

        // store prescaler in DAQ list
        currentConfig.getHANtuneManager().storeDaqlist(id);

        // modify related statusbar elements
        updateFrequencyLabels();

        // update GUI
        updateDAQlistGUI();

        updateDaqListListeners();

        return true;
    }

    public void updateFrequencyLabels() {
        float slaveFrequency = currentConfig.getXcpsettings().getSlaveFrequency();
        for (int i = 0; i < 3; i++) {
            if (getDaqListId(i) != -1) {
                double daqListFrequency = daqLists.get(getDaqListId(i)).getSampleFrequency(slaveFrequency);
                String frequency = util.Util.getStringValue(daqListFrequency, "0.##") + "Hz";
                if (i == 0) {
                    labelDAQcurrD1.setText(frequency);
                    labelDAQcurrD1.setToolTipText("Current frequency DAQ list 1: " + frequency);
                } else if (i == 1) {
                    labelDAQcurrD2.setText(frequency);
                    labelDAQcurrD2.setToolTipText("Current frequency DAQ list 2: " + frequency);
                } else if (i == 2) {
                    labelDAQcurrD3.setText(frequency);
                    labelDAQcurrD3.setToolTipText("Current frequency DAQ list 3: " + frequency);
                }
            }
        }
    }

    private void updateMaxFrequencyLabel() {
        int maxFrequency = Math.round(currentConfig.getXcpsettings().getSlaveFrequency() * 100) / 100;
        labelDAQmax.setText("Max: " + maxFrequency + "Hz");
        labelDAQmax.setToolTipText("Maximum frequency for DAQ lists: " + maxFrequency + "Hz");
        labelDAQmax.setEnabled(true);
    }

    private void handleFrequencyLabelEvent(MouseEvent event) {
        if (currentConfig.isServiceToolMode()) {
            return;
        }
        String name = ((JLabel) event.getSource()).getName();
        if (name != null) {
            int id = Integer.parseInt(name);
            currentConfig.getHANtuneManager().modifyDaqlist(id);
        }
    }

    public void setDaqListReceiveTimoutTimer() {
        long timeout = (long) (1000 * currentConfig.getXcpsettings().getTPrescaler()); // return timeout
        // setting in ms

        // ScheduledExecutorService scheduler = new ScheduledThreadPoolExecutor(1);
        daqListReceiveTimoutTimer
                = new ResettableTimer(scheduler, 2 * timeout, TimeUnit.MILLISECONDS, new Runnable() {
                    @Override
                    public void run() {
                        Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
                        labelRotator.setBackground(Color.red);
                        if (connectionTimoutTimer == null) {
                            connectionTimoutTimer = new Timer();

                            connectionTimoutTimer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    Thread.currentThread()
                                            .setUncaughtExceptionHandler(new defaultExceptionHandler());
                                    HANtune.this.connectProtocol(false);
                                    MessagePane.showError("Connection has been lost!");
                                }
                            }, CONNECTION_TIMEOUT);
                        }
                    }
                });
    }

    private void cancelDaqListReceiveTimoutTimer() {
        if (daqListReceiveTimoutTimer != null) {
            daqListReceiveTimoutTimer.clear();
            daqListReceiveTimoutTimer = null;
        }
    }

    private void cancelConnectionTimoutTimer() {
        if (connectionTimoutTimer != null) {
            connectionTimoutTimer.cancel();
            connectionTimoutTimer = null;
        }
    }

    /**
     * Performs a rotation of the rotating connection indicator.
     */
    public void stepRotator() {
        // return to first character or simply increment?
        rotatorId++;
        if (rotatorId >= ROTATOR_CHARS_LEN) {
            rotatorId = 0;
        }

        labelRotator.setBackground(null);
        labelRotator.setText(ROTATOR_CHARS[rotatorId]);
        labelRotator.repaint();
    }

    /**
     * Updates the lag indicator bar.
     */
    private void updateLagIndicator() {
        long timestamp = System.currentTimeMillis();
        updatePeriodAvg = (updatePeriodAvg * 2 + timestamp - lastUpdateTimestamp) / 3;
        lastUpdateTimestamp = timestamp;

        float div = 10000 / currentConfig.getHANtuneManager().getDaqListHighestFrequency();

        int lag = (int) (4 * (updatePeriodAvg - div) / div);
        if (lag < 1) {
            lagIndicatorBar.setForeground(Color.GREEN);
        } else if (lag < 5) {
            lagIndicatorBar.setForeground(Color.ORANGE);
        } else {
            lagIndicatorBar.setForeground(Color.RED);
        }

        // if connected show at least a line to indicate "load"
        if (currentConfig.getXcp() != null && currentConfig.getXcp().isConnected()) {
            lag++;
        } else {
            lag = 0;
        }

        lagIndicatorBar.setValue(lag);
    }

    /**
     * reset the moving average for the lag indicator
     */
    private void resetLagIndicator() {
        updatePeriodAvg = (long) (10000 / currentConfig.getHANtuneManager().getDaqListHighestFrequency());
    }

    /**
     * clear the lag indicator
     */
    private void clearLagIndicator() {
        lagIndicatorBar.setValue(0);
    }

    public void updateBusloadIndicator(int busload) {
        if (busload < 0) {
            busload = 0; // Todo: temporary fix because busload could become -1 occasionally
        }
        busloadBar.setToolTipText("Busload " + busload + "% (estimate)");
        if (busload < BUSLOAD_WARNING_PERCENTAGE) {
            busloadBar.setForeground(Color.GREEN);
        } else if (busload < BUSLOAD_CRITICAL_PERCENTAGE) {
            busloadBar.setForeground(Color.ORANGE);
        } else {
            busloadBar.setForeground(Color.RED);
        }
        busloadBar.setValue(busload);
        updateBusloadText(busload);
    }

    /**
     * Updates the busload indicator bar.
     */
    private void updateBusloadIndicator() {
        int busload = 0;
        // if connected, calculate the busload
        if (currentConfig.getXcp() != null && currentConfig.getXcp().isConnected()) {
            int totalSize = 0;
            for (int i = 0; i < currentConfig.getXcp().getDaqLists().length; i++) {
                totalSize += currentConfig.getXcp().getDaqLists()[i].getDataLen();
            }

            int load = (int) (currentConfig.getHANtuneManager().getDaqListHighestFrequency() * totalSize);
            int bandwidth = currentConfig.getBusBandwidth();

            busload = 100 * load / bandwidth;
        }

        updateBusloadIndicator(busload);
    }

    /**
     * Enables the export related items
     */
    public void enableExport(boolean enabled) {
        boolean asap2RamFlashOffsetPresent = false;

        // change tooltip
        if (enabled) {
            mCalibrationUpdate
                    .setToolTipText("Update calibration with data from current Working Parameter Set");
            mCalibrationLoad.setToolTipText("Write calibration into current Working Parameter Set");

            String basedOn = "Create a new calibration based on ";
            mCalibrationCreate.setToolTipText(basedOn + "current Working Parameter Set");
            mCalibrationCreateFromDCM.setToolTipText(basedOn + FileType.DCM_FILE);
            mCalibrationCreateFromSRec.setToolTipText(basedOn + FileType.SREC_FILE);
            mCalibrationCreateFromIHex.setToolTipText(basedOn + FileType.IHEX_FILE);
            mWorkingParameterCreate.setToolTipText(basedOn + "current Working Parameter Set");

            String exportTo = "Export current calibration data to ";
            mCalibrationExportMatlab.setToolTipText(exportTo + FileType.MATLAB_FILE.getDescription());
            mCalibrationExportDcm.setToolTipText(exportTo + FileType.DCM_FILE.getDescription());

            mCalibrationImportDcm.setToolTipText(
                    "Import an " + FileType.DCM_FILE.getDescription() + " into current snapshot");
            mCalibrationImportSRec.setToolTipText(
                    "Import a " + FileType.SREC_FILE.getDescription() + " into current snapshot");
            mCalibrationImportIHex.setToolTipText(
                    "Import a " + FileType.IHEX_FILE.getDescription() + " into current snapshot");

            mCalibrationWriteIntoAndFlashSRec.setToolTipText("Write current snapshot data into an existing "
                    + FileType.SREC_FILE.getDescription() + " followed by flashing all in target");
            mCalibrationWriteIntoAndFlashIHex.setToolTipText("Write current snapshot data into an existing "
                    + FileType.IHEX_FILE.getDescription() + " followed by flashing all in target");

            if (currentConfig.getASAP2Data() != null) {
                asap2RamFlashOffsetPresent = currentConfig.getASAP2Data().getModPar().isRamFlashOffsetPresent();
            }

        } else {
            String asapFirst = "Please open an ASAP2 file first";
            mCalibrationUpdate.setToolTipText(asapFirst);
            mCalibrationLoad.setToolTipText(asapFirst);

            mCalibrationCreate.setToolTipText(asapFirst);
            mCalibrationCreateFromDCM.setToolTipText(asapFirst);
            mCalibrationCreateFromSRec.setToolTipText(asapFirst);
            mCalibrationCreateFromIHex.setToolTipText(asapFirst);
            mWorkingParameterCreate.setToolTipText(asapFirst);

            mCalibrationExportMatlab.setToolTipText(asapFirst);
            mCalibrationExportDcm.setToolTipText(asapFirst);

            mCalibrationImportDcm.setToolTipText(asapFirst);
            mCalibrationImportSRec.setToolTipText(asapFirst);
            mCalibrationImportIHex.setToolTipText(asapFirst);
        }

        // change enabled state of export menuitem
        mCalibrationUpdate.setEnabled(enabled);
        mCalibrationLoad.setEnabled(enabled);

        mCalibrationExportMatlab.setEnabled(enabled);
        mCalibrationExportDcm.setEnabled(enabled);
        mCalibrationImportDcm.setEnabled(enabled);

        mCalibrationCreate.setEnabled(enabled);
        mCalibrationCreateFromDCM.setEnabled(enabled);
        mCalibrationCreateFromSRec.setEnabled(enabled && asap2RamFlashOffsetPresent);
        mCalibrationCreateFromIHex.setEnabled(enabled && asap2RamFlashOffsetPresent);
        mWorkingParameterCreate.setEnabled(enabled);

        mCalibrationImportSRec.setEnabled(enabled && asap2RamFlashOffsetPresent);
        mCalibrationImportIHex.setEnabled(enabled && asap2RamFlashOffsetPresent);
        mCalibrationWriteIntoAndFlashSRec.setEnabled(enabled);
        mCalibrationWriteIntoAndFlashIHex.setEnabled(enabled);
    }

    /**
     * Terminates HANtune.
     */
    public void exitHANtune() {
        // check length of logfile, if nessecary, trim logfile.
        ErrorLogRestrictor.Restrictor(); // Check errorLog.txt file size and trim file if nesseccary

        connectProtocol(false);
        System.exit(0);
    }

    /**
     * Starts exit procedure for HANtune
     */
    public void requestExitHANtune() {

        // save last used file paths
        userPrefs.put("lastAsap2Path", currentConfig.getPreviousPath(FileType.ASAP2_FILE).getPath());
        userPrefs.put("lastDBCPath", currentConfig.getPreviousPath(FileType.DBC_FILE).getPath());
        userPrefs.put("lastLogPath", currentConfig.getPreviousPath(FileType.LOG_FILE).getPath());
        userPrefs.put("lastMapPath", currentConfig.getPreviousPath(FileType.MAP_FILE).getPath());
        userPrefs.put("lastMatlabPath", currentConfig.getPreviousPath(FileType.MATLAB_FILE).getPath());
        userPrefs.put("lastDcmPath", currentConfig.getPreviousPath(FileType.DCM_FILE).getPath());
        userPrefs.put("lastSRecPath", currentConfig.getPreviousPath(FileType.SREC_FILE).getPath());
        userPrefs.put("lastHexPath", currentConfig.getPreviousPath(FileType.IHEX_FILE).getPath());
        userPrefs.put("lastPngPath", currentConfig.getPreviousPath(FileType.PNG_FILE).getPath());
        userPrefs.put("lastProjectPath", currentConfig.getPreviousPath(FileType.PROJECT_FILE).getPath());
        userPrefs.put("lastServiceProjectPath", currentConfig.getPreviousPath(FileType.SERVICE_PROJECT_FILE).getPath());
        userPrefs.put("lastPythonPath", currentConfig.getPreviousPath(FileType.PYTHON_FILE).getPath());

        userPrefs.put(LAST_FLASH_SREC_BASE_FILE, currentConfig.getFlashSRecBaseFileName());
        userPrefs.put(LAST_FLASH_IHEX_BASE_FILE, currentConfig.getFlashIHexBaseFileName());
        userPrefs.put("minimizeConsole", String.valueOf(consolePanel.isMinimized()));

        if (currentConfig.getProjectFile() != null) {
            userPrefs.put("lastOpenProject", currentConfig.getProjectFile().getAbsolutePath());
        }

        if (CurrentConfig.getInstance().isServiceToolMode()) {
            exitHANtune();
        } else {
            switch (hanTuneProject.checkChangedAndRequestSaveAndSave()) {
                case JOptionPane.YES_OPTION:
                case JOptionPane.NO_OPTION:
                    exitHANtune();
                    break;

                case JOptionPane.CANCEL_OPTION:
                case JOptionPane.CLOSED_OPTION:
                default:
                    // do nothing, abort shutdown
                    break;
            }
        }
    }

    /**
     * Starts open ASAP2 procedure.
     */
    public void requestOpenASAP2() {
        CustomFileChooser fileChooser = new CustomFileChooser("Open", FileType.ASAP2_FILE);
        File asap2File = fileChooser.chooseOpenDialog(this, "Open");

        if (asap2File != null) {
            currentConfig.getHANtuneManager().newASAP2(asap2File);
            showProjectPanel(true);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated
    // Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabPopupMenu = new javax.swing.JPopupMenu();
        menuTabEdit = new javax.swing.JMenuItem();
        menuTabDelete = new javax.swing.JMenuItem();
        asap2sPopupMenu = new javax.swing.JPopupMenu();
        mASAP2Add = new javax.swing.JMenuItem();
        asap2PopupMenu = new javax.swing.JPopupMenu();
        mASAP2Load = new javax.swing.JMenuItem();
        mASAP2Remove = new javax.swing.JMenuItem();
        daqlistsPopupMenu = new javax.swing.JPopupMenu();
        mDaqlistNew = new javax.swing.JMenuItem();
        daqlistPopupMenu = new javax.swing.JPopupMenu();
        mDaqlistLoad = new javax.swing.JMenuItem();
        mDaqlistModify = new javax.swing.JMenuItem();
        mDaqlistRename = new javax.swing.JMenuItem();
        mDaqlistCopy = new javax.swing.JMenuItem();
        mDaqlistRemove = new javax.swing.JMenuItem();
        layoutsPopupMenu = new javax.swing.JPopupMenu();
        mLayoutNew = new javax.swing.JMenuItem();
        layoutPopupMenu = new javax.swing.JPopupMenu();
        mLayoutLoad = new javax.swing.JMenuItem();
        mLayoutRename = new javax.swing.JMenuItem();
        mLayoutCopy = new javax.swing.JMenuItem();
        mLayoutRemove = new javax.swing.JMenuItem();
        calibrationsPopupMenu = new javax.swing.JPopupMenu();
        mCalibrationCreate = new javax.swing.JMenuItem();
        mCalibrationCreateFromSRec = new javax.swing.JMenuItem();
        mCalibrationCreateFromIHex = new javax.swing.JMenuItem();
        mCalibrationCreateFromDCM = new javax.swing.JMenuItem();
        workingParameterPopupMenu = new JPopupMenu();
        mWorkingParameterCreate = new JMenuItem();
        calibrationPopupMenu = new JPopupMenu();
        mCalibrationUpdate = new javax.swing.JMenuItem();
        mCalibrationLoad = new javax.swing.JMenuItem();
        mCalibrationRename = new javax.swing.JMenuItem();
        mCalibrationCopy = new JMenuItem();
        mCalibrationRemove = new JMenuItem();
        mCalibrationSubExport = new JMenu();
        mCalibrationSubImport = new JMenu();
        mCalibrationExportMatlab = new JMenuItem();
        mCalibrationExportDcm = new JMenuItem();
        // mCalibrationExportSRec = new JMenuItem();
        mCalibrationImportDcm = new JMenuItem();
        mCalibrationImportSRec = new JMenuItem();
        mCalibrationImportIHex = new JMenuItem();
        mCalibrationWriteIntoAndFlashSRec = new javax.swing.JMenuItem();
        mCalibrationWriteIntoAndFlashIHex = new javax.swing.JMenuItem();
        dbcsPopupMenu = new javax.swing.JPopupMenu();
        mDbcAdd = new javax.swing.JMenuItem();
        dbcPopupMenu = new JPopupMenu();
        mDbcLoad = new javax.swing.JMenuItem();
        mDbcRemove = new javax.swing.JMenuItem();
        layeredPane = new javax.swing.JLayeredPane();
        tabbedPane = new javax.swing.JTabbedPane();
        projectButton = new VerticalButton("Project data", false);
        asap2Button = new VerticalButton("ASAP2 elements", false);
        dbcButton = new VerticalButton("CAN elements", false);
        statusPanel = new javax.swing.JPanel();
        labelError = new javax.swing.JLabel();
        labelConnected = new javax.swing.JLabel();
        busloadBar = new javax.swing.JProgressBar();
        labelDAQmax = new javax.swing.JLabel();
        labelDAQD1 = new javax.swing.JLabel();
        labelDAQD2 = new javax.swing.JLabel();
        labelDAQD3 = new javax.swing.JLabel();
        labelDAQcurrD1 = new javax.swing.JLabel();
        labelDAQcurrD2 = new javax.swing.JLabel();
        labelDAQcurrD3 = new javax.swing.JLabel();
        labelRotator = new javax.swing.JLabel() {

            private static final long serialVersionUID = 7526471155622776154L;


            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                updateLagIndicator();
            }
        };
        lagIndicatorBar = new javax.swing.JProgressBar();
        labelDataLog = new javax.swing.JLabel();


        // Init and fill main menu bar here
        mainMenuBar = new HANtuneMainMenuBar(this);
        setJMenuBar(mainMenuBar.getMenuBar());


        menuTabEdit.setText("Rename Tab");
        menuTabEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTabEditActionPerformed(evt);
            }
        });
        tabPopupMenu.add(menuTabEdit);

        menuTabDelete.setText("Delete Tab");
        menuTabDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuTabDeleteActionPerformed(evt);
            }
        });
        tabPopupMenu.add(menuTabDelete);

        mASAP2Add.setText("Add ASAP2 file");
        mASAP2Add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mASAP2AddActionPerformed(evt);
            }
        });
        asap2sPopupMenu.add(mASAP2Add);

        mASAP2Load.setText("Load File");
        mASAP2Load.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mASAP2LoadActionPerformed(evt);
            }
        });
        asap2PopupMenu.add(mASAP2Load);

        mASAP2Remove.setText("Remove");
        mASAP2Remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mASAP2RemoveActionPerformed(evt);
            }
        });
        asap2PopupMenu.add(mASAP2Remove);

        mDaqlistNew.setText("New DAQ list");
        mDaqlistNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mDaqlistNewActionPerformed(evt);
            }
        });
        daqlistsPopupMenu.add(mDaqlistNew);

        mDaqlistLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mDaqlistLoadActionPerformed(evt);
            }
        });
        daqlistPopupMenu.add(mDaqlistLoad);

        mDaqlistModify.setText("Modify DAQ list");
        mDaqlistModify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mDaqlistModifyActionPerformed(evt);
            }
        });
        daqlistPopupMenu.add(mDaqlistModify);

        mDaqlistRename.setText("Rename DAQ list");
        mDaqlistRename.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mDaqlistRenameActionPerformed(evt);
            }
        });
        daqlistPopupMenu.add(mDaqlistRename);

        mDaqlistCopy.setText("Copy DAQ list");
        mDaqlistCopy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mDaqlistCopyActionPerformed(evt);
            }
        });
        daqlistPopupMenu.add(mDaqlistCopy);

        mDaqlistRemove.setText("Remove DAQ list");
        mDaqlistRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mDaqlistRemoveActionPerformed(evt);
            }
        });
        daqlistPopupMenu.add(mDaqlistRemove);

        mLayoutNew.setText("New Layout");
        mLayoutNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mLayoutNewActionPerformed(evt);
            }
        });
        layoutsPopupMenu.add(mLayoutNew);

        mLayoutLoad.setText("Load Layout");
        mLayoutLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mLayoutLoadActionPerformed(evt);
            }
        });
        layoutPopupMenu.add(mLayoutLoad);

        mLayoutRename.setText("Rename");
        mLayoutRename.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mLayoutRenameActionPerformed(evt);
            }
        });
        layoutPopupMenu.add(mLayoutRename);

        mLayoutCopy.setText("Copy");
        mLayoutCopy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mLayoutCopyActionPerformed(evt);
            }
        });
        layoutPopupMenu.add(mLayoutCopy);

        mLayoutRemove.setText("Remove");
        mLayoutRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mLayoutRemoveActionPerformed(evt);
            }
        });
        layoutPopupMenu.add(mLayoutRemove);


        // CalibrationSSSS PopUp
        mCalibrationCreate.setText("Create calibration from Working Parameter Set");
        mCalibrationCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationCreateActionPerformed(evt);
            }
        });
        calibrationsPopupMenu.add(mCalibrationCreate);

        mCalibrationCreateFromDCM.setText("Create calibration from DCM...");
        mCalibrationCreateFromDCM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationCreateFromDCMActionPerformed(evt);
            }
        });
        calibrationsPopupMenu.add(mCalibrationCreateFromDCM);

        mCalibrationCreateFromSRec.setText("Create calibration from SREC...");
        mCalibrationCreateFromSRec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationCreateFromSRecActionPerformed(evt);
            }
        });
        calibrationsPopupMenu.add(mCalibrationCreateFromSRec);

        mCalibrationCreateFromIHex.setText("Create calibration from HEX...");
        mCalibrationCreateFromIHex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationCreateFromHexActionPerformed(evt);
            }
        });
        calibrationsPopupMenu.add(mCalibrationCreateFromIHex);


        // Working parameter set PopUp
        mWorkingParameterCreate.setText("Create calibration from Working Parameter Set");
        mWorkingParameterCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationCreateActionPerformed(evt);
            }
        });
        workingParameterPopupMenu.add(mWorkingParameterCreate);

        // CalibratioNNN PopUp
        mCalibrationUpdate.setText("Update from Working Parameter Set");
        mCalibrationUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationUpdateActionPerformed(evt);
            }
        });
        calibrationPopupMenu.add(mCalibrationUpdate);

        mCalibrationLoad.setText("Load into Working Parameter Set");
        mCalibrationLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationLoadActionPerformed(evt);
            }
        });
        calibrationPopupMenu.add(mCalibrationLoad);

        calibrationPopupMenu.addSeparator();

        mCalibrationRename.setText("Rename");
        mCalibrationRename.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationRenameActionPerformed(evt);
            }
        });
        calibrationPopupMenu.add(mCalibrationRename);

        mCalibrationCopy.setText("Copy");
        mCalibrationCopy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationCopyActionPerformed(evt);
            }
        });
        calibrationPopupMenu.add(mCalibrationCopy);

        mCalibrationRemove.setText("Remove");
        mCalibrationRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationRemoveActionPerformed(evt);
            }
        });
        calibrationPopupMenu.add(mCalibrationRemove);

        calibrationPopupMenu.addSeparator();

        // submenu: export
        mCalibrationSubExport.setText("Export/Flash");

        mCalibrationExportMatlab.setText("to MATLAB file...");
        mCalibrationExportMatlab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationExportMatlabActionPerformed(evt);
            }
        });
        mCalibrationSubExport.add(mCalibrationExportMatlab);

        mCalibrationExportDcm.setText("to DCM file...");
        mCalibrationExportDcm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationExportDcmActionPerformed(evt);
            }
        });
        mCalibrationSubExport.add(mCalibrationExportDcm);


        mCalibrationWriteIntoAndFlashSRec.setText("Export/Flash SREC...");
        mCalibrationWriteIntoAndFlashSRec.setEnabled(false);
        mCalibrationWriteIntoAndFlashSRec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationExportIntoSRecAndFlashActionPerformed(evt);
            }
        });
        mCalibrationSubExport.add(mCalibrationWriteIntoAndFlashSRec);

        mCalibrationWriteIntoAndFlashIHex.setText("Export/Flash IHEX...");
        mCalibrationWriteIntoAndFlashIHex.setEnabled(false);
        mCalibrationWriteIntoAndFlashIHex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationExportIntoIHexAndFlashActionPerformed(evt);
            }
        });
        mCalibrationSubExport.add(mCalibrationWriteIntoAndFlashIHex);

        calibrationPopupMenu.add(mCalibrationSubExport);
        // end: export submenu

        // submenu: import
        mCalibrationSubImport.setText("Import");

        mCalibrationImportDcm.setText("from DCM file...");
        mCalibrationImportDcm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationImportDcmActionPerformed(evt);
            }
        });
        mCalibrationSubImport.add(mCalibrationImportDcm);

        mCalibrationImportSRec.setText("from SREC file...");
        mCalibrationImportSRec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationImportSRecActionPerformed(evt);
            }
        });
        mCalibrationSubImport.add(mCalibrationImportSRec);

        mCalibrationImportIHex.setText("from HEX file...");
        mCalibrationImportIHex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCalibrationImportHexActionPerformed(evt);
            }
        });
        mCalibrationSubImport.add(mCalibrationImportIHex);

        calibrationPopupMenu.add(mCalibrationSubImport);
        // end: import submenu


        mDbcAdd.setText("Add DBC file");
        mDbcAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importDbcActionPerformed(evt);
            }
        });
        dbcsPopupMenu.add(mDbcAdd);

        mDbcLoad.setText("Load File");
        mDbcLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadDbcActionPerformed(evt);
            }
        });
        dbcPopupMenu.add(mDbcLoad);

        mDbcRemove.setText("Remove File");
        mDbcRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeDbcActionPerformed(evt);
            }
        });
        dbcPopupMenu.add(mDbcRemove);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("HANtune");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        layeredPane.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                layeredPaneComponentResized(evt);
            }
        });
        layeredPane.add(tabbedPane);
        tabbedPane.setBounds(24, 10, 1040, 550);

        projectButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        projectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                projectButtonActionPerformed(evt);
            }
        });
        layeredPane.add(projectButton);
        projectButton.setBounds(2, 60, 24, 140);

        asap2Button.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        asap2Button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                asap2ButtonActionPerformed(evt);
            }
        });
        layeredPane.add(asap2Button);
        asap2Button.setBounds(2, 200, 24, 140);

        dbcButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        dbcButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dbcButtonActionPerformed(evt);
            }
        });
        layeredPane.add(dbcButton);
        dbcButton.setBounds(2, 340, 24, 140);

        statusPanel.setBackground(new java.awt.Color(204, 204, 204));
        statusPanel.setPreferredSize(new java.awt.Dimension(1105, 30));

        labelError.setForeground(new java.awt.Color(255, 255, 255));
        labelError.setBorder(
            javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(),
                javax.swing.BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        labelError.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        labelError.setOpaque(true);
        labelError.setPreferredSize(new java.awt.Dimension(95, 20));
        labelError.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                labelErrorMouseClicked(evt);
            }
        });

        labelConnected.setForeground(new java.awt.Color(255, 255, 255));
        labelConnected.setBorder(
            javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(),
                javax.swing.BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        labelConnected.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        labelConnected.setOpaque(true);
        labelConnected.setPreferredSize(new java.awt.Dimension(175, 20));
        labelConnected.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                labelConnectedMouseClicked(evt);
            }
        });

        BasicProgressBarUI busloadBarUI = new BasicProgressBarUI() {
            @Override
            public void installUI(JComponent c) {
                super.installUI(c);
                setCellLength(1);
                setCellSpacing(0);
            }


            @Override
            protected Color getSelectionBackground() {
                return Color.black;
            }


            @Override
            protected Color getSelectionForeground() {
                return Color.black;
            }
        };
        busloadBar.setUI(busloadBarUI);
        busloadBar.setPreferredSize(new java.awt.Dimension(170, 20));
        busloadBar.setString("");
        busloadBar.setStringPainted(true);

        labelDAQmax.setToolTipText("Maximum frequency for DAQ lists");
        labelDAQmax.setBorder(
            javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(),
                javax.swing.BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        labelDAQmax.setEnabled(false);
        labelDAQmax.setPreferredSize(new java.awt.Dimension(90, 20));

        labelDAQD1.setForeground(new java.awt.Color(255, 255, 255));
        labelDAQD1.setText("D1");
        labelDAQD1.setToolTipText("DAQ list 1");
        labelDAQD1.setBorder(
            javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(),
                javax.swing.BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        labelDAQD1.setOpaque(true);
        labelDAQD1.setPreferredSize(new java.awt.Dimension(100, 20));

        labelDAQD2.setForeground(new java.awt.Color(255, 255, 255));
        labelDAQD2.setText("D2");
        labelDAQD2.setToolTipText("DAQ list 2");
        labelDAQD2.setBorder(
            javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(),
                javax.swing.BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        labelDAQD2.setOpaque(true);
        labelDAQD2.setPreferredSize(new java.awt.Dimension(100, 20));

        labelDAQD3.setForeground(new java.awt.Color(255, 255, 255));
        labelDAQD3.setText("D3");
        labelDAQD3.setToolTipText("DAQ list 3");
        labelDAQD3.setBorder(
            javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(),
                javax.swing.BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        labelDAQD3.setOpaque(true);
        labelDAQD3.setPreferredSize(new java.awt.Dimension(100, 20));

        labelDAQcurrD1.setToolTipText("Current frequency DAQ list 1");
        labelDAQcurrD1.setBorder(
            javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(),
                javax.swing.BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        labelDAQcurrD1.setEnabled(false);
        labelDAQcurrD1.setPreferredSize(new java.awt.Dimension(100, 20));

        labelDAQcurrD2.setToolTipText("Current frequency DAQ list 2");
        labelDAQcurrD2.setBorder(
            javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(),
                javax.swing.BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        labelDAQcurrD2.setEnabled(false);
        labelDAQcurrD2.setPreferredSize(new java.awt.Dimension(100, 20));

        labelDAQcurrD3.setToolTipText("Current frequency DAQ list 3");
        labelDAQcurrD3.setBorder(
            javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(),
                javax.swing.BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        labelDAQcurrD3.setEnabled(false);
        labelDAQcurrD3.setPreferredSize(new java.awt.Dimension(100, 20));

        labelRotator.setFont(new java.awt.Font("Courier New", 0, 11)); // NOI18N
        labelRotator.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelRotator.setText("-");
        labelRotator.setToolTipText("Receive indicator");
        labelRotator.setBorder(
            javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(),
                javax.swing.BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        labelRotator.setPreferredSize(new java.awt.Dimension(25, 20));

        BasicProgressBarUI progressBarUI = new BasicProgressBarUI() {
            @Override
            public void installUI(JComponent c) {
                super.installUI(c);
                setCellLength(3);
                setCellSpacing(1);
            }
        };
        lagIndicatorBar.setUI(progressBarUI);
        lagIndicatorBar.setMaximum(10);
        lagIndicatorBar.setOrientation(1);
        lagIndicatorBar.setToolTipText("Lagindicator");
        lagIndicatorBar.setAlignmentX(0.0F);
        lagIndicatorBar.setMaximumSize(new java.awt.Dimension(20, 20));
        lagIndicatorBar.setMinimumSize(new java.awt.Dimension(20, 20));
        lagIndicatorBar.setPreferredSize(new java.awt.Dimension(20, 20));

        labelDataLog.setForeground(new java.awt.Color(255, 255, 255));
        labelDataLog.setBorder(
            javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(),
                javax.swing.BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        labelDataLog.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        labelDataLog.setOpaque(true);
        labelDataLog.setPreferredSize(new java.awt.Dimension(295, 20));
        labelDataLog.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                labelDataLogMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(statusPanelLayout.createSequentialGroup().addGap(25, 25, 25)
                    .addComponent(labelConnected, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(labelError, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(busloadBar, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(labelDAQmax, javax.swing.GroupLayout.PREFERRED_SIZE, 73,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(4, 4, 4)
                    .addComponent(labelDAQD1, javax.swing.GroupLayout.PREFERRED_SIZE, 25,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(labelDAQcurrD1, javax.swing.GroupLayout.PREFERRED_SIZE, 50,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(labelDAQD2, javax.swing.GroupLayout.PREFERRED_SIZE, 25,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(labelDAQcurrD2, javax.swing.GroupLayout.PREFERRED_SIZE, 50,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(labelDAQD3, javax.swing.GroupLayout.PREFERRED_SIZE, 25,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(labelDAQcurrD3, javax.swing.GroupLayout.PREFERRED_SIZE, 50,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(labelRotator, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(lagIndicatorBar, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(labelDataLog, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(142, Short.MAX_VALUE)));

        statusPanelLayout.setVerticalGroup(statusPanelLayout
            .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup().addGap(5, 5, 5)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelDAQcurrD1, javax.swing.GroupLayout.PREFERRED_SIZE, 20,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelDAQD1, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelDAQD3, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelDAQcurrD3, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelDAQD2, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelDAQcurrD2, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelConnected, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelError, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(busloadBar, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelDAQmax, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelRotator, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lagIndicatorBar, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelDataLog, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))));

        lagIndicatorBar.getAccessibleContext().setAccessibleDescription("Lag indicator");


        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(layeredPane, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(statusPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 1308, Short.MAX_VALUE));
        layout
            .setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addComponent(layeredPane, javax.swing.GroupLayout.DEFAULT_SIZE, 518, Short.MAX_VALUE)
                    .addGap(0, 0, 0).addComponent(statusPanel, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void asap2ButtonActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_asap2ButtonActionPerformed
        showPanel(asap2Panel, !asap2Panel.isVisible());
    }// GEN-LAST:event_asap2ButtonActionPerformed

    private void layeredPaneComponentResized(java.awt.event.ComponentEvent evt) {// GEN-FIRST:event_layeredPaneComponentResized
        setSidePanelSize();
        adjustLayoutSize(sidePanelManager.isSidePanelVisible());
    }// GEN-LAST:event_layeredPaneComponentResized

    private void menuTabEditActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_menuTabEditActionPerformed
        String name = "";

        // show popup to enter applicationName of layout
        while (name != null && name.equals("")) {
            name
                    = showQuestion("Rename Tab", "Name:", tabbedPane.getTitleAt(tabbedPane.getSelectedIndex()));
        }
        if (name != null) {
            currentConfig.getHANtuneManager().renameTab(tabbedPane.getSelectedIndex(), name);
        }
    }// GEN-LAST:event_menuTabEditActionPerformed

    private void menuTabDeleteActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_menuTabDeleteActionPerformed
        // delete the specified tab
        if (HANtune.showConfirm("Are you sure you want to delete this Tab and all its content?") == 0) {
            currentConfig.getHANtuneManager().deleteTab(tabbedPane.getSelectedIndex());
        }
    }// GEN-LAST:event_menuTabDeleteActionPerformed

    private void menuTabAddActionPerformed(java.awt.event.ActionEvent evt) {
        currentConfig.getHANtuneManager().addTab();
    }

    private void formWindowClosing(java.awt.event.WindowEvent evt) {// GEN-FIRST:event_formWindowClosing
        // close current layout
        requestExitHANtune();
    }// GEN-LAST:event_formWindowClosing

    private void projectButtonActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_projectButtonActionPerformed
        showProjectPanel(!projectPanel.isVisible());
    }// GEN-LAST:event_projectButtonActionPerformed

    private void mCalibrationCreateActionPerformed(java.awt.event.ActionEvent evt) {
        CreateCalibration createCalib = new CreateCalibration(this);
        createCalib.createNewCalibration();
    }

    private void mCalibrationCreateFromSRecActionPerformed(ActionEvent evt) {
        CreateCalibration createCalib = new CreateCalibration(this);
        createCalib.doActionCreateCalibFromSRec();
    }

    private void mCalibrationCreateFromHexActionPerformed(ActionEvent evt) {
        CreateCalibration createCalib = new CreateCalibration(this);
        createCalib.doActionCreateCalibFromIHex();
    }

    private void mCalibrationCreateFromDCMActionPerformed(ActionEvent evt) {
        CreateCalibration createCalib = new CreateCalibration(this);
        createCalib.doActionCreateCalibFromDCM();
    }

    private void mASAP2LoadActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mASAP2LoadActionPerformed
        if (mASAP2Load.getText().equals("Load File")) {
            DefaultMutableTreeNode node
                    = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
            if (node == null) {
                return;
            }

            currentConfig.getHANtuneManager().loadASAP2(((ProjectInfo) node).getId());
        } else {
            DefaultMutableTreeNode node
                    = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
            if (node == null) {
                return;
            }

            if (((ProjectInfo) node).getId() == activeAsap2Id) {
                currentConfig.getHANtuneManager().loadASAP2(-1);
            } else {
                currentConfig.getHANtuneManager().loadASAP2(((ProjectInfo) node).getId());
            }
        }

    }// GEN-LAST:event_mASAP2LoadActionPerformed

    private void mASAP2AddActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mASAP2AddActionPerformed
        requestOpenASAP2();
    }// GEN-LAST:event_mASAP2AddActionPerformed

    private void mASAP2RemoveActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mASAP2RemoveActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }
        if (showConfirm("Are you sure you want to remove \"" + ((ProjectInfo) node).getName()
                + "\" from this project?") != 0) {
            return;
        }
        if (((ProjectInfo) node).getId() == activeAsap2Id) {
            mASAP2Load.setText("Load File");
        }
        currentConfig.getHANtuneManager().removeASAP2(((ProjectInfo) node).getId());
    }// GEN-LAST:event_mASAP2RemoveActionPerformed

    private void mLayoutRemoveActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mLayoutRemoveActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }
        if (showConfirm("Are you sure you want to remove \"" + ((ProjectInfo) node).getName()
                + "\" from this project?") != 0) {
            return;
        }
        currentConfig.getHANtuneManager().removeLayout(((ProjectInfo) node).getId());
    }// GEN-LAST:event_mLayoutRemoveActionPerformed

    private void mLayoutRenameActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mLayoutRenameActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }

        String name = "";

        // show popup to enter applicationName of DAQ list
        boolean validName = false;
        while (!validName) {
            name = HANtune.showQuestion("Rename layout", "Name:", "");
            validName = true;
            if (name.equals("")) {
                MessagePane.showError("Please enter a valid name");
                validName = false;
            }
            for (Layout HANtuneLayout : currentConfig.getHANtuneDocument().getHANtune().getLayoutList()) {
                if (HANtuneLayout.getTitle().equals(name)) {
                    MessagePane
                            .showError("A layout with this name already exists\n please choose another name");
                    validName = false;
                }
            }
        }
        currentConfig.getHANtuneManager().renameLayout(((ProjectInfo) node).getId(), name);
    }// GEN-LAST:event_mLayoutRenameActionPerformed

    private void mLayoutLoadActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mLayoutLoadActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }
        if (((ProjectInfo) node).getId() == activeLayoutId) {
            currentConfig.getHANtuneManager().loadLayout(-1);
        } else {
            currentConfig.getHANtuneManager().loadLayout(((ProjectInfo) node).getId());
        }
    }// GEN-LAST:event_mLayoutLoadActionPerformed

    private void mLayoutNewActionPerformed(java.awt.event.ActionEvent evt) {
        String name = HANtune.showQuestion("New layout", "Name:", "");
        if (name == null) {
            return; // cancel has been pressed.
        }

        currentConfig.createNewLayout(name);
    }

    private void mCalibrationRenameActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mCalibrationRenameActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }

        String name = "";
        // show popup to enter applicationName of DAQ list
        boolean validName = false;
        while (!validName) {
            name = HANtune.showQuestion("Rename calibration", "Name:", "");
            validName = true;
            if (name.equals("")) {
                MessagePane.showError("Please enter a valid name");
                validName = false;
            }
            for (Calibration calibration : currentConfig.getHANtuneDocument().getHANtune()
                    .getCalibrationList()) {
                if (calibration.getTitle().equals(name)) {
                    MessagePane.showError(
                            "A calibration with this name already exists\n please choose another name");
                    validName = false;
                }
            }
        }
        calibrationHandler.renameCalibration(((ProjectInfo) node).getId(), name);
        // update projectTree
        updateProjectTree();
    }// GEN-LAST:event_mCalibrationRenameActionPerformed

    private void mCalibrationRemoveActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mCalibrationRemoveActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }
        if (showConfirm("Are you sure you want to remove \"" + ((ProjectInfo) node).getName()
                + "\" from this project?") != 0) {
            return;
        }
        calibrationHandler.removeCalibration(((ProjectInfo) node).getId());
        // reload project
        updateProjectTree();
    }// GEN-LAST:event_mCalibrationRemoveActionPerformed

    private void mCalibrationLoadActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mCalibrationLoadActionPerformed
        if (activeAsap2Id == -1) {
            MessagePane.showInfo("Please open an ASAP2 file first!");
            return;
        }

        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }

        int id = ((ProjectInfo) node).getId();

        calibrationHandler.loadCalibration(id, this);
    }// GEN-LAST:event_mCalibrationLoadActionPerformed

    private void mCalibrationExportMatlabActionPerformed(java.awt.event.ActionEvent evt) {
        ExportCalibration exportCalib = new ExportCalibration(this);
        exportCalib.doActionExportCalibrationMatlab((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }

    private void mCalibrationExportDcmActionPerformed(java.awt.event.ActionEvent evt) {
        ExportCalibration exportCalib = new ExportCalibration(this);
        exportCalib.doActionExportCalibrationDCM((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }

    private void mCalibrationImportDcmActionPerformed(java.awt.event.ActionEvent evt) {
        ImportCalibration importCalib = new ImportCalibration(this);
        importCalib.doActionImportDCM((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }

    private void mCalibrationImportSRecActionPerformed(java.awt.event.ActionEvent evt) {
        ImportCalibration importCalib = new ImportCalibration(this);
        importCalib.doActionImportSRec((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }

    private void mCalibrationImportHexActionPerformed(java.awt.event.ActionEvent evt) {
        ImportCalibration importCalib = new ImportCalibration(this);
        importCalib.doActionImportHex((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }

    private void mCalibrationExportIntoSRecAndFlashActionPerformed(java.awt.event.ActionEvent evt) {
        ExportFlashSRecAction action = new ExportFlashSRecAction(this);
        action.handleExportFlashDialog((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }

    private void mCalibrationExportIntoIHexAndFlashActionPerformed(java.awt.event.ActionEvent evt) {
        ExportFlashIHexAction action = new ExportFlashIHexAction(this);
        action.handleExportFlashAction((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }

    private void mLayoutCopyActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mLayoutCopyActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }
        if (showConfirm("Are you sure you want to copy \"" + ((ProjectInfo) node).getName()
                + "\"") != 0) {
            return;
        }
        currentConfig.getHANtuneManager().copyLayout(((ProjectInfo) node).getId());
    }// GEN-LAST:event_mLayoutCopyActionPerformed

    private void mCalibrationCopyActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mCalibrationCopyActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }
        if (showConfirm("Are you sure you want to copy \"" + ((ProjectInfo) node).getName()
                + "\"") != 0) {
            return;
        }
        calibrationHandler.copyCalibration(((ProjectInfo) node).getId());
        // reload project
        updateProjectTree();
    }// GEN-LAST:event_mCalibrationCopyActionPerformed

    private void labelConnectedMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_labelConnectedMouseClicked
        if (evt.getButton() == MouseEvent.BUTTON1) {
            // toggle currentConfig.getXcp() connection on/off
            if (!isConnected()) {
                connectProtocol(true);
            } else {
                connectProtocol(false);
            }
        }
    }// GEN-LAST:event_labelConnectedMouseClicked

    private void labelDataLogMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getButton() == MouseEvent.BUTTON1) {
            // toggle datalog mode
            boolean newToggleValue = !mainMenuBar.dataLoggingIsSelected();
            mainMenuBar.dataLoggingSetSelected(newToggleValue);
            toggleDatalog(newToggleValue);
        }
    }

    private void labelErrorMouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_labelErrorMouseClicked
        if (isConnected() && currentConfig.getXcpsettings().isErrorMonitoringEnabled()) {
            if (currentConfig.getXcpsettings().isErrorMonitoringSupported()) {
                currentConfig.getHANtuneManager().addWindowToTab(tabbedPane.getSelectedIndex(),
                        HANtuneWindowType.ErrorViewer);
            } else {
                MessagePane.showInfo("Controller does not support error monitoring.");
            }
        } else {
            MessagePane.showInfo("Please reconnect with error monitoring checked");
        }
    }// GEN-LAST:event_labelErrorMouseClicked

    private void mCalibrationUpdateActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mCalibrationUpdateActionPerformed
        if (activeAsap2Id == -1) {
            MessagePane.showInfo("Please open an ASAP2 file first!");
            return;
        }

        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }

        calibrationHandler.updateCalibration(((ProjectInfo) node).getId());
        // reload projecttree
        updateProjectTree();
    }// GEN-LAST:event_mCalibrationUpdateActionPerformed

    private void mDaqlistNewActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mDaqlistNewActionPerformed
        DaqlistAction daqlistAction = new DaqlistAction(this);
        daqlistAction.daqlistCreate();
    }// GEN-LAST:event_mDaqlistNewActionPerformed

    private void mDaqlistLoadActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mDaqlistLoadActionPerformed
        DaqlistAction daqlistAction = new DaqlistAction(this);
        daqlistAction.daqlistToggleLoad((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }// GEN-LAST:event_mDaqlistLoadActionPerformed

    private void mDaqlistRenameActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mDaqlistRenameActionPerformed
        DaqlistAction daqlistAction = new DaqlistAction(this);
        daqlistAction.daqlistRename((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }// GEN-LAST:event_mDaqlistRenameActionPerformed

    private void mDaqlistCopyActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mDaqlistCopyActionPerformed
        DaqlistAction daqlistAction = new DaqlistAction(this);
        daqlistAction.daqlistCopy((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }// GEN-LAST:event_mDaqlistCopyActionPerformed

    private void mDaqlistRemoveActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mDaqlistRemoveActionPerformed
        DaqlistAction daqlistAction = new DaqlistAction(this);
        daqlistAction.daqlistRemove((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }// GEN-LAST:event_mDaqlistRemoveActionPerformed

    private void mDaqlistModifyActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mDaqlistModifyActionPerformed
        DaqlistAction daqlistAction = new DaqlistAction(this);
        daqlistAction.daqlistModify((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }// GEN-LAST:event_mDaqlistModifyActionPerformed

    private void loadDbcActionPerformed(java.awt.event.ActionEvent evt) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        File dbcFile = ((ProjectInfo) node).getFile();
        if (!currentConfig.hasDescriptionFile(dbcFile)) {
            currentConfig.getHANtuneManager().loadDbcFile(dbcFile);
        } else {
            currentConfig.getHANtuneManager().unLoadDbcFile(dbcFile);
        }
        updateProjectTree();
        currentConfig.getHANtuneManager().updateAllWindows();
    }

    private void importDbcActionPerformed(java.awt.event.ActionEvent evt) {
        CustomFileChooser fileChooser = new CustomFileChooser("Import", FileType.DBC_FILE);
        File dbcFile = fileChooser.chooseOpenDialog(this, "Import");

        if (dbcFile != null) {
            currentConfig.getHANtuneManager().newDbc(dbcFile);
            showProjectPanel(true);
        }
    }

    private void removeDbcActionPerformed(java.awt.event.ActionEvent evt) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (showConfirm("Are you sure you want to remove \"" + ((ProjectInfo) node).getName()
                + "\" from this project?") == 0) {
            String dbcId = ((ProjectInfo) node).getFile().getName();
            currentConfig.getHANtuneManager().removeDbcFile(dbcId);
        }
    }

    private void dbcButtonActionPerformed(java.awt.event.ActionEvent evt) {
        showPanel(dbcPanel, !dbcPanel.isVisible());
    }

    public void daqListReceivedUpdate(long arg) {
        stepRotator();

        if (daqListReceiveTimoutTimer != null) {
            cancelConnectionTimoutTimer();
            daqListReceiveTimoutTimer.reset(false);
        }
    }

    private void scriptedElementsButtonActionPerformed() {
        showPanel(scriptedElementsPanel, !scriptedElementsPanel.isVisible());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
        arguments = args;
        HANtuneSplash.splashInit();

        /* Closes the splash art
        if (HANtuneSplash != null) {
            HANtuneSplash.close();
        }*/
        try {
            Thread.sleep(1500);
        } catch (InterruptedException ex) {
            AppendToLogfile.appendError(Thread.currentThread(), ex);
            System.out.println(ex.toString());
        }

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
                HANtuneManager manager = new HANtuneManager(HANtune.getInstance());
                manager.startHantune();
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPopupMenu asap2PopupMenu;
    private javax.swing.JPopupMenu asap2sPopupMenu;
    private javax.swing.JProgressBar busloadBar;
    private javax.swing.JPopupMenu calibrationPopupMenu;
    private javax.swing.JPopupMenu calibrationsPopupMenu;
    private javax.swing.JPopupMenu workingParameterPopupMenu;
    private javax.swing.JPopupMenu daqlistsPopupMenu;
    private javax.swing.JPopupMenu daqlistPopupMenu;
    private javax.swing.JPopupMenu dbcPopupMenu;
    private javax.swing.JPopupMenu dbcsPopupMenu;
    private javax.swing.JLabel labelConnected;
    private javax.swing.JLabel labelDAQD1;
    private javax.swing.JLabel labelDAQD2;
    private javax.swing.JLabel labelDAQD3;
    private javax.swing.JLabel labelDAQcurrD1;
    private javax.swing.JLabel labelDAQcurrD2;
    private javax.swing.JLabel labelDAQcurrD3;
    private javax.swing.JLabel labelDAQmax;
    private javax.swing.JLabel labelDataLog;
    public javax.swing.JLabel labelError;
    private javax.swing.JLabel labelRotator;
    private javax.swing.JProgressBar lagIndicatorBar;
    private javax.swing.JLayeredPane layeredPane;
    private javax.swing.JPopupMenu layoutPopupMenu;
    private javax.swing.JPopupMenu layoutsPopupMenu;
    private javax.swing.JMenuItem mASAP2Add;
    private javax.swing.JMenuItem mASAP2Load;
    private javax.swing.JMenuItem mASAP2Remove;
    private javax.swing.JMenuItem mCalibrationCopy;
    private javax.swing.JMenuItem mCalibrationSubExport;
    private javax.swing.JMenuItem mCalibrationExportMatlab;
    private javax.swing.JMenuItem mCalibrationExportDcm;
    private javax.swing.JMenuItem mCalibrationSubImport;
    private javax.swing.JMenuItem mCalibrationImportDcm;
    private javax.swing.JMenuItem mCalibrationImportSRec;
    private javax.swing.JMenuItem mCalibrationImportIHex;
    private javax.swing.JMenuItem mCalibrationWriteIntoAndFlashSRec;
    private javax.swing.JMenuItem mCalibrationWriteIntoAndFlashIHex;
    private javax.swing.JMenuItem mCalibrationLoad;
    private javax.swing.JMenuItem mCalibrationCreate;
    private javax.swing.JMenuItem mCalibrationCreateFromSRec;
    private javax.swing.JMenuItem mCalibrationCreateFromIHex;
    private javax.swing.JMenuItem mCalibrationCreateFromDCM;
    private javax.swing.JMenuItem mWorkingParameterCreate;
    private javax.swing.JMenuItem mCalibrationRemove;
    private javax.swing.JMenuItem mCalibrationRename;
    private javax.swing.JMenuItem mCalibrationUpdate;
    private javax.swing.JMenuItem mDaqlistCopy;
    private javax.swing.JMenuItem mDaqlistLoad;
    private javax.swing.JMenuItem mDaqlistModify;
    private javax.swing.JMenuItem mDaqlistNew;
    private javax.swing.JMenuItem mDaqlistRemove;
    private javax.swing.JMenuItem mDaqlistRename;
    private javax.swing.JMenuItem mDbcAdd;
    private javax.swing.JMenuItem mDbcLoad;
    private javax.swing.JMenuItem mDbcRemove;
    private javax.swing.JMenuItem mLayoutCopy;
    private javax.swing.JMenuItem mLayoutLoad;
    private javax.swing.JMenuItem mLayoutNew;
    private javax.swing.JMenuItem mLayoutRemove;
    private javax.swing.JMenuItem mLayoutRename;
    private javax.swing.JMenuItem menuTabDelete;
    private javax.swing.JMenuItem menuTabEdit;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JPopupMenu tabPopupMenu;
    private javax.swing.JTabbedPane tabbedPane;
    // End of variables declaration//GEN-END:variables
}
