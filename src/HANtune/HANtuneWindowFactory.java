/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import nl.han.hantune.gui.windows.im.ImageManipulator;
import ErrorMonitoring.ErrorViewer;
import nl.han.hantune.gui.windows.ScriptWindow;
import HANtune.editors.ButtonEditor;
import HANtune.editors.multiEditor.MultiEditor;
import HANtune.editors.SliderEditor;
import HANtune.editors.TableEditor;
import HANtune.editors.RadioButtonEditor;
import HANtune.markup.ImageMarkup;
import HANtune.markup.TextMarkup;
import HANtune.viewers.*;
import java.util.EnumSet;

/**
 *
 * @author JRikkers
 */
public class HANtuneWindowFactory{

    public enum HANtuneWindowType {
        ButtonEditor, MultiEditor, RadioButtonEditor, SliderEditor, TableEditor,
        BarViewer, BooleanViewer, DigitalViewer, GaugeViewer, LedViewer, MultiLedViewer,
        MultiViewer, ScopeViewer, TextViewer, TextMarkup, ImageMarkup, ErrorViewer, NewErrorViewer, ScriptWindow, ImageManipulator
    };
    
    public static final EnumSet<HANtuneWindowType> HANtuneEditorType = EnumSet.of(
            HANtuneWindowType.ButtonEditor,
            HANtuneWindowType.MultiEditor,
            HANtuneWindowType.RadioButtonEditor,
            HANtuneWindowType.SliderEditor);
    public static final EnumSet<HANtuneWindowType> HANtuneTableEditorType = EnumSet.of(
            HANtuneWindowType.TableEditor);
    public static final EnumSet<HANtuneWindowType> HANtuneMarkupType = EnumSet.of(
            HANtuneWindowType.TextMarkup,
            HANtuneWindowType.ImageMarkup);
    public static final EnumSet<HANtuneWindowType> HANtuneViewerType = EnumSet.of(
            HANtuneWindowType.BarViewer,
            HANtuneWindowType.BooleanViewer,            
            HANtuneWindowType.DigitalViewer,
            HANtuneWindowType.GaugeViewer,
            HANtuneWindowType.LedViewer,
            HANtuneWindowType.MultiLedViewer,
            HANtuneWindowType.MultiViewer,            
            HANtuneWindowType.ScopeViewer,
            HANtuneWindowType.TextViewer,
            HANtuneWindowType.ImageManipulator);
    public static final EnumSet<HANtuneWindowType> HANtuneMultiViewerType = EnumSet.of(
            HANtuneWindowType.MultiLedViewer,
            HANtuneWindowType.MultiViewer,
            HANtuneWindowType.ScopeViewer,
            HANtuneWindowType.ImageManipulator);
    
    public static final EnumSet<HANtuneWindowType> HANtuneMultiEditorType = HANtuneEditorType;
    private final static HANtuneWindowFactory instance = new HANtuneWindowFactory();

    private HANtuneWindowFactory() {
    }

    public static HANtuneWindowFactory getInstance() {
        return instance;
    }

    public HANtuneWindow createHANtuneWindow(HANtuneWindowType hantuneWindowType) {
        HANtuneWindow hantuneWindow = null;
        switch (hantuneWindowType) {
            case MultiEditor: {
                hantuneWindow = new MultiEditor();
                break;
            }
            case TableEditor: {
                hantuneWindow = new TableEditor();
                break;
            }
            case SliderEditor: {
                hantuneWindow = new SliderEditor();
                break;
            }
            case ButtonEditor: {
                hantuneWindow = new ButtonEditor();
                break;
            }
            case RadioButtonEditor: {
                hantuneWindow = new RadioButtonEditor();
                break;
            }
            case MultiViewer: {
                hantuneWindow = new MultiViewer();
                break;
            }
            case DigitalViewer: {
                hantuneWindow = new DigitalViewer();
                break;
            }
            case BooleanViewer: {
                hantuneWindow = new BooleanViewer();
                break;
            }
            case BarViewer: {
                hantuneWindow = new BarViewer();
                break;
            }
            case GaugeViewer: {
                hantuneWindow = new GaugeViewer();
                break;
            }
            case ScopeViewer: {
                hantuneWindow = new ScopeViewer();
                break;
            }
            case MultiLedViewer: {
                hantuneWindow = new MultiLedViewer();
                break;
            }
            case LedViewer: {
                hantuneWindow = new LedViewer();
                break;
            }
            case TextViewer: {
                hantuneWindow = new TextViewer();
                break;
            }
            case TextMarkup: {
                hantuneWindow = new TextMarkup();
                break;
            }
            case ImageMarkup: {
                hantuneWindow = new ImageMarkup();
                break;
            }
            case ErrorViewer: {
                hantuneWindow = new ErrorViewer();
                break;
            }
            case ScriptWindow: {
                hantuneWindow = new ScriptWindow();
                break;
            }
            case ImageManipulator:{
                hantuneWindow = new ImageManipulator();
                break;
            }
        }
        return hantuneWindow;
    }

    public static HANtuneWindowType getHANtuneWindowType(HANtuneWindow hantuneWindow) {
        HANtuneWindowType hantuneWindowType = null;
        if (hantuneWindow instanceof MultiEditor) {
            hantuneWindowType = HANtuneWindowType.MultiEditor;
        } else if (hantuneWindow instanceof TableEditor) {
            hantuneWindowType = HANtuneWindowType.TableEditor;
        } else if (hantuneWindow instanceof SliderEditor) {
            hantuneWindowType = HANtuneWindowType.SliderEditor;
        } else if (hantuneWindow instanceof ButtonEditor) {
            hantuneWindowType = HANtuneWindowType.ButtonEditor;
        } else if (hantuneWindow instanceof RadioButtonEditor) {
            hantuneWindowType = HANtuneWindowType.RadioButtonEditor;
        } else if (hantuneWindow instanceof MultiViewer) {
            hantuneWindowType = HANtuneWindowType.MultiViewer;
        } else if (hantuneWindow instanceof DigitalViewer) {
            hantuneWindowType = HANtuneWindowType.DigitalViewer;
        } else if (hantuneWindow instanceof BooleanViewer) {
            hantuneWindowType = HANtuneWindowType.BooleanViewer;
        } else if (hantuneWindow instanceof BarViewer) {
            hantuneWindowType = HANtuneWindowType.BarViewer;
        } else if (hantuneWindow instanceof GaugeViewer) {
            hantuneWindowType = HANtuneWindowType.GaugeViewer;
        } else if (hantuneWindow instanceof ScopeViewer) {
            hantuneWindowType = HANtuneWindowType.ScopeViewer;
        } else if (hantuneWindow instanceof MultiLedViewer) {
            hantuneWindowType = HANtuneWindowType.MultiLedViewer;
        } else if (hantuneWindow instanceof LedViewer) {
            hantuneWindowType = HANtuneWindowType.LedViewer;
        } else if (hantuneWindow instanceof TextViewer) {
            hantuneWindowType = HANtuneWindowType.TextViewer;
        } else if (hantuneWindow instanceof TextMarkup) {
            hantuneWindowType = HANtuneWindowType.TextMarkup;
        } else if (hantuneWindow instanceof ImageMarkup) {
            hantuneWindowType = HANtuneWindowType.ImageMarkup;
        } else if (hantuneWindow instanceof ErrorViewer) {
            hantuneWindowType = HANtuneWindowType.ErrorViewer;
        } else if (hantuneWindow instanceof ScriptWindow) {
            hantuneWindowType = HANtuneWindowType.ScriptWindow;
        } else if (hantuneWindow instanceof ImageManipulator) {
            hantuneWindowType = HANtuneWindowType.ImageManipulator;
        }
        return hantuneWindowType;
    }
}
