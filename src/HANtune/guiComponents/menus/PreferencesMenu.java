/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.guiComponents.menus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;

import HANtune.HANtune;
import HANtune.UserPreferences;
import datahandling.CurrentConfig;

@SuppressWarnings("serial")
public class PreferencesMenu extends JMenu {

    private MenuItem userPreferencesMenuItem;


    public PreferencesMenu(HANtune hantune) {
        setText("Preferences");
        setMnemonic('P');

        userPreferencesMenuItem =
            new MenuItem.Builder("User preferences").actionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    userPreferencesMenuItemActionPerformed(evt);
                }
            }).build();
        add(userPreferencesMenuItem);
    }


    public void updateItemsServiceModeVisibility() {
        this.setVisible(!CurrentConfig.getInstance().isServiceToolMode());
    }


    private void userPreferencesMenuItemActionPerformed(ActionEvent evt) {
        UserPreferences pref = new UserPreferences();
        pref.setVisible(true);
    }


}
