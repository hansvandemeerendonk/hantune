/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.guiComponents.menus;

import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.swing.JMenu;

import ErrorLogger.AppendToLogfile;
import HANtune.About;
import HANtune.HANtune;
import util.MessagePane;
import util.Util;

@SuppressWarnings("serial")
public class HelpMenu extends JMenu {
    private HANtune hanTune;

    private MenuItem manualMenuItem;
    private MenuItem licenseMenuItem;
    private MenuItem noticeMenuItem;
    private MenuItem aboutMenuItem;


    public HelpMenu(HANtune hantune) {
        this.hanTune = hantune;
        setText("Help");
        setMnemonic('H');

        manualMenuItem = new MenuItem.Builder("User Manual").acceleratorKey(KeyEvent.VK_F1, 0)
            .actionListener(evt -> manualMenuItemActionPerformed()).build();
        add(manualMenuItem);

        licenseMenuItem = new MenuItem.Builder("End-User License Agreement")
            .actionListener(evt -> Util.openFile("LICENSE.txt")).build();
        add(licenseMenuItem);

        noticeMenuItem = new MenuItem.Builder("Third-party Notices")
                .actionListener(evt -> Util.openFile("NOTICE.txt")).build();
        add(noticeMenuItem);

        aboutMenuItem =
            new MenuItem.Builder("About").actionListener(evt -> aboutMenuItemActionPerformed()).build();
        add(aboutMenuItem);
    }

    private void manualMenuItemActionPerformed() {
        try {
            Process p =
                Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler HANtune_manual.pdf");
            p.waitFor();
        } catch (IOException | InterruptedException e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            MessagePane.showError("Could not open User Manual");
        }
    }

    private void aboutMenuItemActionPerformed() {
        About abt = new About();
        abt.setVisible(true);
    }
}
