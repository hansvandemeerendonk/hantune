/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import ErrorLogger.AppendToLogfile;
import datahandling.Signal;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import util.MessagePane;



/**
 *  Displays a dialog in which the user can select the limits and colors used by the viewer or signal in a viewer that use this.
 *  The selections are saved and fetched to and from settings, so no special handling is required in the viewer.
 * @author Sander
 */
@SuppressWarnings("serial")
public class SubrangeDialog extends HanTuneDialog {
    
    private HANtuneWindow hantuneWindow = null;
    private int minimumRanges = 0;
    private int maximumRanges = 0;
    private Double upperConstraint=null;
    private Double lowerConstraint=null;
    private boolean ignoreDisplayRange;
    private List<JSpinner> limitSpinners = new ArrayList<>();
            
    /**
     * The array where the colors are stored, these are not always equal to the colors of the color fields
     */
    Color[] colors = new Color[5];
    /**
     * The array where the limits are stored, these are not always equal to the values in the spinners.
     */
    Double[] limits = new Double[4];
    /**
     * The array where the initial spinner values are stored.
     */
    double[] initSpinnerValue = {0, 0, 0, 0};
    int usedRanges = 0;
    private Signal signal;
    
     /**
     * A return status code - returned if Cancel button has been pressed
     */
    public static final int RET_CANCEL = 0;
    /**
     * A return status code - returned if OK button has been pressed
     */
    public static final int RET_OK = 1; 
    
    /**
     * The constructor for the LimitsAndColorsDialog class
     * @param hantuneWindow The window that called the dialog
     * @param signal The signal for which the limits and colors will be set (and read)
     * @param minimumRanges The minimum amount of ranges for this instance
     * @param maximumRanges The maximum amount of ranges for this instance
     * @param ignoreDisplayRange
     */
    public SubrangeDialog(HANtuneWindow hantuneWindow, Signal signal, int minimumRanges, int maximumRanges, boolean ignoreDisplayRange) {
        this.hantuneWindow = hantuneWindow;
        this.signal=signal;
        this.minimumRanges=minimumRanges;
        this.maximumRanges=maximumRanges;
        this.ignoreDisplayRange=ignoreDisplayRange;
        // use System LookAndFeel
        String nativeLF = UIManager.getSystemLookAndFeelClassName();
        try {
            UIManager.setLookAndFeel(nativeLF);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
        }

        // set program window icon
        Image img = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/HANlogo.png"));
        this.setIconImage(img);
        usedRanges = minimumRanges;
        getSettings();
        initComponents();
        for(int loopcount=0; loopcount<usedRanges; loopcount++){
            addRange();
        }
        if(colors[0]==null){
            setDefaultColors();
        }
        if(usedRanges <= minimumRanges){
            removeRangeButton.setEnabled(false);
        }
        setTitle(HANtune.applicationName + " - Ranges");
        setLocationRelativeTo(null);
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            private static final long serialVersionUID = 7526471155622776160L;
            @Override
            public void actionPerformed(ActionEvent e) {
                doClose(RET_CANCEL);
            }
        });
    }
    
    /**
     * @return the return status of this dialog - one of RET_OK or RET_CANCEL
     */
    public int getReturnStatus() {
        return returnStatus;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        addRangeButton = new javax.swing.JButton();
        editColor0 = new javax.swing.JButton();
        editColor1 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        editColor2 = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        removeRangeButton = new javax.swing.JButton();
        editColor3 = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        editColor4 = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        defaultColorsButton = new javax.swing.JButton();
        colorField0 = new javax.swing.JPanel();
        colorField1 = new javax.swing.JPanel();
        colorField2 = new javax.swing.JPanel();
        colorField3 = new javax.swing.JPanel();
        colorField4 = new javax.swing.JPanel();
        limitSpinner0 = new javax.swing.JSpinner();
        limitSpinners.add(limitSpinner0);
        limitSpinner1 = new javax.swing.JSpinner();
        limitSpinners.add(limitSpinner1);
        limitSpinner2 = new javax.swing.JSpinner();
        limitSpinners.add(limitSpinner2);
        limitSpinner3 = new javax.swing.JSpinner();
        limitSpinners.add(limitSpinner3);
        limitLabel0 = new javax.swing.JLabel();
        limitLabel1 = new javax.swing.JLabel();
        limitLabel2 = new javax.swing.JLabel();
        limitLabel3 = new javax.swing.JLabel();

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
                
            }
        });

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        addRangeButton.setText("Add range");
        addRangeButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addRangeButtonActionPerformed(evt);
            }
        });

        editColor0.setText("Edit color");
        editColor0.setEnabled(false);
        editColor0.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editColor0ActionPerformed(evt);
            }
        });

        editColor1.setText("Edit color");
        editColor1.setEnabled(false);
        editColor1.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editColor1ActionPerformed(evt);
            }
        });

        editColor2.setText("Edit color");
        editColor2.setEnabled(false);
        editColor2.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editColor2ActionPerformed(evt);
            }
        });

        removeRangeButton.setText("Remove range");
        removeRangeButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeRangeButtonActionPerformed(evt);
            }
        });

        editColor3.setText("Edit color");
        editColor3.setEnabled(false);
        editColor3.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editColor3ActionPerformed(evt);
            }
        });

        editColor4.setText("Edit color");
        editColor4.setEnabled(false);
        editColor4.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editColor4ActionPerformed(evt);
            }
        });

        defaultColorsButton.setText("Set default colors");
        defaultColorsButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                defaultColorsButtonActionPerformed(evt);
            }
        });

        
        if(colors[0]==null){
            colorField0.setBackground(Color.gray);
        }
        else{
            colorField0.setBackground(colors[0]);
        }

        javax.swing.GroupLayout colorField0Layout = new javax.swing.GroupLayout(colorField0);
        colorField0.setLayout(colorField0Layout);
        colorField0Layout.setHorizontalGroup(
            colorField0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        colorField0Layout.setVerticalGroup(
            colorField0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        if(colors[1]==null){
            colorField1.setBackground(Color.gray);
        }
        else{
            colorField1.setBackground(colors[1]);
        }

        javax.swing.GroupLayout colorField1Layout = new javax.swing.GroupLayout(colorField1);
        colorField1.setLayout(colorField1Layout);
        colorField1Layout.setHorizontalGroup(
            colorField1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        colorField1Layout.setVerticalGroup(
            colorField1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        
        if(colors[2]==null){
            colorField2.setBackground(Color.gray);
        }
        else{
            colorField2.setBackground(colors[2]);
        }
        javax.swing.GroupLayout colorField2Layout = new javax.swing.GroupLayout(colorField2);
        colorField2.setLayout(colorField2Layout);
        colorField2Layout.setHorizontalGroup(
            colorField2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        colorField2Layout.setVerticalGroup(
            colorField2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        if(colors[3]==null){
            colorField3.setBackground(Color.gray);
        }
        else{
            colorField3.setBackground(colors[3]);
        }

        javax.swing.GroupLayout colorField3Layout = new javax.swing.GroupLayout(colorField3);
        colorField3.setLayout(colorField3Layout);
        colorField3Layout.setHorizontalGroup(
            colorField3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        colorField3Layout.setVerticalGroup(
            colorField3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        if(colors[4]==null){
            colorField4.setBackground(Color.gray);
        }
        else{
            colorField4.setBackground(colors[4]);
        }

        javax.swing.GroupLayout colorField4Layout = new javax.swing.GroupLayout(colorField4);
        colorField4.setLayout(colorField4Layout);
        colorField4Layout.setHorizontalGroup(
            colorField4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        colorField4Layout.setVerticalGroup(
            colorField4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        limitSpinner0.setEnabled(false);
        limitSpinner0.setModel(new javax.swing.SpinnerNumberModel(initSpinnerValue[0], null, null, Double.valueOf(1.0d)));
        limitSpinner0.setEditor(new javax.swing.JSpinner.NumberEditor(limitSpinner0, "#.#"));
        limitSpinner0.addChangeListener(new javax.swing.event.ChangeListener() {
            @Override
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                limitSpinner0StateChanged(evt);
            }
        });

        limitSpinner1.setEnabled(false);
        limitSpinner1.setModel(new javax.swing.SpinnerNumberModel(initSpinnerValue[1], null, null, Double.valueOf(1.0d)));
        limitSpinner1.setEditor(new javax.swing.JSpinner.NumberEditor(limitSpinner1, "#.#"));
        limitSpinner1.addChangeListener(new javax.swing.event.ChangeListener() {
            @Override
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                limitSpinner1StateChanged(evt);
            }
        });
        
        limitSpinner2.setEnabled(false);
        limitSpinner2.setModel(new javax.swing.SpinnerNumberModel(initSpinnerValue[2], null, null, Double.valueOf(1.0d)));
        limitSpinner2.setEditor(new javax.swing.JSpinner.NumberEditor(limitSpinner2, "#.#"));
        limitSpinner2.addChangeListener(new javax.swing.event.ChangeListener() {
            @Override
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                limitSpinner2StateChanged(evt);
            }
        });
        
        limitSpinner3.setEnabled(false);
        limitSpinner3.setModel(new javax.swing.SpinnerNumberModel(initSpinnerValue[3], null, null, Double.valueOf(1.0d)));
        limitSpinner3.setEditor(new javax.swing.JSpinner.NumberEditor(limitSpinner3, "#.#"));
        limitSpinner3.addChangeListener(new javax.swing.event.ChangeListener() {
            @Override
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                limitSpinner3StateChanged(evt);
            }
        });
        
        limitLabel0.setText("Limit 1");
        limitLabel0.setEnabled(false);

        limitLabel1.setText("Limit 2");
        limitLabel1.setEnabled(false);

        limitLabel2.setText("Limit 3");
        limitLabel2.setEnabled(false);

        limitLabel3.setText("Limit 4");
        limitLabel3.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jSeparator3))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(colorField1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(colorField0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(colorField2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(colorField4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(editColor0, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(editColor1))
                            .addComponent(editColor2)
                            .addComponent(editColor4)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(limitSpinner3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(limitSpinner2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(limitSpinner1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(limitSpinner0, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(editColor3)
                                    .addComponent(limitLabel0)
                                    .addComponent(limitLabel1)
                                    .addComponent(limitLabel2)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(limitLabel3))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(colorField3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(83, 83, 83))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cancelButton))
                            .addComponent(defaultColorsButton, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(addRangeButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(removeRangeButton))))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jSeparator1))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jSeparator2))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jSeparator5))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jSeparator4)))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cancelButton, okButton});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(editColor0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(colorField0, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(limitSpinner0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(limitLabel0))
                .addGap(9, 9, 9)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(editColor1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(colorField1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 3, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(limitSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(limitLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(editColor2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(colorField2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(limitSpinner2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(limitLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(editColor3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(colorField3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(limitSpinner3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(limitLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(editColor4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(colorField4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.DEFAULT_SIZE, 1, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addRangeButton)
                    .addComponent(removeRangeButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(defaultColorsButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton)
                    .addComponent(okButton))
                .addGap(10, 10, 10))
        );

        getRootPane().setDefaultButton(okButton);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        doClose(RET_OK);
    }//GEN-LAST:event_okButtonActionPerformed
    
    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        doClose(RET_CANCEL);
    }//GEN-LAST:event_cancelButtonActionPerformed

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        doClose(RET_CANCEL);
    }//GEN-LAST:event_closeDialog

    private void editColor1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editColor1ActionPerformed
        Color newColor = JColorChooser.showDialog(SubrangeDialog.this,"Choose color 2", colorField1.getBackground());
        if(! (newColor==null)){
        colors[1] = newColor;
        colorField1.setBackground(newColor);
        }
    }//GEN-LAST:event_editColor1ActionPerformed

    private void editColor2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editColor2ActionPerformed
        Color newColor = JColorChooser.showDialog(SubrangeDialog.this,"Choose color 3", colorField2.getBackground());
        if(! (newColor==null)){
        colors[2] = newColor;
        colorField2.setBackground(newColor);
        }
    }//GEN-LAST:event_editColor2ActionPerformed

    private void editColor4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editColor4ActionPerformed
        Color newColor = JColorChooser.showDialog(SubrangeDialog.this,"Choose color 5", colorField4.getBackground());
        if(! (newColor==null)){
        colors[4] = newColor;
        colorField4.setBackground(newColor);
        }
    }//GEN-LAST:event_editColor4ActionPerformed

    private void editColor0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editColor0ActionPerformed
        Color newColor = JColorChooser.showDialog(SubrangeDialog.this,"Choose color 1", colorField0.getBackground());
        if(! (newColor==null)){
        colors[0] = newColor;
        colorField0.setBackground(newColor);
        }
    }//GEN-LAST:event_editColor0ActionPerformed

    private void editColor3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editColor3ActionPerformed
        Color newColor = JColorChooser.showDialog(SubrangeDialog.this,"Choose color 4", colorField3.getBackground());
        if(! (newColor==null)){
        colors[3] = newColor;
        colorField3.setBackground(newColor);
        }
    }//GEN-LAST:event_editColor3ActionPerformed

    private void addRangeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addLimitButtonActionPerformed
            addRange();
            usedRanges++;
    }//GEN-LAST:event_addLimitButtonActionPerformed
    
    /**
     * Adds a limits by enabling the next limits spinner and edit colors button. 
     * Does not raise usedRanges, this should be handled elsewhere.
     * Disables addRangeButton when maximumRanges is reached.
     */
    private void addRange(){
        if(!limitSpinner0.isEnabled()){
            limitSpinner0.setEnabled(true);
            editColor0.setEnabled(true);
            editColor1.setEnabled(true);
            limitLabel0.setEnabled(true);
            removeRangeButton.setEnabled(true);
            if(maximumRanges==1) addRangeButton.setEnabled(false);
        }
        else{
            if(!limitSpinner1.isEnabled()){
                limitSpinner1.setEnabled(true);
                editColor2.setEnabled(true);
                limitLabel1.setEnabled(true);
                removeRangeButton.setEnabled(true);
                if(maximumRanges==2) addRangeButton.setEnabled(false);
            }
            else{
                if(!limitSpinner2.isEnabled()){
                    limitSpinner2.setEnabled(true);
                    editColor3.setEnabled(true);
                    limitLabel2.setEnabled(true);
                    removeRangeButton.setEnabled(true);
                    if(maximumRanges==3) addRangeButton.setEnabled(false);
                }
                else{
                    if(!limitSpinner3.isEnabled()){
                        limitSpinner3.setEnabled(true);
                        editColor4.setEnabled(true);
                        limitLabel3.setEnabled(true);
                        removeRangeButton.setEnabled(true);
                        addRangeButton.setEnabled(false);
                    }   
                }
            }
        }
    }
    

    private void removeRangeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeLimitbuttonActionPerformed
        removeRange();
    }//GEN-LAST:event_removeLimitbuttonActionPerformed

    /**
     * Removes a the highest limits by disabling the limitspinner and edit colors button. 
     * Lowers usedRanges by 1, writes a null to the respective colors and limits and sets the corresponding colorField to gray. 
     * Disables the remove limits button when the last limits has been removed.
     */
    private void removeRange(){
        if(!addRangeButton.isEnabled()){
            addRangeButton.setEnabled(true);
        }
        if(limitSpinner3.isEnabled() && minimumRanges<4){
            limitSpinner3.setEnabled(false);
            editColor4.setEnabled(false);
            limitLabel3.setEnabled(false);
            colorField4.setBackground(Color.gray);
            colors[4]=null;
            limits[3]=null;
            if(minimumRanges==3) removeRangeButton.setEnabled(false);
            usedRanges--;
        }
        else{
            if(limitSpinner2.isEnabled() && minimumRanges<3){
                limitSpinner2.setEnabled(false);
                editColor3.setEnabled(false);
                limitLabel2.setEnabled(false);
                colorField3.setBackground(Color.gray);
                colors[3]=null;
                limits[2]=null;
                if(minimumRanges==2) removeRangeButton.setEnabled(false);
                usedRanges--;
            }
            else{
                if(limitSpinner1.isEnabled() && minimumRanges<2){
                    limitSpinner1.setEnabled(false);
                    editColor2.setEnabled(false);
                    limitLabel1.setEnabled(false);
                    colorField2.setBackground(Color.gray);
                    colors[2]=null;
                    limits[1]=null;
                    if(minimumRanges==1)removeRangeButton.setEnabled(false);
                    usedRanges--;
                }
                else{
                    if(limitSpinner0.isEnabled() && minimumRanges<1){
                        limitSpinner0.setEnabled(false);
                        editColor0.setEnabled(false);
                        editColor1.setEnabled(false);
                        limitLabel0.setEnabled(false);
                        colorField1.setBackground(Color.gray);
                        colorField0.setBackground(Color.gray);
                        colors[1]=null;
                        colors[0]=null;
                        limits[0]=null;
                        removeRangeButton.setEnabled(false);
                        usedRanges--;
                    }
                }
            }
        }
    }

    private void defaultColorsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        setDefaultColors();       
    }//GEN-LAST:event_jButton1ActionPerformed
    
    /**
     * Sets the default colors. 
     * Default colors can be changed here. This is not advisable.
     */
    private void setDefaultColors(){
        if(usedRanges==1){
            colorField0.setBackground(Color.green);
            colors[0]=Color.green;
            colorField1.setBackground(Color.red);
            colors[1]=Color.red;
        }
        if(usedRanges==2){
            colorField0.setBackground(Color.blue);
            colors[0]=Color.blue;
            colorField1.setBackground(Color.green);
            colors[1]=Color.green;
            colorField2.setBackground(Color.red);
            colors[2]=Color.red;
        }
        if(usedRanges==3){
            colorField0.setBackground(Color.blue);
            colors[0]=Color.blue;
            colorField1.setBackground(Color.green);
            colors[1]=Color.green;
            colorField2.setBackground(Color.orange);
            colors[2]=Color.orange;
            colorField3.setBackground(Color.red);
            colors[3]=Color.red;
        }
        if(usedRanges==4){
            colorField0.setBackground(Color.blue);
            colors[0]=Color.blue;
            colorField1.setBackground(Color.green);
            colors[1]=Color.green;
            colorField2.setBackground(Color.yellow);
            colors[2]=Color.yellow;
            colorField3.setBackground(Color.orange);
            colors[3]=Color.orange;
            colorField4.setBackground(Color.red);
            colors[4]=Color.red;
        }
    }
    
    private void limitSpinner0StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_limitSpinner0StateChanged
        if(checkConstraints(evt)){
           limits[0]=Double.parseDouble(limitSpinner0.getValue().toString()); 
        }
        else{
            MessagePane.showInfo("limits need to be in the displayrange");
            if(lowerConstraint==null) limitSpinner0.setValue(upperConstraint-1);
            else if(upperConstraint==null) limitSpinner0.setValue(lowerConstraint+1);
            else if(!(upperConstraint==null) && !(lowerConstraint==null)) limitSpinner0.setValue((upperConstraint-lowerConstraint)/2);
        }
    }//GEN-LAST:event_limitSpinner0StateChanged
    
    private void limitSpinner1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_limitSpinner0StateChanged
        if(checkConstraints(evt)){
            limits[1]=Double.parseDouble(limitSpinner1.getValue().toString());            
        }
        else{
            MessagePane.showInfo("limits need to be in the displayrange");
            if(lowerConstraint==null) limitSpinner1.setValue(upperConstraint-1);
            else if(upperConstraint==null) limitSpinner1.setValue(lowerConstraint+1);
            else if(!(upperConstraint==null) && !(lowerConstraint==null)) limitSpinner1.setValue((upperConstraint-lowerConstraint)/2);
        }
    }//GEN-LAST:event_limitSpinner0StateChanged
    
    private void limitSpinner2StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_limitSpinner0StateChanged
        if(checkConstraints(evt)){
            limits[2]=Double.parseDouble(limitSpinner2.getValue().toString());
        }
        else{
            MessagePane.showInfo("limits need to be in the displayrange"); 
            if(lowerConstraint==null) limitSpinner2.setValue(upperConstraint-1);
            else if(upperConstraint==null) limitSpinner2.setValue(lowerConstraint+1);
            else if(!(upperConstraint==null) && !(lowerConstraint==null)) limitSpinner2.setValue((upperConstraint-lowerConstraint)/2);
        }
      }//GEN-LAST:event_limitSpinner0StateChanged
    
    private void limitSpinner3StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_limitSpinner0StateChanged
        if(checkConstraints(evt)){
            limits[3]=Double.parseDouble(limitSpinner3.getValue().toString());
        }
        else{
            MessagePane.showInfo("limits need to be in the displayrange");
            if(lowerConstraint==null) limitSpinner3.setValue(upperConstraint-1);
            else if(upperConstraint==null) limitSpinner3.setValue(lowerConstraint+1);
            else if(!(upperConstraint==null) && !(lowerConstraint==null)) limitSpinner3.setValue((upperConstraint-lowerConstraint)/2);
        }
    }//GEN-LAST:event_limitSpinner0StateChanged
     
    /**
     * The close action for this dialog.
     * Writes the limits and colors to the settings if retStatus == RET_OK
     * @param retStatus The return status int for which close action is required
     */
    private void doClose(int retStatus) {
        String reference = signal.getName();
        returnStatus = retStatus;
        if(returnStatus==RET_OK){
            for(int loopcount=0; loopcount<=usedRanges; loopcount++){
                if(colors[loopcount]==null && usedRanges !=0){
                    MessagePane.showInfo("Not all colors have been defined");
                    return;
                }
            }
            for (int loopcount = 0; loopcount < usedRanges; loopcount++) {
                if (limits[loopcount] == null) {
                    JSpinner spinner = limitSpinners.get(loopcount);
                    if (spinner.isEnabled() && spinner.getValue() != null) {
                        limits[loopcount] = (double) spinner.getValue();
                    } else {
                        MessagePane.showInfo("Not all limits have been defined");
                        return;
                    }
                }
                if (!(checkValue(loopcount))) {
                    MessagePane.showInfo("Limits need to be in sequential order");
                    return;
                }
            }
            for(int loopcount=0; loopcount<limits.length; loopcount++){
                if(limits[loopcount]==null){
                    if(hantuneWindow.settings.containsKey(reference) && hantuneWindow.settings.get(reference).containsKey("limit"+loopcount)){
                        // TODO check if this removes the setting
                        hantuneWindow.removeSetting(reference, "limit"+loopcount);
                    }
                }
                else{
                    //TODO check if old setting is overwritten
                    hantuneWindow.addSetting(reference, "limit"+loopcount, Double.toString(limits[loopcount]));                    
                }
            }
            for(int loopcount=0; loopcount<colors.length; loopcount++){
                if(colors[loopcount]==null){
                    if(hantuneWindow.settings.containsKey(reference) && hantuneWindow.settings.get(reference).containsKey("color"+loopcount)){
                        hantuneWindow.removeSetting(reference, "color"+loopcount);
                    }
                }
                else{
                    hantuneWindow.addSetting(reference, "color"+loopcount, Integer.toString(colors[loopcount].hashCode()));   
                }
            }
            hantuneWindow.loadSettings();
            hantuneWindow.repaint();
        }
        dispose();
    }
    
    /**
     * Checks if the value in the limitSpinner is a valid entry.
     * A valid entry is smaller then the next limitSpinner if that one is enabled and exists, and bigger then the previous one, if it exists.
     * @param limitIndex the limitSpinner to be checked.
     * @return boolean True the value is valid.
     */
    private boolean checkValue(int limitIndex){
        Double previousSpinnerValue  =null;
        Double spinnerValue = null;
        Double nextSpinnerValue = null;
              
        if(limitIndex==0){
           if(!limitSpinner1.isEnabled()){
               return true;
           }
           spinnerValue = Double.parseDouble(limitSpinner0.getValue().toString());
           nextSpinnerValue = Double.parseDouble(limitSpinner1.getValue().toString());
        }    
        if(limitIndex==1){
           previousSpinnerValue = Double.parseDouble(limitSpinner0.getValue().toString());
           spinnerValue = Double.parseDouble(limitSpinner1.getValue().toString());
           nextSpinnerValue = Double.parseDouble(limitSpinner2.getValue().toString());
           if(!limitSpinner2.isEnabled() && previousSpinnerValue<spinnerValue){
               return true;
           }
        }
        if(limitIndex==2){
           previousSpinnerValue = Double.parseDouble(limitSpinner1.getValue().toString());
           spinnerValue = Double.parseDouble(limitSpinner2.getValue().toString());
           nextSpinnerValue = Double.parseDouble(limitSpinner3.getValue().toString());
           if(!limitSpinner3.isEnabled() && previousSpinnerValue<spinnerValue){
               return true;
           }
        }
        if(limitIndex==3){
           previousSpinnerValue = Double.parseDouble(limitSpinner2.getValue().toString());
           spinnerValue = Double.parseDouble(limitSpinner3.getValue().toString());
           if(previousSpinnerValue<spinnerValue){
               return true;
           }
        }
        if(spinnerValue<nextSpinnerValue && previousSpinnerValue==null){
            return true;
        }
        if(spinnerValue<nextSpinnerValue && spinnerValue>previousSpinnerValue){
            return true;
        }
        else{
            return false;
        }
    }
    
    /**
     * Checks if the value of the respective spinner is between the constrains, if they are present.
     * @param evt the ChangeEvent of the respective spinner
     * @return True if the spinner is between the constrains, or if they are not present.
     */
    private boolean checkConstraints(javax.swing.event.ChangeEvent evt){
        if(lowerConstraint==null && upperConstraint==null) return true;
        
        Double spinnerValue = Double.parseDouble(((JSpinner) evt.getSource()).getValue().toString());
        
        if(lowerConstraint==null && spinnerValue<upperConstraint) return true;
        if(upperConstraint==null && spinnerValue>lowerConstraint) return true;
        if(lowerConstraint<spinnerValue && spinnerValue<upperConstraint) return true;
        else return false;
    }
    /**
     *  Getter for the previous settings, if they exist. 
     * The setting limit0 overrules limits. 
     * Places the results in limits[] and colors[], sets initSpinnerValue[].
     */
    private void getSettings(){
        String reference = signal.getName();
        for(int loopcount=0; loopcount<=maximumRanges; loopcount++){
                if(hantuneWindow.settings.containsKey(reference) && hantuneWindow.settings.get(reference).containsKey("color"+loopcount)){
                    colors[loopcount]=(Color.decode(hantuneWindow.settings.get(reference).get("color"+loopcount)));
                }
                else{
                    break;
                }
        }
            // if limit0 is not present in settings, get limits setting for limit0
        if(!(hantuneWindow.settings.containsKey(reference) && hantuneWindow.settings.get(reference).containsKey("limit0"))){
                if(hantuneWindow.settings.containsKey(reference) && hantuneWindow.settings.get(reference).containsKey("limit")){
                    limits[0]=(Double.parseDouble(hantuneWindow.settings.get(reference).get("limit")));
                }
        }        
        for(int loopcount=0; loopcount<maximumRanges; loopcount++){
            if(hantuneWindow.settings.containsKey(reference) && hantuneWindow.settings.get(reference).containsKey("limit"+loopcount)){
                limits[loopcount]= (Double.parseDouble(hantuneWindow.settings.get(reference).get("limit"+loopcount)));
                if(loopcount>=minimumRanges){
                    usedRanges = loopcount+1;
                }
            }
            else{
                break;
            }
        }
        //get upper and lower constraint
        if(!ignoreDisplayRange){
            if(hantuneWindow.settings.containsKey(reference) && hantuneWindow.settings.get(reference).containsKey("limit_lower")){
                    lowerConstraint = (Double.parseDouble(hantuneWindow.settings.get(reference).get("limit_lower")));
            } 
            else{
                lowerConstraint = signal.getMinimum();
            }
            if(hantuneWindow.settings.containsKey(reference) && hantuneWindow.settings.get(reference).containsKey("limit_upper")){
                    upperConstraint= (Double.parseDouble(hantuneWindow.settings.get(reference).get("limit_upper")));
            }
            else{
                upperConstraint = signal.getMaximum();
            }
        }
        
        // get the initSpinnerValue[]
        for(int loopcount=0; loopcount<initSpinnerValue.length; loopcount++){
            if(!(limits[loopcount]==null)){
                initSpinnerValue[loopcount]= limits[loopcount];
            }
            if(!(lowerConstraint==null)){
                if(initSpinnerValue[loopcount]<lowerConstraint) initSpinnerValue[loopcount]=lowerConstraint; 
            }
            if(!(upperConstraint==null)){
                if(initSpinnerValue[loopcount]>upperConstraint) initSpinnerValue[loopcount]=upperConstraint;
            }
         }
    }

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel limitLabel3;
    private javax.swing.JButton addRangeButton;
    private javax.swing.JButton cancelButton;
    private javax.swing.JPanel colorField0;
    private javax.swing.JPanel colorField1;
    private javax.swing.JPanel colorField2;
    private javax.swing.JPanel colorField3;
    private javax.swing.JPanel colorField4;
    private javax.swing.JButton editColor0;
    private javax.swing.JButton editColor1;
    private javax.swing.JButton editColor2;
    private javax.swing.JButton editColor3;
    private javax.swing.JButton editColor4;
    private javax.swing.JButton defaultColorsButton;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JLabel limitLabel0;
    private javax.swing.JLabel limitLabel1;
    private javax.swing.JLabel limitLabel2;
    private javax.swing.JSpinner limitSpinner0;
    private javax.swing.JSpinner limitSpinner1;
    private javax.swing.JSpinner limitSpinner2;
    private javax.swing.JSpinner limitSpinner3;
    private javax.swing.JButton okButton;
    private javax.swing.JButton removeRangeButton;
    // End of variables declaration//GEN-END:variables
    private int returnStatus = RET_CANCEL;
}
