/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.viewers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.Rectangle2D;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;

import ErrorLogger.AppendToLogfile;
import HANtune.DisplayrangeDialog;
import HANtune.SubrangeDialog;
import datahandling.CurrentConfig;
import datahandling.Signal;
import datahandling.SignalListener;
import nl.han.hantune.config.ApplicationProperties;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultValueDataset;
import org.jfree.ui.RectangleInsets;
import util.ThermometerDial;

import static nl.han.hantune.config.ConfigProperties.BARVIEWER_LABEL_POSITION;

/**
 * Displays the value of an asap2Data signal using a vertical bar display
 *
 * @author Aart-Jan
 */
@SuppressWarnings("serial")
public class BarViewer extends HANtuneViewer<BarViewer> implements SignalListener {

    public static final int MINIMUMRANGES = 0;
    public static final int MAXIMUMRANGES = 2;
    private static final Dimension DEFAULT_SIZE = new Dimension(150, 320);
    private Signal signal;
    private JLabel valueJLabel;
    private ChartPanel dialChartPanel;
    private DefaultValueDataset valueDataset;


    public double default_lower = 0;
    public double default_upper = 100;
//    private double previousDrawWith = 0; // used to keep track of the width of the window
    public static final Color DEFAULTCOLOR = Color.gray;

    /**
     * The constructor of the MultiLedViewer class
     */
    public BarViewer() {
        super(false);
        defaultMinimumSize = new Dimension(150, 150);
        getContentPane().setPreferredSize(DEFAULT_SIZE);
        setTitle("BarViewer");
        setDefaultLabelPosition(ApplicationProperties.getTopBottomLabelPos(BARVIEWER_LABEL_POSITION).getLabelPositionString());

        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                scaleBarViewer();
            }
        });
    }

    public void scaleBarViewer() {
        if (dialChartPanel != null) {

            // first determine the size of the chart rendering area...
            Dimension size = dialChartPanel.getSize();
            Insets insets = dialChartPanel.getInsets();
            Rectangle2D available = new Rectangle2D.Double(insets.left, insets.top,
                    size.getWidth() - insets.left - insets.right,
                    size.getHeight() - insets.top - insets.bottom);

            // work out real width
            double drawWidth = available.getWidth();
            ThermometerDial plot = ((ThermometerDial) dialChartPanel.getChart().getPlot());
            plot.setColumnRadius((int) ((drawWidth) / 2) - 55);
//            previousDrawWith = drawWidth;
        }
    }

    @Override
    protected BarViewer getListenerForSignal(Signal signal) {
        return createBarViewer(signal);
    }

    /**
     * This method rebuilds the content of this HANtuneWindow and should be
     * called only if the content changed (adding or removing asap2 references).
     * There is no need to call this method after a value or setting has
     * changed.
     *
     * The method loops through all asap2 references and will build the
     * components required for each asap2 reference.
     *
     * NOTE: Although the method loops through all asap2 references, this is not
     * yet supported. This HANtuneWindow allows for just a single asap2 reference
     * to be present. The developer will informed by a AssertException otherwise
     *
     */
    private BarViewer createBarViewer(Signal signal) {
        this.signal = signal;
        setTitle(signal.getName());
        setTitleTooltip(signal.getSource());
        valueDataset = new DefaultValueDataset(0);

        // dial
        final ThermometerDial plot = new ThermometerDial(valueDataset);
        final JFreeChart chart = new JFreeChart(null, JFreeChart.DEFAULT_TITLE_FONT, plot, false);
        chart.setBackgroundPaint(new Color(0,0,0,0));
        plot.setBulbRadius(0);
        plot.setGap(0);
        plot.setValueLocation(ThermometerDial.NONE);
        plot.setUnits(ThermometerDial.UNITS_NONE);
        plot.setOutlineVisible(false);
        plot.setBackgroundAlpha(0);
        plot.setInsets(new RectangleInsets(0, 0, -10, 0));
        plot.getRangeAxis().setTickLabelFont(new Font("Tahoma", 0, 11));

        // limits and ranges
        plot.setMercuryPaint(DEFAULTCOLOR);
        default_lower = signal.getMinimum();
        default_upper = signal.getMaximum();

        plot.setLowerBound(default_lower);
        plot.setUpperBound(default_upper);

        dialChartPanel = new ChartPanel(chart, false);
        dialChartPanel.setMinimumDrawWidth(0);
        dialChartPanel.setMinimumDrawHeight(0);
        dialChartPanel.setMaximumDrawWidth(Integer.MAX_VALUE);
        dialChartPanel.setMaximumDrawHeight(Integer.MAX_VALUE);
        dialChartPanel.setOpaque(false);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.weighty = 1;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        getContentPane().add(dialChartPanel, gridBagConstraints);

        valueJLabel = new JLabel(inactiveReferences.contains(signal) ? "N/A" : "0");
        valueJLabel.setHorizontalAlignment(SwingConstants.CENTER);
        valueJLabel.setFont(valueJLabel.getFont().deriveFont(Font.BOLD, 14.0f));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new Insets(0, 0, 5, 0);
        getContentPane().add(valueJLabel, gridBagConstraints);

        if (CurrentConfig.getInstance().isServiceToolMode()) {
            dialChartPanel.setPopupMenu(null);
        }
        else {
            addPopupMenuItems(signal);
            dialChartPanel.setPopupMenu(getMenu());
        }

        return this;
    }

    private void addPopupMenuItems(Signal signal) {
        //Menu item that calls DisplayrangeDialog, copy and edit this for viewer that use the dialog
        JMenuItem modDisplayrangeMenuItem = new JMenuItem("Modify display range");
        modDisplayrangeMenuItem.addActionListener(e -> new DisplayrangeDialog(this, signal));
        addMenuItemToPopupMenu(modDisplayrangeMenuItem);
        // Menu item that calls SubrangeDialog, copy and edit this for viewers that use the dialog
        JMenuItem modSubrangesMenuItem = new JMenuItem("Modify subranges");
        modSubrangesMenuItem.addActionListener(e -> {
            SubrangeDialog dialog = new SubrangeDialog(this, signal, MINIMUMRANGES, MAXIMUMRANGES, false);
            dialog.setVisible(true);
        });
        addMenuItemToPopupMenu(modSubrangesMenuItem);

        JMenuItem resetSizeMenuItem = new JMenuItem("Reset to default size");
        resetSizeMenuItem.addActionListener(e -> {
            Dimension defaultWindowSize = (new Dimension(((getSize().width - getContentPane().getSize().width) + DEFAULT_SIZE.width + 9), ((getSize().height - getContentPane().getSize().height) + DEFAULT_SIZE.height + 9)));
            setSize(defaultWindowSize);
            repaint();
        });
        addMenuItemToWindowMenu(resetSizeMenuItem);
    }

    /**
     * This method load the HANtuneWindow specific settings for each asap2
     * reference. This method should be called only if settings have been
     * changed. The method automatically triggers a repaint after the settings
     * are loaded.
     *
     * The method loops through all asap2 references and will load the settings
     * for each asap2 reference.
     *
     * NOTE: Although the method loops through all asap2 references, this is not
     * yet supported. This HANtuneWindow allows for just a single asap2 reference
     * to be present. The developer will informed by a AssertException otherwise
     */
    @Override
    public void loadSettings() {

        // loadSettings for each reference
        if (signal != null) {
            ThermometerDial plot = ((ThermometerDial) dialChartPanel.getChart().getPlot());



            // read settings

            // read settings for subranges, copy and edit this into viewers that use the SubrangeDialog
            java.util.List<Color> colors = new java.util.ArrayList<>();
            java.util.List<Double> limits = new java.util.ArrayList<>();
            for (int loopcount = 0; loopcount <= MAXIMUMRANGES; loopcount++) {
                // get colors settings
                if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("color" + loopcount)) {
                    colors.add(loopcount, Color.decode(settings.get(signal.getName()).get("color" + loopcount)));
                }
                // if limit0 is not present in settings, get limit setting for limit0
                if (!(settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("limit0"))) {
                    if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("limit")) {
                        limits.add(loopcount, Double.parseDouble(settings.get(signal.getName()).get("limit")));
                    }

                }
                // get limits settings
                if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("limit" + loopcount) && loopcount < MAXIMUMRANGES) {
                    limits.add(loopcount, Double.parseDouble(settings.get(signal.getName()).get("limit" + loopcount)));
                } // break if no more limits or colors are found
                else {
                    break;
                }
            }
            // END

            // loadSettings for displayRange, copy and edit this for viewers that use DisplayrangeDialog
            if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("limit_lower")) {
                plot.setLowerBound(Double.parseDouble(settings.get(signal.getName()).get("limit_lower")));
            } else {
                plot.setLowerBound(default_lower);
            }
            if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("limit_upper")) {
                plot.setUpperBound(Double.parseDouble(settings.get(signal.getName()).get("limit_upper")));
            } else {
                plot.setUpperBound(default_upper);
            }
            if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("color")) {
                //try and catch for backwards compatibility
                try {
                    plot.setMercuryPaint(Color.decode(settings.get(signal.getName()).get("color")));
                } catch (NumberFormatException e) {
                    AppendToLogfile.appendMessage("try and catch for backwards compatibility");
                    AppendToLogfile.appendError(Thread.currentThread(), e);
                    plot.setMercuryPaint(util.Util.getColor(settings.get(signal.getName()).get("color")));
                }
            } else {
                plot.setMercuryPaint(DEFAULTCOLOR);
            }
            //END

            // update subranges

            //drop all invalid limits and their respective colors

            for (int loopcount = limits.size() - 1; loopcount >= 0; loopcount--) {
                if (limits.get(loopcount) < plot.getLowerBound()) {
                    limits.remove(loopcount);
                    colors.remove(loopcount);
                }
                if (limits.get(loopcount) > plot.getUpperBound()) {
                    limits.remove(loopcount);
                    colors.remove(loopcount + 1);
                    if (loopcount == 0) {
                        colors.remove(loopcount);
                    }
                }
            }



            for (int loopcount = 0; loopcount <= MAXIMUMRANGES; loopcount++) {
                Double disabledPosition = plot.getLowerBound() - 1;
                plot.setSubrange(loopcount, disabledPosition, disabledPosition);
            }
            for (int loopcount = 0; loopcount <= MAXIMUMRANGES && loopcount < colors.size(); loopcount++) {

                if (loopcount == 0) {
                    plot.setSubrange(0, plot.getLowerBound(), limits.get(0));
                    plot.setSubrangePaint(0, colors.get(0));
                } else if (loopcount == limits.size()) {
                    plot.setSubrange(loopcount, limits.get(loopcount - 1), plot.getUpperBound());
                    plot.setSubrangePaint(loopcount, colors.get(loopcount));
                } else {
                    plot.setSubrange(loopcount, limits.get(loopcount - 1), limits.get(loopcount));
                    plot.setSubrangePaint(loopcount, colors.get(loopcount));
                }

            }


            // below is for backwards compatibility with projects made in versions below 0.7,
            //  this is only used when no subrange settings have been found
            if (limits.isEmpty() && colors.isEmpty()) {
                boolean oldRangesUsed = false;
                if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("range_normal")) {
                    String[] parts = settings.get(signal.getName()).get("range_normal").split(";");
                    if (parts.length == 3) {
                        settings.get(signal.getName()).put("limit0", parts[1]);
                        settings.get(signal.getName()).put("color0", Integer.toString(util.Util.getColor(parts[2]).hashCode()));
                        oldRangesUsed = true;
                    }
                }

                if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("range_warning")) {
                    String[] parts = settings.get(signal.getName()).get("range_warning").split(";");
                    if (parts.length == 3) {
                        settings.get(signal.getName()).put("limit1", parts[1]);
                        settings.get(signal.getName()).put("color1", Integer.toString(util.Util.getColor(parts[2]).hashCode()));
                        oldRangesUsed = true;
                    }
                }

                if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("range_critical")) {
                    String[] parts = settings.get(signal.getName()).get("range_critical").split(";");
                    if (parts.length == 3) {
                        settings.get(signal.getName()).put("color2", Integer.toString(util.Util.getColor(parts[2]).hashCode()));
                        oldRangesUsed = true;
                    }
                }
                if (oldRangesUsed) {
                    loadSettings();
                }
            }

            //END
        }
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        // set label size

        super.paintComponent(g);
    }

    @Override
    public void valueUpdate(double value, long timeStamp) {
        valueJLabel.setText(getFormat(signal.getName(), value).format(value));
        valueDataset.setValue(value);
    }

    @Override
    public void stateUpdate() {

    }

    @Override
    public void restoreSize() {
        super.restoreSize();
        scaleBarViewer();
    }
}
