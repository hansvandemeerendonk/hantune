/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextArea;

import can.CanConnectionHandler;
import datahandling.CurrentConfig;
import datahandling.CurrentConfig.Protocol;
import util.MessagePane;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class ConnectionDialog extends HanTuneDialog {

    public static final String REQUEST = "Request";
    public static final String CALIBRATE = "Calibrate";
    public static final String CONNECT = "Connect";


    private final CurrentConfig currentConfig = CurrentConfig.getInstance();
    private final List<Step> steps = new ArrayList<>();
    private JComboBox<Protocol> protocolComboBox;
    private JProgressBar progressBar;
    private JTextArea infoLabel;
    private Box body;
    private JButton connectButton;
    private Protocol orgProtocol;

    public ConnectionDialog() {
        super(true);
        createHeader();
        createFooter();
        createBody();
        updateBody("");
        pack();
        setSize(480, 380);
        setTitle(CONNECT);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(HanTuneDialog.DISPOSE_ON_CLOSE);
        JPanel contentPane = (JPanel) getContentPane();
        contentPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        orgProtocol = currentConfig.getProtocol();
    }

    private void createHeader() {
        Box header = Box.createHorizontalBox();
        JLabel connectToLabel = new JLabel("Connection type: ");
        connectToLabel.setFont(new Font("Tahoma", 1, 11));
        protocolComboBox = new JComboBox<>(Protocol.values());
        protocolComboBox.addActionListener(this::handleProtocolComboBoxAction);
        protocolComboBox.setSelectedIndex(currentConfig.getProtocol().ordinal());
        header.add(connectToLabel);
        header.add(protocolComboBox);
        header.add(Box.createHorizontalStrut(6));
        JButton settingsButton = new JButton("Settings");
        settingsButton.addActionListener(this::handleSettingsButtonAction);
        header.add(settingsButton);
        add(header, BorderLayout.NORTH);
        add(Box.createVerticalStrut(5));
    }

    private void createBody() {
        body = Box.createVerticalBox();
        add(body, BorderLayout.CENTER);
    }

    private void updateBody(String errorString) {
        if (body != null) {
            body.removeAll();
            steps.clear();
            switch (currentConfig.getProtocol()) {
                case XCP_ON_CAN:
                case XCP_ON_ETHERNET:
                case XCP_ON_UART:
                    createXcpBody();
                    if (!errorString.isEmpty()) {
                        showError(errorString);
                    }
                    break;
                case CAN:
                    createCanBody();
                    break;
            }
            validate();
            repaint();
        }
    }

    private void createXcpBody() {
        showError("");
        ButtonGroup buttonGroup = new ButtonGroup();
        JRadioButton requestRadioButton = new JRadioButton("Request parameters (Default)");
        requestRadioButton.setAlignmentX(Component.LEFT_ALIGNMENT);
        requestRadioButton.addActionListener(e -> handleRadioButtonAction(REQUEST));
        requestRadioButton.setSelected(true);
        JRadioButton calibrateRadioButton = new JRadioButton("Calibrate parameters");
        calibrateRadioButton.addActionListener(e -> handleRadioButtonAction(CALIBRATE));
        calibrateRadioButton.setAlignmentX(Component.LEFT_ALIGNMENT);
        buttonGroup.add(requestRadioButton);
        buttonGroup.add(calibrateRadioButton);
        body.add(Box.createVerticalStrut(5));
        body.add(requestRadioButton);
        body.add(calibrateRadioButton);

        JCheckBox errorMonitoringCheckBox = new JCheckBox("Enable error monitoring");
        errorMonitoringCheckBox.setSelected(currentConfig.getXcpsettings().isErrorMonitoringEnabled());
        errorMonitoringCheckBox.addActionListener(e -> handleErrorMonitoringCheckBoxAction(errorMonitoringCheckBox.isSelected()));
        body.add(errorMonitoringCheckBox);
        JSeparator separator = new JSeparator();
        Dimension size = new Dimension(separator.getMaximumSize().width, separator.getPreferredSize().height);
        separator.setMaximumSize(size);
        body.add(separator);


        addStepLabel(1, 0, "Connect to network");
        addStepLabel(2, 15, "Initialize XCP session");
        addStepLabel(3, 30, "Validate identifier");
        addStepLabel(4, 45, "Request event information");
        addStepLabel(5, 60, "Request parameter values");
        addStepLabel(6, 80, "Configure DAQ list");
        addSteps();

        connectButton.setText("Connect & Request");
    }

    private void createCanBody() {
        showError("");
        addStepLabel(1, 50, "Connect to network");
        addSteps();
        connectButton.setText(CONNECT);
    }
    private void addStepLabel(int index, int progress, String text) {
        Step step = new Step(index, progress, text);
        steps.add(step);
    }

    private void addSteps() {
        Box horizontalStepBox = Box.createHorizontalBox();
        Box verticalStepBox = Box.createVerticalBox();
        for (Step step : steps) {
            verticalStepBox.add(step.label);
            verticalStepBox.add(Box.createVerticalStrut(3));
        }
        horizontalStepBox.add(Box.createHorizontalStrut(10));
        horizontalStepBox.add(verticalStepBox);
        horizontalStepBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        body.add(Box.createVerticalGlue());
        body.add(horizontalStepBox);
        body.add(Box.createVerticalGlue());
    }

    private void createFooter() {
        Box footer = Box.createVerticalBox();
        progressBar = new JProgressBar();
        progressBar.setStringPainted(true);
        footer.add(progressBar);
        Box infoLabelWrapper = Box.createHorizontalBox();
        infoLabel = new JTextArea();
        infoLabel.setEditable(false);
        infoLabel.setHighlighter(null);
        infoLabel.setRows(4);
        infoLabel.setOpaque(false);
        infoLabel.setLineWrap(true);
        infoLabel.setWrapStyleWord(true);
        infoLabel.setFont(new Font("Tahoma", 0, 11));
        infoLabelWrapper.add(infoLabel);
        infoLabelWrapper.add(Box.createHorizontalGlue());
        footer.add(infoLabelWrapper);
        Box buttonWrapper = Box.createHorizontalBox();
        connectButton = new JButton(CONNECT);
        connectButton.addActionListener(e -> connect());
        connectButton.setActionCommand(REQUEST);
        getRootPane().setDefaultButton(connectButton);
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> dispose());
        buttonWrapper.add(connectButton);
        buttonWrapper.add(Box.createHorizontalGlue());
        buttonWrapper.add(cancelButton);
        footer.add(buttonWrapper);
        add(footer, BorderLayout.SOUTH);
    }

    @SuppressWarnings("unchecked")
    private void handleProtocolComboBoxAction(ActionEvent e) {
        boolean previous = currentConfig.canUseDoubles();
        String doublesWarningString = "";
        currentConfig.setProtocol((Protocol) protocolComboBox.getSelectedItem());

        int changedSignals = currentConfig.getHANtuneManager().set64BitSignalsAllowed(currentConfig.canUseDoubles());
        int changedParams = currentConfig.getHANtuneManager().set64BitParamsActive(currentConfig.canUseDoubles());
        currentConfig.getHANtuneManager().updateDefaultDaqList();

        if (previous != currentConfig.canUseDoubles() && (changedSignals > 0 || changedParams > 0)) {
            if (currentConfig.getProtocol() == Protocol.XCP_ON_CAN && orgProtocol != Protocol.XCP_ON_CAN) {
                doublesWarningString =
                    getDoublesWarningString(Protocol.XCP_ON_CAN.toString(), changedSignals, changedParams);
            }
        }
        updateBody(doublesWarningString);

        currentConfig.updateAsap2Listeners();
        HANtune.getInstance().updateDaqListListeners();
    }

    static String getDoublesWarningString(String protocol, int changedSignals, int changedParams) {
        String warn =
            String.format("Warning: connection type %s cannot handle double datatype (64Bit) in DAQ list.", protocol);
        if (changedSignals > 0) {
            warn += String.format("\n%d Signal" + (changedSignals == 1 ? "" : "(s)") +
                " will be disabled in DAQ lists.", changedSignals);
        }
        if (changedParams > 0) {
            warn += String.format("\n%d Parameter" + (changedParams == 1 ? "" : "(s)") +
                " will be disabled in ASAP2 list.", changedParams);
        }
        return warn;
    }

    private void handleSettingsButtonAction(ActionEvent e) {
        CommunicationSettings settingsDialog = new CommunicationSettings();
        settingsDialog.showProtocolTab(currentConfig.getProtocol());
        settingsDialog.setVisible(true);
        protocolComboBox.setSelectedIndex(currentConfig.getProtocol().ordinal());
    }

    private void handleRadioButtonAction(String option) {
        steps.get(4).label.setText("- " + option + " parameter values");
        connectButton.setText("Connect & " + option);
        connectButton.setActionCommand(option);
    }

    private void handleErrorMonitoringCheckBoxAction(boolean selected) {
        currentConfig.getXcpsettings().setErrorMonitoringEnabled(selected);
    }

    private void connect() {
        resetProgress();
        connectButton.setEnabled(false);
        infoLabel.setText("");
        switch (currentConfig.getProtocol()) {
            case XCP_ON_CAN:
            case XCP_ON_ETHERNET:
            case XCP_ON_UART:
                if (HANtune.getInstance().getActiveAsap2Id() == -1) {
                    MessagePane.showInfo("Please load an ASAP2 file first!");
                    finished(false);
                    break;
                }
                new ConnectionDialogWorker(this).connect(connectButton.getActionCommand());
                break;
            case CAN:
                if (currentConfig.getCan() == null) {
                    currentConfig.setCan(new CanConnectionHandler());
                }
                updateProgress(1);
                if(!currentConfig.getCan().startSession()){
                    showError("Could not connect to CAN bus:\n\n" + currentConfig.getCan().getErrorMsg());
                    finished(false);
                    break;
                };
                finished(true);
                break;
        }
    }

    public void finished(boolean successfully) {
        if (successfully) {
            progressBar.setValue(100);
            super.dispose();
        } else {
            connectButton.setEnabled(true);
            HANtune.getInstance().disconnect();
        }
    }

    public void updateProgress(int index) {
        for (Step step : steps) {
            if (step.index == index) {
                step.setEnabled(true);
                step.setBusy(true);
                progressBar.setValue(step.progress);
                progressBar.setString(step.progress + "%");
            } else {
                step.setBusy(false);
            }
        }
    }

    private void resetProgress() {
        for (Step step : steps) {
            step.setEnabled(false);
            step.setBusy(false);
        }
        progressBar.setValue(0);
    }

    public void showInfo(String info) {
        infoLabel.setForeground(Color.BLACK);
        infoLabel.setText(info);
    }

    public void showError(String error) {
        infoLabel.setForeground(Color.RED);
        infoLabel.setText(error);
    }

    @Override
    public void dispose() {
        HANtune.getInstance().disconnect();
        super.dispose();
    }

    private class Step {

        private final int index;
        private final int progress;
        private final JLabel label;

        public Step(int index, int progress, String text) {
            this.index = index;
            this.progress = progress;
            this.label = new JLabel("- " + text);
            this.label.setEnabled(false);
        }

        private void setEnabled(boolean enable) {
            label.setEnabled(enable);
        }

        private void setBusy(boolean busy) {
            if (busy) {
                label.setText(label.getText().replace("-", ">"));
                label.setFont(new Font("Tahoma", 1, 11));
            } else {
                label.setText(label.getText().replace(">", "-"));
                label.setFont(new Font("Tahoma", 0, 11));
            }
        }

    }
}
