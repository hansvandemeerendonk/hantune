/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.io.File;
import nl.han.hantune.gui.sidepanel.tree.TransferableTreeNode;

@SuppressWarnings("serial")
public class ProjectInfo extends TransferableTreeNode {

    public enum Type {Root, Folder, Asap2File, DbcFile, Daqlist, Layout, Calibration, Info, Script};
    private int id;
    private Type type;
    private String name;
    private boolean selected = false;
    private boolean active = false;
    private boolean modified = false;
    private int daqlist = -1;
    private File file = null;

    public ProjectInfo(Type type, String name, int id, boolean active, int daqlist) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.active = active;
        this.daqlist = daqlist;
    }
    
    public ProjectInfo(Type type, File file, int id, boolean active, int daqlist) {
        this.id = id;
        this.type = type;
        this.name = file.getName();
        this.active = active;
        this.daqlist = daqlist;
        this.file = file;
    }

    @Override
    public String toString() {
        if (modified) return "*" + name;
        return name;
    }

    public int getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean Selected) {
        this.selected = Selected;
    }

    public boolean isActive() {
        return active;
    }
    
    public int getDaqlist() {
        return daqlist;
    }

    public void setActive(boolean Active) {
        this.active = Active;
    }
    
    public File getFile() {
        return file;
    }
    
    /**
     * @return the modified
     */
    public boolean isModified() {
        return modified;
    }

    /**
     * @param modified the modified to set
     */
    public void setModified(boolean modified) {
        this.modified = modified;
    }
}
