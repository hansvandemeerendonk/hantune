/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import datahandling.CurrentConfig;
import util.Util;

/**
 *
 * @author JRikkers
 */
public class CustomFileChooser extends JFileChooser {

    private static final long serialVersionUID = 7526471155622776150L;

    private CurrentConfig currentConfig = CurrentConfig.getInstance();
    private final FileType[] fileTypes;

    static public enum FileType {
        PROJECT_FILE("HANtune project file", "hml"),
        ASAP2_FILE("ASAP2 file", "a2l"),
        MAP_FILE("MAP file", "map"),
        LOG_FILE("LOG file", "csv"),
        MATLAB_FILE("MATLAB M-file", "m"),
        PNG_FILE("PNG image", "png"),
        DBC_FILE("DBC file", "dbc"),
        DCM_FILE("Etas DCM-file", "dcm"),
        SREC_FILE("Motorola SREC-file", "srec"),
        SERVICE_PROJECT_FILE("HANtune Service project file", "shml"),
        PYTHON_FILE("Python Script", "py"),
        LOG_DIRECTORY("Log Directory", ""), // Special type, will result in DIRECTORIES_ONLY chooser
        IHEX_FILE("Intel HEX-file", "hex"),
        EXECUTABLE_FILE("Executable file", "exe");

        private final String description;
        private final String acceptedExtension;

        FileType(String descrStr, String extStr) {
            this.description = descrStr;
            this.acceptedExtension = extStr;
        }

        public String getDescription() {
            return description;
        }

        public String getExtension() {
            return acceptedExtension;
        }
    };

    /**
     * Construct a custom chooser
     *
     * @param action appears in title of dialog
     * @param fileTypes must hold AT LEAST 1 fileType
     */
    public CustomFileChooser(String actionTitle, FileType... fileTypes) {
        super();
        this.fileTypes = fileTypes;

        initFileChooser(actionTitle, "", fileTypes);
    }

    /**
     * Construct a custom chooser with suggested file name
     *
     * @param fileType contains file type. Special fileType:
     * FileType.LOG_DIRECTORY. file chooser will be set to DIRECTORIES_ONLY
     * @param action appears in title of dialog
     * @param suggestedFName holds a suggested filename, or a directory path if
     * fileType is LOG_DIRECTORY
     */
    public CustomFileChooser(FileType fileType, String actionTitle, String suggestedFName) {
        super();

        if (fileType == FileType.LOG_DIRECTORY) {
            this.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        }
        this.fileTypes = new FileType[]{fileType};

        initFileChooser(actionTitle, suggestedFName, fileType);
    }

    private void initFileChooser(String actionTitle, String suggestedFName, FileType... fileTypes) {
        if (fileTypes[0] == FileType.LOG_DIRECTORY) {
            setAcceptAllFileFilterUsed(true);
            setDialogTitle(actionTitle);
            setCurrentDirectory(new File(suggestedFName));
        } else {
            for (int iCnt = 0; iCnt < fileTypes.length; iCnt++) {
                HANtuneFileFilter filter = new HANtuneFileFilter(fileTypes[iCnt]);
                addChoosableFileFilter(filter);
                if (iCnt == 0) {
                    // Set filter to first element of array
                    setFileFilter(filter);
                }
            }
            if (fileTypes.length > 1) {
                setAcceptAllFileFilterUsed(false);
            }

            if (suggestedFName.length() > 0) {
                setSelectedFile(new File(
                        currentConfig.getPreviousPath(fileTypes[0]) + File.separator + suggestedFName));
            } else {
                setCurrentDirectory(currentConfig.getPreviousPath(fileTypes[0]));
            }

            setDialogTitle(actionTitle + " " + fileTypes[0].getDescription());

            // Add listener on chooser to detect changes to selected file
            addPropertyChangeListener(new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent evt) {
                    if (CustomFileChooser.DIRECTORY_CHANGED_PROPERTY.equals(evt.getPropertyName())) {
                        CustomFileChooser customFileChooser = (CustomFileChooser) evt.getSource();
                        currentConfig.setPreviousPath(getChooserSelectedFileType(),
                                customFileChooser.getCurrentDirectory());
                    }
                }
            });
        }
    }

    private FileType getChooserSelectedFileType() {
        if (fileTypes.length == 1) {
            return fileTypes[0];
        }
        String dlgFilter = getFileFilter().getDescription();
        for (FileType fileType : fileTypes) {
            if (dlgFilter.contains(fileType.getDescription())) {
                return fileType;
            }
        }
        return fileTypes[0];
    }

    private File getSelectedFileAddExtension() {
        File file = getSelectedFile();

        if (getFileSelectionMode() == JFileChooser.DIRECTORIES_ONLY) {
            return file;
        }

        // Add extension to filename if necessary
        String ext = Util.getExtension(file.getName());

        String dialogFilter = getChooserSelectedFileType().getExtension();
        if (ext == null || !ext.equals(dialogFilter)) {
            file = new File(file.getPath() + "." + dialogFilter);
            setSelectedFile(file);
        }
        return file;
    }

    @Override
    public void approveSelection() {
        File file = getSelectedFileAddExtension();

        if (file.exists() && getDialogType() == CUSTOM_DIALOG) {
            int result = JOptionPane.showConfirmDialog(this,
                    String.format("File %s already exists.%n Do you want to replace the existing file?",
                            file.getName()),
                    "File already exists", JOptionPane.YES_NO_OPTION);

            switch (result) {
                case JOptionPane.YES_OPTION:
                    super.approveSelection();
                    return;

                case JOptionPane.NO_OPTION:
                case JOptionPane.CLOSED_OPTION:
                    return;
            }
        } else if (!file.exists() && getDialogType() == OPEN_DIALOG) {
            String dlgMsg;
            if (getFileSelectionMode() == JFileChooser.DIRECTORIES_ONLY) {
                dlgMsg = String.format("Folder %s not found.%nPlease select another folder.",
                        file.getAbsolutePath());
            } else {
                dlgMsg = String.format("File %s not found.%nPlease select another file.", file.getName());
            }

            JOptionPane.showMessageDialog(this, dlgMsg, "File not found", JOptionPane.WARNING_MESSAGE);

            return;
        }

        super.approveSelection();
    }

    public File chooseSaveDialogConfirmOverwrite(Component parent, String approveButtonText) {
        setApproveButtonMnemonic(' ');
        setDialogType(JFileChooser.CUSTOM_DIALOG);
        if (showDialog(parent, approveButtonText) == JFileChooser.APPROVE_OPTION) {
            return getSelectedFile();
        } else {
            return null;
        }
    }

    public File chooseOpenDialog(Component parent, String approveButtonText) {
        setApproveButtonText(approveButtonText);
        if (showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
            return getSelectedFile();
        } else {
            return null;
        }
    }

    private class HANtuneFileFilter extends FileFilter {

        private FileType saveFileType = null;

        public HANtuneFileFilter(FileType ft) {
            saveFileType = ft;
        }

        @Override
        // Accept all directories and acceptedExtension
        public boolean accept(File f) {
            boolean rtn = false;

            if (f.isDirectory()) {
                rtn = true;
            } else {
                String extension = Util.getExtension(f.getName());
                if (extension != null) {
                    if (extension.equals(saveFileType.getExtension())) {
                        rtn = true;
                    }
                }
            }
            return rtn;
        }

        @Override
        // The description of this filter
        public String getDescription() {
            return (saveFileType.getDescription() + "s " + "(*." + saveFileType.getExtension() + ")");
        }

    }
}
