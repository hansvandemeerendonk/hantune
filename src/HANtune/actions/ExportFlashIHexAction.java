/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.actions;

import java.io.File;
import java.io.IOException;

import javax.swing.tree.DefaultMutableTreeNode;

import ASAP2.ASAP2Data;
import ASAP2.ASAP2Data.ByteOrder;
import ASAP2.ASAP2ModPar;
import HANtune.CustomFileChooser.FileType;
import HANtune.HANtune;
import HANtune.ProjectInfo;
import datahandling.CalibrationImportReader;
import datahandling.CalibrationImportReaderIHex;
import datahandling.CalibrationMerger;
import datahandling.CalibrationMergerIHex;
import datahandling.CurrentConfig;
import util.MessagePane;
import util.Util;

@SuppressWarnings("serial")
public class ExportFlashIHexAction extends ExportFlashDialogAction {

    private static final String BODAS_FLASH_TOOL = "BODAS-service.exe";
    private final HANtune hanTune;

    public ExportFlashIHexAction(HANtune hantune) {
        super(hantune);
        this.hanTune = hantune;
    }

    public void handleExportFlashAction(DefaultMutableTreeNode node) {
        if (node == null) {
            return;
        }

        int calibrationId = ((ProjectInfo) node).getId();
        String calibrationName = ((ProjectInfo) node).getName();

        if (!initExportFlashDialogAction(calibrationId, calibrationName,
                CurrentConfig.getInstance().getFlashIHexBaseFileName(), "IHEX", FileType.IHEX_FILE)) {
            return;
        }

        handleExportFlashDialog();

        if (isSuccesfullExport()) {
            CurrentConfig.getInstance().setFlashIHexBaseFileName(getBaseFileAbsolutePath());
        }
    }

    @Override
    protected boolean isExportFlashPreconditionsOk(ASAP2Data a2d) {
        boolean rtn = true;
        boolean flashOk = false;
        String msg = "";
        ASAP2ModPar mp = a2d.getModPar();
        if (mp == null) {
            msg += "MOD_PAR section not found in ASAP2 currently loaded.";
            rtn = false;
        } else {
            if (!mp.isRamFlashOffsetPresent()) {
                msg += " - SYSTEM_CONSTANT 'RAM-FlashOffset' not found in MOD_PAR section ASAP2 file" + Util.LF;
                rtn = false;
            }
            if (!mp.isAddrEpkPresent()) {
                msg += " - ADDR_EPK: Address EPROM ID not found in MOD_PAR section ASAP2 file" + Util.LF;
                rtn = false;
            }
            if (!mp.isEcuPresent()) {
                msg += " - ECU name not present in MOD_PAR section ASAP2 file" + Util.LF;
                rtn = false;
            }
            if (!mp.isCrc32AdressesPresent() || !mp.getCrc32Addresses().isAllFieldsPresent()) {
                msg += " - One or more of SYSTEM_CONSTANTS: CRC_xxx, or PCODE_xxx not found in ASAP2 file" + Util.LF;
                rtn = false;
            }
            String bodas = CurrentConfig.getInstance().getBodasPath();
            if (bodas.isEmpty()) {
                msg += " - Directory of Bodas-service flash tool has not been set. Flashing not possible" + Util.LF;
            } else {
                File bodasFile = new File(bodas + File.separator + BODAS_FLASH_TOOL);
                if (!bodasFile.exists()) {
                    msg += " - Directory of Bodas-service flash tool is not found. Currently set to: " + Util.LF
                            + "     " + bodas + Util.LF + "   Flashing not possible." + Util.LF;
                } else {
                    flashOk = true;
                }
            }
        }

        if (!rtn) {
            MessagePane.showError("<html><b>Cannot export/flash using current ASAP2 file: "
                    + hanTune.currentConfig.getASAP2Data().getName() + "</b></html>" + Util.LF + msg);
        } else if (!flashOk) {
            MessagePane.showWarning("<html><b>Warning: only Export possible using current ASAP2 file: "
                    + hanTune.currentConfig.getASAP2Data().getName() + "<b></html>" + Util.LF + msg);

        }
        setFlashButtonOk(flashOk);
        return rtn;
    }

    protected static boolean isFlashConditionsOk(ASAP2Data ad) {
        boolean rtn = true;
        String msg = "";

        ASAP2ModPar mp = ad.getModPar();
        if (mp == null) {
            msg += "MOD_PAR section not found in ASAP2 currently loaded.";
        } else {
            if (!mp.isEcuPresent()) {
                msg += " - ECU name not present in ASAP2 file" + Util.LF;
                rtn = false;
            }
            String bodas = CurrentConfig.getInstance().getBodasPath();
            if (bodas.isEmpty()) {
                msg += " - Directory of Bodas-service flash tool has not been set." + Util.LF;
                rtn = false;
            } else {
                File bodasFile = new File(bodas + File.separator + BODAS_FLASH_TOOL);
                if (!bodasFile.exists()) {
                    msg += " - Directory of Bodas-service flash tool cannot be found. Currently set to: " + Util.LF
                            + "     " + bodas;
                    rtn = false;
                }
            }
        }

        if (!msg.isEmpty()) {
            MessagePane.showWarning("Cannot flash using ASAP2 file currently loaded: " + ad.getName() + Util.LF + msg);
        }
        return rtn;
    }

    protected CalibrationMerger getCalibrationMerger(ASAP2Data asap) {
        return new CalibrationMergerIHex(calib, asap);
    }

    protected boolean handleExportAction(CalibrationMerger merger, File exportFile) throws IOException {
        // do actual export here...
        merger.writeToFile(exportFile);
        if (merger.getMergeResult() == CalibrationImportReader.ImportReaderResult.READER_RESULT_ERROR) {
            return false;
        }

        BatchIHexCalibration batchCalib = new BatchIHexCalibration(hanTune);
        // A Dos CMD is displayed during flash, therefore no message after completion.
        return batchCalib.doActionPatch(exportFile.getAbsolutePath());
    }

    protected boolean handleExportFlashAction(CalibrationMerger merger, File exportFile) throws IOException {
        // do actual export here...
        merger.writeToFile(exportFile);
        if (merger.getMergeResult() == CalibrationImportReader.ImportReaderResult.READER_RESULT_ERROR) {
            return false;
        }

        FlashCalibration flashCalib = new FlashCalibration((HANtune) super.getParent());
        return flashCalib.doActionPatchFlashIHex(exportFile.getAbsolutePath());
    }

    protected String getSuccessfullString(boolean doOptionalFlash) {
        return "Finished export" + (doOptionalFlash ? " and flash. " : ". ") + "Check terminal for errors.";
    }

}
