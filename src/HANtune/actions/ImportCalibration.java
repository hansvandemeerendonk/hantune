/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.actions;

import java.io.File;
import java.util.TreeMap;

import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;

import ASAP2.ASAP2Characteristic;
import ASAP2.ASAP2Data;
import ASAP2.ASAP2Data.ByteOrder;
import ErrorLogger.AppendToLogfile;
import HANtune.CustomFileChooser;
import HANtune.CustomFileChooser.FileType;
import HANtune.HANtune;
import HANtune.ProjectInfo;
import datahandling.CalibrationImportReader;
import datahandling.CalibrationImportReader.ImportReaderResult;
import datahandling.CalibrationImportReaderDCM;
import datahandling.CalibrationImportReaderIHex;
import datahandling.CalibrationImportReaderSRec;
import datahandling.CurrentConfig;
import haNtuneHML.Calibration;
import util.MessagePane;

public class ImportCalibration {

    private HANtune hanTune;

    public ImportCalibration(HANtune hantune) {
        this.hanTune = hantune;
    }

    public void doActionImportSRec(DefaultMutableTreeNode node) {
        if (node == null) {
            return;
        }

        ASAP2Data asap = hanTune.currentConfig.getASAP2Data();
        if (!asap.getModPar().isRamFlashOffsetPresent() || !asap.getModPar().isAddrEpkPresent()) {
            return;
        }

        int id = ((ProjectInfo) node).getId();
        Calibration calib = hanTune.currentConfig.getHANtuneDocument().getHANtune().getCalibrationArray(id);

        CustomFileChooser fileChooser = new CustomFileChooser("Import", FileType.SREC_FILE);
        File file = fileChooser.chooseOpenDialog(hanTune, "Import");

        handleImportCalib(calib, FileType.SREC_FILE, file,
                new CalibrationImportReaderSRec(asap.getEpromID(), asap.getModPar().getAddrEpk(),
                        asap.getModPar().getRamFlashOffset(), asap.getByteOrder() == ByteOrder.MSB_LAST));
    }

    public void doActionImportHex(DefaultMutableTreeNode node) {
        if (node == null) {
            return;
        }

        ASAP2Data asap = hanTune.currentConfig.getASAP2Data();
        if (!asap.getModPar().isRamFlashOffsetPresent() || !asap.getModPar().isAddrEpkPresent()) {
            return;
        }

        int id = ((ProjectInfo) node).getId();
        Calibration calib = hanTune.currentConfig.getHANtuneDocument().getHANtune().getCalibrationArray(id);

        CustomFileChooser fileChooser = new CustomFileChooser("Import", FileType.IHEX_FILE);
        File file = fileChooser.chooseOpenDialog(hanTune, "Import");

        handleImportCalib(calib, FileType.IHEX_FILE, file,
                new CalibrationImportReaderIHex(asap.getEpromID(), asap.getModPar().getAddrEpk(),
                        asap.getModPar().getRamFlashOffset(), asap.getByteOrder() == ByteOrder.MSB_LAST));
    }

    public void doActionImportDCM(DefaultMutableTreeNode node) {
        if (node == null) {
            return;
        }

        int id = ((ProjectInfo) node).getId();
        Calibration calib = hanTune.currentConfig.getHANtuneDocument().getHANtune().getCalibrationArray(id);

        CustomFileChooser fileChooser = new CustomFileChooser("Import", FileType.DCM_FILE);
        File file = fileChooser.chooseOpenDialog(hanTune, "Import");

        handleImportCalib(calib, FileType.DCM_FILE, file, new CalibrationImportReaderDCM());
    }

    boolean handleImportCalib(Calibration calib, FileType ft, File file, CalibrationImportReader reader) {
        if (file != null) {

            try {
                // do actual reading of import file here, using appropriate method
                ImportReaderResult rslt = reader.readCalibration(file);

                switch (rslt) {
                    case READER_RESULT_OK:
                        // do nothing here
                        break;

                    case READER_RESULT_WARNING:
                        if (MessagePane.showConfirmDialog(reader.getReadResultMessage(),
                                "Continue import?") != JOptionPane.OK_OPTION) {
                            return false;
                        }
                        break;

                    case READER_RESULT_ERROR:
                        MessagePane.showInfo(reader.getReadResultMessage() + "\n" + "Filename: "
                                + file.getName() + "\n\n" + ft.getDescription() + " cannot be imported.\n\n");
                        return false;
                }

                TreeMap<String, ASAP2Characteristic> characteristics
                        = CurrentConfig.getInstance().getASAP2Data().getASAP2Characteristics();
                // write data into calibration
                reader.transferDataIntoCalibration(calib, characteristics);

                MessagePane.showTextAreaOKDialog(ft.getDescription() + " import completed",
                        "Finished importing " + ft.getExtension().toUpperCase() + " data from file "
                        + file.getName() + " into " + calib.getTitle() + System.lineSeparator(),
                        reader.getImportReport());

                return true;
            } catch (Exception e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
                hanTune.currentConfig.getHANtuneManager().setError(e.toString());
                MessagePane.showError("Error while importing " + ft.getDescription() + ":\n\n"
                        + HANtune.getInstance().currentConfig.getHANtuneManager().getError());

                return false;
            }

        }
        return false;
    }

}
