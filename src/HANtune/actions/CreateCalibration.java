/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.actions;

import java.io.File;

import javax.swing.JOptionPane;

import ASAP2.ASAP2Data;
import ASAP2.ASAP2Data.ByteOrder;
import HANtune.CustomFileChooser;
import HANtune.HANtune;
import datahandling.CalibrationHandler;
import datahandling.CalibrationImportReader;
import datahandling.CalibrationImportReaderDCM;
import datahandling.CalibrationImportReaderIHex;
import datahandling.CalibrationImportReaderSRec;
import haNtuneHML.Calibration;
import util.MessagePane;

public class CreateCalibration {

    private HANtune hanTune;

    public CreateCalibration(HANtune hantune) {
        this.hanTune = hantune;
    }

    /**
     * Creates a new calibration to be shown in the project tree
     */
    public void createNewCalibration() {
        String name = requestNewCalibName("New Calibration", hanTune);
        if (name != null) {

            // perform add action
            CalibrationHandler.getInstance().newCalibration(name);

            // reload project
            hanTune.updateProjectTree();
        }
    }

    public void doActionCreateCalibFromSRec() {
        ASAP2Data asap = hanTune.currentConfig.getASAP2Data();
        if (!asap.getModPar().isRamFlashOffsetPresent() || !asap.getModPar().isAddrEpkPresent()) {
            return;
        }

        createNewCalibrationFromFiletype(
                new CalibrationImportReaderSRec(asap.getEpromID(), asap.getModPar().getAddrEpk(),
                        asap.getModPar().getRamFlashOffset(), asap.getByteOrder() == ByteOrder.MSB_LAST));
    }

    public void doActionCreateCalibFromIHex() {
        ASAP2Data asap = hanTune.currentConfig.getASAP2Data();
        if (!asap.getModPar().isRamFlashOffsetPresent() || !asap.getModPar().isAddrEpkPresent()) {
            return;
        }

        createNewCalibrationFromFiletype(
                new CalibrationImportReaderIHex(asap.getEpromID(), asap.getModPar().getAddrEpk(),
                        asap.getModPar().getRamFlashOffset(), asap.getByteOrder() == ByteOrder.MSB_LAST));
    }

    public void doActionCreateCalibFromDCM() {
        createNewCalibrationFromFiletype(new CalibrationImportReaderDCM());
    }

    private void createNewCalibrationFromFiletype(CalibrationImportReader reader) {

        CustomFileChooser fileChooser = new CustomFileChooser(
                "Create new calibration from " + reader.getFileType().getDescription(), reader.getFileType());
        File file = fileChooser.chooseOpenDialog(hanTune, "Open");

        if (file != null) {

            String name = CreateCalibration
                    .requestNewCalibName(file.getName().substring(0, file.getName().lastIndexOf('.')), hanTune);
            if (name != null) {
                int id = CalibrationHandler.getInstance().newCalibration(name);
                Calibration calib
                        = hanTune.currentConfig.getHANtuneDocument().getHANtune().getCalibrationArray(id);

                // Create read method to be used during import
                ImportCalibration importCalib = new ImportCalibration(hanTune);
                if (importCalib.handleImportCalib(calib, reader.getFileType(), file, reader)) {
                    // reload project
                    hanTune.updateProjectTree();
                } else {
                    CalibrationHandler.getInstance().removeCalibration(id);
                }

            }
        }
    }

    static String requestNewCalibName(String suggestedName, HANtune hanTune) {
        // show dialog to enter calibration name
        String name = "";
        while (true) {
            if ((name = (String) JOptionPane.showInputDialog(HANtune.getInstance(), "Name:",
                    "New calibration", JOptionPane.QUESTION_MESSAGE, null, null, suggestedName)) == null) {
                return null;
            }
            if (name.equals("")) {
                MessagePane.showError("Please enter a valid name");
            } else {
                for (Calibration calibration : hanTune.currentConfig.getHANtuneDocument().getHANtune()
                        .getCalibrationList()) {
                    if (calibration.getTitle().equals(name)) {
                        MessagePane.showError(
                                "A calibration with this name already exists\n please choose another name");
                        name = null;
                        break;
                    }
                }

            }
            if (name != null && name.length() > 0) {
                return name; // finished, a valid name has been entered
            }
        }
    }

}
