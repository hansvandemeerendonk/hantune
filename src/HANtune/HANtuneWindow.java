/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicInternalFrameUI;

import ASAP2.ASAP2Data;
import ErrorLogger.AppendToLogfile;
import ErrorMonitoring.ErrorViewer;
import can.DbcManager;
import components.ComponentMover;
import components.ComponentResizer;
import components.CustomButton;
import components.OpaqueTitledBorder;
import datahandling.CurrentConfig;
import util.MessagePane;

/**
 *
 * @author Aart-Jan, Michiel Klifman
 */
@SuppressWarnings("serial")
public abstract class HANtuneWindow extends JInternalFrame {

    private final int grid = 6;
    private final int overlap = getGrid()-1; //causes windows to overlap 1px
    private static final int MAX_DECIMALS = 24;
    public static final String DEFAULT_BORDER = "lineBorder";
    public static final String NO_BORDER = "noBorder";
    protected CurrentConfig currentConfig = CurrentConfig.getInstance();
    protected final HANtune hantune = HANtune.getInstance();
    protected Map<String, Map<String, String>> settings = new HashMap<>();
    protected Map<String, String> windowSettings = new HashMap<>();
    private boolean verticalResizable = true;
    private boolean horizontalResizable = true;
    private final Preferences userPrefs = Preferences.userNodeForPackage(UserPreferences.class);
    protected Dimension defaultMinimumSize = null;
    protected long lastRepaintTimestamp = 0;
    protected long repaintPeriod = 1000 / 10; // repaint period in ms
    private JPanel contentPanel;
    private JPopupMenu popupMenu;
    private ComponentResizer resizer;
    private ComponentMover mover;
    private boolean selectedInTab;
    private boolean markedForDeletion;
    private Border border;
    private CustomButton closeButton;
    private boolean resizeAndMoveModeEnabled = false;
    private int height;
    private int width;

    public HANtuneWindow() {
        // configure window
        setClosable(true);
        setIconifiable(true);
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/images/HANlogo.png")));
        setVisible(true);
        setResizable(false);

        contentPanel = new JPanel();
        contentPanel.setLayout(new GridBagLayout());
        contentPanel.setOpaque(false);
        super.getContentPane().add(contentPanel);
        JPanel panel = (JPanel) super.getContentPane();
        panel.setOpaque(false);

        createResizeAndMoveComponents();
        setKeyBindActions();
        addInternalFrameListener();
        addComponentListener();

        ((BasicInternalFrameUI) this.getUI()).setNorthPane(null);
    }

    @Override
    public JPanel getContentPane() {
        return contentPanel;
    }

    public Container getSuperContentPane() {
        return super.getContentPane();
    }

    public void showPopupMenu(Component comp, int x, int y) {
        popupMenu.show(comp, x, y);
    }

    /**
     * Creates the pop-up menu in the title bar.
     */
    protected void initializePopupMenu() {
        popupMenu = new JPopupMenu();
        JMenu windowMenu = new JMenu("Window");

        JMenu moveMenu = new JMenu("Move");
        JMenuItem moveToTabMenuItem = new JMenuItem("Move to tab");
        moveToTabMenuItem.addActionListener(e -> moveToAnotherTab());
        moveMenu.add(moveToTabMenuItem);

        JMenuItem moveToLayerMenuItem = new JMenuItem("Move to layer");
        moveToLayerMenuItem.addActionListener(e -> changeLayer());
        moveMenu.add(moveToLayerMenuItem);

        JMenuItem moveToFrontMenuItem = new JMenuItem("Move to front (current layer)");
        moveToFrontMenuItem.addActionListener(e -> moveHANtuneWindowToFront());
        moveMenu.add(moveToFrontMenuItem);

        JMenuItem moveToBackMenuItem = new JMenuItem("Move to back (current layer)");
        moveToBackMenuItem.addActionListener(e -> moveHANtuneWindowToBack());
        moveMenu.add(moveToBackMenuItem);
        windowMenu.add(moveMenu);

        JMenu borderMenu = new JMenu("Border");
        ButtonGroup group = new ButtonGroup();
        JRadioButtonMenuItem noBorderMenuItem = new JRadioButtonMenuItem("No border");
        noBorderMenuItem.addActionListener(e -> saveAndShowBorder("noBorder"));
        noBorderMenuItem.setSelected(getWindowSetting("showBorder", DEFAULT_BORDER).equals("noBorder"));
        group.add(noBorderMenuItem);
        borderMenu.add(noBorderMenuItem);

        JRadioButtonMenuItem lineBorderMenuItem = new JRadioButtonMenuItem("Line border");
        lineBorderMenuItem.addActionListener(e -> saveAndShowBorder("lineBorder"));
        lineBorderMenuItem.setSelected(getWindowSetting("showBorder", DEFAULT_BORDER).equals("lineBorder"));
        group.add(lineBorderMenuItem);
        borderMenu.add(lineBorderMenuItem);
        windowMenu.add(borderMenu);

        JRadioButtonMenuItem titledBorderMenuItem = new JRadioButtonMenuItem("Titled border");
        titledBorderMenuItem.addActionListener(e -> saveAndShowBorder("titledBorder"));
        titledBorderMenuItem.setSelected(getWindowSetting("showBorder", DEFAULT_BORDER).equals("titledBorder"));
        group.add(titledBorderMenuItem);
        borderMenu.add(titledBorderMenuItem);

        borderMenu.addSeparator();
        JMenuItem editTitleMenuItem = new JMenuItem("Edit title");
        editTitleMenuItem.addActionListener(e -> editBorderTitle());
        borderMenu.add(editTitleMenuItem);

        JMenu backgroundMenu = new JMenu("Background");
        JCheckBoxMenuItem showBackgroundMenuItem = new JCheckBoxMenuItem("Visible");
        showBackgroundMenuItem.addActionListener(e -> saveAndShowBackground(showBackgroundMenuItem.isSelected()));
        showBackgroundMenuItem.setSelected(getBooleanWindowSetting("showBackground", true));
        backgroundMenu.add(showBackgroundMenuItem);

        JMenuItem backgroundColorMenuItem = new JMenuItem("Color");
        backgroundColorMenuItem.addActionListener(e -> editBackgroundColor());
        backgroundMenu.add(backgroundColorMenuItem);
        windowMenu.add(backgroundMenu);

        JMenuItem closeWindowMenuItem = new JMenuItem("Close window");
        closeWindowMenuItem.addActionListener(e -> doDefaultCloseAction());
        windowMenu.add(closeWindowMenuItem);

        popupMenu.add(windowMenu);

        JMenuItem resizeAndMoveMenuItem = new JMenuItem("Resize and Move mode");
        resizeAndMoveMenuItem.addActionListener(e -> toggleResizeAndMoveMode());
        resizeAndMoveMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
        popupMenu.add(resizeAndMoveMenuItem);

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                if (SwingUtilities.isRightMouseButton(event)) {
                    if (!CurrentConfig.getInstance().isServiceToolMode()) {
                        popupMenu.show(HANtuneWindow.this, event.getX(), event.getY());
                    }
                }
            }
        });
    }

    protected JPopupMenu getMenu() {
        return popupMenu;
    }

    protected void addMenuToPopupMenu(JMenu menu) {
        if (!CurrentConfig.getInstance().isServiceToolMode()) {
            popupMenu.insert(menu, popupMenu.getComponentCount() - 1);
        }
    }

    protected void addMenuItemToPopupMenu(JMenuItem menuItem) {
        if (!CurrentConfig.getInstance().isServiceToolMode()) {
            popupMenu.insert(menuItem, popupMenu.getComponentCount() - 1);
        }
    }

    protected void addMenuItemToWindowMenu(JMenuItem menuItem) {
        if (!CurrentConfig.getInstance().isServiceToolMode()) {
            JMenu windowMenu = (JMenu) popupMenu.getComponent(0);
            windowMenu.insert(menuItem, windowMenu.getItemCount() - 1);
        }
    }

    private void moveToAnotherTab() {
        List<String> tabNames = new ArrayList<>();
        for (int i = 0; i < hantune.getTabbedPane().getTabCount(); i++) {
            if (!(hantune.getTabbedPane().getComponentAt(i) == getParent().getParent().getParent())) {
                tabNames.add(hantune.getTabbedPane().getTitleAt(i));
            }
        }
        String chosenTabName = ((String) (JOptionPane.showInputDialog(HANtuneWindow.this, "Select a tab to move to:", getTitle() + " move to tab", JOptionPane.QUESTION_MESSAGE, null, tabNames.toArray(), null)));
        HANtuneTab chosenTab = currentConfig.getHANtuneManager().getTabByTitle(chosenTabName);
        if (chosenTab != null) {
            for (int i = 0; i < chosenTab.getComponentCount(); i++) {
                if (chosenTab.getComponent(i) instanceof ErrorViewer) {
                    MessagePane.showInfo("This tab already contains an errorViewer \nOnly one ErrorViewer per tab possible");
                    return;
                }
            }
            chosenTab.add(HANtuneWindow.this);
            int indexOfCurrentTab = HANtune.getInstance().getTabbedPane().getSelectedIndex();
            currentConfig.getHANtuneManager().getTab(indexOfCurrentTab).repaint();
        }
    }

    private void changeLayer() {
        Object[] layers = {"background layer", "default layer", "foreground layer"};
        String layer = (String) JOptionPane.showInputDialog(hantune, "Select a layer to move to:", "Move window", JOptionPane.PLAIN_MESSAGE, null, layers, "default layer");
        if ("background layer".equals(layer)) {
            setLayer(-1);
        } else if ("default layer".equals(layer)) {
            setLayer(0);
        } else if ("foreground layer".equals(layer)) {
            setLayer(1);
        }
    }

    /**
     * Rebuilds the content of the component. This method should be overridden
     * by the class that extends this class.
     */
    public void rebuild() {
        loadWindowSettings();
        initializePopupMenu();
    }

    public void loadWindowSettings() {
        if (windowSettings.containsKey("width")) {                              // Remove keys "width" and "height" as these are deprecated as of HANtune 2.1
            windowSettings.remove("width");
        }
        if (windowSettings.containsKey("height")) {
            windowSettings.remove("height");
        }

        showBackground(getBooleanWindowSetting("showBackground", true));
        contentPanel.setBackground(Color.decode(getWindowSetting("background", "-1")));

        if (!resizeAndMoveModeEnabled) {
            showBorder(getWindowSetting("showBorder", DEFAULT_BORDER));
        }
    }

    /**
     * Loads the settings to the window and triggers an repaint to apply the new
     * settings. This method should be overridden by the class that extends this
     * class.
     */
    public abstract void loadSettings();

    public String getWindowSetting(String setting, String defaultSetting) {
        if (windowSettings.containsKey(setting)) {
            return windowSettings.get(setting);
        }
        return defaultSetting;
    }

    public int getWindowSetting(String setting, int defaultSetting) {
        if (windowSettings.containsKey(setting)) {
            return Integer.parseInt(windowSettings.get(setting));
        }
        return defaultSetting;
    }

    public boolean getBooleanWindowSetting(String key, boolean defaultSetting) {
        String setting = windowSettings.get(key);
        return setting != null ? Boolean.parseBoolean(setting) : defaultSetting;
    }

    public Map<String, String> getSetting(String name, String setting) {
        Map<String, String> value = null;
        if (hasSetting(name, setting)) {
            value = settings.get(name);
        }
        return value;
    }

    /**
     * Adds reference to setting for specified asap2Data reference
     */
    public void addSetting(String asap2Ref, String setting, String value) {
        // get settings for asap2Ref
        Map<String, String> sett = settings.get(asap2Ref);

        if (sett == null) {
            sett = new HashMap<>();
            settings.put(asap2Ref, sett);
        }

        // add new setting
        sett.put(setting, value);
    }

    public boolean hasSetting(String name, String setting) {
        boolean hasSetting = settings.containsKey(name) && settings.get(name).containsKey(setting);
        return hasSetting;
    }

    /**
     * Removes reference to setting for specified asap2Data reference
     */
    public String removeSetting(String asap2Ref, String setting) {
        return settings.get(asap2Ref).remove(setting);
    }

    /**
     * Adds reference to setting for specified component
     */
    public void addWindowSetting(String windowSetting, String value) {
        windowSettings.put(windowSetting, value);
    }

    /**
     * Removes reference to setting for specified component
     */
    public void removeWindowSetting(String windowSetting) {
        windowSettings.remove(windowSetting);
    }

    /**
     * Returns the format for an asap2Data element
     */
    public NumberFormat getFormat(String asap2Ref, Double value) {
        DecimalFormat format;
        int decimals = 0;
        boolean forceScientificNotation = false;

        if (settings.containsKey(asap2Ref) && settings.get(asap2Ref).containsKey("decimals")) {
            String decimalsStr = settings.get(asap2Ref).get("decimals");
            decimals = Integer.parseInt(decimalsStr);
        }

        if (settings.containsKey(asap2Ref) && settings.get(asap2Ref).containsKey("scientificNotation")) {
            String scientificNotationString = settings.get(asap2Ref).get("scientificNotation");
            forceScientificNotation = Integer.parseInt(scientificNotationString) == 1;
        }

        if (forceScientificNotation || isValueInScientificNotationRange(value)) {
            format = getScientificNotation(decimals);
        } else {
            format = getDecimalNotation(decimals);
        }
        format.setRoundingMode(RoundingMode.HALF_UP);
        return format;
    }

    public DecimalFormat getDecimalNotation(int decimals) {
        String format = decimals <= 0 ? "0" : "0.";
        StringBuilder formatBuilder = new StringBuilder(format);
        for (int i = 0; i < decimals; i++) {
            formatBuilder.append("0");
        }
        return new DecimalFormat(formatBuilder.toString());
    }

    public DecimalFormat getScientificNotation(int decimals) {
        String format = decimals <= 0 ? "##0" : "##0.";
        StringBuilder formatBuilder = new StringBuilder(format);
        for (int i = 0; i < decimals; i++) {
            formatBuilder.append("0");
        }
        formatBuilder.append("E0");
        return new DecimalFormat("0.######E0");
    }

    public boolean isValueInScientificNotationRange(double value) {
        return (value > 999999 || value < -999999
                || (value < 0.000001 && value > 0.0)
                || (value < 0.0 && value > -0.000001));
    }

    /**
     * Performs the actions for the decimals menu item
     *
     * @param reference the asap2Ref to change the decimals for
     */
    public void actionDecimals(String reference) {
        int def;
        ASAP2Data asap2Data;

        if (currentConfig.getASAP2Data() != null) {
            asap2Data = currentConfig.getASAP2Data();
        } else {
            asap2Data = new ASAP2Data();    // Preventing a nullpointerException in the next if-statement
        }

        if (asap2Data.getASAP2Measurements().get(reference) != null) {
            def = asap2Data.getASAP2Measurements().get(reference).getDecimalCount();
        } else if (asap2Data.getASAP2Characteristics().get(reference) != null) {
            def = asap2Data.getASAP2Characteristics().get(reference).getDecimalCount();
        } else if (DbcManager.getSignalByName(reference) != null) {
            def = DbcManager.getSignalByName(reference).getDecimalCount();
        } else {
            MessagePane.showWarning("This signal or parameter could not be found in the currently loaded description files. "
                    + "\nPlease load the description file containing this signal or parameter");
            return;
        }

        int curr = def;
        if (settings.containsKey(reference) && settings.get(reference).containsKey("decimals")) {
            curr = Integer.parseInt(settings.get(reference).get("decimals"));
        }
        String dec = HANtune.showQuestion("Modify decimals", "Number of shown decimals (default=" + def + "):", "" + curr);
        if (dec != null) {
            try {
                int decimals = Integer.parseInt(dec);
                if (decimals > MAX_DECIMALS) {
                    MessagePane.showError("Maximal number of decimals is " + MAX_DECIMALS);
                    decimals = MAX_DECIMALS;
                }
                if (decimals < 0) {
                    decimals = 0;
                }
                addSetting(reference, "decimals", "" + decimals);
            } catch (NumberFormatException ex) {
                AppendToLogfile.appendError(Thread.currentThread(), ex);
                MessagePane.showError("Input was not a number");
            }
        }
        loadSettings();
    }

    /**
     * Toggles the scientific notation on or off and saves the value in the
     * settings
     *
     * @param asap2Ref the asap2Ref to toggle the scientific notation for
     */
    public void forceScientificNotation(String asap2Ref) {
        if (!settings.containsKey(asap2Ref)) {
            addSetting(asap2Ref, "scientificNotation", "1");
        } else if (settings.containsKey(asap2Ref)) {
            if (settings.get(asap2Ref).containsKey("scientificNotation")) {
                Integer scientificNotationOn = Integer.parseInt(settings.get(asap2Ref).get("scientificNotation"));
                if (scientificNotationOn == 1) {
                    addSetting(asap2Ref, "scientificNotation", "0");
                } else if (scientificNotationOn == 0) {
                    addSetting(asap2Ref, "scientificNotation", "1");
                }
            } else {
                addSetting(asap2Ref, "scientificNotation", "1");
            }
        }
    }

    private void setKeyBindActions() {
        getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke("UP"), "UP_WINDOW_ACTION");
        getActionMap().put("UP_WINDOW_ACTION", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                moveWindow(KeyEvent.VK_UP);
            }
        });

        getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke("DOWN"), "DOWN_WINDOW_ACTION");
        getActionMap().put("DOWN_WINDOW_ACTION", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                moveWindow(KeyEvent.VK_DOWN);
            }
        });

        getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke("LEFT"), "LEFT_WINDOW_ACTION");
        getActionMap().put("LEFT_WINDOW_ACTION", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                moveWindow(KeyEvent.VK_LEFT);
            }
        });

        getInputMap(JComponent.WHEN_FOCUSED).put(KeyStroke.getKeyStroke("RIGHT"), "RIGHT_WINDOW_ACTION");
        getActionMap().put("RIGHT_WINDOW_ACTION", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                moveWindow(KeyEvent.VK_RIGHT);
            }
        });
    }

    private void moveWindow(int direction) {
        if (getTab().isResizeAndMoveModeEnabled()) {
            switch (direction) {
                case KeyEvent.VK_UP:
                    setLocation(getX(), getY() - getGrid());
                    break;
                case KeyEvent.VK_DOWN:
                    setLocation(getX(), getY() + getGrid());
                    break;
                case KeyEvent.VK_LEFT:
                    setLocation(getX() - getGrid(), getY());
                    break;
                case KeyEvent.VK_RIGHT:
                    setLocation(getX() + getGrid(), getY());
                    break;
            }
        }
    }

    /**
     * Adds a confirmation window to the default closing action of the window
     * unless the doNotCloseWarning checkbox in UserPreferences is checked and
     * informs the tabmanager object about the removal.
     */
    @Override
    public void doDefaultCloseAction() {
        setMarkedForDeletion(true);
        if (userPrefs.getBoolean("doNotCloseWarning", false)
                || HANtune.showConfirm("Are you sure you want to close this window?") == 0) {
        forceCloseAction();
        }
        setMarkedForDeletion(false);
    }

    public void forceCloseAction() {
        HANtuneTab tab = getTab();
        clearWindow();
        currentConfig.getHANtuneManager().removeActiveWindowFromList(this);
        super.doDefaultCloseAction();
        tab.adaptSize();

    }

    public boolean isMarkedForDeletion() {
        return markedForDeletion;
    }

    public void setMarkedForDeletion(boolean markedForDeletion) {
        this.markedForDeletion = markedForDeletion;
        if (!getTab().isResizeAndMoveModeEnabled()) setResizeAndMoveMode(markedForDeletion);

    }

    private HANtuneTab getTab() {
        if (getParent() != null && getParent() instanceof HANtuneTab) {
            return (HANtuneTab) getParent();
        }
        return null;
    }

    public void clearWindow() {
        contentPanel.removeAll();
        super.getContentPane().removeAll();
        super.getContentPane().add(contentPanel);
    }

    /**
     * This function will pack the window around its the components
     * (super.pack). Afterwards it will set its minimum size to either the
     * defaultMinimumSize or the current size of the packed window. If
     * !verticalResizable or !horizontalResizable respectively the maximum
     * height or width will be equal to the minimum, to disable resizing.
     *
     */
    public void packAndSetMinMax() {
        super.pack();
        snapToGrid();
        Dimension maximumSize = getMaximumSize();
        Dimension minimumSize;
        if (defaultMinimumSize != null) {
            minimumSize = defaultMinimumSize;
        } else {
            minimumSize = getSize();
        }

        if (!verticalResizable) {
            maximumSize.height = minimumSize.height;
        }

        if (!horizontalResizable) {
            maximumSize.width = minimumSize.width;
        }

        setMaximumSize(maximumSize);
        setMinimumSize(minimumSize);
        resizer.setMinimumSize(minimumSize);
        resizer.setMaximumSize(maximumSize);
    }

    /**
     * This function controls the vertical resizing property of the window
     *
     * @param verticalResizable verticalResizable
     */
    public void setVerticalResizable(boolean verticalResizable) {
        this.verticalResizable = verticalResizable;
    }

    /**
     * This function controls the horizontal resizing property of the window
     *
     * @param horizontalResizable horizontalResizable
     */
    public void setHorizontalResizable(boolean horizontalResizable) {
        this.horizontalResizable = horizontalResizable;
    }

    public void setTitleTooltip(String tooltip) {
    }

    public void storeSize() {
        this.width = super.getWidth();
        this.height = super.getHeight();
    }

    public void restoreSize() {
        if (width != 0 || height != 0) {
            this.setSize(width, height);
        }
    }

    private void snapToGrid() {
        if (getWidth() % getGrid() != overlap || getHeight() % getGrid() != overlap) {
            int width = ((getWidth() - overlap + getGrid() - 1) / getGrid()) * getGrid();
            int height = ((getHeight() - overlap + getGrid() - 1) / getGrid()) * getGrid();
            setSize(width + overlap, height + overlap);
        }

        if (getX() % getGrid() != 0 || getY() % getGrid() != 0) {
            int x = ((getX() + getGrid() / 2) / getGrid()) * getGrid();
            int y = ((getY() + getGrid() / 2) / getGrid()) * getGrid();
            setLocation(x, y);
        }
    }

    private void editBorderTitle() {
        String title = HANtune.showQuestion("Edit border title", "Enter a title for the border of this window:", getBorderTitle());
        windowSettings.put("borderTitle", title);
        loadWindowSettings();
    }

    private String getBorderTitle() {
        String title = getWindowSetting("borderTitle", "");
        if (title.isEmpty()) {
            title = HANtuneWindowFactory.getHANtuneWindowType(this).toString();
        }
        return title;
    }

    protected void saveAndShowBorder(String borderType) {
        windowSettings.put("showBorder", borderType);
        showBorder(borderType);
        if (resizeAndMoveModeEnabled) {
            this.setBorder(createResizeBorder());
        }
    }

    protected void showBorder(String borderType) {
        switch (borderType) {
            default:
            case "noBorder":
                border = BorderFactory.createEmptyBorder(1, 1, 1, 1);
                break;
            case "lineBorder":
                border = BorderFactory.createLineBorder(Color.GRAY);
                break;
            case "titledBorder":
                border = createTitledBorder(BorderFactory.createLineBorder(Color.GRAY));
                break;
        }
        Border empty = BorderFactory.createEmptyBorder(2, 2, 2, 2);
        border = BorderFactory.createCompoundBorder(empty, border);
        setBorder(border);
        pack();
        restoreSize();
    }

    private Border createTitledBorder(Border border) {
        Border emptyOuter = BorderFactory.createEmptyBorder(-1, -2, -2, -2);
        Border titledBorder = new OpaqueTitledBorder(border, getBorderTitle(), contentPanel);
        Border compound = BorderFactory.createCompoundBorder(emptyOuter, titledBorder);
        Border emptyInner = BorderFactory.createEmptyBorder(-2, -2, -2, -2);
        return BorderFactory.createCompoundBorder(compound, emptyInner);
    }

    protected void saveAndShowBackground(boolean show) {
        windowSettings.put("showBackground", String.valueOf(show));
        showBackground(show);
    }

    protected void showBackground(boolean show) {
        getContentPane().setOpaque(show);
        repaint();
    }

    protected void editBackgroundColor() {
        Color newColor = JColorChooser.showDialog(this, "Choose background color", this.getBackground());
        if (newColor != null) {
            saveAndSetBackground(newColor);
        }
    }

    protected void saveAndSetBackground(Color background) {
        addWindowSetting("background", Integer.toString(background.hashCode()));
        contentPanel.setBackground(background);
        repaint();
    }

    private void createResizeAndMoveComponents() {
        JPanel overlayPanel = new JPanel();
        overlayPanel.setLayout(new BoxLayout(overlayPanel, BoxLayout.Y_AXIS));
        Box labelContainer = Box.createHorizontalBox();
        labelContainer.add(Box.createHorizontalGlue());
        closeButton = new CustomButton("/images/close_icon_gray.png");
        closeButton.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 2));
        closeButton.setButtonActionPerformed(e -> doDefaultCloseAction());
        labelContainer.add(closeButton);
        overlayPanel.add(labelContainer);
        overlayPanel.setOpaque(false);
        this.setGlassPane(overlayPanel);

        Insets dragInsets = new Insets(3, 3, 3, 3);
        Insets edgeInsets = new Insets(0, 0, -500, -500);

        resizer = new ComponentResizer();
        resizer.setDragInsets(dragInsets);
        resizer.setSnapSize(new Dimension(getGrid(), getGrid()));

        mover = new ComponentMover(this, overlayPanel);
        mover.setDragInsets(dragInsets);
        mover.setSnapSize(new Dimension(getGrid(), getGrid()));
        mover.setEdgeInsets(edgeInsets);
    }

    /**
     * Toggles Resize and Move mode on this window.
     *
     * @param resizeMode wether to enable or disable Resize and Move mode
     */
    public void setResizeAndMoveMode(boolean resizeMode) {
        if (resizeMode != getGlassPane().isVisible()) {
            getGlassPane().setVisible(resizeMode);
            if (resizeMode) {
                this.resizeAndMoveModeEnabled = true;
                this.setBorder(createResizeBorder());
                resizer.registerComponent(this);
                this.setCursor(new Cursor(Cursor.MOVE_CURSOR));
            } else {
                storeSize();
                this.resizeAndMoveModeEnabled = false;
                this.setBorder(border);
                resizer.deregisterComponent(this);
                this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        }
    }

    /**
     * Toggles Resize and Move mode on this window's tab (and all its children).
     */
    private void toggleResizeAndMoveMode() {
        HANtuneTab tab = getTab();
        if (tab != null) {
            tab.enableResizeAndMoveMode(!tab.isResizeAndMoveModeEnabled());
        }
    }

    private Border createResizeBorder() {
        Color color = isSelectedInTab() ? Color.BLACK : Color.LIGHT_GRAY;
        Border emptyOuter = BorderFactory.createEmptyBorder(2, 2, 2, 2);
        Border dashed = BorderFactory.createDashedBorder(color, 1, 5, 5, false);
        Border resize = getWindowSetting("showBorder", DEFAULT_BORDER).equals("titledBorder") ? createTitledBorder(dashed) : dashed;
        return BorderFactory.createCompoundBorder(emptyOuter, resize);
    }

    /**
     * @return whether or not this window is part of a HANtuneTab's "selection"
     */
    public boolean isSelectedInTab() {
        return selectedInTab;
    }

    /**
     * Set whether or not this window is part of a HANtuneTab's "selection"
     *
     * @param selected
     */
    public void setSelectedInTab(boolean selected) {
        selectedInTab = selected;
        if (selected) {
            this.requestFocusInWindow();
            closeButton.setImage("/images/close_icon_black.png");
        } else if (!markedForDeletion) {
            closeButton.setImage("/images/close_icon_gray.png");
        }
        if (getGlassPane().isVisible()) {
            HANtuneWindow.this.setBorder(createResizeBorder());
        }
    }

    /**
     * @return this HAtuneWindow's ComponentMover
     */
    public ComponentMover getMover() {
        return mover;
    }

    private void addInternalFrameListener() {
    }

    private void addComponentListener() {
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentMoved(ComponentEvent event) {
                if (getTab() != null) {
                    getTab().adaptSize();
                }
            }
        });
    }

    @Override
    public void setSize(int width, int height) {
        super.setSize(width, height);
        snapToGrid();
    }

    @Override
    public void setLocation(int x, int y) {
        super.setLocation(x, y);
        snapToGrid();
    }

    /**
     * Convenience method that moves this component to position 0 if its parent
     * is a <code>JLayeredPane</code>.
     */
    @Override
    public void moveToFront() {
        //Make sure nothing happens here. moveToFront should be managed by the rmb context menu, not by super class.
    }

    /**
     * Convenience method that moves this component to position -1 if its parent
     * is a <code>JLayeredPane</code>.
     */
    @Override
    public void moveToBack() {
        //Make sure nothing happens here. moveToBack should be managed by the rmb context menu, not by super class.
    }

    public void moveHANtuneWindowToBack() {
        ((JLayeredPane) getParent()).moveToBack(this);
    }

    public void moveHANtuneWindowToFront() {
        ((JLayeredPane) getParent()).moveToFront(this);
    }

    /**
     * @return the grid
     */
    public int getGrid() {
        return grid;
    }
}
