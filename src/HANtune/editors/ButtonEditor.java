/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.editors;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import ErrorLogger.AppendToLogfile;
import HANtune.editors.ButtonEditor.ButtonEditorElement;
import datahandling.CurrentConfig;
import datahandling.Parameter;
import datahandling.ParameterListener;
import util.MessagePane;

/**
 * Shows multiple asap2Data parameters to edit between 2 states using a button.
 *
 * @author Sander, Michiel Klifman
 */
@SuppressWarnings("serial")
public class ButtonEditor extends HANtuneEditor<ButtonEditorElement> {

    /**
     * The constructor of the ButtonEditor class
     */
    public ButtonEditor() {
        setTitle("ButtonEditor");
        setVerticalResizable(false);
    }
    
    @Override
    public void rebuild() {
        super.rebuild();
        packAndSetMinMax();
    }
    
    @Override
    protected ButtonEditorElement getListenerForParameter(Parameter parameter) {
        return createButtonEditorElement(parameter);
    }
    
    
    private ButtonEditorElement createButtonEditorElement(Parameter parameter) {
        int row = getContentPane().getComponentCount();

        GridBagConstraints gridBagConstraints;
        final ButtonEditorElement asap2Row = new ButtonEditorElement(parameter);
        JLabel nameLabel = asap2Row.getNameLabel();
        nameLabel.setToolTipText(parameter.getSource());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = row;
        gridBagConstraints.insets = new Insets(7, 7, 7, 7);
        getContentPane().add(nameLabel, gridBagConstraints);

        JLabel valueLabel = new JLabel("");
        valueLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        valueLabel.setPreferredSize(new Dimension(50, 18));
        asap2Row.setValueLabel(valueLabel);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = row;
        gridBagConstraints.insets = new Insets(7, 7, 7, 7);
        getContentPane().add(valueLabel, gridBagConstraints);

        if (parameter.getUnit() != null && !parameter.getUnit().equals("")) {
            JLabel unitLabel = new JLabel("[" + parameter.getUnit() + "]");
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 2;
            gridBagConstraints.gridy = row;
            gridBagConstraints.insets = new Insets(7, 7, 7, 7);
            getContentPane().add(unitLabel, gridBagConstraints);
        }

        JToggleButton valueButton = new JToggleButton();
        valueButton.setFocusable(false);
        valueButton.addActionListener(e -> {
            if (asap2Row.getValue() != null) {
                asap2Row.toggleState();
                double value = asap2Row.getValue();
                parameter.send(value);
            }
        });
        
        valueButton.setPreferredSize(new Dimension(130, 25));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = row;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(2, 2, 2, 2);
        getContentPane().add(valueButton, gridBagConstraints);
        
        asap2Row.setValueComponent(valueButton);
        
        loadSettings();

        // add popup menu //
        if (!CurrentConfig.getInstance().isServiceToolMode()) {
            addPopupMenuItems(parameter, asap2Row, valueButton);
        } else {
            valueButton.setComponentPopupMenu(null);
        }

        row++;
        return asap2Row;
    }

    
    private void addPopupMenuItems(Parameter parameter, final ButtonEditorElement asap2Row,
        JToggleButton valueButton) {
        JPopupMenu popupMenu = new JPopupMenu();

        JMenu modStateValueMenuItem = new JMenu("Modify values");

        JMenuItem modState1ValueMenuItem = new JMenuItem("Modify active value");
        modState1ValueMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String currentActiveValue = asap2Row.state1Value == null ? ""
                    : util.Util.getStringValue(asap2Row.state1Value, "0.##########");
                String newValue = HANtune.HANtune.showQuestion("Set active value",
                    "Set " + parameter.getName() + " to this value when button is pressed:",
                    currentActiveValue);
                if (newValue != null) {
                    try {
                        double value = Double.parseDouble(newValue);
                        double validValue = parameter.validateValue(value);
                        asap2Row.setState1Value(validValue);
                        addSetting(parameter.getName(), "state1Value", Double.toString(validValue));

                        if (asap2Row.getState1Value() != null && asap2Row.getState2Value() != null
                            && asap2Row.getFirstUse() == true) {
                            asap2Row.setFirstUse(false);
                            addSetting(parameter.getName(), "FirstUse", "false");
                            valueButton.setText("Ready to use");
                            repaint();
                        }
                    } catch (Exception ex) {
                        AppendToLogfile.appendMessage("Input is not a number");
                        AppendToLogfile.appendError(Thread.currentThread(), ex);
                        String valueMessage = currentActiveValue.isEmpty() ? "No value has been set."
                            : "Current value (" + currentActiveValue + ") will remain to be used.";
                        MessagePane
                            .showError("Input is not a number. " + valueMessage + " Please try again.");
                    }
                }
            }
        });
        modStateValueMenuItem.add(modState1ValueMenuItem);

        JMenuItem modState2ValueMenuItem = new JMenuItem("Modify inactive value");
        modState2ValueMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String currentInactiveValue = asap2Row.state2Value == null ? ""
                    : util.Util.getStringValue(asap2Row.state2Value, "0.##########");
                String newValue = HANtune.HANtune.showQuestion("Set inactive value",
                    "Set " + parameter.getName() + " to this value when button is not pressed:",
                    currentInactiveValue);
                if (newValue != null) {
                    try {
                        double value = Double.parseDouble(newValue);
                        double validValue = parameter.validateValue(value);
                        asap2Row.setState2Value(validValue);
                        addSetting(parameter.getName(), "state2Value", Double.toString(validValue));

                        if (asap2Row.getState1Value() != null && asap2Row.getState2Value() != null
                            && asap2Row.getFirstUse() == true) {
                            asap2Row.setFirstUse(false);
                            addSetting(parameter.getName(), "FirstUse", "false");
                            valueButton.setText("Ready to use");
                            repaint();
                        }
                    } catch (Exception exc) {
                        AppendToLogfile.appendMessage("Input is not a number");
                        AppendToLogfile.appendError(Thread.currentThread(), exc);
                        String valueMessage = currentInactiveValue.isEmpty() ? "No value has been set."
                            : "Current value (" + currentInactiveValue + ") will remain to be used.";
                        MessagePane
                            .showError("Input is not a number. " + valueMessage + " Please try again.");
                    }
                }
            }
        });
        modStateValueMenuItem.add(modState2ValueMenuItem);

        JMenu modStateTextMenuItem = new JMenu("Modify Texts");

        JMenuItem modState1TextMenuItem = new JMenuItem("Modify active text");
        modState1TextMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String newValue = HANtune.HANtune.showQuestion("Set active text",
                    "Set this(" + parameter.getName() + ") button's text to this when it is pressed:", "");
                if (newValue.matches("")) {
                    newValue = " ";
                }
                addSetting(parameter.getName(), "state1Text", newValue);
                asap2Row.setState1Text(settings.get(parameter.getName()).get("state1Text"));
            }
        });
        modStateTextMenuItem.add(modState1TextMenuItem);

        JMenuItem modState2TextMenuItem = new JMenuItem("Modify inactive text");
        modState2TextMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String newValue = HANtune.HANtune.showQuestion("Set inactive text",
                    "Set this (" + parameter.getName() + ") button's text to this when it is not pressed:",
                    "");
                if (newValue.matches("")) {
                    newValue = " ";
                }
                addSetting(parameter.getName(), "state2Text", newValue);
                asap2Row.setState2Text(settings.get(parameter.getName()).get("state2Text"));
            }
        });
        modStateTextMenuItem.add(modState2TextMenuItem);

        popupMenu.add(modStateValueMenuItem);
        popupMenu.add(modStateTextMenuItem);
        valueButton.setComponentPopupMenu(popupMenu);
    }

    /**
     * This method load the HANtuneWindow specific settings for each asap2
     * reference. This method should be called only if settings have been
     * changed. The method automatically triggers a repaint after the settings
     * are loaded.
     */
    @Override
    public void loadSettings() {
        repaint();
        for (Map.Entry<Parameter, ButtonEditorElement> entry : parameters.entrySet()) {
            Parameter parameter = entry.getKey();
            ButtonEditorElement element = entry.getValue();
            JToggleButton valueButton = (JToggleButton) element.getValueComponent();
                if (settings.containsKey(parameter.getName()) && settings.get(parameter.getName()).containsKey("state1Value")) {
                    element.setState1Value((Double.parseDouble(settings.get(parameter.getName()).get("state1Value"))));
                }

                if (settings.containsKey(parameter.getName()) && settings.get(parameter.getName()).containsKey("state2Value")) {
                    element.setState2Value((Double.parseDouble(settings.get(parameter.getName()).get("state2Value"))));
                }

                if (settings.containsKey(parameter.getName()) && settings.get(parameter.getName()).containsKey("state1Text")) {
                    if (valueButton.isSelected()) {
                        valueButton.setText(settings.get(parameter.getName()).get("state1Text"));
                    }
                    element.setState1Text(settings.get(parameter.getName()).get("state1Text"));
                } else {
                    if (valueButton.isSelected()) {
                        valueButton.setText(element.state1Text);
                    }
                }

                if (settings.containsKey(parameter.getName()) && settings.get(parameter.getName()).containsKey("state2Text")) {
                    if (!valueButton.isSelected()) {
                        valueButton.setText(settings.get(parameter.getName()).get("state2Text"));
                    }
                    element.setState2Text(settings.get(parameter.getName()).get("state2Text"));
                } else {
                    if (!valueButton.isSelected()) {
                        valueButton.setText(element.state2Text);
                    }
                }

                if (settings.containsKey(parameter.getName()) && settings.get(parameter.getName()).containsKey("FirstUse")) {
                    element.setFirstUse(Boolean.parseBoolean(settings.get(parameter.getName()).get("FirstUse")));
                }
            
        }
    }

    public class ButtonEditorElement extends HANtuneEditorElement implements ParameterListener {
        
        private final Parameter parameter;
        
        public ButtonEditorElement(Parameter parameter) {
            super(parameter, ButtonEditor.this);
            this.parameter = parameter;
        }

        @Override
        public void valueUpdate(double value) {
            if (state1Value != null && state2Value != null) {
                if (value == state1Value) {
                    getButton().setSelected(true);
                } else if (value == state2Value) {
                    getButton().setSelected(false);
                }

                getValueLabel().setText(util.Util.getStringValue(value, "0.##########"));
                toggleState();
            }
        }
        
        @Override
        public void stateUpdate() {
            super.stateUpdate();
            JToggleButton button = (JToggleButton) getValueComponent();
            Border border;
            if (!parameter.isActive()) {
                button.setEnabled(false);
                border = BorderFactory.createEmptyBorder(3, 3, 3, 3);
            } else if (!parameter.isInSync()) {
                button.setEnabled(true);
                border = BorderFactory.createLineBorder(WARNING_COLOR, 3);
            } else {
                button.setEnabled(true);
                border = BorderFactory.createEmptyBorder(3, 3, 3, 3);
            }
            button.setBorder(border);
        }

        private Double state1Value = null;
        private Double state2Value = null;
        private String state1Text = "Active";
        private String state2Text = "Inactive";
        private Boolean FirstUse = true;

        public String getState1Name() {
            return state1Text;
        }

        public Double getState1Value() {
            return state1Value;
        }

        public Double getValue() {
            return getButton().isSelected() ? state1Value : state2Value;
        }
        
        public JToggleButton getButton() {
            return (JToggleButton) getValueComponent();
        }

        public Double getState2Value() {
            return state2Value;
        }

        public void setState1Text(String state1Name) {
            this.state1Text = state1Name;
        }

        public void setState1Value(Double state1Value) {
            this.state1Value = state1Value;
        }

        public void setState2Text(String state2Name) {
            this.state2Text = state2Name;
        }

        public void setState2Value(Double state2Value) {
            this.state2Value = state2Value;
        }

        public boolean getFirstUse() {
            return FirstUse;
        }

        public void setFirstUse(boolean FirstUse) {
            this.FirstUse = FirstUse;
        }

        public void toggleState() {
            JToggleButton button = (JToggleButton) getValueComponent();
            if (button.isSelected()) {
                button.setText(state1Text);
            } else {
                button.setText(state2Text);
            }
        }
    }
}
