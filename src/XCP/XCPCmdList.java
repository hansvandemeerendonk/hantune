/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XCP;

/**
 * Command List class for storing and handling the XCP command sequence to be
 * executed by the XCP Protocol Layer driver.
 *
 * @author Aart-Jan
 */
public class XCPCmdList {

    /**
     * Flag to determine whether the list is active
     */
    private boolean active = false;
    /**
     * Flag to determine whether the list is completed
     */
    private boolean sequence_completed = true;
    /**
     * Maximum number of commands in this list
     */
    private char MAX_ID = 4096;
    /**
     * Maximum number of bytes that can be send by one command
     */
    private char MAX_CTO = 0x08;
    /**
     * Currently selected command identifier
     */
    private char curr_id = 0;
    /**
     * Array of Command List items
     */
    public XCPCmdListItem[] cmd = new XCPCmdListItem[MAX_ID];

    /**
     * Constructor, initializes the array of list items
     */
    public XCPCmdList(){
        for(char i=0; i<MAX_ID; i++){
            cmd[i] = new XCPCmdListItem();
        }
    }

    /**
     * Sets maximum number of commands in this list
     *
     * @param max_cto Maximum number of commands in this list
     */
    public void setMAX_CTO(char max_cto){
        this.MAX_CTO = max_cto;
    }

    /**
     * Start (enable) this list
     */
    public void start(){
        sequence_completed = false;
        active = true;
    }

    /**
     * Stop (disable) this list
     */
    public void stop(){
        active = false;
    }

    /**
     * Terminate this list
     */
    public void exit(){
        stop();
        reset();
        sequence_completed = true;
    }

    /**
     * Returns whether this list is active
     *
     * @return whether this list is active
     */
    public boolean isActive(){
        return active;
    }

    /**
     * Sets whether this list is completed
     *
     * @param sc Flag to determine whether the list is completed
     */
    public void setSequenceCompleted(boolean sc){
        sequence_completed = sc;
    }

    /**
     * Returns whether this list is completed
     *
     * @return whether this list is completed
     */
    public boolean isSequenceCompleted(){
        return sequence_completed;
    }

    /**
     * Empties the whole list
     */
    public void reset(){
        if(XCP.DEBUG_PRINT){
            System.out.println("\r\nEXIT XCPCmdList");
            print();
        }
        reset((char)0, MAX_ID);
        curr_id = 0;
    }

    /**
     * Empties a part of this list
     * @param from first identifier to reset
     * @param to last identifier to reset
     */
    public void reset(char from, char to){
        if(from < (char)0 || from > MAX_ID) return;
        if(to < (char)0 || to > MAX_ID) return;
        for(char i=from; i<to; i++){
            cmd[i].reset();
        }
    }

    /**
     * Swaps remaining list items back to beginning of the list an reset unused items
     *
     * @param id highest identifier to swap
     * @param reserved lowest identifier to swap
     */
    public void swap(char id, char reserved){
        char cnt = (char) (MAX_ID - id - 1);
        for(char i=(char)(MAX_ID-1); i>=cnt && cnt >= 0; i--){
            copy(i, cnt);
            cmd[i].reset();
            cnt--;
        }
        reset((char) (MAX_ID - id - 1), reserved);
    }

    /**
     * Moves items down the list a specified amount of items
     *
     * @param id lowest identifier to move down
     * @param steps number of items to move down
     */
    public void move(char id, char steps){
        if(steps < 0) steps = 0;
        
        // check for overflow
        char hid = getHighestID();
        if((hid + steps) >= MAX_ID){
            swap((char) (MAX_ID - steps), id);
        }

        // move commands to new location
        for(char i=(char)(MAX_ID-1); i>=(id+steps); i--){
            if(cmd[(char)(i-steps)].cmd > 0){
                copy((char)(i-steps), i);
                cmd[(char)(i-steps)].reset();
            }
        }
    }

    /**
     * Copies one item into another
     *
     * @param src source command identifier to copy
     * @param dst destination command identifier to copy into
     */
    public void copy(char src, char dst){
        cmd[dst].cmd = cmd[src].cmd;
        cmd[dst].error = cmd[src].error;
        cmd[dst].param = cmd[src].param;
        cmd[dst].repeat = cmd[src].repeat;
    }

    /**
     * Inserts a command into the specified location. Already present commands
     * at this location and below, will move one item down the list to make
     * place for the new command.
     *
     * @param id command identifier to insert the new command into
     * @param command command code
     * @param param command parameters
     * @param repeat number of command repeats
     */
    public void insert(char id, char command, char[] param, char repeat) {
        move(id, (char)1);
        cmd[id].reset();
        cmd[id].cmd = command;
        cmd[id].param = param;
        cmd[id].repeat = repeat;
    }

    /**
     * Inserts command into command list by moving the remaining commands down
     * one step and placing the new command in the available empty space.
     *
     * @param id the place where the command will be inserted
     * @param command the command to be inserted
     * @param param the parameters of the command to be inserted
     */
    public void insertCmd(char id, char command, char[] param){
        char repeat = 0;
        if(cmd[getPreviousID((char)2)].cmd == command
                && cmd[getPreviousID((char)2)].param == param
                && cmd[getPreviousID((char)2)].error != XCP.ERR_EMPTY){
            repeat = (char) (cmd[getPreviousID((char)2)].repeat + 1);
        }
        if(cmd[getPreviousID((char)1)].cmd == command
                && cmd[getPreviousID((char)1)].param == param
                && cmd[getPreviousID((char)1)].error != XCP.ERR_EMPTY){
            repeat = (char) (cmd[getPreviousID((char)1)].repeat + 1);
        }
        insert(id, command, param, repeat);
    }

    /**
     * Inserts a copy of another list item into the list
     *
     * @param src source command identifier to copy
     * @param dst destination command identifier to insert the command into
     */
    public void insertCopy(char src, char dst){
        insert(dst, cmd[src].cmd, cmd[src].param, (char)(cmd[src].repeat+1));
    }

    /**
     * Deletes all commands with the specified command code
     *
     * @param command command code to delete
     */
    public void deleteCmds(char command){
        for(char i=(char)(MAX_ID-1); i>=getCurrentID(); i--){
            if(cmd[i].cmd == command) deleteCmd(i);
        }
    }

    /**
     * Deletes the specified command and moves following commands back one item
     *
     * @param id command identifier to delete
     */
    public void deleteCmd(char id){
        char hid = getHighestID();

        // move commands to new location
        for(char i=id; i<hid; i++){
            if(cmd[i+1].cmd > 0){
                copy((char)(i+1), i);
            }
        }

        // reset highest id in list
        cmd[hid].reset();
    }

    /**
     * Returns currently selected command identifier
     *
     * @return current command identifier
     */
    public char getCurrentID(){
        return curr_id;
    }

    /**
     * Increments the currently selected command identifier with one
     */
    public void incrementCurrentID(){
        curr_id = (char) ((curr_id + 1) & 0xFFF);
    }

    /**
     * Returns previous selected command identifier
     *
     * @return previous command identifier
     */
    public char getPreviousID(){
        return getPreviousID((char)1);
    }

    /**
     * Returns previous selected command identifier a specified number of items ago
     *
     * @param steps number of stepszzz
     * @return previous command identifier
     */
    public char getPreviousID(char steps){
        return (char)((curr_id - steps) & 0xFFF);
    }

    /**
     * Returns the highest present command identifier
     *
     * @return highest command identifier
     */
    public char getHighestID(){
        for(char i=(char)(MAX_ID-1); i>0; i--){
            if(cmd[i].cmd > 0) return i;
        }
        return 0;
    }

    /**
     * Prints the content of the command list to the command line
     */
    public void print(){
        System.out.println("");
        System.out.println("----- COMMAND LIST -----");
        System.out.println("ID   CMD   ERROR  REPEAT");
        for(char i=0; i<MAX_ID; i++){
            if(cmd[i].cmd > 0){
                System.out.print(Integer.toHexString(i) + "   ");
                if(i < 0x10) System.out.print(" ");
                System.out.print(Integer.toHexString(cmd[i].cmd) + "    ");
                if(cmd[i].cmd < 0x10) System.out.print(" ");
                System.out.print(Integer.toHexString(cmd[i].error) + "     ");
                if(cmd[i].error < 0x10) System.out.print(" ");
                System.out.print(Integer.toHexString(cmd[i].repeat));
                System.out.print("\n");
            }
        }
        System.out.println("");
    }

    /**
     * Contains the information about an item in the command list
     */
    public class XCPCmdListItem {
        /**
         * Command code
         */
        public char cmd = 0;
        /**
         * Array of command parameters
         */
        public char[] param = new char[MAX_CTO];
        /**
         * Error code. Default value is 0xFE. Value for no error is 0xFF.
         */
        public char error = XCP.ERR_EMPTY;
        /**
         * Number of command repeats
         */
        public char repeat = 0;

        /**
         * Resets this list item to default values
         */
        public void reset(){
            cmd = 0;
            //param = new char[MAX_CTO];
            error = XCP.ERR_EMPTY;
            repeat = 0;
        }
        
        @Override
        public String toString() {
            return "cmd: " + (int)cmd + ", error: " + (int)error + ", repeat: " + (int)repeat;
        }
    }
    
}
