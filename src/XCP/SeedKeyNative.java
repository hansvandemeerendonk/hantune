/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XCP;

/**
 * Acts as an interface between Java and the native Seed&Key DLL.
 * This interface is needed because Java cannot pass a pointer as a parameter
 * when calling a native method.
 *
 * @author Aart-Jan
 */
public class SeedKeyNative {

    /**
     * Path to the Seed&Key DLL file to be used.
     */
    public String DLL_file = "";
    /**
     * Array containing the key values as calculated by ComputeKeyFromSeed
     * @see XCP.SeedKeyNative#ComputeKeyFromSeed(char, char[])
     */
    public char[] key = new char[0];
    /**
     * Indicates the length of key
     * @see XCP.SeedKeyNative#key
     */
    public char key_length = 0;
    /**
     * Bit mask containing the available priviliges, returned by GetAvailablePriviliges
     * @see XCP.SeedKeyNative#GetAvailablePrivileges() 
     */
    public char resources_mask = 0;

}
