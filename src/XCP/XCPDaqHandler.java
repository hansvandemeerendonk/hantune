/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XCP;

import XCP.XCPDaqHandler.DaqEventListener;
import java.util.EventListener;
import java.util.EventObject;
import javax.swing.event.EventListenerList;

/**
 * Handles incoming XCP DAQ messages. It also provides a mechanism to
 * manually request parameters using the XCP UPLOAD commands. All this
 * data can be saved realtime into a logfile.
 *
 * @author Aart-Jan
 */
public class XCPDaqHandler {

    /**
     * List of receive event listeners
     */
    protected EventListenerList listenerList = new EventListenerList();
    /**
     * 
     */
    private DaqEvent evt = new DaqEvent("DaqReceiveEvent");
    /**
     * Element data array
     */
    private XCPDaqItem[] items = new XCPDaqItem[1];

    /**
     * Receives a XCPDaqItem from the XCP Protocol Layer.
     * @param item the received item
     */
    public void receiveDaq(XCPDaqItem item){
        items[0] = item;
        evt.element_id = item.element_id;
        fireReceiveEvent(evt);
    }

    /**
     * Declarate receive event
     */
    public class DaqEvent extends EventObject {
        
        private static final long serialVersionUID = 7526471155622776185L;
        
        /**
         * Identifier of the ASAP2 element related to the DAQ data
         */
        public char element_id;
        
        /**
         * DaqEvent constructor
         * @param source source
         */
        public DaqEvent(Object source) {
            super(source);
        }
    }

    /**
     * Declarate daq listener class
     */
    public abstract class DaqEventListener implements EventListener {
        /**
         * Identifier of the ASAP2 element listening to
         */
        public char element_id = 0;

        /**
         * Called when an event for the specified element is given
         * @param evt event
         */
        public abstract void daqEventOccurred(DaqEvent evt);
    }

    /**
     * Allows classes to register for DaqEvents
     * @param listener listener
     */
    public void addDaqEventListener(DaqEventListener listener) {
        listenerList.add(DaqEventListener.class, listener);
    }

    /**
     * Allows classes to unregister for DaqEvents
     * @param listener listener
     */
    public void removeDaqEventListener(DaqEventListener listener) {
        listenerList.remove(DaqEventListener.class, listener);
    }

    /**
     * Fires DaqEvents
     * @param evt event
     */
    public void fireReceiveEvent(DaqEvent evt) {
        DaqEventListener[] listeners = listenerList.getListeners(DaqEventListener.class);
        // each listener occupies two elements - the first is the listener class
        // and the second is the listener instance
        for (int i=0; i<listeners.length; i+=2) {
            if (listeners[i+1].element_id == evt.element_id){                   // TODO: Find out how to cope with this build error
                listeners[i+1].daqEventOccurred(evt);
            }
        }
    }

}
