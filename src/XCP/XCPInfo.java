/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XCP;

/**
 * Contains information about the XCP support, configuration and status
 * at the XCP slave device.
 *
 * @author Aart-Jan
 */
public class XCPInfo {

    /* Parameters returned by CONNECT command */

    /**
     * Indicates whether Calibration and Paging support is available
     */
    public boolean support_cal_pag    = false;
    /**
     * Indicates whether DAQ lists support is available.
     */
    public boolean support_daq        = false;
    /**
     * Indicates whether data stimulation mode of a DAQ list is available.
     */
    public boolean support_stim       = false;
    /**
     * Indicates whether Flash programming support is available.
     */
    public boolean support_pgm        = false;
    /**
     * Indicates the byte order as used by the XCP slave device.
     * 0 = Intel format, 1 = Motorola format.
     */
    public char byte_order            = 0x01;
    /**
     * Indicates the address granularity.
     * That is the size contained at a single address.
     */
    public char address_granularity   = 0x00;
    /**
     * Indicates whether the XCP slave device supports
     * block communication mode from slave to master.
     */
    public boolean slave_block_mode   = false;
    /**
     * Indicates whether the command GET_COMM_MODE_INFO is supported.
     */
    public boolean comm_info_optional = false;
    /**
     * Indicates the XCP protocol layer version as used by the XCP slave device.
     */
    public char protocol_version      = 0x00;
    /**
     * Indicates the XCP transport layer version as used by the XCP slave device.
     */
    public char transport_version     = 0x00;


    /* Parameters returned by GET_STATUS command */

    /**
     * Indicates whether STORE_CAL_REQ is set.
     */
    public boolean store_cal_req      = false;
    /**
     * Indicates whether STORE_DAQ_REQ is set.
     */
    public boolean store_daq_req      = false;
    /**
     * Indicates whether CLEAR_DAQ_REQ is set.
     */
    public boolean clear_daq_req      = false;
    /**
     * Indicates whether data transfer is running.
     */
    public boolean daq_running        = false;
    /**
     * Indicates whether the slave is in RESUME mode.
     */
    public boolean slave_resume       = false;
    /**
     * Indicates whether the Calibration and Paging commands are protected.
     */
    public boolean protected_cal_pag  = false;
    /**
     * Indicates whether the DAQ list commands are protected.
     */
    public boolean protected_daq      = false;
    /**
     * Indicates whether the data stimulation commands are protected.
     */
    public boolean protected_stim     = false;
    /**
     * Indicates whether the Flash programming commands are protected.
     */
    public boolean protected_pgm      = false;
    /**
     * Session configuration identifier.
     */
    public char session_id            = 0x00;


    /* Parameters returned by GET_COMM_MODE_INFO command */

    /**
     * Indicates whether the XCP slave device supports
     * block communication mode from master to slave.
     */
    public boolean master_block_mode  = false;
    /**
     * Indicates whether the interleaved communication mode is supported.
     */
    public boolean interleaved_mode   = false;
    /**
     * Indicates the version number of the XCP driver used in the slave device.
     */
    public char xcp_driver_version    = 0x00;


    /* Parameters returned by GET_ID command */

    /**
     * Array of slave identifier bytes.
     */
    public char[] slave_identifier    = new char[0];
    /**
     * Indicates the length of the slave identifier array.
     */
    public int slave_identifier_len   = 0;


    /* XCP LIMITS */

    /**
     * Indicates the maximum CTO packet size in bytes.
     */
    public char MAX_CTO        = 0x08;
    /**
     * Indicates the maximum DTO packet size in bytes.
     */
    public char MAX_DTO        = 0x08;
    /**
     * Indicates the maximum allowed block size as the number of
     * consecutive command packets.
     */
    public static char MAX_BS         = 0x00;
    /**
     * Indicates the minimum required separation time between the packets
     * in block communication mode in units of 100 microseconds.
     */
    public static char MIN_ST         = 0x00;
    /**
     * Indicates the maximum number of consecutive packets in interleaved
     * communication mode the master can send to the receipt queue of
     * the slave device.
     */
    public static char QUEUE_SIZE     = 0x00;
    /**
     * Indicates the offset at which the command codes start.
     */
    public static final char OS       = 0xC8;

}
