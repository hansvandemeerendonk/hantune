/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ErrorLogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import nl.han.hantune.config.VersionInfo;

/**
 * GetSystemInfo retrieves and writes the system info to the errorlog for debugging purposes.
 * @author alaap
 */
public class GetSystemInfo extends Thread{

    @Override
    public void run() {
        writeSystemInfoToLogfile();
    }


    /**
    * Gathers all the relevant info to write to the logfile.
    */
    private static void writeSystemInfoToLogfile() {
        //update logfile
        VersionInfo vers = VersionInfo.getInstance();
        String msg = vers.getFullVersionText();
        String[] systemProperties = GetSystemInfo.getSystemProperties();
        for (String prop : systemProperties) {
            msg += " " + prop;
        }
        AppendToLogfile.appendMessage(msg);
    }

    /**
     * Returns an array of strings containing the system information which can
     * be very useful while debugging HANtune errors. [0] JVM version [1] JRE
     * version [2] Full OS name [3] CPU Architecture according to java [4] CPU
     * Architecture according to windows [5] NR of processors [6] Used RAM [7]
     * Total RAM reserved by JVM [8] Free RAM [9] MAX RAM to be reserved by JVM
     * [10] reserved [11] reserved
     *
     * @return SystemProperties[]
     */
    private static String[] getSystemProperties() {
        int mb = 1024 * 1024;
        Runtime rt = Runtime.getRuntime(); // source:
                                           // http://stackoverflow.com/questions/6109679/how-to-check-windows-edition-in-java
        Process pr;
        BufferedReader in;
        String line;
        String fullOSName = "";
        final String SEARCH_TERM = "OS Name:";

        try {
            pr = rt.exec("SYSTEMINFO");
            in = new BufferedReader(new InputStreamReader(pr.getInputStream()));

            // add all the lines into a variable
            while ((line = in.readLine()) != null) {
                if (line.contains(SEARCH_TERM)) // found the OS you are using
                {
                    // extract the full os name
                    fullOSName = line.substring(line.lastIndexOf(SEARCH_TERM) + SEARCH_TERM.length(),
                        line.length() - 1);
                    break;
                }
            }

        } catch (IOException ioe) {
            AppendToLogfile.appendError(Thread.currentThread(), ioe);
            System.err.println(ioe.getMessage());
        }
        return new String[] { "JVM Version: " + System.getProperty("java.vm.version"), // Java VM version
            "JRE Version: " + System.getProperty("java.version"), // Java JRE version
            fullOSName.trim(), // Full windows name
            "Java CPU Arch: " + System.getProperty("os.arch"), // CPU architecture according to java
            "Chipset: " + System.getenv("PROCESSOR_IDENTIFIER"), // CPU architecture according to windows,
                                                                 // yes there could be differences
            System.getenv("NUMBER_OF_PROCESSORS") + " CPU_Cores", // Number of CPU-cores
            Long.toString((rt.totalMemory() - rt.freeMemory()) / mb) + " mb_RAM_Used", // Used RAM
            Long.toString(rt.totalMemory() / mb) + " mb_RAM_Total", // Total RAM
            Long.toString(rt.freeMemory() / mb) + " mb_RAM_Free", // Free RAM
            Long.toString(rt.maxMemory() / mb) + " mb_RAM_Max" }; // Max RAM }
    }

}
