/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ErrorLogger;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author alaap
 */
public final class ErrorLogRestrictor {
    
    private static final int maxFileSize = (5242880);                             // max logfile size in byte (5mb)
    
    /**
     * This method checks the logfile size. 
     * When it exceeds 5mb it will remove the file 
     * and create a new (empty) errorLog.
     */
    public static void Restrictor() {

        long fileSize;                                                          //fileSize in Byte

        File file = new File("errorLog.txt");
        fileSize = file.length();
        if (fileSize >= maxFileSize) {                                          //If the file gets too big
            try{
            file.delete();                                                      //delete the errorLog.txt
        }
        catch(Exception e){
                System.out.println("Deleting errorlog.txt went wrong");
                }
        }
    }

}

