/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package DAQList;

import java.awt.Component;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import ASAP2.ASAP2Measurement;
import ErrorLogger.AppendToLogfile;
import HANtune.HANtune;
import datahandling.CurrentConfig;
import util.MessagePane;

/**
 * Dialog to cofigure the DAQ list that is added or modified.
 *
 * @author Jeroen
 */
public class DAQListDialog extends javax.swing.JDialog {

    private static final long serialVersionUID = 7526471155622776163L;

    // Names to be displayed in dialog list
    private DefaultListModel<Object> filteredDaqListNames = new DefaultListModel<>();
    private DefaultListModel<Object> filteredAsap2Names = new DefaultListModel<>();

    // All names available for selection
    private List<String> asap2Names = new ArrayList<>();
    private List<String> daqlistNames = new ArrayList<>();

    private float prescaler;
    private CurrentConfig currentConfig = CurrentConfig.getInstance();
    private boolean canceled = true;
    private boolean daqListContentChanged = false;

    /**
     * Creates new form DaqlistDialog
     *
     * @param hantune
     * @param modal
     * @param name
     */
    public DAQListDialog(HANtune hantune, boolean modal, String name) {
        super(hantune, modal);

        String nativeLF = UIManager.getSystemLookAndFeelClassName();
        try {
            UIManager.setLookAndFeel(nativeLF);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            AppendToLogfile.appendError(Thread.currentThread(), ex);
        }

        // set program window icon
        Image img = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/HANlogo.png"));
        this.setIconImage(img);

        initComponents();

        daqList.setModel(filteredDaqListNames);
        asap2List.setModel(filteredAsap2Names);
        asap2List.setSelectionModel(new AllowedSelectionModel());

        // set title and position
        setTitle("Modifying DAQ list: " + name);
        setLocationRelativeTo(null);

        if (currentConfig.getXcpsettings().getSlaveFrequency() != 0) {
            // set default prescaler value
            prescaler = (float) Math.ceil(currentConfig.getXcpsettings().getSlaveFrequency() / currentConfig.getXcpsettings().sample_freq_init);
            // set maximum frequency
            maxPrescalerLabel.setText("Max: " + Math.round(currentConfig.getXcpsettings().getSlaveFrequency() * 100) / 100 + "Hz");
            // set current frequency
            float value = (float) Math.ceil(currentConfig.getXcpsettings().getSlaveFrequency() / (float) prescalerSpinner.getValue());
            currentPrescalerLabel.setText(util.Util.getStringValue(value, "0.##") + " Hz");
        } else {
            prescaler = currentConfig.getXcpsettings().sample_freq_init;
            maxPrescalerLabel.setText("");
            currentPrescalerLabel.setText("");
        }
        // set default spinner value
        prescalerSpinner.setValue(prescaler);

        for (String mmtName : currentConfig.getASAP2Data().getASAP2MeasurementsNames()) {
            asap2Names.add(mmtName);
        }

        // find current DaqList by name
        DAQList currentDaqList = null;
        for (DAQList daq : hantune.daqLists) {
            if (daq.getName().equals(name)) {
                currentDaqList = daq;
                break; // found it
            }
        }

        if (currentDaqList != null) {
            prescalerSpinner.setValue(currentDaqList.getPrescaler());

            // add existing daq list items and remove daqlist items from asap2 list: keep them balanced
            currentDaqList.getASAP2Measurements().forEach(mt -> {
                if (mt.isAllowed()) {
                    daqlistNames.add(mt.getName());
                    asap2Names.remove(mt.getName());
                }
            });
        }

        // set available names equal to filtered names
        resetfilteredList(filteredAsap2Names, asap2Names);
        resetfilteredList(filteredDaqListNames, daqlistNames);

        if (name.equals("Default")) {
            switchButton.setEnabled(false);
            switchButton.setToolTipText("Cannot modify the default DAQ list.");
        } else {
            switchButton.setEnabled(true);
            switchButton.setToolTipText("Add/Remove selected items to/from DAQ list");
        }

        setVisible(true);
    }

    private class AllowedSelectionModel extends DefaultListSelectionModel {
        private static final long serialVersionUID = 1L;

        @Override
        public boolean isSelectedIndex(int index) {

            boolean isAllowed = false;
            ASAP2Measurement mmt
                    = currentConfig.getASAP2Data().getASAP2Measurements().get((String) filteredAsap2Names.get(index));
            if (mmt != null) {
                isAllowed = mmt.isAllowed();
            }

            return isAllowed && super.isSelectedIndex(index);
        }
    }

    private class DisabledItemListCellRenderer extends DefaultListCellRenderer {

        private static final long serialVersionUID = 1L;

        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
                boolean cellHasFocus) {
            Component comp;
            boolean isAllowed = false;
            ASAP2Measurement mmt = currentConfig.getASAP2Data().getASAP2Measurements().get((String) value);
            if (mmt != null) {
                isAllowed = mmt.isAllowed();
            }
            comp = super.getListCellRendererComponent(list, value, index, isSelected && isAllowed,
                    cellHasFocus);
            comp.setEnabled(isAllowed);
            return comp;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        asap2ScrollPane = new JScrollPane();
        asap2List = new javax.swing.JList<>();
        cancelButton = new javax.swing.JButton();
        applyButton = new javax.swing.JButton();
        daqScrollPane = new javax.swing.JScrollPane();
        daqList = new javax.swing.JList<>();
        switchButton = new javax.swing.JButton();
        prescalerPanel = new javax.swing.JPanel();
        prescalerSpinner = new javax.swing.JSpinner();
        maxPrescalerLabel = new javax.swing.JLabel();
        currentPrescalerLabel = new javax.swing.JLabel();
        prescalerLabel = new javax.swing.JLabel();
        asap2Label = new javax.swing.JLabel();
        daqlistLabel = new javax.swing.JLabel();
        asap2SearchBar = new javax.swing.JTextField();
        DaqSearchBar = new javax.swing.JTextField();

        setMinimumSize(new java.awt.Dimension(550, 600));


        asap2List.setMaximumSize(new java.awt.Dimension(200, 288));
        asap2List.setMinimumSize(new java.awt.Dimension(200, 288));
        asap2List.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                asap2ListMousePressed(evt);
            }
        });
        asap2List.setCellRenderer(new DisabledItemListCellRenderer());

        asap2ScrollPane.setMinimumSize(new java.awt.Dimension(200, 250));
        asap2ScrollPane.setViewportView(asap2List);
        asap2ScrollPane.setPreferredSize(new java.awt.Dimension(200, 250));

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        applyButton.setText("Apply");
        applyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                applyButtonActionPerformed(evt);
            }
        });

        daqScrollPane.setMinimumSize(new java.awt.Dimension(200, 250));
        daqScrollPane.setPreferredSize(new java.awt.Dimension(200, 250));

        daqList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                daqListMousePressed(evt);
            }
        });
        daqScrollPane.setViewportView(daqList);

        switchButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/exchange_arrows.png"))); // NOI18N
        switchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                switchButtonActionPerformed(evt);
            }
        });

        prescalerPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        prescalerSpinner.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(10.0f), Float.valueOf(1.0f), Float.valueOf(255.0f), Float.valueOf(1.0f)));
        prescalerSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                prescalerSpinnerStateChanged(evt);
            }
        });

        maxPrescalerLabel.setText("Max: 100 Hz");
        maxPrescalerLabel.setMinimumSize(new java.awt.Dimension(61, 14));

        currentPrescalerLabel.setText("10 Hz");
        currentPrescalerLabel.setMaximumSize(new java.awt.Dimension(60, 14));
        currentPrescalerLabel.setMinimumSize(new java.awt.Dimension(60, 14));
        currentPrescalerLabel.setPreferredSize(new java.awt.Dimension(60, 14));

        prescalerLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        prescalerLabel.setText("Prescaler");

        javax.swing.GroupLayout prescalerPanelLayout = new javax.swing.GroupLayout(prescalerPanel);
        prescalerPanel.setLayout(prescalerPanelLayout);
        prescalerPanelLayout.setHorizontalGroup(
            prescalerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(prescalerPanelLayout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(maxPrescalerLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(prescalerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(prescalerLabel)
                    .addComponent(prescalerSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(currentPrescalerLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52))
        );
        prescalerPanelLayout.setVerticalGroup(
            prescalerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(prescalerPanelLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(prescalerLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(prescalerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(prescalerSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(maxPrescalerLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(currentPrescalerLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(56, Short.MAX_VALUE))
        );

        asap2Label.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        asap2Label.setText("ASAP2");

        daqlistLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        daqlistLabel.setText("DAQ list");

        asap2SearchBar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                asap2SearchBarActionPerformed(evt);
            }
        });
        asap2SearchBar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                asap2SearchBarKeyReleased(evt);
            }
        });

        DaqSearchBar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                DaqSearchBarKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(prescalerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(applyButton, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(asap2ScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(90, 90, 90)
                                .addComponent(asap2Label)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(18, 18, 18)
                        .addComponent(switchButton, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(daqScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(daqlistLabel)
                                .addGap(73, 73, 73)))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(asap2SearchBar, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(DaqSearchBar, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(prescalerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(asap2Label)
                    .addComponent(daqlistLabel))
                .addGap(4, 4, 4)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(asap2SearchBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(DaqSearchBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(switchButton)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(asap2ScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 305, Short.MAX_VALUE)
                            .addComponent(daqScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelButton)
                    .addComponent(applyButton))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void applyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_applyButtonActionPerformed
        if (filteredDaqListNames.getSize() <= 0) {
            JOptionPane.showMessageDialog(this, "No ASAP2 items were put in the DAQ list.", "No items in DAQ list", JOptionPane.WARNING_MESSAGE);
            return;
        }

        prescaler = (float) prescalerSpinner.getValue();
        if (filteredDaqListNames.getSize() > 251) {
            MessagePane.showError("You cannot create a daqlist with more than 251 signals you are using " + filteredDaqListNames.getSize() + " signals");
            return;
        }

        for (int i = 0; i < filteredDaqListNames.size(); i++) {
            daqlistNames.add((String) filteredDaqListNames.get(i));
        }
        canceled = false;
        dispose();
    }//GEN-LAST:event_applyButtonActionPerformed

    private void switchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_switchButtonActionPerformed
        if (asap2List.getSelectedIndices().length > 0) {
            for (Object object : asap2List.getSelectedValuesList()) {
                daqlistNames.add((String) object);
                filteredDaqListNames.addElement(object);

                asap2Names.remove(object);
                filteredAsap2Names.removeElement(object);
            }
            daqListContentChanged = true;
        }

        if (daqList.getSelectedIndices().length > 0) {
            for (Object object : daqList.getSelectedValuesList()) {
                daqlistNames.remove(object);
                filteredDaqListNames.removeElement(object);

                asap2Names.add((String) object);
                filteredAsap2Names.addElement(object);
            }
            daqListContentChanged = true;
        }
    }//GEN-LAST:event_switchButtonActionPerformed

    private void prescalerSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_prescalerSpinnerStateChanged
        if (currentConfig.getXcpsettings().getSlaveFrequency() != 0) {
            prescalerSpinner.setValue((float) Math.round((float) prescalerSpinner.getValue()));
            float value = (float) Math.ceil(currentConfig.getXcpsettings().getSlaveFrequency() / (float) prescalerSpinner.getValue());
            currentPrescalerLabel.setText(util.Util.getStringValue(value, "0.##") + " Hz");
        } else {
            currentPrescalerLabel.setText("");
        }
    }//GEN-LAST:event_prescalerSpinnerStateChanged

    private void asap2ListMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_asap2ListMousePressed
        daqList.clearSelection();
    }//GEN-LAST:event_asap2ListMousePressed

    private void daqListMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_daqListMousePressed
        asap2List.clearSelection();
    }//GEN-LAST:event_daqListMousePressed

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        canceled = true;
        dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed

    private void asap2SearchBarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_asap2SearchBarActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_asap2SearchBarActionPerformed

    private void asap2SearchBarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_asap2SearchBarKeyReleased
        // filter the asap2 values
        filterList(asap2SearchBar.getText(), filteredAsap2Names, asap2Names);
    }//GEN-LAST:event_asap2SearchBarKeyReleased

    private void DaqSearchBarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DaqSearchBarKeyReleased
        filterList(DaqSearchBar.getText(), filteredDaqListNames, daqlistNames);
    }//GEN-LAST:event_DaqSearchBarKeyReleased

    /**
     * Filters the elements in the model, using given filter text. Element is
     * placed in filtNames when availNames contains filter text. Regardless of
     * upper/lower-case
     *
     * @param filter holds the text to filter
     * @param filtNames upon exit, contains elements found in availNames
     * containing filter text
     * @param availNames holds all available elements to filter from
     */
    private void filterList(String filter, DefaultListModel<Object> filtNames, List<String> availNames) {
        if (filter.length() != 0) {
            String lowCaseFilt = filter.toLowerCase();

            filtNames.clear();
            for (String str : availNames) {
                if (str.toLowerCase().contains(lowCaseFilt)) {
                    filtNames.addElement(str);
                }
            }
        } else {
            resetfilteredList(filtNames, availNames);
        }
    }

    /**
     * Resets filtNames to the way it was before filtering (containing same
     * elements as availNames)
     *
     * filtNames upon exit, will hold same elements as availNames availNames
     * holds all available asap2 names for selection
     */
    public void resetfilteredList(DefaultListModel<Object> filtNames, List<String> availNames) {
        // add all elements

        filtNames.clear();
        for (String nm : availNames) {
            filtNames.addElement(nm);
        }
    }

    public List<String> getDaqlistNames() {
        return daqlistNames;
    }

    public float getPrescaler() {
        return prescaler;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public boolean isDaqListContentChanged() {
        return daqListContentChanged;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField DaqSearchBar;
    private javax.swing.JButton applyButton;
    private javax.swing.JLabel asap2Label;
    private javax.swing.JList<Object> asap2List;
    private javax.swing.JScrollPane asap2ScrollPane;
    private javax.swing.JTextField asap2SearchBar;
    private javax.swing.JButton cancelButton;
    private javax.swing.JLabel currentPrescalerLabel;
    private javax.swing.JList<Object> daqList;
    private javax.swing.JScrollPane daqScrollPane;
    private javax.swing.JLabel daqlistLabel;
    private javax.swing.JLabel maxPrescalerLabel;
    private javax.swing.JLabel prescalerLabel;
    private javax.swing.JPanel prescalerPanel;
    private javax.swing.JSpinner prescalerSpinner;
    private javax.swing.JButton switchButton;
    // End of variables declaration//GEN-END:variables
}
