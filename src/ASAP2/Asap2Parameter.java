/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

import datahandling.CurrentConfig;
import datahandling.Parameter;
import datahandling.ParameterListener;
import datahandling.SendParameterDataHandler;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Michiel Klifman
 */
public class Asap2Parameter extends ASAP2Characteristic implements Parameter {
    
    private  double value;
    private long timeStamp;
    private double lastSentValue;
    boolean inSync = false;
    private final List<ParameterListener> listeners = new ArrayList<>();
    
    @Override
    public double getValue() {
        return value;
    }
    
    @Override
    public void setValue(double value) {
        this.timeStamp = System.currentTimeMillis();
        if (this.value != value) {
            this.value = value;
            updateListeners();
        }
    }
    
    @Override
    public double getRawValue() {
        return getRawValue(value);
    }
    
    @Override
    public double getRawValue(double value) {
        return value / factor;
    }
    
    public long getTimeStamp() {
        return timeStamp;
    }
    
    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
    
    @Override
    public void send(double value) {
        double validatedValue = validateValue(value);
        SendParameterDataHandler dataHandler = CurrentConfig.getInstance().getHANtuneManager().getSendParameterDataHandler();
        dataHandler.setParameter(getName(), validatedValue);
    }
    
    @Override
    public double getLastSentValue() {
        return lastSentValue;
    }
    
    @Override
    public boolean isInSync() {
        return inSync;
    }
    
    @Override
    public void setInSync(boolean inSync) {
        if (inSync) {
            lastSentValue = value;
        }
        
        if (this.inSync != inSync) {
            this.inSync = inSync;
            updateStateListeners();
        }
    }
    
    @Override
    public void addListener(ParameterListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(ParameterListener listener) {
        listeners.remove(listener);
    }

    /**
     *
     */
    public void updateListeners() {
        for (ParameterListener listener : listeners) {
            listener.valueUpdate(value);
        }
    }

    public boolean isConnectedToEditor() {
        return listeners.size() > 0;
    }
    
    /**
     * Only used by calibration management. Do not use this method. Use 
     * getValue() instead.
     * @return
     * @deprecated
     */
    @Deprecated
    @Override
    public ASAP2Data.Data getData() {
        if (isInSync() || isConnectedToEditor()) {
            return new ASAP2Data.Data(timeStamp, value);
        } else {
            return null;
        }
    }
    
}
