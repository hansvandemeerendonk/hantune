/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

import java.util.ArrayList;
import java.util.List;

/**
 * Functions are typically used to bundle characteristics and measurements, which belong to one software function. 
 * Function hierarchies that include sub-functions can be expressed.
 * @author Michiel Klifman
 */
public class Asap2Function extends ASAP2Object {

    private List<Asap2Function> subFunctions = new ArrayList<>();
    private final List<ASAP2Characteristic> characteristics = new ArrayList<>();
    private final List<ASAP2Measurement> inputMeasurements = new ArrayList<>();
    private final List<ASAP2Measurement> outputMeasurements = new ArrayList<>();
    public int indexed = 1;
    
    
    
    public List<Asap2Function> getSubFunctions() {
        return subFunctions;
    }
    
    public void setSubFunctions(List<Asap2Function> subFunctions) {
        this.subFunctions = subFunctions;
    }
    
    public void addSubFunction(Asap2Function subFunction) {
        if (!subFunctions.contains(subFunction)) {
            subFunctions.add(subFunction);
        }
    }
    
     /**
     * @return the characteristics
     */
    public List<ASAP2Characteristic> getCharacteristics() {
        return characteristics;
    }
    
    /**
     * @param characteristic the characteristic to add to this group
     */
    public void addCharacteristic(ASAP2Characteristic characteristic) {
        characteristics.add(characteristic);
    }

    /**
     * @return the input measurements
     */
    public List<ASAP2Measurement> getInputMeasurements() {
        return inputMeasurements;
    }
    
    /**
     * @param inputMeasurement the input measurement to add to this group
     */
    public void addInputMeasurement(ASAP2Measurement inputMeasurement) {
        inputMeasurements.add(inputMeasurement);
    }
    
    /**
     * @return the output measurements
     */
    public List<ASAP2Measurement> getOutputMeasurements() {
        return outputMeasurements;
    }
    
    /**
     * @param outputMeasurement the output measurement to add to this group
     */
    public void addOutputMeasurement(ASAP2Measurement outputMeasurement) {
        outputMeasurements.add(outputMeasurement);
    }
    
    public List<ASAP2Measurement> getInAndOutputMeasurements() {
        List<ASAP2Measurement> inAndOutputMeasurements = new ArrayList<>();
        inAndOutputMeasurements.addAll(inputMeasurements);
        inAndOutputMeasurements.addAll(outputMeasurements);
        ASAP2Data.sortAsap2Objects(inAndOutputMeasurements);
        return inAndOutputMeasurements;
    }
    
    /**
     *
     * @return all characteristics of this group and its subgroups
     */
    public List<ASAP2Characteristic> getAllChildCharacteristics() {
        List<ASAP2Characteristic> allCharacteristics = addAllChildCharacteristics(new ArrayList<>());
        ASAP2Data.sortAsap2Objects(allCharacteristics);
        return allCharacteristics;
    }

    private List<ASAP2Characteristic> addAllChildCharacteristics(List<ASAP2Characteristic> allCharacteristics) {
        characteristics.stream().forEach((characteristic) -> allCharacteristics.add(characteristic));
        subFunctions.stream().forEach((subGroup) -> subGroup.addAllChildCharacteristics(allCharacteristics));
        return allCharacteristics;
    }
    
    /**
     * @return all measurements of this group and its subgroups
     */
    public List<ASAP2Measurement> getAllChildMeasurements() {
        List<ASAP2Measurement> allChildMeasurements = addAllChildMeasurements(new ArrayList<>());
        ASAP2Data.sortAsap2Objects(allChildMeasurements);
        return allChildMeasurements;
    }

    private List<ASAP2Measurement> addAllChildMeasurements(List<ASAP2Measurement> allMeasurements) {
        getInAndOutputMeasurements().stream().forEach((measurement) -> allMeasurements.add(measurement));
        subFunctions.stream().forEach((subGroup) -> subGroup.addAllChildMeasurements(allMeasurements));
        return allMeasurements;
    }
    
    public int getFunctionDepth() {
        int depth = 1;
        for (Asap2Function subFunction : getSubFunctions()) {
            if (!subFunction.getSubFunctions().isEmpty()) {
                int subFunctionDepth = subFunction.getFunctionDepth();
                depth = subFunctionDepth > depth ? subFunctionDepth : depth;
            }
        }
        return depth + 1;
    }
}
