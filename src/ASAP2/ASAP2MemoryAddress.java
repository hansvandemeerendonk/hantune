/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

import ErrorLogger.AppendToLogfile;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Aart-Jan
 */
public class ASAP2MemoryAddress {

    private static String error = "";
    private static int number = 0;

    public static int getNumber() {
        return number;
    }

    public static String getError() {
        return error;
    }

    public static void setError(String err) {
        error = err;
    }

    /**
     * @param args[0] path to ASAP2 file
     * @param args[1] path to MAP file
     */
    public static void main(String args[]) {
        // arguments available or start with default values?
        if (args.length == 2) {
            convert(args[0], args[1]);
        }
    }

    public static boolean convert(String file_ASAP2, String file_MAP) {
        try {
            number = 0;
            
            // prepare regular expression pattern
            Pattern p_input = Pattern.compile("([a-f0-9]{8})[\\s][a-z][\\s]{5}[A-Z][\\s][.][A-Za-z]+[\\t][a-f0-9]{8}[\\s]([^\\s]+)");
            Matcher m;

            // init content buffers
            StringBuilder ASAP2_content = new StringBuilder("");
            String ASAP2_string, ASAP2_buffer, MAP_line;

            // read ASAP2 file and place content of file in string
            FileInputStream ASAP2_stream = new FileInputStream(file_ASAP2);
            BufferedReader ASAP2_reader = new BufferedReader(new InputStreamReader(ASAP2_stream));
            while ((ASAP2_buffer = ASAP2_reader.readLine()) != null) {
                ASAP2_content.append(ASAP2_buffer);
                ASAP2_content.append('\n');
            }
            ASAP2_reader.close();
            ASAP2_stream.close();

            ASAP2_string = ASAP2_content.toString();

            // read MAP file line by line and parse lines by given regular expression format
            // achieved data is used to replace necessary values in ASAP2 file
            FileInputStream MAP_stream = new FileInputStream(file_MAP);
            BufferedReader MAP_content = new BufferedReader(new InputStreamReader(MAP_stream));
            while ((MAP_line = MAP_content.readLine()) != null) {
                m = p_input.matcher(MAP_line);
                if (m.matches()) {
                    if (ASAP2_string.contains("@MemoryAddress@" + m.group(2) + "@")) {
                        number++;
                    }
                    ASAP2_string = ASAP2_string.replaceAll("@MemoryAddress@" + m.group(2) + "@", "0x" + m.group(1));
                }
            }
            MAP_content.close();
            MAP_stream.close();

            // write ASAP2 file
            FileOutputStream ASAP2_outputstream = new FileOutputStream(file_ASAP2);
            PrintStream ASAP2_output = new PrintStream(ASAP2_outputstream);
            ASAP2_output.print(ASAP2_string);
            ASAP2_output.close();
            ASAP2_outputstream.close();
        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            setError(e.getMessage());
            return false;
        }
        return true;
    }

}
