![HANtune](/images/hantune.png)
>Copyright (c) 2020 [HAN University of Applied Sciences]
>
>Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
>
>The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.
>
>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Introduction
HANtune is part of the open model based development (openMBD) suite created by HAN Automotive Research. At its core HANtune is a dashboard application that allows you to create and customize a layout to visualize and calibrate data from different communication protocols. The following protocols are currently supported:

* XCP (using an .a2l file) over:
  * CAN
  * Ethernet
  * USB/UART
* CAN (using a .dbc file)

For more information about the use of HANtune and its features see the [openMBD wiki](https://www.openmbd.eu/wiki).

![HANtune Dashboard](/images/hantune_dashboard.png)

## Requirements
 * [Java SE Development Kit 8](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)

## Getting Started
#### Cloning The Repository
Use the following command to clone the repository. Replace **username** with your gitlab username.

    git clone https://username@gitlab.com/AutomotiveResearch/hantune.git
    
#### Building The Project
The HANtune project is IDE independent. But since HANtune was originally made with NetBeans, that is also the easiest way to build it. First we need to open the project. In Netbeans go to:

    File > Open Project...

To build the project in Netbeans go to:

    Run > Build Project (HANtune)

#### Running HANtune
In NetBeans:

    Run > Run Project (HANtune)

After you have built HANtune a .jar file will be created in the dist folder. To run HANtune from the command line, open a terminal, navigate to the .jar file and enter the following:

    java -jar HANtune.jar

## Contribute to HANtune
HANtune is an open source project and everyone is welcome to join the development. HANtune is published under the MIT license which means you are free to create your own (commercial) tool based on this project. For more information contact us at: 

hantune@han.nl 

#### HANtune workflow
HANtune uses Git for version control. 
Please use the following Branching Strategy when committing to the HANtune repository: [HANtune workflow](https://gitlab.com/AutomotiveResearch/han-ar-shared/-/blob/master/HAN-AR-workflow.md)

#### Contribution guidelines
If your have great ideas that can be added to HANtune please read these guidelines before contributing to HANtune: [HANtune contribute](https://gitlab.com/AutomotiveResearch/han-ar-shared/-/blob/master/HAN-AR-contributing.md)